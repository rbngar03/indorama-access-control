import requests
import json
import ssl
import csv
from datetime import datetime, timedelta
from collections import defaultdict
import pyodbc
import time
from collections import defaultdict

server = '192.168.10.20'  
database = 'Indorama_AccessControl'
username = 'sa'
password = 'Datawar326'         
conn_str = f'DRIVER={{ODBC Driver 17 for SQL Server}};SERVER={server};DATABASE={database};UID={username};PWD={password}'
url_login = "https://192.168.10.20/api/login"
url_marcajes = "https://192.168.10.20/api/events/search"

# Function to get loginAccess
def login():
    #cert_path = './cert-register-f305705478/biostar_cert.crt'

    #url = "https://192.168.10.20/api/login"

    payload = json.dumps({
    "User": {
        "login_id": "admin",
        "password": "1nd0r4m4"
    }
    })
    headers = {
    'Content-Type': 'application/json',
    'Cookie': 'JSESSIONID=AD254CE6AEEF3CF63A5A9991F7BFF2D2'
    }

    response = requests.request("POST", url_login, headers=headers, data=payload, verify=False)

    if response.status_code == 200:
        specific_header = response.headers.get('bs-session-id')
        return specific_header
    else:
        print(f"Error: {response.status_code} - {response.text}")
# Function to get information
def search_marking(bs_session_id, start_time, end_time, filename):
    payload = json.dumps({
    "Query": {
        "limit": 10000,
        "conditions": [
        {
            "column": "datetime",
            "operator": 3,
            "values": [
            start_time,
            end_time
            ]
        }
        ],
        "orders": [
        {
            "column": "datetime",
            "descending": True
        }
        ]
    }
    })
    headers = {
    'bs-session-id': bs_session_id,
    'Content-Type': 'application/json',
    'Cookie': 'JSESSIONID=4535EC6008F6566B2450C9294CD6D320'
    }
    
    response = requests.request("POST", url_marcajes, headers=headers, data=payload, verify=False)
    json_string = json.dumps(response.json(), indent=4)
    with open(filename, 'w', encoding='utf-8') as file:
        file.write(json_string)
    return response.json()
# Function to write information in txt
def write_to_txt(data, filename):
    filter_results = []
    if not data:
        #print("No data to write.")
        return []
    filter_results = []
    rows = data.get('EventCollection', {}).get('rows', [])
    
    if not rows:
        print("No rows found.")
        return []
    grouped_rows = defaultdict(list)
    for row in rows:
        server_datetime = row.get('server_datetime', 'Unknown')
        grouped_rows[server_datetime].append(row)

    with open(filename, 'w', encoding='utf-8') as txtfile:
        header = [
            "server_datetime", "id", "datetime", "index",
            "user_id", "user_name", "user_photo_exists", 
            "user_group_id_name", "device_id", "device_name", 
            "event_type_code", "is_dst", "timezone_half", 
            "timezone_hour", "timezone_negative", 
            "user_update_by_device", "hint", "door_id", "door_name"
        ]
        txtfile.write("\t".join(header) + "\n")  # Tab-separated for better readability

        for server_datetime, group in grouped_rows.items():
            door_ids = []
            door_names = []

            for row in group:
                merged_row = {
                    'server_datetime': server_datetime,
                    'id': row.get('id', ''),
                    'datetime': row.get('datetime', ''),
                    'index': row.get('index', ''),
                    'user_id': row.get('user_id', {}).get('user_id', ''),
                    'user_name': row.get('user_id', {}).get('name', ''),
                    'user_photo_exists': row.get('user_id', {}).get('photo_exists', ''),
                    'user_group_id_name': row.get('user_group_id', {}).get('name', ''),
                    'device_id': row.get('device_id', {}).get('id', ''),
                    'device_name': row.get('device_id', {}).get('name', ''),
                    'event_type_code': row.get('event_type_id', {}).get('code', ''),
                    'is_dst': row.get('is_dst', ''),
                    'timezone_half': row.get('timezone', {}).get('half', ''),
                    'timezone_hour': row.get('timezone', {}).get('hour', ''),
                    'timezone_negative': row.get('timezone', {}).get('negative', ''),
                    'user_update_by_device': row.get('user_update_by_device', ''),
                    'hint': row.get('hint', '')
                }

                door_id = row.get('door_id', [])
                for door in door_id:
                    door_ids.append(door.get('id', 'Unknown'))
                    door_names.append(door.get('name', 'Unknown'))

                merged_row['door_id'] = ', '.join(door_ids)  
                merged_row['door_name'] = ', '.join(door_names)  

                if merged_row['user_id'] and merged_row['user_name']:
                    txtfile.write("\t".join([str(merged_row[field]) for field in header]) + "\n")
                    filter_results.append(merged_row)

    return filter_results
# Function to write in database
def insert_marking(data):
    try:
        connection = pyodbc.connect(conn_str)
        print("Connection established successfully!")
        cursor = connection.cursor()
        # Start processing data in batches
        max_rows = 300  
        total_rows = len(data) 
        batch_start = 0

        while batch_start < total_rows:
            sql = "DECLARE @tvp dbo.dtMarcaje;"

            row_count = 0
            for i in range(batch_start, min(batch_start + max_rows, total_rows)):
                row = data[i]
                sql += f"INSERT INTO @tvp (user_id, device_id, server_datetime, entrada_salida) VALUES ({row['user_id']}, {row['device_id']}, '{row['server_datetime']}', '{row['door_name']}');"
                row_count += 1

            sql += "EXEC dbo.spProcesaMarcajeBioStar @data = @tvp;"

            print(f"Generated SQL query for batch {batch_start // max_rows + 1}:")

            cursor.execute(sql)
            connection.commit()
            print(f"Transaction for batch {batch_start // max_rows + 1} committed successfully.")

            batch_start += max_rows

    except pyodbc.Error as e:
        print("Error in connection:", e)
    except Exception as e:
        print("General error:", e)

    finally:
        connection.close()
        #print("Connection closed.")
# Function to eliminate duplicate data and data without doors
def eliminate_duplicate(data):
    seen = set()
    unique_data = []

    for row in data:
        if row['door_name'] in ('', None):
            continue
        unique_identifier = (row['user_id'], row['device_id'], row['server_datetime'], row['door_name'])
        
        if unique_identifier not in seen:
            unique_data.append(row)
            seen.add(unique_identifier)
    return unique_data

def marking(bs_session_id, start_time, end_time, bk):
    file_name_api = ""
    file_name_link_data = ""
    if bk:
        start_time_str = start_time.replace(":", "_")
        file_name_api = "Response_API_BK_" + start_time_str.split('.')[0] + ".txt"
        file_name_link_data = "Link_data_BK_"
    else:
        file_name_api = "Response_API_" + datetime.now().strftime("%Y_%m_%d") + "T" + datetime.now().strftime("%H_%M") + ".txt"
        file_name_link_data = "Link_data_"
    link_data = []
    data = search_marking(bs_session_id, start_time, end_time, file_name_api)
    if data:
        start_time = start_time.replace(":", "_")
        file_name = file_name_link_data + start_time.split('.')[0] + ".txt"
        link_data = write_to_txt(data, file_name)
    if link_data:
        #print("FilterResult: " + str(len(link_data)))
        data_clean = eliminate_duplicate(link_data)
        #print("Rows clean: " + str(len(data_clean)))
        insert_marking(data_clean)
    #print("ExecuteTime: " + str(datetime.now()))

def define_process(bk):
    now = datetime.now()
    start_time_str = ''
    end_time_str = ''
    if bk:
        #BK 3 days before
        time_complement_start = "T06:00:00.000Z"
        time_complement_end = "T05:59:00.000Z"
        three_days_ago = now - timedelta(days=3)
        one_day_later = now + timedelta(days=1)
        start_time_str = three_days_ago.strftime("%Y-%m-%d") + time_complement_start
        end_time_str = one_day_later.strftime("%Y-%m-%d") + time_complement_end 
    else:
        #Each hour
        start_time = now + timedelta(hours=5)
        end_time = start_time + timedelta(hours=1)
        #now_str = now.strftime("%Y-%m-%dT%H:%M:%S")
        time_complement = "00:00.00Z"
        start_time_str = start_time.strftime("%Y-%m-%dT%H:") + time_complement
        end_time_str = end_time.strftime("%Y-%m-%dT%H:") + time_complement 
    
    #Test
    #start_time_str = "2024-12-26T06:00:00.000Z"
    #end_time_str = "2024-12-27T05:59:00.000Z"
    #print(start_time_str)
    #print(end_time_str)
    return start_time_str, end_time_str

def main():
    try:
        bkdate = datetime.now().date()
        while True:
            bs_session_id = login()
            if bkdate == datetime.now().date():
                start_time, end_time = define_process(True)
                marking(bs_session_id, start_time, end_time, True)
                bkdate = datetime.now().date() + timedelta(days=3)
            else:
                start_time, end_time = define_process(False)
                marking(bs_session_id, start_time, end_time, False)
                
            time.sleep(3600)
    except KeyboardInterrupt:
        print("Process was interrupted by the user.")

if __name__ == "__main__":
    main()