-- ==========================================================================================
-- Author     : <Daniel Leal>
-- Create date: <28/11/2024>
-- Description: <SP que procesa los marcajes.>
-- ==========================================================================================

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'spProcesaMarcajeBioStar')
	DROP PROCEDURE [dbo].[spProcesaMarcajeBioStar]
GO

CREATE PROCEDURE [dbo].[spProcesaMarcajeBioStar]
    @data [dbo].[dtMarcaje] READONLY
AS 
BEGIN
  BEGIN TRY
    
    INSERT INTO Marcajes (
        idTerminales, FechaRegistro, idGafete, idEmpleado, idContratistas, idVisitantes, 
        idTipoPersonal, EntradaSalida, AccesoOtorgado, campo, idClasificacion, SegundosCubiertos, 
        IdTipoLector, idGafeteTmp, idCompanias, idCcostos, idMarcajesEntrada, NombrePersonal, ResponsableInvista,
        user_id, device_id
    )
    SELECT 
    t.idTerminales, 
    LPM.server_datetime,
    COALESCE(E.idGafete, C.idGafete, V.idGafete),
    
    E.idEmpleado, 
    C.idContratistas, 
    V.idVisitantes, 

    CASE 
        WHEN E.Nombre IS NOT NULL THEN 1
        WHEN C.NombreContratista IS NOT NULL AND TC.TipoContratista = 'Residente' THEN 2
        WHEN C.NombreContratista IS NOT NULL THEN 4
        WHEN V.EmpResponsable IS NOT NULL THEN 3
        ELSE NULL
    END,
    CASE 
        WHEN LPM.entrada_salida IS NOT NULL AND LPM.entrada_salida LIKE 'E%' THEN 'E'
        WHEN LPM.entrada_salida IS NOT NULL AND LPM.entrada_salida LIKE 'S%' THEN 'S'
        WHEN LPM.entrada_salida IS NOT NULL AND LPM.entrada_salida LIKE 'C%' THEN 'C'
        WHEN LPM.entrada_salida IS NOT NULL AND LPM.entrada_salida LIKE 'G%' THEN 'G'
        ELSE 'C'
    END,
    1, 
    '', -- FIX FACT
    CASE 
        WHEN C.NombreContratista IS NOT NULL THEN C.idClasificacion
        ELSE NULL
    END,
    NULL, 
    1,

    COALESCE(EG.idGafete, CG.idGafete),

    CASE 
        WHEN C.NombreContratista IS NOT NULL THEN C.idCompanias
        ELSE NULL
    END,

    COALESCE(E.idCcostos, C.idCcostos, V.idCcostos),
    
    NULL, -- FIX FACT
    
    CONCAT(
        COALESCE(CONCAT(E.Nombre, ' ', E.ApePaterno, ' ', E.ApeMaterno), ''),
        COALESCE(CONCAT(C.NombreContratista, ' ', C.ApPaterno, ' ', C.ApMaterno), ''),
        COALESCE(V.NombreVisitante, '')
    ),
	COALESCE(V.EmpResponsable, NULL),
    LPM.user_id,
    LPM.device_id
    FROM 
        @data LPM
    INNER JOIN 
        Terminales T ON T.Clave = CAST(LPM.device_id AS VARCHAR)
    LEFT JOIN 
        Empleado E ON E.userIdBioStar = LPM.user_id
    LEFT JOIN 
        Contratistas C ON C.userIdBioStar = LPM.user_id
    LEFT JOIN 
        Visitantes V ON V.userIdBioStar = LPM.user_id
    LEFT JOIN 
        TipoContratista TC ON TC.idTipoContratista = C.idTipoContratista
    LEFT JOIN 
        Gafete EG ON EG.Gafete = E.NumeroProvisionalGafete
    LEFT JOIN 
        Gafete CG ON CG.Gafete = C.NumeroProvisionalGafete 
    WHERE 
        (E.idEmpleado IS NOT NULL 
        OR C.idContratistas IS NOT NULL 
        OR V.idVisitantes IS NOT NULL)
        AND NOT EXISTS (
            SELECT 1
            FROM Marcajes M
            WHERE 
                M.FechaRegistro = LPM.server_datetime
                AND M.user_id = LPM.user_id
                AND M.device_id = LPM.device_id
        );
  END TRY 
  BEGIN CATCH
  	-- Variables para notificar error
  	DECLARE @ErrMsg VARCHAR(max), @ErrSev INT, @ErrSt  INT, @ErrDate DATE
  	SET @ErrMsg = ERROR_MESSAGE()
	SET @ErrSev = ERROR_SEVERITY()
	SET @ErrSt = ERROR_STATE()
	SET @ErrDate = CONVERT(DATE, GETDATE());
  	RAISERROR(@ErrMsg, @ErrSev, @ErrSt) 
    INSERT INTO LogError (fecha, ErrMsg, ErrSev, ErrSt) VALUES (@ErrDate, @ErrMsg, @ErrSev, @ErrSt);
  END CATCH 
END
GO

