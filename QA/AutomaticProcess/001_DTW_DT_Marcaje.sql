-- ==========================================================================================
-- Author     : <Daniel Leal>
-- Create date: <28/11/2024>
-- Description: <Table type para marcajes>
-- ==========================================================================================

IF EXISTS(SELECT * FROM SYS.TYPES WHERE IS_TABLE_TYPE = 1 AND name = 'dtMarcaje')
  BEGIN
    DROP TYPE dbo.[dtMarcaje];
  END
CREATE TYPE dbo.dtMarcaje AS TABLE
    (
      user_id BIGINT,
      device_id BIGINT,
      server_datetime DATETIME,
      entrada_salida VARCHAR(255)
    );
GO
