---- ==========================================================================================
-- Author     : <Daniel Leal>
-- Create date: <28/11/2024>
-- Description: <Altera la tabla de Marcajes para almacenar (insert/update) >
-- ==========================================================================================
-- Check if 'user_id' column exists, if not, add it
IF NOT EXISTS (
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'Marcajes' 
    AND COLUMN_NAME = 'user_id'
)
BEGIN
    ALTER TABLE Marcajes
    ADD user_id BIGINT;
END

-- Check if 'device_id' column exists, if not, add it
IF NOT EXISTS (
    SELECT * 
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'Marcajes' 
    AND COLUMN_NAME = 'device_id'
)
BEGIN
    ALTER TABLE Marcajes
    ADD device_id BIGINT;
END