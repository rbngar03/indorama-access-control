IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_DepuracionTableLogUsers')
  BEGIN
	DROP PROCEDURE DTW_SP_DepuracionTableLogUsers
  END
GO
-- =============================================
-- Author:	Karla Ramirez
-- Create date: 08/08/2024
-- Description:	Script que realiza la depuracion la tabla de LogUsers.
-- =============================================

CREATE PROCEDURE [dbo].[DTW_SP_DepuracionTableLogUsers] 	
AS 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('[dbo].[DTW_CA_LogUsers]') AND type in ('U'))
DROP TABLE [dbo].[DTW_CA_LogUsers]


BEGIN
	
	BEGIN TRY

		CREATE TABLE DTW_CA_LogUsers (
					 IDLog INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
					 IDUser INT NOT NULL,
					 DateOperation DATETIME NOT NULL,
					 MessageOperation VARCHAR(500)
		);

	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
  		SET @ErrMsg = ERROR_MESSAGE()
  		SET @ErrSev = ERROR_SEVERITY()
  		SET @ErrSt  = ERROR_STATE()
  		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
END