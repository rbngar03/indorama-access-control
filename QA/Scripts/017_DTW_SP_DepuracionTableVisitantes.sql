IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_DepuracionTableVisitantes')
  BEGIN
	DROP PROCEDURE DTW_SP_DepuracionTableVisitantes
  END
GO
-- =============================================
-- Author:	Karla Ramirez
-- Create date: 08/08/2024
-- Description:	Script que realiza la depuracion la tabla de Visitantes.
-- =============================================

CREATE PROCEDURE [dbo].[DTW_SP_DepuracionTableVisitantes] 	

-- Variables 

@VigenciaRegistroVisitantes INT = NULL

AS
BEGIN
	
	BEGIN TRY
	
	   SELECT @VigenciaRegistroVisitantes = (SELECT Valor 
	   FROM DTW_CA_Parametros
	   WHERE IdParametro = 3)

	    UPDATE [dbo].[Visitantes]
        SET [idEstatus] = 2
		WHERE FechaIngreso <= DATEADD(DAY,- @VigenciaRegistroVisitantes,GETDATE());

	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
  		SET @ErrMsg = ERROR_MESSAGE()
  		SET @ErrSev = ERROR_SEVERITY()
  		SET @ErrSt  = ERROR_STATE()
  		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
END
