IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_InsertNewModules')
  BEGIN
	DROP PROCEDURE DTW_SP_InsertNewModules
  END
GO
-- =============================================
-- Author:	Rub�n Garc�a Garc�a
-- Create date: 12/09/2024
-- Description:	Inserta registros con los datos de los nuevos m�dulos en la tabla de M�dulos
-- =============================================

CREATE PROCEDURE DTW_SP_InsertNewModules 	
AS
BEGIN
	
	BEGIN TRY

	INSERT INTO Modulos(nombre, HasEditableFields)
	SELECT 'Cat�logo de perfiles', 0 
	WHERE NOT EXISTS (SELECT 1 FROM Modulos WHERE nombre = 'Cat�logo de perfiles');

	INSERT INTO Modulos(nombre, HasEditableFields)
	SELECT 'Reporte de empleados', 0 
	WHERE NOT EXISTS (SELECT 1 FROM Modulos WHERE nombre = 'Reporte de empleados');

	INSERT INTO Modulos(nombre, HasEditableFields)
	SELECT 'Reporte No. comidas por turnos', 0 
	WHERE NOT EXISTS (SELECT 1 FROM Modulos WHERE nombre = 'Reporte No. comidas por turnos');

	INSERT INTO Modulos(nombre, HasEditableFields)
	SELECT 'Parametrizaciones', 0 
	WHERE NOT EXISTS (SELECT 1 FROM Modulos WHERE nombre = 'Parametrizaciones');

	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
  		SET @ErrMsg = ERROR_MESSAGE()
  		SET @ErrSev = ERROR_SEVERITY()
  		SET @ErrSt  = ERROR_STATE()
  		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
END
GO