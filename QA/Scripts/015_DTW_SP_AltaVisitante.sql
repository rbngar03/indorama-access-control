IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_AltaVisitante')
  BEGIN
	DROP PROCEDURE DTW_SP_AltaVisitante
  END
GO
-- =============================================
-- Author:		Karla Ramirez
-- Create date: 27/08/24
-- Description:	Script que da de alta un Visitante
-- History:
-- <29/11/2024> <r.garcia> <Se modifica ccostos por idCcostos.>
-- <08/01/2025> <k.ramirez> <Se agrega parametro planta>
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_AltaVisitante]
	@pNombreVisitante VARCHAR(150),
	@pFechaIngreso VARCHAR(20),
	@pRFC VARCHAR(15),
	@pEmpresaVisitante VARCHAR (150),
	@pHoras VARCHAR(7),
	@pEmpleadoResponsable VARCHAR (150),
	@pStatus INT, -- {1-Activo / 2-Inactivo}
	@pCcostos VARCHAR(50),
	@pDerechoComedor INT,
	@pidPlanta INT,
	@Response VARCHAR(100) OUTPUT

AS
BEGIN
	DECLARE @IdCcostos INT
	BEGIN TRY
		BEGIN TRAN
		IF EXISTS (SELECT 1 FROM ccostos WHERE idCcostos = @pCcostos)
			BEGIN
					SET @IdCcostos = (SELECT idCcostos FROM ccostos WHERE idCcostos = CAST(@pCcostos AS INT))
					INSERT INTO [dbo].[Visitantes]
				   ([FechaIndSeguridad]
				   ,[FechaIngreso]
				   ,[FechaTerminacion]
				   ,[Horas]
				   ,[NombreVisitante]
				   ,[idOrigen]
				   ,[DescOrigen]
				   ,[Foto]
				   ,[idGafete]
				   ,[EmpResponsable]
				   ,[Notas]
				   ,[RFC]
				   ,[idComedor]
				   ,[idCcostos]
				   ,[ArchivoAdjunto]
				   ,[idEstatus]
				   ,[fecha_actualizacion]
				   ,[idPlanta])
				 VALUES
				   (NULL,
					CONVERT(DATETIME, @pFechaIngreso, 103),
					NULL,
					@pHoras,
					@pNombreVisitante,
					1,
					@pEmpresaVisitante,
					NULL,
					NULL,
					@pEmpleadoResponsable,
					NULL,
					@pRFC,
					@pDerechoComedor,
					@IdCcostos,
					NULL,
					@pStatus,
					GETDATE(),
					@pidPlanta)

				 COMMIT TRAN
				 SET @Response = 'Ok';
			END
		ELSE
			BEGIN 
				SET @Response = 'El centro de Costos no existe';
				RAISERROR('El centro de Costos no existe', 16, 1)
			    ROLLBACK TRAN;
			    RETURN
			 END 	  
	
	END TRY
   BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRAN;
			DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
			SET @ErrMsg = ERROR_MESSAGE()
			SET @ErrSev = ERROR_SEVERITY()
			SET @ErrSt = ERROR_STATE()
			RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
 END

