IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DW_SP_LiberacionGafete')
  BEGIN
	DROP PROCEDURE DW_SP_LiberacionGafete
  END
GO

-- =============================================
-- Author:		Alonso Sánchez
-- Create date: 19/12/24
-- Description:	Script para la liberación de gafetes provisionales.
-- History:
-- =============================================
CREATE PROCEDURE [dbo].[DW_SP_LiberacionGafete] 	 
AS
BEGIN
    BEGIN TRY
        DECLARE @GafeteTable TABLE (idGafete INT, tipo CHAR(1))
        DECLARE @Gafete TABLE(idGafete INT, tipo CHAR(1))
        DECLARE @IdGafete  INT

        INSERT INTO @GafeteTable 
        SELECT C.NumeroProvisionalGafete, 'C' FROM Contratistas C
        INNER JOIN Gafete G ON C.NumeroProvisionalGafete = G.idGafete
        WHERE G.FechaVigencia < GETDATE()
        UNION
        SELECT E.NumeroProvisionalGafete, 'E' FROM Empleado E
        INNER JOIN Gafete G ON E.NumeroProvisionalGafete = G.idGafete
        WHERE G.FechaVigencia < GETDATE()
        UNION
        SELECT V.idGafete, 'V' FROM Visitantes V
        INNER JOIN Gafete G ON V.idGafete = G.idGafete
        WHERE V.idGafete IS NOT NULL

		UPDATE Contratistas SET idEstatus = 2, idGafete = NULL, NumeroProvisionalGafete = NULL WHERE FechaTerminacion < GETDATE()
		
        WHILE EXISTS (SELECT * FROM @GafeteTable)
        BEGIN
            SET @IdGafete = (SELECT TOP 1 idGafete FROM @GafeteTable ORDER BY idGafete ASC)
            IF ((SELECT TOP 1 tipo FROM @GafeteTable ORDER BY idGafete ASC) = 'C')
            BEGIN
                UPDATE Contratistas SET NumeroProvisionalGafete = NULL WHERE NumeroProvisionalGafete = @IdGafete
            END
            ELSE IF ((SELECT TOP 1 tipo FROM @GafeteTable ORDER BY idGafete ASC) = 'E')
            BEGIN
                UPDATE Empleado SET NumeroProvisionalGafete = NULL WHERE NumeroProvisionalGafete = @IdGafete
            END
            ELSE IF ((SELECT TOP 1 tipo FROM @GafeteTable ORDER BY idGafete ASC) = 'V')
            BEGIN
                UPDATE Visitantes SET idGafete = NULL WHERE idGafete = @IdGafete
            END
            DELETE FROM @GafeteTable WHERE idGafete = @IdGafete
        END
    END TRY
    BEGIN CATCH
        DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT;
        SET @ErrMsg = ERROR_MESSAGE();
        SET @ErrSev = ERROR_SEVERITY();
        SET @ErrSt  = ERROR_STATE();
        RAISERROR(@ErrMsg, @ErrSev, @ErrSt);
    END CATCH
END

