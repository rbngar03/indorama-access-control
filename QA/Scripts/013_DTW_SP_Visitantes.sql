IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_Visitantes')
  BEGIN
	DROP PROCEDURE DTW_SP_Visitantes
  END
GO

-- =============================================
-- Author:		Karla Ramirez
-- Create date: 10/06/24
-- Description:	Script que obtiene la consulta de Visitantes
-- History:
-- <28/11/2024> <r.garcia> <Se elimina páginación y agregan filtros de ID.>
-- <22/01/2025> <k.ramirez> <Se limpia Script.>
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_Visitantes](
@pStatus INT = NULL, -- {1-Activo / 2-Inactivo}
@pIdVisitante INT = NULL,
@pEmpresaVisitante VARCHAR (150) = NULL,
@pCentroCostos VARCHAR(50) = NULL,
@pFiltro VARCHAR(100) = NULL
)
AS
BEGIN 
	BEGIN TRY 
	IF @pStatus = 0
	SET @pStatus = NULL

	IF @pIdVisitante = 0
	SET @pIdVisitante = NULL

	IF @pEmpresaVisitante IS NOT NULL
	SET @pEmpresaVisitante = CONCAT('%', @pEmpresaVisitante, '%')

	IF @pFiltro = ''
		SET @pFiltro = NULL

    IF @pFiltro IS NOT NULL
		SET @pFiltro = CONCAT('%', @pFiltro, '%')

	SELECT T0.idVisitantes, T0.NombreVisitante, T0.RFC, T5.idEstatus AS Estatus, ISNULL(T2.Gafete,0) AS Gafete, ISNULL(T4.idCcostos,0) as idCcostos, T0.EmpResponsable, T0.DescOrigen, T0.idComedor AS DerechoComedor, 
	FORMAT(CAST(T0.FechaIngreso AS datetime), 'dd/MM/yyyy') AS FechaIngreso, ISNULL(T0.Horas,' ') AS Horas, T0.idPlanta
	FROM Visitantes AS T0
	LEFT JOIN Origen AS T1 ON T1.idOrigen = T0.idOrigen
	LEFT JOIN Comedor AS T3 ON T3.idComedor = T0.idComedor
	LEFT JOIN ccostos AS T4 ON T4.idCcostos = T0.idCcostos
	LEFT JOIN Estatus AS T5 ON T5.idEstatus = T0.idEstatus
	LEFT JOIN Gafete AS T2 ON T2.idGafete = T0.idGafete
	WHERE (@pStatus IS NULL OR (T0.idEstatus = @pStatus))
	AND (@pIdVisitante IS NULL OR (T0.idVisitantes = @pIdVisitante))
	AND (@pEmpresaVisitante IS NULL OR (T0.DescOrigen LIKE @pEmpresaVisitante))
	AND (@pCentroCostos IS NULL OR (T4.idCcostos = CAST(@pCentroCostos AS INT)))
	AND (@pFiltro IS NULL
			OR CONVERT(VARCHAR, T0.FechaIngreso, 103) LIKE @pFiltro			
			OR T0.Horas	LIKE @pFiltro
			OR T0.NombreVisitante LIKE @pFiltro			
			OR T0.RFC LIKE @pFiltro
			OR T5.Estatus LIKE @pFiltro
			OR T2.Gafete LIKE @pFiltro
			OR T4.Ccostos LIKE @pFiltro
			OR T4.Descripcion LIKE @pFiltro
			OR T0.EmpResponsable LIKE @pFiltro
			OR T0.DescOrigen LIKE @pFiltro)
	ORDER BY T0.idEstatus ASC, T0.idVisitantes DESC

END TRY
	BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
  		SET @ErrMsg = ERROR_MESSAGE()
  		SET @ErrSev = ERROR_SEVERITY()
  		SET @ErrSt  = ERROR_STATE()
  		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
END