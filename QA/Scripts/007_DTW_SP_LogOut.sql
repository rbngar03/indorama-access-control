IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_LogOut')
  BEGIN
	DROP PROCEDURE DTW_SP_LogOut
  END
GO
-- =============================================
-- Author:		Karla Ramirez
-- Create date: 10/06/24
-- Description:	Script que valida el tiempo la ultima operacion de usuario para el LogOut 
-- History:
--<17/10/2024> <r.garcia> <Se agrega validación de último y primer registro.>
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_LogOut]

-- PARAMETROS DE ENTRADA 
@pIdUser INT,     
@pMessageOperation VARCHAR(500),

-- Variables 
@Response VARCHAR(2) OUTPUT,
@DateOperation DATETIME = NULL,
@UltimaOperacion DATETIME = NULL,
@BloqueoSesion INT = NULL,
@NumeroRegistros INT = 0,
@UltimoMensaje VARCHAR(500) = ''
AS
	BEGIN 

	   SELECT @DateOperation = GETDATE()
	   SELECT @BloqueoSesion = (SELECT Valor 
								FROM DTW_CA_Parametros
								WHERE Nombre = 'Tiempo de inactividad')

	   SET @NumeroRegistros = (SELECT COUNT(*) FROM DTW_CA_LogUsers WHERE IDUser = @pIdUser)
	   
	   SELECT TOP 1 @UltimaOperacion = DATEADD(MINUTE, @BloqueoSesion, DateOperation),
					@UltimoMensaje = MessageOperation
	   FROM DTW_CA_LogUsers 
	   WHERE IDUser = @pIdUser
	   ORDER BY DateOperation DESC		   

	   IF @NumeroRegistros > 0
	   BEGIN
			IF (@DateOperation <= @UltimaOperacion OR @UltimoMensaje = 'Logout')
			BEGIN
				INSERT INTO DTW_CA_LogUsers(IDUser,DateOperation,MessageOperation) 
				VALUES (@pIdUser,@DateOperation,@pMessageOperation);
				SET @Response = 'Ok';
			END
			ELSE
			BEGIN
				INSERT INTO DTW_CA_LogUsers(IDUser,DateOperation,MessageOperation) 
				VALUES (@pIdUser,@DateOperation,'Logout');
				SET @Response = 'No';
			END
		END
		ELSE
		BEGIN
			INSERT INTO DTW_CA_LogUsers(IDUser,DateOperation,MessageOperation) 
			VALUES (@pIdUser,@DateOperation,@pMessageOperation);
			SET @Response = 'Ok';
		END

	RETURN
	END
	