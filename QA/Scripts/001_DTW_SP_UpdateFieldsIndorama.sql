IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_UpdateFieldsIndorama')
  BEGIN
	DROP PROCEDURE DTW_SP_UpdateFieldsIndorama
  END
GO
-- =============================================
-- Author:	Rubén García García
-- Create date: 14/10/2024
-- Description:	Actualiza los campos de algunas tablas
--				para que sean compatibles con la nueva aplicación.
-- <17/10/2024> <r.garcia> <Se agrega actualiación de tabla Planta.>
-- <04/11/2024>	<r.garcia> <Se agrega actualización de campo Usuario en la tabla Usuarios.>
-- <11/11/2024>	<r.garcia> <Se agrega renombrado de campo ResponsableInvista a ResponsableIndorama.>
-- <19/11/2024> <k.ramirez> <Se agrega actualización de tabla Clasificacion.>
-- =============================================

CREATE PROCEDURE DTW_SP_UpdateFieldsIndorama 	
AS
BEGIN
	
	BEGIN TRY

		ALTER TABLE Empleado
		ALTER COLUMN NumEmpleado NVARCHAR(10);

		ALTER TABLE Empleado
		ALTER COLUMN ApePaterno NVARCHAR(100);

		ALTER TABLE Empleado
		ALTER COLUMN ApeMaterno NVARCHAR(100);

		ALTER TABLE Empleado
		ALTER COLUMN Nombre NVARCHAR(100);		

		IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'SFID' AND TABLE_NAME = 'Empleado')
		BEGIN
			ALTER TABLE Empleado
			ADD SFID NVARCHAR(10);
		END

		IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'Tipo' AND TABLE_NAME = 'Terminales')
		BEGIN
			ALTER TABLE Terminales
			ADD Tipo INT;
		END

		IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'Grupo' AND TABLE_NAME = 'Terminales')
		BEGIN
			ALTER TABLE Terminales
			ADD Grupo INT;
		END

		IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'userIdBioStar' AND TABLE_NAME = 'Empleado')
		BEGIN
			ALTER TABLE Empleado
			ADD userIdBioStar INT;
		END

		IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'userIdBioStar' AND TABLE_NAME = 'Contratistas')
		BEGIN
			ALTER TABLE Contratistas
			ADD userIdBioStar INT;
		END

		IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'userIdBioStar' AND TABLE_NAME = 'Visitantes')
		BEGIN
			ALTER TABLE Visitantes
			ADD userIdBioStar INT;
		END

		
		UPDATE Planta SET Descripcion = 'QUERÉTARO FT' WHERE idPlanta = 3
		
		ALTER TABLE Usuarios
		ALTER COLUMN Usuario NVARCHAR(30) NOT NULL;
		
		IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'ResponsableIndorama' AND TABLE_NAME = 'Contratistas')
		BEGIN
			EXEC sp_rename 'Contratistas.ResponsableInvista', 'ResponsableIndorama', 'column';
		END
		
		UPDATE Clasificacion SET Descripcion = 'Non-Indorama IT',fecha_actualizacion = GETDATE() WHERE idClasificacion = 15

		IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = 'Visitantes' AND CONSTRAINT_NAME = 'IX_Visitantes' AND CONSTRAINT_TYPE = 'UNIQUE')
		BEGIN
			ALTER TABLE Visitantes
			DROP CONSTRAINT IX_Visitantes
		END
		
		IF NOT EXISTS (SELECT * FROM TipoLector WHERE idTipoLector = 6)
		BEGIN
			INSERT INTO TipoLector
			VALUES (5, 'I');
		END

	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
  		SET @ErrMsg = ERROR_MESSAGE()
  		SET @ErrSev = ERROR_SEVERITY()
  		SET @ErrSt  = ERROR_STATE()
  		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
END
GO

EXEC DTW_SP_UpdateFieldsIndorama