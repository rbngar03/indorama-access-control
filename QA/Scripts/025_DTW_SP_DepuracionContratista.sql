IF EXISTS (SELECT 1
           FROM SYSOBJECTS 
           WHERE NAME = 'DW_SP_DepuracionContratista')
BEGIN
    DROP PROCEDURE DW_SP_DepuracionContratista
END
GO

-- =============================================
-- Author:       Marco J. Mu�oz Matuk
-- Create date:  28/10/2024
-- Description:  Este SP sirve para depurar la tabla de contratista del campo estatus; se manda llamar mediante un job a las 12:00 de la noche
-- =============================================

CREATE PROCEDURE [dbo].[DW_SP_DepuracionContratista] 	
AS
BEGIN
    BEGIN TRY
        DECLARE @ValorParam INT;
        DECLARE @FechaActual DATETIME = GETDATE();

        -- Obtenemos el valor del par�metro desde la tabla DTW_CA_Parametros
        SELECT @ValorParam = Valor FROM DTW_CA_Parametros WHERE idparametro = 2;

        -- Actualizamos la columna 'idEstatus' en la tabla 'contratistas'
        UPDATE contratistas
        SET idEstatus = CASE 
            WHEN VigenciaIMSS <= @FechaActual 
                AND DATEDIFF(DAY, VigenciaIMSS, @FechaActual) > @ValorParam THEN 2
            WHEN VigenciaIMSS <= @FechaActual 
                AND DATEDIFF(DAY, VigenciaIMSS, @FechaActual) <= @ValorParam THEN 1
            WHEN VigenciaIMSS > @FechaActual THEN 1
            ELSE NULL 
        END
        FROM contratistas 
    END TRY
    BEGIN CATCH
        DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT;
        SET @ErrMsg = ERROR_MESSAGE();
        SET @ErrSev = ERROR_SEVERITY();
        SET @ErrSt  = ERROR_STATE();
        RAISERROR(@ErrMsg, @ErrSev, @ErrSt);
    END CATCH
END
