IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_CreateTableParametros')
  BEGIN
	DROP PROCEDURE DTW_SP_CreateTableParametros
  END
GO
-- =============================================
-- Author:	Karla Ramirez
-- Create date: 26/07/2024
-- Description:	Crea la tabla para almacenar parametros.
-- <14/10/2024> <r.garcia> <Se agrega validación de creación previa de la tabla DTW_CA_Parametros.>
-- <05/12/2024> <k.ramirez> <Se realizan ajustes para agregar datos de Biostar.>
-- <10/12/2024>	<r.garcia>	<Se agrega validación de existencia de tabla.>
-- =============================================

CREATE PROCEDURE [dbo].[DTW_SP_CreateTableParametros]	 	
AS
BEGIN

	DECLARE @URLBIOSTAR2 VARCHAR(200);
	DECLARE @UsuarioBIOSTAR2 VARCHAR(200);
	DECLARE @PasswordBIOSTAR2 VARCHAR(200); 

	SET @URLBIOSTAR2 = 'https://192.168.10.20';  -- Modifica esta URL con la direccion donde esta alojado Biostar
	SET @UsuarioBIOSTAR2 = 'admin'; -- Modifica este valor con el usiario que usara para iniciar sesion en biostar
	SET @PasswordBIOSTAR2 = '1nd0r4m4'; -- Modifica este valor con la contraseña del usuario que se usara para acceder a Biostar
	
	BEGIN TRY

		IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DTW_CA_Parametros')
		BEGIN
			CREATE TABLE DTW_CA_Parametros (
					 IdParametro INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
					 Nombre varchar(100),
					 Descripcion varchar(200),
					 Valor int,
					 Unidad varchar(50)					 
			);

			INSERT INTO DTW_CA_Parametros VALUES ('Tiempo de inactividad', 
												  'Tiempo en que la aplicación se cerrará después de haber realizado la última operación.',
												  30, 'minutos')

			INSERT INTO DTW_CA_Parametros VALUES ('Vigencia registro contratistas', 
												  'Tiempo en el que se inactivarán los registros de contratistas basándose en la fecha del campo Vigencia IMSS.',
												  365, 'días')
	
			INSERT INTO DTW_CA_Parametros VALUES ('Vigencia registro visitantes', 
												  'Tiempo de vigencia que tendrán los registros de los visitantes en el sistema antes de ser eliminados básandose en la fecha de ingreso.',
												  183, 'días')

			INSERT INTO DTW_CA_Parametros VALUES ('Vigencia de inducción contratistas', 
												  'Tiempo en el que expira la fecha de inducción de los contratistas.',
												  183, 'días')
												  
			INSERT INTO DTW_CA_Parametros VALUES ('Url Biostar2', @URLBIOSTAR2, NULL, NULL)

			INSERT INTO DTW_CA_Parametros VALUES ('Usuario Biostar2', @UsuarioBIOSTAR2, NULL, NULL)

			INSERT INTO DTW_CA_Parametros VALUES ('Contraseña Biostar2', @PasswordBIOSTAR2, NULL, NULL)
		END
		ELSE
		BEGIN
			DROP TABLE DTW_CA_Parametros;

			CREATE TABLE DTW_CA_Parametros (
					 IdParametro INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
					 Nombre varchar(100),
					 Descripcion varchar(200),
					 Valor int,
					 Unidad varchar(50)					 
			);

			INSERT INTO DTW_CA_Parametros VALUES ('Tiempo de inactividad', 
												  'Tiempo en que la aplicación se cerrará después de haber realizado la última operación.',
												  30, 'minutos')

			INSERT INTO DTW_CA_Parametros VALUES ('Vigencia registro contratistas', 
												  'Tiempo en el que se inactivarán los registros de contratistas basándose en la fecha del campo Vigencia IMSS.',
												  365, 'días')
	
			INSERT INTO DTW_CA_Parametros VALUES ('Vigencia registro visitantes', 
												  'Tiempo de vigencia que tendrán los registros de los visitantes en el sistema antes de ser eliminados básandose en la fecha de ingreso.',
												  183, 'días')

			INSERT INTO DTW_CA_Parametros VALUES ('Vigencia de inducción contratistas', 
												  'Tiempo en el que expira la fecha de inducción de los contratistas.',
												  183, 'días')
												  
			INSERT INTO DTW_CA_Parametros VALUES ('Url Biostar2', @URLBIOSTAR2, NULL, NULL)

			INSERT INTO DTW_CA_Parametros VALUES ('Usuario Biostar2', @UsuarioBIOSTAR2, NULL, NULL)

			INSERT INTO DTW_CA_Parametros VALUES ('Contraseña Biostar2', @PasswordBIOSTAR2, NULL, NULL)
		END

	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
  		SET @ErrMsg = ERROR_MESSAGE()
  		SET @ErrSev = ERROR_SEVERITY()
  		SET @ErrSt  = ERROR_STATE()
  		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
END
GO

EXEC DTW_SP_CreateTableParametros