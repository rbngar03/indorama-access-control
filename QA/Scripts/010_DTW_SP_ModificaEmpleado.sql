IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_ModificaEmpleado')
  BEGIN
	DROP PROCEDURE DTW_SP_ModificaEmpleado
  END
GO
-- =============================================
-- Author:		Karla Ramirez
-- Create date: 27/08/24
-- Description:	Script que actualiza datos de los Empleados
-- History:
-- <19/11/2024> <k.ramirez> <Se agrega campo de SFID>
-- <06/12/2024>	<r.garcia>	<Se agrega valor inicial a parametro @pSFID.>
-- <07/01/2025>	<k.ramirez>	<Se realiza ajuste para omitir edicion de gafete y se asigna null al gafete cuando se inactiva al empleado>
-- <30/01/2025> <ag.marin>  <Se agrega validación para cambiar de ccostos cuando sea nulo.>
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_ModificaEmpleado]
	@pIdEmpleado INT,
	@pNumEmpleado VARCHAR(10), 
    @pApePaterno VARCHAR(100),
	@pApeMaterno VARCHAR(100),
	@pNombre VARCHAR(100),
	@pidCcostos INT,
	@pidEstatus INT,
	@pidPlanta INT,
	@pSFID VARCHAR(10) = '',
	@Response VARCHAR(100) OUTPUT

AS
BEGIN
	DECLARE @IdEstatusAct INT, @NumEmpleadoAct VARCHAR(10),
            @CcostosAct INT, @GafeteAct INT, @IdGafete INT,
			@IdGafeteAct INT, @GafeteProvAct INT, @IdGafeteProv INT

	BEGIN TRY 
       
		BEGIN TRAN

		-- Se obtienen valores actuales 
		SET @IdEstatusAct = (SELECT idEstatus FROM Empleado WHERE idEmpleado = @pIdEmpleado)
		SET @NumEmpleadoAct = (SELECT NumEmpleado FROM Empleado WHERE idEmpleado = @pIdEmpleado)
		SET @CcostosAct = (SELECT idCcostos FROM Empleado WHERE idEmpleado = @pIdEmpleado)--CENTRO DE COSTOS ACTUAL 
		SET @GafeteAct = (SELECT idGafete FROM Empleado WHERE idEmpleado = @pIdEmpleado)--NUMERO DE GAFETE
		SET @IdGafete = (SELECT idGafete FROM Gafete WHERE Gafete = @GafeteAct AND idTipoGafete = 1 AND idTipoPersonal = 1)--ID
		SET @GafeteProvAct = (SELECT NumeroProvisionalGafete FROM Empleado WHERE idEmpleado = @pIdEmpleado)--NUMERO DE GAFETE

		   -- Validaciones y actualizaciones
		IF @pidEstatus = 2
		BEGIN
			UPDATE Gafete
            SET FechaVigencia = NULL, idEstatus = 2
            WHERE idGafete IN (@GafeteAct, @IdGafeteProv);
			
			UPDATE Empleado 
			SET idEstatus = @pidEstatus, 
			--Foto = isnull(@pFoto,Foto), 
			idGafete = NULL,
			NumeroProvisionalGafete = NULL

			WHERE idEmpleado = @pIdEmpleado   
		END
	    ELSE IF @pidEstatus = 1
	    BEGIN
			IF EXISTS (SELECT 1 FROM Empleado WHERE NumEmpleado = @pNumEmpleado AND idEmpleado <> @pIdEmpleado)
            BEGIN
                SET @Response = 'El numero de empleado esta asignado a otro empleado';
                RAISERROR('El numero de empleado esta asignado a otro Empleado', 16, 1);
                ROLLBACK TRAN;
                RETURN;
            END

	     		 -- Actualizacion de Centro de Costos 
		  IF @CcostosAct IS NULL OR @pidCcostos <> @CcostosAct
		  BEGIN
			IF EXISTS (SELECT 1 FROM ccostos WHERE IdCcostos = @pidCcostos)
			UPDATE Empleado SET idCcostos = @pidCcostos WHERE idEmpleado = @pIdEmpleado
			ELSE
			BEGIN
				SET @Response = 'El centro de Costos no existe';
				RAISERROR('El centro de Costos no existe', 16, 1)
			    ROLLBACK TRAN;
			    RETURN
			END
		  END 
		END

		-- Actualizacion de datos generales --
		UPDATE [dbo].[Empleado]
		   SET [ApePaterno] = @pApePaterno,
			   [ApeMaterno] = ISNULL(@pApeMaterno,NULL),
			   [Nombre] = @pNombre,
			   [idTipoNomina] = NULL,
			   [NominaProcesada] = '',
			   [idEstatus] = @pidEstatus,
			   [Notas] = NULL,
			   [idPlanta] = @pidPlanta,
			   [fecha_actualizacion] = GETDATE(),
			   [SFID] = ISNULL(@pSFID, NULL)
		 WHERE idEmpleado = @pIdEmpleado
		 
	 COMMIT TRAN
	 SET @Response = 'Ok'; 
	
   END TRY
   BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRAN;
			DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
			SET @ErrMsg = ERROR_MESSAGE()
			SET @ErrSev = ERROR_SEVERITY()
			SET @ErrSt = ERROR_STATE()
			RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
 END
