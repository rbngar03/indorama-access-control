IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_Contratistas')
  BEGIN
	DROP PROCEDURE DTW_SP_Contratistas
  END
GO
-- =============================================
-- Author:		Karla Ramirez
-- Create date: 10/06/24
-- Description:	Script que obtiene la consulta de Contratistas
-- History:
-- <26/09/2024> <ag.marin> <Se modifica tipo de dato de la Clasificacion.>
-- <26/09/2024> <ag.marin> <Se modifica INNER JOIN por LEFT JOIN en la tabla Gafete.>
-- <26/09/2024> <ag.marin> <Se agrega ISNULL al campo Gafete para que regreso 0, en caso de serlo.>
-- <14/10/2024> <k.ramirez> <Se modifica sp para realizar exportacion>
-- <18/10/2024> <k.ramirez> <Se agrega filtro derecho de comedor>
-- <29/10/2024> <mj.munoz> <Se agrega columna validaInduccion para verificar que contratista necesita renovar la vigencia de induccion>
-- <03/12/2024> <ag.marin> <Se comentan lineas de paginación, se cambia JOINs.>
-- <04/12/2024> <ag.marin, k.ramirez> <Se limpia Script. Se corrige ORDER BY.>
-- <16/01/2025>	<r.garcia> <Se agrega JOIN para obtener Gafete Provisional.>
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_Contratistas](
@pStatus INT = NULL, -- {1-Activo / 2-Inactivo}
@pIdContratista INT = NULL,
@pTipoContratista INT = NULL, -- {1-CortoPlazo / 2-Residente}
@pEmpresa VARCHAR (50) = NULL,
@pCentroCostos VARCHAR (150) = NULL,
@pClasificacion VARCHAR (50) = NULL,
@pComedor INT = NULL, -- {1-SI / 2-NO }
@pFiltro VARCHAR(100) = NULL
)AS
 BEGIN
	BEGIN TRY
	IF @pStatus = 0
		SET @pStatus = NULL

	IF @pIdContratista = 0
		SET @pIdContratista = NULL

	IF @pTipoContratista = 0
		SET @pTipoContratista = NULL

	IF @pEmpresa IS NOT NULL
		SET @pEmpresa = CONCAT('%', @pEmpresa, '%')

	IF @pCentroCostos IS NOT NULL
		SET @pCentroCostos = CONCAT('%', @pCentroCostos, '%')

	IF @pClasificacion = ''
		SET @pClasificacion = NULL

	IF @pComedor = 0
		SET @pComedor = NULL

	IF @pFiltro = ''
		SET @pFiltro = NULL

    IF @pFiltro IS NOT NULL
		SET @pFiltro = CONCAT('%', @pFiltro, '%')
		
	DECLARE @ValorParam INT;
	SELECT @ValorParam = Valor FROM DTW_CA_Parametros WHERE idparametro = 4;

	BEGIN
		SELECT T0.idContratistas,
			   T1.idTipoContratista,
			   T0.NombreContratista,
			   T0.ApPaterno,
			   T0.ApMaterno,
			   FORMAT(CAST(T0.FechaIndSeguridad AS datetime),'dd/MM/yyyy HH:mm') AS FechaIndSeguridad, 
			   FORMAT(CAST(T0.FechaContrato AS datetime), 'dd/MM/yyyy HH:mm') AS FechaContrato,
			   FORMAT(CAST(T0.FechaTerminacion AS datetime), 'dd/MM/yyyy HH:mm') AS FechaTerminacion,
			   T0.IMSS, 
			   FORMAT(CAST(T0.VigenciaIMSS AS datetime), 'dd/MM/yyyy HH:mm') AS VigenciaIMSS,
			   T0.NumeroContrato,
			   T0.NombreCompañia,
			   T0.RFC,
			   ISNULL(T3.idClasificacion, 0) AS idClasificacion,
			   T0.ResponsableIndorama,
			   T4.idCcostos,
			   ISNULL(T5.Gafete, 0) AS Gafete,
			   ISNULL(T8.Gafete, 0) AS NumeroProvisionalGafete,
			   T6.idComedor AS DerechoComedor,
			   T7.idEstatus,
			   T0.idPlanta,
			   CASE WHEN T0.VigenciaIMSS > GETDATE() THEN 'N' WHEN DATEADD(DAY, @ValorParam, T0.VigenciaIMSS) < GETDATE() THEN 'S' ELSE 'N' END AS ValidaInduccion
		FROM Contratistas AS T0
		INNER JOIN TipoContratista AS T1 
		ON T0.idTipoContratista = T1.idTipoContratista
		LEFT JOIN Compania AS T2 
		ON T0.idCompanias = T2.idCompanias
		LEFT JOIN Clasificacion AS T3
		ON T0.idClasificacion = T3.idClasificacion
		INNER JOIN ccostos AS T4
		ON T0.idCcostos = T4.idCcostos
		LEFT JOIN Gafete AS T5
		ON T0.idGafete = T5.idGafete		
		INNER JOIN Comedor AS T6
		ON T0.idComedor = T6.idComedor 
		INNER JOIN Estatus AS T7 
		ON T0.idEstatus = T7.idEstatus
		LEFT JOIN Gafete AS T8
		ON T0.NumeroProvisionalGafete = T8.idGafete
		WHERE (@pStatus IS NULL OR (T0.idEstatus = @pStatus))
		AND (@pIdContratista IS NULL OR (T0.idContratistas = @pIdContratista))
		AND (@pTipoContratista IS NULL OR (T1.idTipoContratista = @pTipoContratista))
		AND (@pEmpresa IS NULL OR (T0.NombreCompañia LIKE @pEmpresa))
		AND (@pCentroCostos  IS NULL OR (T4.Ccostos LIKE @pCentroCostos OR T4.Descripcion LIKE @pCentroCostos))
		AND (@pClasificacion IS NULL OR (T3.Clasificacion = @pClasificacion))
		AND (@pComedor IS NULL OR (T6.idComedor = @pComedor))
		AND (@pFiltro IS NULL
				OR T1.TipoContratista LIKE @pFiltro
				OR T7.Estatus LIKE @pFiltro
				OR T0.NombreContratista LIKE @pFiltro
				OR T0.ApPaterno LIKE @pFiltro
				OR T0.ApMaterno LIKE @pFiltro
				OR CONCAT(T0.NombreContratista, ' ', T0.ApPaterno, ' ', T0.ApMaterno) LIKE @pFiltro
				OR FORMAT(CAST(T0.FechaIndSeguridad AS DATETIME), 'dd/MM/yyyy HH:mm') LIKE @pFiltro
				OR FORMAT(CAST( T0.FechaContrato AS datetime), 'dd/MM/yyyy HH:mm') LIKE @pFiltro
				OR FORMAT(CAST( T0.FechaTerminacion AS datetime), 'dd/MM/yyyy HH:mm') LIKE @pFiltro
				OR T0.IMSS LIKE @pFiltro
				OR FORMAT(CAST( T0.VigenciaIMSS AS datetime), 'dd/MM/yyyy HH:mm') LIKE @pFiltro
				OR T0.NumeroContrato LIKE @pFiltro
				OR T0.NombreCompañia LIKE @pFiltro
				OR T0.RFC LIKE @pFiltro
				OR T3.Clasificacion LIKE @pFiltro
				OR T0.ResponsableIndorama LIKE @pFiltro
				OR T4.Ccostos LIKE @pFiltro
				OR T5.Gafete LIKE @pFiltro
				OR T8.Gafete LIKE @pFiltro)
		ORDER BY T0.idEstatus ASC, T0.idContratistas DESC
	 END
	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT, @ErrDate DATE
  		SET @ErrMsg = ERROR_MESSAGE()
  		SET @ErrSev = ERROR_SEVERITY()
  		SET @ErrSt  = ERROR_STATE()
		SET @ErrDate = CONVERT(DATE, GETDATE());
  		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
		INSERT INTO LogError (fecha, ErrMsg, ErrSev, ErrSt) VALUES (@ErrDate, @ErrMsg, @ErrSev, @ErrSt)
	END CATCH
END
 