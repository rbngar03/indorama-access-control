IF EXISTS (SELECT 1
           FROM SYSOBJECTS 
           WHERE NAME = 'DW_SP_DepuracionVisitantes')
BEGIN
    DROP PROCEDURE DW_SP_DepuracionVisitantes
END
GO

/****** Object:  StoredProcedure [dbo].[DW_SP_DepuracionContratista]    Script Date: 28/10/2024 09:31:54 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:       Marco J. Muñoz Matuk
-- Create date:  28/10/2024
-- Description:  Este SP sirve para depurar la tabla visitantes y actualiza el responsable y el gafete se manda llamar mediante un job a las 12:00 de la noche
-- =============================================
CREATE PROCEDURE [dbo].[DW_SP_DepuracionVisitantes] 	
AS
BEGIN
    BEGIN TRY
        DECLARE @valor INT;

        -- Obtenemos el valor del par�metro desde la tabla DTW_CA_Parametros
        SELECT @valor = valor FROM DTW_CA_Parametros WHERE idparametro = 3;
		
		UPDATE Visitantes
		SET idEstatus = 2
		WHERE DATEDIFF(DAY, FechaIngreso, GETDATE()) > @valor;
		
        -- Actualizamos la columna 'EmpResponsable' y 'idGafete' de la tabla Visitantes
        UPDATE Visitantes
        SET EmpResponsable = '', 
            idGafete = NULL        

    END TRY
    BEGIN CATCH
        DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT;
        SET @ErrMsg = ERROR_MESSAGE();
        SET @ErrSev = ERROR_SEVERITY();
        SET @ErrSt  = ERROR_STATE();
        RAISERROR(@ErrMsg, @ErrSev, @ErrSt);
    END CATCH
END
