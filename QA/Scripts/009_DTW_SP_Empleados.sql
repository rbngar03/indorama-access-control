IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_Empleados')
  BEGIN
	DROP PROCEDURE DTW_SP_Empleados
  END
GO
-- =============================================
-- Author:		Karla Ramirez
-- Create date: 20/08/24
-- Description:	Script que obtiene la consulta de Empleados
-- History:
-- <14/10/2024> <k.ramirez> <Se modifica sp para realizar exportacion>
-- <07/11/2024> <r.garcia>	<Se sustituye ccostos por idCcostos.>
-- <08/11/2024>	<r.garcia>	<Se agrega criterio idEmpleado al filtro.>
-- <20/11/2024>	<r.garcia>	<Se agrega campo SFID.>
-- <02/12/2024>	<r.garcia>	<Se elimina obtención de Foto.>
-- <02/12/2024>	<r.garcia>	<Se elimina páginación.>
-- <19/12/2024>	<r.garcia>	<Se agrega filtro por nombre completo.>
-- <16/01/2024>	<r.garcia>	<Se agrega JOIN para obtener Gafete Provisional.>
-- <22/01/2025> <k.ramirez> <Se limpia Script.>
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_Empleados](
@pStatus INT = NULL, -- {1-Activo / 2-Inactivo}
@pPlanta VARCHAR(50) = NULL, -- {1-Qro Pet / 2-Santa Fe / 3-Qro Fit}
@pCentroCostos VARCHAR(50) = NULL,
@pFiltro VARCHAR(100) = NULL, 
@pIdEmpleado INT = NULL
)AS
BEGIN 
  BEGIN TRY
  IF @pStatus = 0
	SET @pStatus = NULL

	IF @pPlanta IS NOT NULL
	SET @pPlanta = CONCAT('%', @pPlanta, '%')	

	IF @pIdEmpleado = 0
	SET @pIdEmpleado = NULL

    IF @pFiltro = ''
		SET @pFiltro = NULL

	IF @pFiltro IS NOT NULL	
		SET @pFiltro = CONCAT('%',@pFiltro,'%')

	SELECT T0.idEmpleado, T0.NumEmpleado, T0.idEstatus, T0.Nombre, T0.ApePaterno ,ISNULL(T0.ApeMaterno,' ') AS ApeMaterno,
	ISNULL(T2.idCcostos, 0) AS CentroCostos, ISNULL(T4.Gafete, 0) AS Gafete, ISNULL(T6.Gafete, 0) AS GafeteProvisional, T0.idPlanta, T0.SFID
	FROM Empleado AS T0
	INNER JOIN Estatus AS T1 ON T1.idEstatus = T0.idEstatus 
	LEFT JOIN ccostos AS T2 ON T2.idCcostos = T0.idCcostos
	LEFT JOIN Gafete AS T4 ON T4.idGafete = T0.idGafete 		
	INNER JOIN Planta AS T5 ON T5.idPlanta = T0.idPlanta
	LEFT JOIN Gafete AS T6 ON T6.idGafete = T0.NumeroProvisionalGafete
	WHERE (@pStatus IS NULL OR (T1.idEstatus = @pStatus))
	AND (@pPlanta IS NULL OR (T5.Descripcion LIKE @pPlanta))
	AND (@pCentroCostos IS NULL OR (T2.idCcostos = CAST(@pCentroCostos AS INT)))
	AND (@pIdEmpleado IS NULL OR (T0.idEmpleado = @pIdEmpleado))
	AND ( @pFiltro IS NULL
			OR T0.NumEmpleado LIKE @pFiltro
			OR T1.Estatus LIKE @pFiltro
			OR T0.Nombre LIKE @pFiltro
			OR CONCAT(T0.Nombre,' ',T0.ApePaterno,' ',T0.ApeMaterno) LIKE @pFiltro
			OR T0.ApePaterno LIKE @pFiltro
			OR T0.ApeMaterno LIKE @pFiltro
			OR T2.Ccostos LIKE @pFiltro
			OR T2.Descripcion LIKE @pFiltro
			OR T4.Gafete LIKE @pFiltro
			OR T5.Descripcion LIKE @pFiltro
			OR T6.Gafete LIKE @pFiltro)
	ORDER BY T0.idEstatus ASC, T0.idEmpleado DESC		
       
END TRY
	BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
  		SET @ErrMsg = ERROR_MESSAGE()
  		SET @ErrSev = ERROR_SEVERITY()
  		SET @ErrSt  = ERROR_STATE()
  		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
END
