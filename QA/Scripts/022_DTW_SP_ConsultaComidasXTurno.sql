IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_ConsultaComidasXTurno')
  BEGIN
	DROP PROCEDURE DTW_SP_ConsultaComidasXTurno
  END
GO
/****** Object:  StoredProcedure [dbo].[DTW_SP_ConsultaComidasXTurno]    Script Date: 17/10/2024 10:25:02 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Marco J. Muñoz Matuk
-- Create date: 14/10/2024
-- Description:	Consulta que trae la cantidad de salidas a comedor que se hicieron por dia en los diferentes turnos del dia
-- History:
-- <04/01/2024>	<ja.ponce>	<se modifica consulta.>
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_ConsultaComidasXTurno]  
@pStartDate DATE,
@pEndDate   DATE,
@pSitio     NVARCHAR(30) = ''
AS
BEGIN
    -- Evitar mensajes adicionales de "N filas afectadas"
    SET NOCOUNT ON;

    BEGIN TRY
		--Si la fecha es del mismo día (Por como esta desarrollada la consulta poner fecha final +1 día)
       -- SET @pEndDate = DATEADD(day, 1, @pEndDate)
        -- Condición para verificar si el parámetro es uno de los sitios específicos o está vacío
        --IF @pSitio IN ('QUERÉTARO PET', 'QUERÉTARO FT', 'SANTA FE')
		IF @pSitio IN (select Descripcion from Planta)
        BEGIN

			-- JAPG obtenemos marcajes de comedor y gurdamos en TMP
			SELECT M1.FechaRegistro, M1.idEmpleado, M1.idContratistas, M1.idVisitantes INTO #Marcajes
			FROM Marcajes M1
			LEFT JOIN Empleado E1 ON M1.idEmpleado = E1.idEmpleado
			LEFT JOIN Contratistas C1 ON M1.idContratistas = C1.idContratistas
			LEFT JOIN Visitantes V1 ON M1.idVisitantes = V1.idVisitantes
			WHERE M1.EntradaSalida = 'C'AND CAST(M1.FechaRegistro AS DATE) BETWEEN @pStartDate and @pEndDate
			AND ISNULL(E1.idPlanta, ISNULL(C1.idPlanta, V1.idPlanta)) = (SELECT TOP 1 IdPlanta FROM Planta WHERE Descripcion = @pSitio);

            -- Si se proporciona un sitio específico, ejecutar la lógica actual
            WITH Turnos AS (
                SELECT '1' AS Turno, '06:00:00' AS HoraInicio, '14:59:59' AS HoraFin
                UNION ALL
                SELECT '2' AS Turno, '15:00:00' AS HoraInicio, '21:59:59' AS HoraFin
                UNION ALL
                SELECT '3' AS Turno, '22:00:00' AS HoraInicio, '05:59:59' AS HoraFin
            ),
			Fechas AS(
				-- JAPG se agrega comedor y cast en where
				SELECT DISTINCT CAST(FechaRegistro AS DATE) FechaRegistro
				FROM Marcajes
				WHERE EntradaSalida = 'C'AND CAST(FechaRegistro AS DATE) BETWEEN @pStartDate and @pEndDate
			)
            SELECT 
                b.FechaRegistro,
                b.Descripcion,
                b.Turno,
				COUNT(CASE 
							WHEN t1.idEmpleado IS NOT NULL OR t1.idContratistas IS NOT NULL OR t1.idVisitantes IS NOT NULL 
							THEN 1 
							ELSE NULL 
						END) AS TotalRegistros
            FROM (
                SELECT 
                    F.FechaRegistro,
                    T3.Descripcion,
                    T.Turno
                FROM Fechas F
                CROSS JOIN Turnos T
                CROSS JOIN (SELECT DISTINCT Descripcion FROM Planta WHERE Descripcion = @pSitio) t3
            ) AS b
            LEFT JOIN #Marcajes t1 ON 
                (
					-- JAPG se modifica where
                    (b.Turno = '1' AND CONVERT(TIME, t1.FechaRegistro) BETWEEN '06:00:00' AND '14:59:59')
					OR (b.Turno = '2' AND CONVERT(TIME, t1.FechaRegistro) BETWEEN '15:00:00' AND '21:59:59')
					OR (b.Turno = '3' AND (
						CONVERT(TIME, t1.FechaRegistro) BETWEEN '22:00:00' AND '23:59:59'
						OR CONVERT(TIME, t1.FechaRegistro) BETWEEN '00:00:00' AND '05:59:59'
					))
                ) AND CAST(t1.FechaRegistro AS DATE) = b.FechaRegistro
            GROUP BY 
                b.FechaRegistro,
                b.Turno,
                b.Descripcion
            ORDER BY 
				b.Descripcion ASC,
                b.FechaRegistro ASC, 
                b.Turno ASC;

			-- JAPG borramos tabla temporal
			DROP TABLE #Marcajes;
        END
        ELSE
        BEGIN
            -- Si el parámetro @pSitio es "TODAS", hacer un UNION ALL para todos los sitios
            IF @pSitio = 'Todas'
            BEGIN
				-- JAPG se modifica consulta para que tome todas las plantas de la tabla
				SELECT M1.FechaRegistro, M1.idEmpleado, M1.idContratistas, M1.idVisitantes, ISNULL(E1.idPlanta, ISNULL(C1.idPlanta, V1.idPlanta)) idPlanta INTO #MarcajesAll
				FROM Marcajes M1
				LEFT JOIN Empleado E1 ON M1.idEmpleado = E1.idEmpleado
				LEFT JOIN Contratistas C1 ON M1.idContratistas = C1.idContratistas
				LEFT JOIN Visitantes V1 ON M1.idVisitantes = V1.idVisitantes
				LEFT JOIN Planta P1 ON ISNULL(E1.idPlanta, ISNULL(C1.idPlanta, V1.idPlanta)) = P1.idPlanta
				WHERE M1.EntradaSalida = 'C'AND CAST(M1.FechaRegistro AS DATE) BETWEEN @pStartDate and @pEndDate;


				--definimos los turnos
				WITH Turnos AS (
								SELECT '1' AS Turno, '06:00:00' AS HoraInicio, '14:59:59' AS HoraFin
								UNION ALL
								SELECT '2' AS Turno, '15:00:00' AS HoraInicio, '21:59:59' AS HoraFin
								UNION ALL
								SELECT '3' AS Turno, '22:00:00' AS HoraInicio, '05:59:59' AS HoraFin
							),
					Fechas AS(
								SELECT DISTINCT CAST(M1.FechaRegistro AS DATE) FechaRegistro
								FROM Marcajes M1
								WHERE M1.EntradaSalida = 'C'AND CAST(M1.FechaRegistro AS DATE) BETWEEN @pStartDate and @pEndDate
							)

								SELECT 
								b.FechaRegistro,
								b.Descripcion,
								b.Turno,
								COUNT(CASE 
											WHEN t1.idEmpleado IS NOT NULL OR t1.idContratistas IS NOT NULL OR t1.idVisitantes IS NOT NULL 
											THEN 1 
											ELSE NULL 
										END) AS TotalRegistros
							FROM (
								SELECT 
									F.FechaRegistro,
									T3.idPlanta,
									T3.Descripcion,
									T.Turno
								FROM Fechas F
								CROSS JOIN Turnos T
								CROSS JOIN Planta t3
							) AS b
							LEFT JOIN #MarcajesAll t1 ON 
								(
									(b.Turno = '1' AND CONVERT(TIME, t1.FechaRegistro) BETWEEN '06:00:00' AND '14:59:59')
									OR (b.Turno = '2' AND CONVERT(TIME, t1.FechaRegistro) BETWEEN '15:00:00' AND '21:59:59')
									OR (b.Turno = '3' AND (
										CONVERT(TIME, t1.FechaRegistro) BETWEEN '22:00:00' AND '23:59:59'
										OR CONVERT(TIME, t1.FechaRegistro) BETWEEN '00:00:00' AND '05:59:59'
									))
								) AND CAST(t1.FechaRegistro AS DATE) = b.FechaRegistro AND b.idPlanta = t1.idPlanta
							GROUP BY 
								b.FechaRegistro,
								b.Turno,
								--b.idPlanta,
								b.Descripcion
							ORDER BY 
							    b.Descripcion ASC,
								b.FechaRegistro ASC, 
								b.Turno ASC;

							DROP TABLE #MarcajesAll;
            END
            ELSE
            BEGIN
                -- Si el parámetro @pSitio está vacío, no se ejecuta ninguna consulta.
                -- Puedes dejar esto como un mensaje informativo o una consulta predeterminada si lo deseas.
                SELECT 'No se especificó un sitio válido.' AS Mensaje;
            END
        END
    END TRY
    BEGIN CATCH
        DECLARE @v_errMsg VARCHAR(MAX), @v_errSev INT, @v_errSt INT;
        SET @v_errMsg = ERROR_MESSAGE() + ' Linea: ' + CAST(ERROR_LINE() AS VARCHAR);
        SET @v_errSev = ERROR_SEVERITY();
        SET @v_errSt = ERROR_STATE();
        RAISERROR(@v_errMsg, @v_errSev, @v_errSt);
    END CATCH
END