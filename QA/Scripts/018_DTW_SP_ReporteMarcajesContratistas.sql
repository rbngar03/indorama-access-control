IF EXISTS (	SELECT 1
			FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_ReporteMarcajesContratistas' )
  BEGIN
	DROP PROCEDURE DTW_SP_ReporteMarcajesContratistas
  END
GO
-- =============================================
-- Author:		Alejandra Gpe. Marín Ascencio
-- Create date: 03/10/24
-- Description:	Script que obtiene los marcajes para generar reporte por contratista
-- History:
-- <27/12/2024>	<ag.marin>	<Se crean tablas temporales para guardar marcajes y se modifica consulta para obtenerlos de estas.>
-- <27/12/2024>	<ja.ponce>	<Se modifica AND por OR en consulta de fechas. Se agrega MIN a la fecha de entrada y MAX a la de salida.>
-- <30/12/2024>	<ag.marin>	<Se agrega día de registro a a la agrupación de entradas y salidas.>
-- <30/12/2024>	<ja.ponce>	<Se agrega UPPER al nombre de contratista, se agregan los filtros por fecha a las entradas y salidas.>
-- <28/01/2025>	<k.ramirez> <Se toma el nombre del contratista de la tabla Contratistas y se ajustan las condiciones del where>
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_ReporteMarcajesContratistas]
@pFechaInicial 		VARCHAR (20),
@pFechaFinal 		VARCHAR (20),
@pEmpresa 			VARCHAR (150) = NULL,
@pNombreContratista VARCHAR (150) = NULL
AS
BEGIN
	DECLARE @vFechaInicial		DATE,
			@vFechaFinal		DATE,
			@vEmpresa			VARCHAR(150),
			@vNombreContratista VARCHAR(150)
			
	BEGIN TRY 
		SET @vFechaInicial		= CONVERT(DATE, @pFechaInicial, 103)
		SET @vFechaFinal		= CONVERT(DATE, @pFechaFinal, 103)
		SET @vEmpresa			= UPPER (@pEmpresa)
		SET @vNombreContratista = CONCAT('%', UPPER(@pNombreContratista), '%')
		
		CREATE TABLE #Entradas (
			FechaRegistro DATETIME,
			idContratistas INT,
			DiaRegistro DATE,
			NombrePersonal VARCHAR(150)
		);

		CREATE TABLE #Salidas (
			FechaRegistro DATETIME,
			idContratistas INT,
			DiaRegistro DATE,
			NombrePersonal VARCHAR(150)
		);
		
		INSERT INTO #Entradas (FechaRegistro, idContratistas, DiaRegistro, NombrePersonal)
		SELECT 
			MIN(T1.FechaRegistro) AS FechaRegistro,
			T1.idContratistas,
			CONVERT(DATE, FechaRegistro) AS DiaRegistro,
			T1.NombrePersonal
		FROM Marcajes T1
		WHERE T1.EntradaSalida = 'E' AND idContratistas IS NOT NULL
		AND CAST(FechaRegistro AS DATE) BETWEEN @vFechaInicial AND @vFechaFinal
		GROUP BY idContratistas, CONVERT(DATE, FechaRegistro), NombrePersonal;
		
		INSERT INTO #Salidas (FechaRegistro, idContratistas, DiaRegistro, NombrePersonal)
		SELECT 
			MAX(T1.FechaRegistro) AS FechaRegistro,
			T1.idContratistas,
			CONVERT(DATE, FechaRegistro) AS DiaRegistro,
			T1.NombrePersonal
		FROM Marcajes T1
		WHERE T1.EntradaSalida = 'S' AND idContratistas IS NOT NULL
		AND CAST(FechaRegistro AS DATE) BETWEEN @vFechaInicial AND @vFechaFinal
		GROUP BY idContratistas, CONVERT(DATE, FechaRegistro), NombrePersonal;

		SELECT E.FechaRegistro AS FechaEntrada,   
			   S.FechaRegistro AS FechaSalida,   
			   UPPER(T3.NombreCompañia) AS EmpresaContratista,  
			   UPPER(CONCAT(T3.NombreContratista, ' ', T3.ApPaterno, ' ', T3.ApMaterno)) AS NombreContratista
		FROM #Entradas E 
		FULL OUTER JOIN #Salidas S
            ON E.idContratistas = S.idContratistas
			AND CAST(E.FechaRegistro AS DATE) = CAST(S.FechaRegistro AS DATE)
        INNER JOIN Contratistas T3
            ON ISNULL(E.idContratistas, S.idContratistas) = T3.idContratistas
		WHERE (@pEmpresa IS NULL OR @vEmpresa LIKE CONCAT('%', UPPER(T3.NombreCompañia), '%'))
			  AND (@pNombreContratista IS NULL 
			     OR  UPPER(T3.NombreContratista) LIKE @vNombreContratista
				 OR  UPPER(T3.ApPaterno) LIKE @vNombreContratista
				 OR  UPPER(T3.ApMaterno) LIKE @vNombreContratista
				 OR  UPPER(CONCAT(T3.NombreContratista, ' ', T3.ApPaterno, ' ', T3.ApMaterno)) LIKE @vNombreContratista)
				 
		ORDER BY ISNULL(E.FechaRegistro, S.FechaRegistro);
		
		DROP TABLE #Entradas;
		DROP TABLE #Salidas;
		
	END TRY
	BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRAN;
			DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
			SET @ErrMsg = ERROR_MESSAGE()
			SET @ErrSev = ERROR_SEVERITY()
			SET @ErrSt = ERROR_STATE()
			RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
 END
