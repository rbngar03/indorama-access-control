IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_AltaContratista')
  BEGIN
	DROP PROCEDURE DTW_SP_AltaContratista
  END
GO

-- =============================================
-- Author:		Karla Ramirez
-- Create date: 14/11/24
-- Description:	Script para la carga de contratistas
-- History:
-- <19/12/2024> <k.ramirez> <Se aplican validaciones para eviatr duplicidad en RFC y No. IMSS>
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_AltaContratista]
	@pidTipoContratista INT,
	@pNombreContratista VARCHAR(150),
	@pApPaterno VARCHAR(50),
	@pApMaterno VARCHAR(50),
	@pFechaIndSeguridad VARCHAR(20),
	@pFechaContrato VARCHAR(20),
	@pIMSS VARCHAR(50),
	@pRFC VARCHAR(15),
	@pVigenciaIMSS VARCHAR(20),
	@pFechaTerminacion VARCHAR(20),
	@pResponsableIndorama VARCHAR(150),
	@pidClasificacion INT,
	@pNombreCompañia VARCHAR(50),
	@pNumeroContrato VARCHAR(10),
	@pidCcostos INT,
	@Response VARCHAR(100) OUTPUT

AS
BEGIN
	DECLARE @IdContratistas INT;
	DECLARE @IdCompania INT;


	BEGIN TRY 
		BEGIN TRAN
		SELECT @IdCompania = idCompanias FROM Compania WHERE UPPER(Descripcion) = UPPER(@pNombreCompañia)
		
		IF @IdCompania IS NULL
		BEGIN
			INSERT INTO Compania (Descripcion, fecha_actualizacion)
			VALUES (UPPER(@pNombreCompañia), GETDATE())

			SET @IdCompania = SCOPE_IDENTITY()
		END

		SELECT @IdContratistas = idContratistas FROM Contratistas WHERE RFC = @pRFC AND IMSS = @pIMSS;

		IF @IdContratistas IS NOT NULL
		BEGIN
			UPDATE [dbo].[Contratistas]
			   SET [idTipoContratista] = @pidTipoContratista,
				   [idEstatus] = 1,
				   [NombreContratista] = UPPER(@pNombreContratista),
				   [ApPaterno] = UPPER(@pApPaterno),
				   [ApMaterno] = ISNULL(UPPER(@pApMaterno),NULL),
				   [FechaIndSeguridad] =  CONVERT(DATETIME, @pFechaIndSeguridad, 120),
				   [FechaContrato] = CONVERT(DATETIME, @pFechaContrato, 120),
				   [IMSS] = @pIMSS,
				   [VigenciaIMSS] = CONVERT(DATETIME, @pVigenciaIMSS, 120),
				   [NumeroContrato] = @pNumeroContrato,
				   [NombreCompañia] = UPPER(@pNombreCompañia),
				   [idCompanias] = @IdCompania,
				   [RFC] = @pRFC,
				   [FechaTerminacion] = CONVERT(DATETIME, @pFechaTerminacion, 120),
				   [idClasificacion] = @pidClasificacion,
				   [idCcostos] = @pidCcostos,
				   [ResponsableIndorama] = @pResponsableIndorama,
				   [fecha_actualizacion] = GETDATE()
				WHERE idContratistas = @IdContratistas

				SET @Response = 'Ok'; 
		END
		ELSE
		BEGIN
		-- Verificar si ya existe un RFC duplicado
			IF EXISTS (SELECT 1 FROM Contratistas WHERE RFC = @pRFC)
			BEGIN
				SET @Response = 'El RFC ya existe para otro Contratista, favor de revisar ' + 'RFC:'+@pRFC;
				ROLLBACK TRAN;
				RETURN;
			END

			-- Verificar si ya existe un IMSS duplicado
			IF EXISTS (SELECT 1 FROM Contratistas WHERE IMSS = @pIMSS)
			BEGIN
				SET @Response = 'El No. IMSS ya existe para otro Contratista, favor de revisar ' + 'IMSS:'+@pIMSS;
				ROLLBACK TRAN;
				RETURN;
			END
			INSERT INTO [dbo].[Contratistas] (
				[idTipoContratista],
				[NombreContratista],
				[ApPaterno],
				[ApMaterno],
				[FechaIndSeguridad],
				[FechaContrato],
				[FechaTerminacion],
				[IMSS],
				[VigenciaSeguridad],
				[VigenciaIMSS],
				[NumeroContrato],
				[PeriodoVigencia],
				[idCompanias],
				[NombreCompañia],
				[RFC],
				[idClasificacion],
				[ResponsableIndorama],
				[idCcostos],
				[idComedor],
				[idEstatus],
				[fecha_actualizacion])
			VALUES(
				@pidTipoContratista,
				UPPER(@pNombreContratista),
				UPPER(@pApPaterno),
				UPPER(@pApMaterno),
				CONVERT(DATETIME, @pFechaIndSeguridad, 120),
				CONVERT(DATETIME, @pFechaContrato, 120),
				CONVERT(DATETIME, @pFechaTerminacion, 120),
				@pIMSS,
				CONVERT(DATETIME, @pFechaTerminacion, 120),
				CONVERT(DATETIME, @pVigenciaIMSS, 120),
				@pNumeroContrato,
				CONVERT(DATETIME, @pVigenciaIMSS, 120),
				@IdCompania,
				UPPER(@pNombreCompañia),
				@pRFC,
				@pidClasificacion,
				@pResponsableIndorama,
				@pidCcostos,
				2,
				1,
				GETDATE());

				SET @Response = 'Ok';
		END
		COMMIT TRAN;
	END TRY
    BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRAN;
			DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
			SET @ErrMsg = ERROR_MESSAGE()
			SET @ErrSev = ERROR_SEVERITY()
			SET @ErrSt = ERROR_STATE()
			RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	 END CATCH
 END
