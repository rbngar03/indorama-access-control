IF EXISTS (	SELECT 1
			FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_ReporteMarcajesComedorGeneral' )
  BEGIN
	DROP PROCEDURE DTW_SP_ReporteMarcajesComedorGeneral
  END
GO
-- =============================================
-- Author:		Alejandra Gpe. Marín Ascencio
-- Create date: 09/10/24
-- Description:	Script que obtiene los marcajes del Comedor
-- History:
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_ReporteMarcajesComedorGeneral]
@pFechaInicial VARCHAR (20),
@pFechaFinal   VARCHAR (20),
@pTipoPersonal INT = NULL,
@pPlanta 	   INT = NULL
AS
BEGIN
	DECLARE @vFechaInicial DATETIME,
			@vFechaFinal   DATETIME
			
	BEGIN TRY 
		SET @vFechaInicial = CONVERT(DATETIME, @pFechaInicial, 103)
		SET @vFechaFinal   = CONVERT(DATETIME, @pFechaFinal, 103)

		SELECT T1.FechaRegistro  AS FechaEntrada,
			   ISNULL(UPPER(T1.NombrePersonal), '') AS Nombre,
			   ISNULL(T6.Descripcion, '') AS Planta,
			   CASE
				  WHEN T1.idEmpleado IS NOT NULL THEN ''
				  WHEN T1.idContratistas IS NOT NULL THEN UPPER(T4.NombreCompañia)
				  WHEN T1.idVisitantes IS NOT NULL THEN UPPER(T5.DescOrigen)
				  ELSE 'DESCONOCIDO'
			   END AS Compania
		FROM Marcajes T1
		LEFT JOIN Empleado T3
			ON T1.idEmpleado = T3.idEmpleado
		LEFT JOIN Contratistas T4
			ON T1.idContratistas = T4.idContratistas
		LEFT JOIN Visitantes T5
			ON T1.idVisitantes = T5.idVisitantes
		LEFT JOIN Planta T6
			ON T3.idPlanta = T6.idPlanta
			OR T4.idPlanta = T6.idPlanta
			OR T5.idPlanta = T6.idPlanta
		WHERE T1.EntradaSalida = 'C'
			AND (CAST(T1.FechaRegistro AS DATE) >= @vFechaInicial AND CAST(T1.FechaRegistro AS DATE) <= @vFechaFinal)
			AND (@pPlanta 		IS NULL OR (T6.idPlanta = @pPlanta))
			AND (@pTipoPersonal IS NULL OR (@pTipoPersonal = 2 AND (T1.idTipoPersonal = 2 OR T1.idTipoPersonal = 4))
										OR (@pTipoPersonal <> 2 AND T1.idTipoPersonal = @pTipoPersonal))
		ORDER BY T1.FechaRegistro
		
	END TRY
	BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRAN;
			DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
			SET @ErrMsg = ERROR_MESSAGE()
			SET @ErrSev = ERROR_SEVERITY()
			SET @ErrSt = ERROR_STATE()
			RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
 END