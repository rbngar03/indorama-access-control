﻿using DW_INDORAMA_API.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace DW_INDORAMA_API.Interfaces
{
    public interface IBiostarService
    {
        Task LoginAsync(string username, string password);
        Task LogoutAsync();
        Task UserCredential();
        Task<CardDetail> ViewCardDetail(string query);
        Task UserCredentialAdd(int idUsuario, int idCredential);

    }
}
