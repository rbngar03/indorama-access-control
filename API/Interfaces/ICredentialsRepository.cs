﻿namespace DW_INDORAMA_API.Interfaces;
using DW_INDORAMA_API.Dtos;

    public interface ICredentialsRepository
    {
        GenericHttpResponse<CredencialResponse> GetEmployeeCredential(string numEmpleado, string message = "");
        GenericHttpResponse<CredencialResponse> GetContractorCredential(string rfc, string message = "");
        GenericHttpResponse<CredencialResponse> GetVisitorCredential(string rfc, string message = "");
        Task<string> UpdateEmployeeCredential(CredentialRequest credential);
        Task<string> UpdateContractorCredential(CredentialRequest credential);
        Task<string> UpdateVisitorCredential(CredentialRequest credential);
        #region EndpointsBiostar
        Task<GenericHttpResponse<CredentialDto>> PostCredential(CredentialDto credential);
        Task GetCredentialBiostar(CredentialRequest credential);
        Task BiostarEndpoints();
        #endregion
    }

