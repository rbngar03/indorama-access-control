﻿using DW_INDORAMA_API.Data.IndoramaModels;
using DW_INDORAMA_API.Dtos;

namespace DW_INDORAMA_API.Interfaces
{
    public interface IPermisosRepository
    {
        PerfilesModuloAddDeletedto GetPermisos(int id);
        IResult Update(PermisosRequest permisos);
        Perfile GetPerfilById(int id);
        IResult DeletePerfilById(int id);
        Task<GenericHttpResponse<PerfilDto>> AddPerfil(PerfilDto perfil);
    }
}
