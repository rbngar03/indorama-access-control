﻿namespace DW_INDORAMA_API.Dtos
{
    public class ReporteContratistaDto
    {
        public string Entrada { get; set; }
        public string Salida { get; set; }
        public string EmpresaContratista { get; set; }
        public string NombreContratista { get; set; }
    }
}
