﻿namespace DW_INDORAMA_API.Dtos
{
    public class ReporteComidasTurnoDto
    {
        public string FechaRegistro { get; set; }
        public string Descripcion { get; set; }
        public int Turno { get; set; }
        public int TotalRegistros { get; set; }
    }
}
