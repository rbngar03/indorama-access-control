﻿using DW_INDORAMA_API.Data.IndoramaModels;

namespace DW_INDORAMA_API.Dtos
{
    public class ModuloAcciones
    {
        public int IdModulo { get; set; }
        public string Nombre { get; set; }
        public List<AccionDto> Acciones { get; set; }
        public List<CamposEditablesDto> CamposEditables { get; set; }
    }
}
