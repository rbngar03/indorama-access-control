﻿namespace DW_INDORAMA_API.Dtos
{
    public class PerfilesModuloAddDeletedto
    {
        public int idPerfil { get; set; }
        public List<ModuloAcciones> moduloAccion { get; set; }

    }
}
