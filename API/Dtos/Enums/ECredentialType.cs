﻿namespace DW_INDORAMA_API.Dtos.Enums
{
    public enum ECredentialType
    {
        Permanent    = 0,
        Provisional  = 1,
        ShortTerm    = 2,
        longerTerm   = 3,
        Visitor      = 4,
    }
}
