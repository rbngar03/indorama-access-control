﻿namespace DW_INDORAMA_API.Dtos.Enums
{
    public enum EModuleType
    {
        Submenu = 0,
        Catalogues = 1,
        Credentials = 2,
        Reports = 3,
    }
}
