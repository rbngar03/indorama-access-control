﻿namespace DW_INDORAMA_API.Dtos
{
    public class ReporteEmpleadoDto
    {
        public DateTime Entrada { get; set; }
        public DateTime Salida { get; set; }
        public string NombreEmpleado { get; set; }
        public string NumeroEmpleado { get; set; }
        public string Planta { get; set; }
    }
}
