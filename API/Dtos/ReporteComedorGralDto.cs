﻿namespace DW_INDORAMA_API.Dtos
{
    public class ReporteComedorGralDto
    {
        public string FechaEntrada { get; set; }
        public string Nombre { get; set;}
        public string Planta { get; set; }
        public string Compania { get; set; }
    }
}
