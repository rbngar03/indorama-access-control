﻿namespace DW_INDORAMA_API.Dtos
{
    public class PerfilDto
    {
        public int IdPerfil { get; set; }

        public string Clave { get; set; } = "";

        public string Descripcion { get; set; } = "";

    }
}
