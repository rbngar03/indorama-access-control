﻿namespace DW_INDORAMA_API.Dtos
{
    public class AccionDto
    {
        public int IdAccion { get; set; }
        public string Nombre { get; set; } = null!;
        public bool IsActivated { get; set; }
    }
}
