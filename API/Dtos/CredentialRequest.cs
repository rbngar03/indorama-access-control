﻿using DW_INDORAMA_API.Dtos.Enums;

namespace DW_INDORAMA_API.Dtos
{
    public class CredentialRequest
    {
        public string identificador { get; set; }
        public string numeroGafete { get; set; }
        public bool isCredentialHabilited { get; set; }
        public Nullable<DateTime> fechaVigencia { get; set; }
        public ECredentialType credentialType { get; set; }
    }
}
