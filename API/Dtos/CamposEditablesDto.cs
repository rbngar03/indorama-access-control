﻿namespace DW_INDORAMA_API.Dtos
{
    public class CamposEditablesDto
    {
        public int Id { get; set; }
        public string NombreCampo { get; set; } = null!;
        public bool IsActivated { get; set; }
    }
}
