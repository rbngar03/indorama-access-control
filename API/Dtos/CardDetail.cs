﻿using Azure;
using System.Text.Json.Serialization;

namespace DW_INDORAMA_API.Dtos
{
    public class CardDetail
    {
        public CardCollection CardCollection { get; set; }

        public Response Response { get; set; }

    }

    public partial class CardCollection
    {
        public Row[] Rows { get; set; }

        public long Total { get; set; }
    }

    public partial class Row
    {
        public long Id { get; set; }

        public long CardId { get; set; }

        public long DisplayCardId { get; set; }

        
        
        public long Status { get; set; }

        
        
        public bool IsBlocked { get; set; }

        
        
        public bool IsAssigned { get; set; }

        
        public CardType CardType { get; set; }

        
        
        public bool MobileCard { get; set; }

        
        
        public long IssueCount { get; set; }

        
        
        public long CardSlot { get; set; }

        
        
        public long CardMask { get; set; }

        
        public WiegandFormatId WiegandFormatId { get; set; }
    }

    public partial class CardType
    {
        
        
        public long Id { get; set; }

        
        public string Name { get; set; }

        
        
        public long Type { get; set; }
    }

    public partial class WiegandFormatId
    {
        
        
        public long Id { get; set; }
    }

    public partial class Response
    {
        
        
        public long Code { get; set; }

        
        public Uri Link { get; set; }

        
        public string Message { get; set; }
    }

}
