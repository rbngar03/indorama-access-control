﻿namespace DW_INDORAMA_API.Dtos
{
    public class PermisosRequest
    {
        public List<ModuloAcciones> moduloAccion { get; set; }
        public int idPerfil { get; set; }
    }
}
