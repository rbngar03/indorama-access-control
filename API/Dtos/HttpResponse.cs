﻿namespace DW_INDORAMA_API.Dtos
{
    public class GenericHttpResponse<T> where T : class
    {
        public T values { get; set; }
        public string message { get; set; }
    }
}
