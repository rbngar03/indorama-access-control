﻿namespace DW_INDORAMA_API.Dtos
{
    public class CredencialResponse
    {
        public string identificador { get; set; }
        public bool isUserActive { get; set; }
        public string numeroGafete { get; set; }
        public string? NumeroProvisionalGafete { get; set; }
        public bool isCredentialHabilited { get; set; }
        public bool isCredentialProvisionalHabilited { get; set; }
        public DateTime? fechaVigencia { get; set; }
        public string NombreCompañia { get; set; } = null!;
        public string Nombre { get; set; }  
        public string apellidos { get; set; } = null!;
        public byte[]? Foto { get; set; }
        public int IdTipoContratista { get; set; }
        public DateTime FechaContrato { get; set; }
        public DateTime FechaTerminacion { get; set; }
    }
}
