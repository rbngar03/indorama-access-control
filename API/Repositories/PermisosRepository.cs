﻿using AutoMapper;
using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using DW_INDORAMA_API.Dtos;
using DW_INDORAMA_API.Dtos.Enums;
using DW_INDORAMA_API.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DW_INDORAMA_API.Repositories
{
    public class PermisosRepository : IPermisosRepository
    {
        private readonly IndoramaAccessControlContext _context;
        private readonly IMapper _mapper;
        public PermisosRepository(IndoramaAccessControlContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        public IResult Update(PermisosRequest request)
        {
            GenericHttpResponse<PerfilesModuloAddDeletedto> response = new GenericHttpResponse<PerfilesModuloAddDeletedto>();
            try
            {
                List<PerfilesModulo> currentypermisos = _context.PerfilesModulos.Where(x => x.IdPerfil.Equals(request.idPerfil)).ToList();

                foreach (var modulo in request.moduloAccion)
                {
                    foreach (var accion in modulo.Acciones)
                    {
                        var search = currentypermisos.Find(x => x.IdModulo == modulo.IdModulo && x.IdAccion == accion.IdAccion);
                        if (!accion.IsActivated && search != null)
                            _context.PerfilesModulos.
                            Where(x =>
                            x.IdModulo == modulo.IdModulo &&
                            x.IdAccion == accion.IdAccion &&
                            x.IdPerfil == request.idPerfil).
                            ExecuteDelete();
                        else if (accion.IsActivated && search == null)
                            _context.PerfilesModulos.Add(new PerfilesModulo
                            {
                                IdAccion = accion.IdAccion,
                                IdModulo = modulo.IdModulo,
                                IdPerfil = request.idPerfil
                            });
                    }
                    foreach (var campo in modulo.CamposEditables)
                    {
                        _context.CamposPerfils.Where(x => x.Id == campo.Id).ExecuteUpdate(y => y.SetProperty(z => z.IsActivated, campo.IsActivated));
                    }
                }
                _context.SaveChanges();
                response.message = "Permisos actualizados";
                return Results.Ok(response);
            }
            catch (Exception ex)
            {
                return Results.BadRequest(ex.Message);
            }
        }

        public List<Accione> GetAcciones()
        {
            return _context.Acciones.ToList();
        }

        public List<Modulo> GetModulos()
        {
            return _context.Modulos.Where(x => x.IdPadre != null).ToList();
        }

        public Perfile GetPerfilById(int id)
        {
            return _context.Perfiles.FirstOrDefault(x => x.IdPerfil == id);
        }

        public PerfilesModuloAddDeletedto GetPermisos(int id)
        {
            List<AccionDto> accionesDto = new List<AccionDto>();
            List<ModuloAcciones> moduloAcc = new List<ModuloAcciones>();
            List<Modulo> modulos = GetModulos();
            List<Accione> acciones = GetAcciones();
            List<PerfilesModulo> permisosPerfil = _context.PerfilesModulos.Where(x => x.IdPerfil == id).ToList();
            List<CamposPerfil> camposPerfils = _context.CamposPerfils.Where(x => x.PerfilId == id).ToList();
            foreach (var modulo in modulos)
            {
                if (modulo.Orden.Value == ((int)EModuleType.Catalogues))
                    accionesDto = _mapper.Map<List<Accione>, List<AccionDto>>(acciones);
                if (modulo.Orden.Value == ((int)EModuleType.Reports))
                    accionesDto = _mapper.Map<List<Accione>, List<AccionDto>>(acciones.FindAll(x =>
                    x.Nombre.ToUpper().Equals("VER") ||
                    x.Nombre.ToUpper().Equals("EXPORTAR") ||
                    x.Nombre.ToUpper().Equals("FILTRAR"))
                    .ToList());
                if (modulo.Orden.Value == ((int)EModuleType.Credentials))
                    accionesDto = _mapper.Map<List<Accione>, List<AccionDto>>(acciones.Where(x =>
                    x.Nombre.ToUpper().Equals("VER") ||
                    x.Nombre.ToUpper().Equals("EDITAR"))
                    .ToList());
                foreach (var accion in accionesDto)
                {
                    var search = permisosPerfil.Where(x => x.IdModulo == modulo.IdModulo && x.IdAccion == accion.IdAccion);
                    accion.IsActivated = search.Count() != 0;
                }
                var camposEditables = (bool)modulo.HasEditableFields ? _mapper.Map<List<CamposPerfil>, List<CamposEditablesDto>>(camposPerfils.FindAll(x => x.ModuloId == modulo.IdModulo)) : [];
                moduloAcc.Add(new ModuloAcciones
                {
                    IdModulo = modulo.IdModulo,
                    Nombre = modulo.Nombre,
                    Acciones = accionesDto,
                    CamposEditables = camposEditables
                });

            }
            return new PerfilesModuloAddDeletedto { idPerfil = id, moduloAccion = moduloAcc };
        }

        public async Task<GenericHttpResponse<PerfilDto>> AddPerfil(PerfilDto perfilDto)
        {
            var perfil =  _mapper.Map<PerfilDto, Perfile>(perfilDto);
            try
            {
                _context.Perfiles.Add(perfil);
                await _context.SaveChangesAsync();
                return new GenericHttpResponse<PerfilDto> { message = "Perfil añadido", values = _mapper.Map<Perfile, PerfilDto>(perfil) };
            }
            catch (Exception ex)
            {
                return new GenericHttpResponse<PerfilDto> { message = ex.Message, values = null }; 
            }
        }

        public IResult DeletePerfilById(int id)
        {
            GenericHttpResponse<PerfilDto> response = new GenericHttpResponse<PerfilDto>();
            try
            {
                var usuarios = _context.Usuarios.Where(x => x.IdPerfil == id).ToList();
                if (usuarios.Count() == 0)
                {
                    var modulos = _context.PerfilesModulos.Where(x => x.IdPerfil == id).ToList();
                    if (modulos.Count() > 0)
                        return Results.BadRequest("Se necesitan retirar lo permisos del perfil antes de continuar");
                    _context.Perfiles.Where(x => x.IdPerfil == id).ExecuteDelete();
                    response.message = "Perfil eliminado";
                    return Results.Ok(response);
                }
                return Results.BadRequest("Existen usuarios con relación a este perfil");
            }
            catch (Exception ex)
            {
                return Results.Problem(ex.Message);
            }

        }
    }
}
