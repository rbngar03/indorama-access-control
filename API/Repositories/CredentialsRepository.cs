﻿using AutoMapper;
using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using DW_INDORAMA_API.Dtos;
using DW_INDORAMA_API.Dtos.Enums;
using DW_INDORAMA_API.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client.Platforms.Features.DesktopOs.Kerberos;
using System.Linq.Expressions;
using System.Net;   

namespace DW_INDORAMA_API.Repositories
{
    public class CredentialsRepository : ICredentialsRepository
    {
        private readonly IMapper _mapper;
        private readonly IndoramaAccessControlContext _context;
        private readonly IBiostarService _biostarService;
        public CredentialsRepository(IndoramaAccessControlContext context, IMapper mapper, IBiostarService biostarService)
        {
            _context = context;
            _mapper = mapper;
            _biostarService = biostarService;
        }

        private bool isCredentialAvailable(Gafete gafete, UserType type, string identifier)
        {
            Expression<Func<Empleado, bool>> employeeSearch = type == UserType.Employee ?
                x => (x.IdGafete == gafete.IdGafete || x.NumeroProvisionalGafete == gafete.IdGafete) && x.NumEmpleado != identifier :
                x => x.IdGafete == gafete.IdGafete || x.NumeroProvisionalGafete == gafete.IdGafete;
            Expression<Func<Contratista, bool>> contractorSearch = type == UserType.Contractor ?
                x => (x.NumeroProvisionalGafete == gafete.IdGafete || x.IdGafete == gafete.IdGafete) && x.Rfc != identifier :
                x => x.NumeroProvisionalGafete == gafete.IdGafete || x.IdGafete == gafete.IdGafete;
            Expression<Func<Visitante, bool>> visitorSearch = type == UserType.Visitor ?
                x => x.IdGafete == gafete.IdGafete && x.Rfc != identifier :
                x => x.IdGafete == gafete.IdGafete;
            Empleado employeeCredentialResult = FindUserCredentialAsigned(employeeSearch);
            Contratista contractorCredentialResult = FindUserCredentialAsigned(contractorSearch);
            Visitante visitorCredentialResult = FindUserCredentialAsigned(visitorSearch);
            return employeeCredentialResult == null && contractorCredentialResult == null && visitorCredentialResult == null;
        }
        
        private GenericHttpResponse<CredencialResponse> GetUserCredential<T>(Expression<Func<T, bool>> filter, string message = "", params string[] includes) where T : class
        {
            GenericHttpResponse<CredencialResponse> response = new GenericHttpResponse<CredencialResponse>();
            IQueryable<T> query = _context.Set<T>();
            query = query.Where(filter);
            if (includes.Length > 0)
            {
                foreach (string include in includes)
                {
                    query = query.Include(include);
                }
            }
            T entity = query.FirstOrDefault();
            var credencial = entity != null
                ? _mapper.Map<T, CredencialResponse>(entity)
                : default;
            response.values = credencial;
            response.message = credencial == null ? "El registro no existe" : message;
            return response;
        }

        public GenericHttpResponse<CredencialResponse> GetEmployeeCredential(string numEmpleado, string message = "Registro encontrado")
        {
            return GetUserCredential<Empleado>(filter: (x => x.NumEmpleado == numEmpleado), message: message, "IdGafeteNavigation", "IdEstatusNavigation", "NumeroProvisionalGafeteNavigation");
        }

        public GenericHttpResponse<CredencialResponse> GetContractorCredential(string rfc, string message = "Registro encontrado")
        {
            return GetUserCredential<Contratista>(filter: (x => x.Rfc == rfc), message: message, "IdGafeteNavigation", "IdEstatusNavigation", "NumeroProvisionalGafeteNavigation");
        }
        
        public GenericHttpResponse<CredencialResponse> GetVisitorCredential(string rfc, string message = "Registro encontrado")
        {
            var response = GetUserCredential<Visitante>(filter: (x => x.Rfc == rfc), message: message, "IdGafeteNavigation", "IdEstatusNavigation");
            if (response.values != null)
            {
                Gafete gafete = _context.Gafetes.FirstOrDefault(x => x.Gafete1 == response.values.numeroGafete);
                if (gafete != null)
                {
                    Estatus estatus = _context.Estatuses.FirstOrDefault(x => x.IdEstatus == gafete.IdEstatus);
                    response.values.isCredentialHabilited = estatus.Estatus1.ToUpper() == "ACTIVO";
                }
            }
            return response;
        }

        private T FindUserCredentialAsigned<T>(Expression<Func<T, bool>> filter) where T : class
        {
            IQueryable<T> query = _context.Set<T>();
            query = query.Where(filter);
            T entity = query.FirstOrDefault();
            return entity;
        }

        private async Task<string> UpdateGafete(CredentialRequest credential)
        {
            await _context.Gafetes.Where(x => x.Gafete1 == credential.numeroGafete.ToString()).ExecuteUpdateAsync(y => y.SetProperty(z => z.IdEstatus, credential.isCredentialHabilited ? 1 : 2));
            if (credential.credentialType != ECredentialType.Permanent)
                await _context.Gafetes.Where(x => x.Gafete1 == credential.numeroGafete.ToString()).ExecuteUpdateAsync(y => y.SetProperty(z => z.FechaVigencia, credential.fechaVigencia));
            return "Registro actualizado correctamente";
        }

        public async Task<string> UpdateEmployeeCredential(CredentialRequest credential)
        {
            Gafete gafete = _context.Gafetes.FirstOrDefault(x => x.Gafete1 == credential.numeroGafete);
            Empleado empleado = _context.Empleados.FirstOrDefault(x => x.NumEmpleado == credential.identificador);
            if (gafete == null)
                return "El numero de gafete no existe";
            bool credentialAvailable = isCredentialAvailable(gafete, UserType.Employee, credential.identificador);
            if (credentialAvailable)
            {
                if (credential.credentialType == ECredentialType.Permanent)
                {
                    if (empleado.NumeroProvisionalGafete != null && empleado.NumeroProvisionalGafete.ToString() != credential.numeroGafete)
                        return "Este número de gafete ya ha sido asignado";
                    else
                        await _context.Empleados.Where(x => x.NumEmpleado == credential.identificador).ExecuteUpdateAsync(y => y.SetProperty(z => z.IdGafete, gafete.IdGafete));
                }
                else
                {
                    if (empleado.IdGafete != null && empleado.IdGafete.ToString() != credential.numeroGafete)
                        return "Este número de gafete ya ha sido asignado";
                    else
                        await _context.Empleados.Where(x => x.NumEmpleado == credential.identificador).ExecuteUpdateAsync(y => y.SetProperty(z => z.NumeroProvisionalGafete, gafete.IdGafete));
                }
                return await UpdateGafete(credential);
            }
            else
                return "Este número de gafete ya ha sido asignado";
        }

        public async Task<string> UpdateContractorCredential(CredentialRequest credential)
        {
            Gafete gafete = _context.Gafetes.FirstOrDefault(x => x.Gafete1 == credential.numeroGafete);
            Contratista contratista = _context.Contratistas.FirstOrDefault(x => x.Rfc == credential.identificador);
            if (gafete == null)
                return "El numero de gafete no existe";
            bool credentialAvailable = isCredentialAvailable(gafete, UserType.Contractor, credential.identificador);
            if (credentialAvailable)
            {
                if (credential.credentialType == ECredentialType.Provisional)
                {
                    if (contratista.IdGafete != null && contratista.IdGafete == gafete.IdGafete)
                        return "Este número de gafete ya ha sido asignado";
                    else
                        await _context.Contratistas.Where(x => x.Rfc == credential.identificador).ExecuteUpdateAsync(y => y.SetProperty(z => z.NumeroProvisionalGafete, gafete.IdGafete));
                }
                else
                {
                    if (contratista.NumeroProvisionalGafete != null && contratista.NumeroProvisionalGafete == gafete.IdGafete)
                        return "Este número de gafete ya ha sido asignado";
                    else
                        await _context.Contratistas.Where(x => x.Rfc == credential.identificador).ExecuteUpdateAsync(y => y.SetProperty(z => z.IdGafete, gafete.IdGafete));
                }
                if (credential.credentialType == ECredentialType.longerTerm || credential.credentialType == ECredentialType.ShortTerm)
                {
                    Contratista contractor = _context.Contratistas.FirstOrDefault(x => x.Rfc == credential.identificador);
                    credential.fechaVigencia = contractor.FechaTerminacion;
                }
                return await UpdateGafete(credential);
            }
            else
                return "Este número de gafete ya ha sido asignado";
        }

        public async Task<string> UpdateVisitorCredential(CredentialRequest credential)
        {
            Visitante visitante = _context.Visitantes.FirstOrDefault(x => x.Rfc == credential.identificador);
            Gafete gafete = _context.Gafetes.FirstOrDefault(x => x.Gafete1 == credential.numeroGafete);
            if (gafete == null)
                return "El numero de gafete no existe";
            bool credentialAvailable = isCredentialAvailable(gafete, UserType.Visitor, credential.identificador);
            if (credentialAvailable)
            {
                if (visitante.IdGafete != null && visitante.IdGafete == gafete.IdGafete)
                    return "Este número de gafete ya ha sido asignado";
                else
                    await _context.Visitantes.Where(x => x.Rfc == credential.identificador).ExecuteUpdateAsync(y => y.SetProperty(z => z.IdGafete, gafete.IdGafete));
                return await UpdateGafete(credential);
            }
            else
                return "Este número de gafete ya ha sido asignado";
        }

        #region EndpointsBiostar
        public Task GetCredentialBiostar(CredentialRequest credential)
        {
            //await _biostarService.LoginAsync("rh", "Elisa1357");
            //await _biostarService.LogoutAsync();
            throw new NotImplementedException();
        }

        public async Task BiostarEndpoints()
        {
            await _biostarService.LoginAsync("rh", "Elisa1357");
            await _biostarService.ViewCardDetail("22221");
            await _biostarService.LogoutAsync();
        }

        public Task<GenericHttpResponse<CredentialDto>> PostCredential(CredentialDto credential)
        {
            throw new NotImplementedException();
        }

        #endregion

        private enum UserType
        {
            Employee = 0,
            Contractor = 1,
            Visitor = 2
        }

    }

}
