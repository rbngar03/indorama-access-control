﻿using System;
using System.Collections.Generic;
using DW_INDORAMA_API.Data.IndoramaModels;
using Microsoft.EntityFrameworkCore;

namespace DW_INDORAMA_API.Data;

public partial class IndoramaAccessControlContext : DbContext
{
    public IndoramaAccessControlContext()
    {
    }

    public IndoramaAccessControlContext(DbContextOptions<IndoramaAccessControlContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Accione> Acciones { get; set; }

    public virtual DbSet<CamposPerfil> CamposPerfils { get; set; }

    public virtual DbSet<Ccosto> Ccostos { get; set; }

    public virtual DbSet<Ccqro> Ccqros { get; set; }

    public virtual DbSet<Clasificacion> Clasificacions { get; set; }

    public virtual DbSet<Comedor> Comedors { get; set; }

    public virtual DbSet<Comedor2018> Comedor2018s { get; set; }

    public virtual DbSet<Comidas2017> Comidas2017s { get; set; }

    public virtual DbSet<ComidasConfIvpm> ComidasConfIvpms { get; set; }

    public virtual DbSet<ComidasPf> ComidasPfs { get; set; }

    public virtual DbSet<ComidasPfhoy> ComidasPfhoys { get; set; }

    public virtual DbSet<ComidasQueretaro> ComidasQueretaros { get; set; }

    public virtual DbSet<Companium> Compania { get; set; }

    public virtual DbSet<Conteocomida> Conteocomidas { get; set; }

    public virtual DbSet<Contratista> Contratistas { get; set; }

    public virtual DbSet<Contrato> Contratos { get; set; }

    public virtual DbSet<CopiaMarcajesEslabon> CopiaMarcajesEslabons { get; set; }

    public virtual DbSet<CopiaMarcajesEslabonOrg> CopiaMarcajesEslabonOrgs { get; set; }

    public virtual DbSet<DtwCaLogUser> DtwCaLogUsers { get; set; }

    public virtual DbSet<DtwCaParametro> DtwCaParametros { get; set; }

    public virtual DbSet<EmpCsv> EmpCsvs { get; set; }

    public virtual DbSet<EmplCc> EmplCcs { get; set; }

    public virtual DbSet<Empleado> Empleados { get; set; }

    public virtual DbSet<EstadoGafeteDeAcceso> EstadoGafeteDeAccesos { get; set; }

    public virtual DbSet<Estatus> Estatuses { get; set; }

    public virtual DbSet<Gafete> Gafetes { get; set; }

    public virtual DbSet<It> Its { get; set; }

    public virtual DbSet<LogError> LogErrors { get; set; }

    public virtual DbSet<Marcaje> Marcajes { get; set; }

    public virtual DbSet<MarcajesASubir> MarcajesASubirs { get; set; }

    public virtual DbSet<MarcajesContatista> MarcajesContatistas { get; set; }

    public virtual DbSet<MarcajesContratIt> MarcajesContratIts { get; set; }

    public virtual DbSet<MarcajesEmplStafe> MarcajesEmplStaves { get; set; }

    public virtual DbSet<MarcajesEmpleadosQro> MarcajesEmpleadosQros { get; set; }

    public virtual DbSet<MarcajesInsertarPf> MarcajesInsertarPfs { get; set; }

    public virtual DbSet<MarcajesIt> MarcajesIts { get; set; }

    public virtual DbSet<MarcajesLogistica> MarcajesLogisticas { get; set; }

    public virtual DbSet<MarcajesPf> MarcajesPfs { get; set; }

    public virtual DbSet<MarcajesPfIt> MarcajesPfIts { get; set; }

    public virtual DbSet<MarcajesSubido> MarcajesSubidos { get; set; }

    public virtual DbSet<Marcajesborrado> Marcajesborrados { get; set; }

    public virtual DbSet<Modulo> Modulos { get; set; }

    public virtual DbSet<Origen> Origens { get; set; }

    public virtual DbSet<Perfile> Perfiles { get; set; }

    public virtual DbSet<PerfilesModulo> PerfilesModulos { get; set; }

    public virtual DbSet<Personal> Personals { get; set; }

    public virtual DbSet<Plantum> Planta { get; set; }

    public virtual DbSet<Terminale> Terminales { get; set; }

    public virtual DbSet<TipoContratistum> TipoContratista { get; set; }

    public virtual DbSet<TipoGafete> TipoGafetes { get; set; }

    public virtual DbSet<TipoLector> TipoLectors { get; set; }

    public virtual DbSet<TipoNomina> TipoNominas { get; set; }

    public virtual DbSet<Usuario> Usuarios { get; set; }

    public virtual DbSet<VComida> VComidas { get; set; }

    public virtual DbSet<VComidasC> VComidasCs { get; set; }

    public virtual DbSet<VEmpleado> VEmpleados { get; set; }

    public virtual DbSet<VRptEh> VRptEhs { get; set; }

    public virtual DbSet<View1> View1s { get; set; }

    public virtual DbSet<ViewMarcajesComedor> ViewMarcajesComedors { get; set; }

    public virtual DbSet<ViewMarcajesComedorSf> ViewMarcajesComedorSfs { get; set; }

    public virtual DbSet<Visitante> Visitantes { get; set; }

    public virtual DbSet<VistaEmpleadosIndo> VistaEmpleadosIndos { get; set; }

    public virtual DbSet<VwPermiso> VwPermisos { get; set; }

    public virtual DbSet<VwReporteNomina> VwReporteNominas { get; set; }

    public virtual DbSet<VwReporteNomina1> VwReporteNomina1s { get; set; }

      protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseSqlServer("Name=ConnectionStrings:IndoramaConnection");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.UseCollation("Modern_Spanish_CI_AS");

        modelBuilder.Entity<Accione>(entity =>
        {
            entity.HasKey(e => e.IdAccion);

            entity.Property(e => e.IdAccion).HasColumnName("id_accion");
            entity.Property(e => e.Nombre)
                .HasMaxLength(50)
                .HasColumnName("nombre");
        });

        modelBuilder.Entity<CamposPerfil>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__CamposPe__3214EC0787D1E90D");

            entity.ToTable("CamposPerfil");

            entity.Property(e => e.NombreCampo)
                .HasMaxLength(40)
                .IsUnicode(false);

            entity.HasOne(d => d.Modulo).WithMany(p => p.CamposPerfils)
                .HasForeignKey(d => d.ModuloId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Modulo_Id");

            entity.HasOne(d => d.Perfil).WithMany(p => p.CamposPerfils)
                .HasForeignKey(d => d.PerfilId)
                .HasConstraintName("FK_Perfil_Id");
        });

        modelBuilder.Entity<Ccosto>(entity =>
        {
            entity.HasKey(e => e.IdCcostos);

            entity.ToTable("ccostos");

            entity.Property(e => e.IdCcostos).HasColumnName("idCcostos");
            entity.Property(e => e.Ccostos).HasMaxLength(50);
            entity.Property(e => e.Descripcion).HasMaxLength(150);
            entity.Property(e => e.FechaActualizacion)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("fecha_actualizacion");
        });

        modelBuilder.Entity<Ccqro>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("Ccqro");

            entity.Property(e => e.Descripción).HasMaxLength(255);
            entity.Property(e => e.Responsable).HasMaxLength(255);
        });

        modelBuilder.Entity<Clasificacion>(entity =>
        {
            entity.HasKey(e => e.IdClasificacion);

            entity.ToTable("Clasificacion");

            entity.Property(e => e.IdClasificacion).HasColumnName("idClasificacion");
            entity.Property(e => e.Clasificacion1)
                .HasMaxLength(50)
                .HasColumnName("Clasificacion");
            entity.Property(e => e.Descripcion).HasMaxLength(50);
            entity.Property(e => e.FechaActualizacion)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("fecha_actualizacion");
        });

        modelBuilder.Entity<Comedor>(entity =>
        {
            entity.HasKey(e => e.IdComedor);

            entity.ToTable("Comedor");

            entity.Property(e => e.IdComedor).HasColumnName("idComedor");
            entity.Property(e => e.DerechoComedor).HasMaxLength(50);
        });

        modelBuilder.Entity<Comedor2018>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Comedor 2018");

            entity.Property(e => e.Costo).HasColumnType("numeric(4, 2)");
            entity.Property(e => e.Descripcion).HasMaxLength(50);
            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.Gafete).HasMaxLength(5);
            entity.Property(e => e.IdTerminales).HasColumnName("idTerminales");
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
        });

        modelBuilder.Entity<Comidas2017>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Comidas 2017");

            entity.Property(e => e.Descripcion).HasMaxLength(50);
            entity.Property(e => e.EntradaSalida)
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.NomEmp).HasMaxLength(152);
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
        });

        modelBuilder.Entity<ComidasConfIvpm>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Comidas_Conf_IVPM");

            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.IdCcostos).HasColumnName("idCcostos");
            entity.Property(e => e.IdEmpleado).HasColumnName("idEmpleado");
            entity.Property(e => e.IdGafete).HasColumnName("idGafete");
            entity.Property(e => e.Idtiponomina).HasColumnName("idtiponomina");
            entity.Property(e => e.NombrePersonal)
                .HasMaxLength(400)
                .IsUnicode(false);
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
            entity.Property(e => e.Personal).HasMaxLength(50);
        });

        modelBuilder.Entity<ComidasPf>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("ComidasPF");

            entity.Property(e => e.ApeMaterno).HasMaxLength(50);
            entity.Property(e => e.ApePaterno).HasMaxLength(50);
            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.Gafete).HasMaxLength(5);
            entity.Property(e => e.IdPlanta).HasColumnName("idPlanta");
            entity.Property(e => e.IdTerminales).HasColumnName("idTerminales");
            entity.Property(e => e.Nombre).HasMaxLength(50);
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
        });

        modelBuilder.Entity<ComidasPfhoy>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("ComidasPFHoy");

            entity.Property(e => e.Empleado)
                .HasMaxLength(7)
                .HasColumnName("empleado");
            entity.Property(e => e.FechaVisita)
                .HasColumnType("datetime")
                .HasColumnName("fecha_visita");
            entity.Property(e => e.Importe).HasColumnType("numeric(4, 2)");
        });

        modelBuilder.Entity<ComidasQueretaro>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("comidas queretaro");

            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.IdCcostos).HasColumnName("idCcostos");
            entity.Property(e => e.IdEmpleado).HasColumnName("idEmpleado");
            entity.Property(e => e.IdGafete).HasColumnName("idGafete");
            entity.Property(e => e.NombrePersonal)
                .HasMaxLength(400)
                .IsUnicode(false);
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
            entity.Property(e => e.Personal).HasMaxLength(50);
        });

        modelBuilder.Entity<Companium>(entity =>
        {
            entity.HasKey(e => e.IdCompanias).HasName("PK_Compañia");

            entity.Property(e => e.IdCompanias).HasColumnName("idCompanias");
            entity.Property(e => e.Compania).HasMaxLength(50);
            entity.Property(e => e.Descripcion).HasMaxLength(150);
            entity.Property(e => e.FechaActualizacion)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("fecha_actualizacion");
        });

        modelBuilder.Entity<Conteocomida>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("conteocomidas");

            entity.Property(e => e.Campo)
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasColumnName("campo");
            entity.Property(e => e.Fecha)
                .HasMaxLength(8000)
                .IsUnicode(false)
                .HasColumnName("fecha");
            entity.Property(e => e.Ficha)
                .HasMaxLength(7)
                .HasColumnName("ficha");
            entity.Property(e => e.Funcion)
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasColumnName("funcion");
            entity.Property(e => e.Hora)
                .HasMaxLength(8000)
                .IsUnicode(false)
                .HasColumnName("hora");
            entity.Property(e => e.Lector).HasColumnName("lector");
        });

        modelBuilder.Entity<Contratista>(entity =>
        {
            entity.HasKey(e => e.IdContratistas);

            entity.HasIndex(e => e.Rfc, "IX_Contratistas").IsUnique();

            entity.Property(e => e.IdContratistas).HasColumnName("idContratistas");
            entity.Property(e => e.ApMaterno).HasMaxLength(50);
            entity.Property(e => e.ApPaterno).HasMaxLength(50);
            entity.Property(e => e.ArchivoAdjunto).HasMaxLength(200);
            entity.Property(e => e.FechaActualizacion)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("fecha_actualizacion");
            entity.Property(e => e.FechaBaja).HasColumnType("datetime");
            entity.Property(e => e.FechaContrato).HasColumnType("datetime");
            entity.Property(e => e.FechaIndSeguridad).HasColumnType("datetime");
            entity.Property(e => e.FechaTerminacion).HasColumnType("datetime");
            entity.Property(e => e.Foto).HasColumnType("image");
            entity.Property(e => e.IdCcostos).HasColumnName("idCcostos");
            entity.Property(e => e.IdClasificacion).HasColumnName("idClasificacion");
            entity.Property(e => e.IdComedor).HasColumnName("idComedor");
            entity.Property(e => e.IdCompanias).HasColumnName("idCompanias");
            entity.Property(e => e.IdEstatus).HasColumnName("idEstatus");
            entity.Property(e => e.IdGafete).HasColumnName("idGafete");
            entity.Property(e => e.IdPlanta).HasColumnName("idPlanta");
            entity.Property(e => e.IdTipoContratista).HasColumnName("idTipoContratista");
            entity.Property(e => e.Imss)
                .HasMaxLength(50)
                .HasColumnName("IMSS");
            entity.Property(e => e.NombreCompañia).HasMaxLength(50);
            entity.Property(e => e.NombreContratista).HasMaxLength(150);
            entity.Property(e => e.Notas).HasMaxLength(150);
            entity.Property(e => e.NumeroContrato).HasMaxLength(10);
            entity.Property(e => e.PeriodoVigencia).HasColumnType("datetime");
            entity.Property(e => e.ResponsableIndorama).HasMaxLength(150);
            entity.Property(e => e.Rfc)
                .HasMaxLength(15)
                .HasColumnName("RFC");
            entity.Property(e => e.VigenciaImss)
                .HasColumnType("datetime")
                .HasColumnName("VigenciaIMSS");
            entity.Property(e => e.VigenciaSeguridad).HasColumnType("datetime");

            entity.HasOne(d => d.IdCcostosNavigation).WithMany(p => p.Contratista)
                .HasForeignKey(d => d.IdCcostos)
                .HasConstraintName("FK_Contratistas_ccostos");

            entity.HasOne(d => d.IdClasificacionNavigation).WithMany(p => p.Contratista)
                .HasForeignKey(d => d.IdClasificacion)
                .HasConstraintName("FK_Contratistas_Clasificacion");

            entity.HasOne(d => d.IdComedorNavigation).WithMany(p => p.Contratista)
                .HasForeignKey(d => d.IdComedor)
                .HasConstraintName("FK_Contratistas_Comedor");

            entity.HasOne(d => d.IdCompaniasNavigation).WithMany(p => p.Contratista)
                .HasForeignKey(d => d.IdCompanias)
                .HasConstraintName("FK_Contratistas_Compania");

            entity.HasOne(d => d.IdEstatusNavigation).WithMany(p => p.Contratista)
                .HasForeignKey(d => d.IdEstatus)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Contratistas_Estatus");

            entity.HasOne(d => d.IdGafeteNavigation).WithMany(p => p.ContratistaIdGafeteNavigations)
                .HasForeignKey(d => d.IdGafete)
                .HasConstraintName("FK_Contratistas_Gafete");

            entity.HasOne(d => d.IdPlantaNavigation).WithMany(p => p.Contratista)
                .HasForeignKey(d => d.IdPlanta)
                .HasConstraintName("FK_Contratistas_Planta");

            entity.HasOne(d => d.IdTipoContratistaNavigation).WithMany(p => p.Contratista)
                .HasForeignKey(d => d.IdTipoContratista)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Contratistas_TipoContratista");

            entity.HasOne(d => d.NumeroProvisionalGafeteNavigation).WithMany(p => p.ContratistaNumeroProvisionalGafeteNavigations)
                .HasForeignKey(d => d.NumeroProvisionalGafete)
                .HasConstraintName("FK_Contratistas_Gafete1");
        });

        modelBuilder.Entity<Contrato>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("contratos");

            entity.Property(e => e.Clase).HasMaxLength(50);
            entity.Property(e => e.ClaveC).HasMaxLength(50);
            entity.Property(e => e.Contrato1)
                .HasMaxLength(50)
                .HasColumnName("Contrato");
            entity.Property(e => e.Ffin).HasMaxLength(50);
            entity.Property(e => e.Finicio)
                .HasMaxLength(50)
                .HasColumnName("FInicio");
            entity.Property(e => e.Nombre).HasMaxLength(50);
            entity.Property(e => e.Nose)
                .HasMaxLength(50)
                .HasColumnName("nose");
            entity.Property(e => e.Posicion).HasMaxLength(50);
            entity.Property(e => e.Texto).HasMaxLength(50);
        });

        modelBuilder.Entity<CopiaMarcajesEslabon>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("copia_marcajes_eslabon");

            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.Lector).HasColumnName("lector");
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
        });

        modelBuilder.Entity<CopiaMarcajesEslabonOrg>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("copia_marcajes_eslabon_org");

            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.Lector).HasColumnName("lector");
        });

        modelBuilder.Entity<DtwCaLogUser>(entity =>
        {
            entity.HasKey(e => e.Idlog).HasName("PK__DTW_CA_L__95D00208CC200252");

            entity.ToTable("DTW_CA_LogUsers");

            entity.Property(e => e.Idlog).HasColumnName("IDLog");
            entity.Property(e => e.DateOperation).HasColumnType("datetime");
            entity.Property(e => e.Iduser).HasColumnName("IDUser");
            entity.Property(e => e.MessageOperation)
                .HasMaxLength(500)
                .IsUnicode(false);

            entity.HasOne(d => d.IduserNavigation).WithMany(p => p.DtwCaLogUsers)
                .HasForeignKey(d => d.Iduser)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__DTW_CA_Lo__IDUse__1C722D53");
        });

        modelBuilder.Entity<DtwCaParametro>(entity =>
        {
            entity.HasKey(e => e.IdParametro).HasName("PK__DTW_CA_P__37B016F4B90153A0");

            entity.ToTable("DTW_CA_Parametros");

            entity.Property(e => e.Descripcion)
                .HasMaxLength(200)
                .IsUnicode(false);
            entity.Property(e => e.Nombre)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Unidad)
                .HasMaxLength(50)
                .IsUnicode(false);
        });

        modelBuilder.Entity<EmpCsv>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("emp_CSV");

            entity.Property(e => e.Apellidom)
                .HasMaxLength(50)
                .HasColumnName("apellidom");
            entity.Property(e => e.Apellidop)
                .HasMaxLength(50)
                .HasColumnName("apellidop");
            entity.Property(e => e.Cc)
                .HasMaxLength(50)
                .HasColumnName("cc");
            entity.Property(e => e.Estatus)
                .HasMaxLength(50)
                .HasColumnName("estatus");
            entity.Property(e => e.Nombre)
                .HasMaxLength(50)
                .HasColumnName("nombre");
            entity.Property(e => e.Nominaproc)
                .HasMaxLength(50)
                .HasColumnName("nominaproc");
            entity.Property(e => e.Numero)
                .HasMaxLength(50)
                .HasColumnName("numero");
            entity.Property(e => e.Tpnomina)
                .HasMaxLength(50)
                .HasColumnName("tpnomina");
        });

        modelBuilder.Entity<EmplCc>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Empl_cc");

            entity.Property(e => e.ApeMaterno).HasMaxLength(50);
            entity.Property(e => e.ApePaterno).HasMaxLength(50);
            entity.Property(e => e.Ccostos).HasMaxLength(50);
            entity.Property(e => e.Descripcion).HasMaxLength(150);
            entity.Property(e => e.Nombre).HasMaxLength(50);
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
        });

        modelBuilder.Entity<Empleado>(entity =>
        {
            entity.HasKey(e => e.IdEmpleado);

            entity.ToTable("Empleado");

            entity.HasIndex(e => e.NumEmpleado, "IX_Empleado_NumEmpleado").IsUnique();

            entity.Property(e => e.IdEmpleado).HasColumnName("idEmpleado");
            entity.Property(e => e.ApeMaterno).HasMaxLength(50);
            entity.Property(e => e.ApePaterno).HasMaxLength(50);
            entity.Property(e => e.FechaActualizacion)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("fecha_actualizacion");
            entity.Property(e => e.Foto).HasColumnType("image");
            entity.Property(e => e.IdCcostos).HasColumnName("idCcostos");
            entity.Property(e => e.IdEstatus).HasColumnName("idEstatus");
            entity.Property(e => e.IdGafete).HasColumnName("idGafete");
            entity.Property(e => e.IdPlanta).HasColumnName("idPlanta");
            entity.Property(e => e.IdTipoNomina).HasColumnName("idTipoNomina");
            entity.Property(e => e.Nombre).HasMaxLength(50);
            entity.Property(e => e.NominaProcesada).HasMaxLength(50);
            entity.Property(e => e.Notas).HasMaxLength(250);
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
            entity.Property(e => e.Sfid)
                .HasMaxLength(10)
                .HasColumnName("SFID");

            entity.HasOne(d => d.IdCcostosNavigation).WithMany(p => p.Empleados)
                .HasForeignKey(d => d.IdCcostos)
                .HasConstraintName("FK_Empleado_ccostos");

            entity.HasOne(d => d.IdEstatusNavigation).WithMany(p => p.Empleados)
                .HasForeignKey(d => d.IdEstatus)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Empleado_Estatus");

            entity.HasOne(d => d.IdGafeteNavigation).WithMany(p => p.EmpleadoIdGafeteNavigations)
                .HasForeignKey(d => d.IdGafete)
                .HasConstraintName("FK_Empleado_Gafete");

            entity.HasOne(d => d.IdPlantaNavigation).WithMany(p => p.Empleados)
                .HasForeignKey(d => d.IdPlanta)
                .HasConstraintName("FK_Empleado_Planta");

            entity.HasOne(d => d.IdTipoNominaNavigation).WithMany(p => p.Empleados)
                .HasForeignKey(d => d.IdTipoNomina)
                .HasConstraintName("FK_Empleado_TipoNomina");

            entity.HasOne(d => d.NumeroProvisionalGafeteNavigation).WithMany(p => p.EmpleadoNumeroProvisionalGafeteNavigations)
                .HasForeignKey(d => d.NumeroProvisionalGafete)
                .HasConstraintName("FK_Empleado_Gafete1");
        });

        modelBuilder.Entity<EstadoGafeteDeAcceso>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Estado Gafete de Acceso");

            entity.Property(e => e.ApeMaterno).HasMaxLength(50);
            entity.Property(e => e.ApePaterno).HasMaxLength(50);
            entity.Property(e => e.EstatusEmpleado).HasColumnName("Estatus Empleado");
            entity.Property(e => e.EstatusGafete).HasColumnName("Estatus Gafete");
            entity.Property(e => e.Gafete).HasMaxLength(5);
            entity.Property(e => e.IdEmpleado).HasColumnName("idEmpleado");
            entity.Property(e => e.IdGafete).HasColumnName("idGafete");
            entity.Property(e => e.IdTipoNomina).HasColumnName("idTipoNomina");
            entity.Property(e => e.Nombre).HasMaxLength(50);
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
        });

        modelBuilder.Entity<Estatus>(entity =>
        {
            entity.HasKey(e => e.IdEstatus).HasName("PK_EstatusEmpleado");

            entity.ToTable("Estatus");

            entity.Property(e => e.IdEstatus).HasColumnName("idEstatus");
            entity.Property(e => e.Estatus1)
                .HasMaxLength(50)
                .HasColumnName("Estatus");
        });

        modelBuilder.Entity<Gafete>(entity =>
        {
            entity.HasKey(e => e.IdGafete);

            entity.ToTable("Gafete");

            entity.HasIndex(e => e.Gafete1, "IX_Gafete").IsUnique();

            entity.Property(e => e.IdGafete).HasColumnName("idGafete");
            entity.Property(e => e.FechaInicio).HasColumnType("datetime");
            entity.Property(e => e.FechaVigencia).HasColumnType("datetime");
            entity.Property(e => e.Gafete1)
                .HasMaxLength(5)
                .HasColumnName("Gafete");
            entity.Property(e => e.GafeteCompleto)
                .HasMaxLength(16)
                .IsFixedLength();
            entity.Property(e => e.IdEstatus).HasColumnName("idEstatus");
            entity.Property(e => e.IdTipoGafete).HasColumnName("idTipoGafete");
            entity.Property(e => e.IdTipoPersonal).HasColumnName("idTipoPersonal");

            entity.HasOne(d => d.IdEstatusNavigation).WithMany(p => p.Gafetes)
                .HasForeignKey(d => d.IdEstatus)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Gafete_Estatus");

            entity.HasOne(d => d.IdTipoGafeteNavigation).WithMany(p => p.Gafetes)
                .HasForeignKey(d => d.IdTipoGafete)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Gafete_TipoGafete");

            entity.HasOne(d => d.IdTipoPersonalNavigation).WithMany(p => p.Gafetes)
                .HasForeignKey(d => d.IdTipoPersonal)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Gafete_Personal");
        });

        modelBuilder.Entity<It>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("IT");

            entity.Property(e => e.Ficha)
                .HasMaxLength(7)
                .HasColumnName("ficha");
            entity.Property(e => e.Nombre)
                .HasMaxLength(152)
                .HasColumnName("nombre");
        });

        modelBuilder.Entity<LogError>(entity =>
        {
            entity.HasKey(e => e.IdLog);

            entity.ToTable("LogError");

            entity.Property(e => e.IdLog).HasColumnName("idLog");
            entity.Property(e => e.ErrMsg).IsUnicode(false);
            entity.Property(e => e.Fecha)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("fecha");
        });

        modelBuilder.Entity<Marcaje>(entity =>
        {
            entity.HasKey(e => e.IdMarcajes);

            entity.HasIndex(e => e.FechaRegistro, "IX_Marcajes_FechaRegistro");

            entity.HasIndex(e => e.AccesoOtorgado, "IX_Marcajes_accesoOtorgado");

            entity.Property(e => e.IdMarcajes).HasColumnName("idMarcajes");
            entity.Property(e => e.AccesoOtorgado).HasDefaultValue(false);
            entity.Property(e => e.Campo)
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasColumnName("campo");
            entity.Property(e => e.EntradaSalida)
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.IdCcostos).HasColumnName("idCcostos");
            entity.Property(e => e.IdCompanias).HasColumnName("idCompanias");
            entity.Property(e => e.IdContratistas).HasColumnName("idContratistas");
            entity.Property(e => e.IdEmpleado).HasColumnName("idEmpleado");
            entity.Property(e => e.IdGafete).HasColumnName("idGafete");
            entity.Property(e => e.IdGafeteTmp).HasColumnName("idGafeteTmp");
            entity.Property(e => e.IdMarcajesEntrada).HasColumnName("idMarcajesEntrada");
            entity.Property(e => e.IdTerminales).HasColumnName("idTerminales");
            entity.Property(e => e.IdTipoPersonal).HasColumnName("idTipoPersonal");
            entity.Property(e => e.IdVisitantes).HasColumnName("idVisitantes");
            entity.Property(e => e.NombrePersonal)
                .HasMaxLength(400)
                .IsUnicode(false);
            entity.Property(e => e.ResponsableInvista)
                .HasMaxLength(150)
                .IsUnicode(false);

            entity.HasOne(d => d.IdCcostosNavigation).WithMany(p => p.Marcajes)
                .HasForeignKey(d => d.IdCcostos)
                .HasConstraintName("FK_Marcajes_ccostos");

            entity.HasOne(d => d.IdClasificacionNavigation).WithMany(p => p.Marcajes)
                .HasForeignKey(d => d.IdClasificacion)
                .HasConstraintName("FK_Marcajes_Clasificacion");

            entity.HasOne(d => d.IdCompaniasNavigation).WithMany(p => p.Marcajes)
                .HasForeignKey(d => d.IdCompanias)
                .HasConstraintName("FK_Marcajes_Compania");

            entity.HasOne(d => d.IdContratistasNavigation).WithMany(p => p.Marcajes)
                .HasForeignKey(d => d.IdContratistas)
                .HasConstraintName("FK_Marcajes_Contratistas");

            entity.HasOne(d => d.IdEmpleadoNavigation).WithMany(p => p.Marcajes)
                .HasForeignKey(d => d.IdEmpleado)
                .HasConstraintName("FK_Marcajes_Empleado");

            entity.HasOne(d => d.IdGafeteNavigation).WithMany(p => p.MarcajeIdGafeteNavigations)
                .HasForeignKey(d => d.IdGafete)
                .HasConstraintName("FK_Marcajes_Gafete");

            entity.HasOne(d => d.IdGafeteTmpNavigation).WithMany(p => p.MarcajeIdGafeteTmpNavigations)
                .HasForeignKey(d => d.IdGafeteTmp)
                .HasConstraintName("FK_Marcajes_Gafete1");

            entity.HasOne(d => d.IdTerminalesNavigation).WithMany(p => p.Marcajes)
                .HasForeignKey(d => d.IdTerminales)
                .HasConstraintName("FK_Marcajes_Terminales");

            entity.HasOne(d => d.IdTipoLectorNavigation).WithMany(p => p.Marcajes)
                .HasForeignKey(d => d.IdTipoLector)
                .HasConstraintName("FK_Marcajes_TipoLector");

            entity.HasOne(d => d.IdTipoPersonalNavigation).WithMany(p => p.Marcajes)
                .HasForeignKey(d => d.IdTipoPersonal)
                .HasConstraintName("FK_Marcajes_Personal");

            entity.HasOne(d => d.IdVisitantesNavigation).WithMany(p => p.Marcajes)
                .HasForeignKey(d => d.IdVisitantes)
                .HasConstraintName("FK_Marcajes_Visitantes");
        });

        modelBuilder.Entity<MarcajesASubir>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Marcajes a subir");

            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.Lector).HasColumnName("lector");
        });

        modelBuilder.Entity<MarcajesContatista>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Marcajes_Contatistas");

            entity.Property(e => e.EntradaSalida)
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.Nombre).HasMaxLength(252);
            entity.Property(e => e.NombreCompañia).HasMaxLength(50);
        });

        modelBuilder.Entity<MarcajesContratIt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("marcajes_contrat_it");

            entity.Property(e => e.ApMaterno).HasMaxLength(50);
            entity.Property(e => e.ApPaterno).HasMaxLength(50);
            entity.Property(e => e.EntradaSalida)
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.NombreContratista).HasMaxLength(150);
        });

        modelBuilder.Entity<MarcajesEmplStafe>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Marcajes_Empl_stafe");

            entity.Property(e => e.Cc)
                .HasMaxLength(255)
                .HasColumnName("CC");
            entity.Property(e => e.Fecha)
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasColumnName("fecha");
            entity.Property(e => e.Hora)
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasColumnName("hora");
            entity.Property(e => e.Lector).HasColumnName("lector");
            entity.Property(e => e.Nombre).HasMaxLength(767);
            entity.Property(e => e.Usuario).HasColumnName("usuario");
        });

        modelBuilder.Entity<MarcajesEmpleadosQro>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Marcajes Empleados Qro");

            entity.Property(e => e.Descripcion).HasMaxLength(150);
            entity.Property(e => e.EntradaSalida)
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Fecha)
                .HasMaxLength(11)
                .IsUnicode(false);
            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.Hora)
                .HasMaxLength(8)
                .IsUnicode(false);
            entity.Property(e => e.NomEmp).HasMaxLength(152);
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
        });

        modelBuilder.Entity<MarcajesInsertarPf>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Marcajes_Insertar_PF");

            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.Gafete).HasMaxLength(5);
            entity.Property(e => e.NumeroDeLector).HasColumnName("Numero de Lector");
        });

        modelBuilder.Entity<MarcajesIt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Marcajes IT");

            entity.Property(e => e.Descripcion).HasMaxLength(150);
            entity.Property(e => e.EntradaSalida)
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Fecha)
                .HasMaxLength(11)
                .IsUnicode(false);
            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.Hora)
                .HasMaxLength(8)
                .IsUnicode(false);
            entity.Property(e => e.NomEmp).HasMaxLength(152);
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
        });

        modelBuilder.Entity<MarcajesLogistica>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("Marcajes Logistica");

            entity.Property(e => e.Descripcion).HasMaxLength(150);
            entity.Property(e => e.EntradaSalida)
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.Fecha)
                .HasMaxLength(11)
                .IsUnicode(false);
            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.Hora)
                .HasMaxLength(8)
                .IsUnicode(false);
            entity.Property(e => e.NomEmp).HasMaxLength(152);
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
        });

        modelBuilder.Entity<MarcajesPf>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("marcajesPF");

            entity.Property(e => e.ApeMaterno).HasMaxLength(50);
            entity.Property(e => e.ApePaterno).HasMaxLength(50);
            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.Gafete).HasMaxLength(5);
            entity.Property(e => e.IdPlanta).HasColumnName("idPlanta");
            entity.Property(e => e.IdTerminales).HasColumnName("idTerminales");
            entity.Property(e => e.Nombre).HasMaxLength(50);
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
        });

        modelBuilder.Entity<MarcajesPfIt>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("marcajes_PF_IT");

            entity.Property(e => e.ApeMaterno).HasMaxLength(50);
            entity.Property(e => e.ApePaterno).HasMaxLength(50);
            entity.Property(e => e.EntradaSalida)
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.Nombre).HasMaxLength(50);
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
        });

        modelBuilder.Entity<MarcajesSubido>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("Marcajes subidos");

            entity.HasIndex(e => e.Fecha, "fecha");

            entity.Property(e => e.Empleado).HasColumnName("empleado");
            entity.Property(e => e.Fecha).HasColumnType("datetime");
        });

        modelBuilder.Entity<Marcajesborrado>(entity =>
        {
            entity.HasNoKey();

            entity.Property(e => e.Campo)
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasColumnName("campo");
            entity.Property(e => e.EntradaSalida)
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.IdCcostos).HasColumnName("idCcostos");
            entity.Property(e => e.IdCompanias).HasColumnName("idCompanias");
            entity.Property(e => e.IdContratistas).HasColumnName("idContratistas");
            entity.Property(e => e.IdEmpleado).HasColumnName("idEmpleado");
            entity.Property(e => e.IdGafete).HasColumnName("idGafete");
            entity.Property(e => e.IdGafeteTmp).HasColumnName("idGafeteTmp");
            entity.Property(e => e.IdMarcajes).HasColumnName("idMarcajes");
            entity.Property(e => e.IdMarcajesEntrada).HasColumnName("idMarcajesEntrada");
            entity.Property(e => e.IdTerminales).HasColumnName("idTerminales");
            entity.Property(e => e.IdTipoPersonal).HasColumnName("idTipoPersonal");
            entity.Property(e => e.IdVisitantes).HasColumnName("idVisitantes");
            entity.Property(e => e.NombrePersonal)
                .HasMaxLength(400)
                .IsUnicode(false);
            entity.Property(e => e.ResponsableInvista)
                .HasMaxLength(150)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Modulo>(entity =>
        {
            entity.HasKey(e => e.IdModulo).HasName("PK_modulos");

            entity.Property(e => e.IdModulo).HasColumnName("id_modulo");
            entity.Property(e => e.Archivo)
                .HasMaxLength(150)
                .HasColumnName("archivo");
            entity.Property(e => e.IdPadre).HasColumnName("id_padre");
            entity.Property(e => e.Nombre)
                .HasMaxLength(150)
                .HasColumnName("nombre");
            entity.Property(e => e.Orden).HasColumnName("orden");
        });

        modelBuilder.Entity<Origen>(entity =>
        {
            entity.HasKey(e => e.IdOrigen);

            entity.ToTable("Origen");

            entity.Property(e => e.IdOrigen).HasColumnName("idOrigen");
            entity.Property(e => e.DescOrigen).HasMaxLength(150);
            entity.Property(e => e.FechaActualizacion)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("fecha_actualizacion");
            entity.Property(e => e.Origen1)
                .HasMaxLength(50)
                .HasColumnName("Origen");
        });

        modelBuilder.Entity<Perfile>(entity =>
        {
            entity.HasKey(e => e.IdPerfil);

            entity.Property(e => e.IdPerfil).HasColumnName("id_perfil");
            entity.Property(e => e.Clave)
                .HasMaxLength(50)
                .HasColumnName("clave");
            entity.Property(e => e.Descripcion)
                .HasMaxLength(50)
                .HasColumnName("descripcion");
        });

        modelBuilder.Entity<PerfilesModulo>(entity =>
        {
            entity.HasKey(e => new { e.IdPerfil, e.IdModulo, e.IdAccion });

            entity.Property(e => e.IdAccion).HasColumnName("id_accion");
            entity.Property(e => e.IdModulo).HasColumnName("id_modulo");
            entity.Property(e => e.IdPerfil).HasColumnName("id_perfil");

            entity.HasOne(d => d.IdAccionNavigation).WithMany()
                .HasForeignKey(d => d.IdAccion)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PerfilesModulos_Acciones");

            entity.HasOne(d => d.IdModuloNavigation).WithMany()
                .HasForeignKey(d => d.IdModulo)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PerfilesModulos_Modulos");

            entity.HasOne(d => d.IdPerfilNavigation).WithMany()
                .HasForeignKey(d => d.IdPerfil)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PerfilesModulos_Perfiles");
        });

        modelBuilder.Entity<Personal>(entity =>
        {
            entity.HasKey(e => e.IdTipoPersonal);

            entity.ToTable("Personal");

            entity.Property(e => e.IdTipoPersonal).HasColumnName("idTipoPersonal");
            entity.Property(e => e.Personal1)
                .HasMaxLength(50)
                .HasColumnName("Personal");
        });

        modelBuilder.Entity<Plantum>(entity =>
        {
            entity.HasKey(e => e.IdPlanta);

            entity.Property(e => e.IdPlanta).HasColumnName("idPlanta");
            entity.Property(e => e.Clave).HasMaxLength(50);
            entity.Property(e => e.Dato1)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Dato2)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Dato3)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Dato4)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Dato5)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Dato6)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Dato7)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Dato8)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Descripcion).HasMaxLength(50);
        });

        modelBuilder.Entity<Terminale>(entity =>
        {
            entity.HasKey(e => e.IdTerminales);

            entity.Property(e => e.IdTerminales).HasColumnName("idTerminales");
            entity.Property(e => e.Clave).HasMaxLength(50);
            entity.Property(e => e.FechaActualizacion)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("fecha_actualizacion");
            entity.Property(e => e.IdTipoLector).HasColumnName("idTipoLector");
            entity.Property(e => e.Ip)
                .HasMaxLength(50)
                .HasColumnName("IP");
            entity.Property(e => e.Nombre).HasMaxLength(50);
            entity.Property(e => e.Ubicacion).HasMaxLength(50);

            entity.HasOne(d => d.IdTipoLectorNavigation).WithMany(p => p.Terminales)
                .HasForeignKey(d => d.IdTipoLector)
                .HasConstraintName("FK_Terminales_TipoLector");
        });

        modelBuilder.Entity<TipoContratistum>(entity =>
        {
            entity.HasKey(e => e.IdTipoContratista);

            entity.Property(e => e.IdTipoContratista).HasColumnName("idTipoContratista");
            entity.Property(e => e.Descripcion).HasMaxLength(150);
            entity.Property(e => e.FechaVigencia).HasColumnType("datetime");
            entity.Property(e => e.TipoContratista).HasMaxLength(50);
        });

        modelBuilder.Entity<TipoGafete>(entity =>
        {
            entity.HasKey(e => e.IdTipoGafete);

            entity.ToTable("TipoGafete");

            entity.Property(e => e.IdTipoGafete)
                .ValueGeneratedNever()
                .HasColumnName("idTipoGafete");
            entity.Property(e => e.TipoGafete1)
                .HasMaxLength(50)
                .HasColumnName("TipoGafete");
        });

        modelBuilder.Entity<TipoLector>(entity =>
        {
            entity.HasKey(e => e.IdTipoLector);

            entity.ToTable("TipoLector");

            entity.Property(e => e.IdTipoLector).HasColumnName("idTipoLector");
            entity.Property(e => e.Descripcion).HasMaxLength(150);
            entity.Property(e => e.TipoLector1).HasColumnName("TipoLector");
        });

        modelBuilder.Entity<TipoNomina>(entity =>
        {
            entity.HasKey(e => e.IdTipoNomina);

            entity.ToTable("TipoNomina");

            entity.Property(e => e.IdTipoNomina).HasColumnName("idTipoNomina");
            entity.Property(e => e.Descripcion).HasMaxLength(50);
            entity.Property(e => e.FechaActualizacion)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("fecha_actualizacion");
            entity.Property(e => e.TipoNomina1).HasColumnName("TipoNomina");
        });

        modelBuilder.Entity<Usuario>(entity =>
        {
            entity.HasKey(e => e.IdUsuarios);

            entity.Property(e => e.IdUsuarios).HasColumnName("idUsuarios");
            entity.Property(e => e.IdPerfil).HasColumnName("id_perfil");
            entity.Property(e => e.NombreUsuario).HasMaxLength(150);
            entity.Property(e => e.Password).HasMaxLength(32);
            entity.Property(e => e.Usuario1)
                .HasMaxLength(16)
                .HasColumnName("Usuario");

            entity.HasOne(d => d.IdPerfilNavigation).WithMany(p => p.Usuarios)
                .HasForeignKey(d => d.IdPerfil)
                .HasConstraintName("FK_Usuarios_Perfiles");
        });

        modelBuilder.Entity<VComida>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("V_comidas");

            entity.Property(e => e.Campo)
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasColumnName("campo");
            entity.Property(e => e.Ccostos).HasMaxLength(50);
            entity.Property(e => e.Empresa).HasColumnName("empresa");
            entity.Property(e => e.Fecha).HasColumnType("datetime");
            entity.Property(e => e.Funcion)
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasColumnName("funcion");
            entity.Property(e => e.Hora)
                .HasMaxLength(10)
                .IsUnicode(false);
            entity.Property(e => e.Lector).HasColumnName("lector");
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
        });

        modelBuilder.Entity<VComidasC>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("V_comidas_C");

            entity.Property(e => e.Campo)
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasColumnName("campo");
            entity.Property(e => e.Ccostos).HasMaxLength(50);
            entity.Property(e => e.Empresa).HasColumnName("empresa");
            entity.Property(e => e.Fecha).HasColumnType("datetime");
            entity.Property(e => e.Funcion)
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasColumnName("funcion");
            entity.Property(e => e.Hora)
                .HasMaxLength(10)
                .IsUnicode(false);
            entity.Property(e => e.IdGafete).HasColumnName("idGafete");
            entity.Property(e => e.Lector).HasColumnName("lector");
        });

        modelBuilder.Entity<VEmpleado>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("v_empleado");

            entity.Property(e => e.Apematerno)
                .HasMaxLength(60)
                .IsUnicode(false)
                .UseCollation("SQL_Latin1_General_CP1_CI_AS")
                .HasColumnName("apematerno");
            entity.Property(e => e.Apepaterno)
                .HasMaxLength(60)
                .IsUnicode(false)
                .UseCollation("SQL_Latin1_General_CP1_CI_AS")
                .HasColumnName("apepaterno");
            entity.Property(e => e.FechaActualizacion)
                .HasMaxLength(4000)
                .UseCollation("SQL_Latin1_General_CP1_CI_AS")
                .HasColumnName("fecha_actualizacion");
            entity.Property(e => e.IdPlanta)
                .HasMaxLength(3)
                .IsUnicode(false)
                .UseCollation("SQL_Latin1_General_CP1_CI_AS")
                .HasColumnName("idPlanta");
            entity.Property(e => e.Idccostos)
                .HasMaxLength(4)
                .IsUnicode(false)
                .UseCollation("SQL_Latin1_General_CP1_CI_AS")
                .HasColumnName("idccostos");
            entity.Property(e => e.Idestatus)
                .HasMaxLength(6)
                .IsUnicode(false)
                .UseCollation("SQL_Latin1_General_CP1_CI_AS")
                .HasColumnName("idestatus");
            entity.Property(e => e.Idtiponomina).HasColumnName("idtiponomina");
            entity.Property(e => e.Nombre)
                .HasMaxLength(60)
                .IsUnicode(false)
                .UseCollation("SQL_Latin1_General_CP1_CI_AS")
                .HasColumnName("nombre");
            entity.Property(e => e.Nominaprocesada).HasColumnName("nominaprocesada");
            entity.Property(e => e.Numempleado).HasColumnName("numempleado");
        });

        modelBuilder.Entity<VRptEh>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("v_rpt_ehs");

            entity.Property(e => e.Ccostos).HasMaxLength(50);
            entity.Property(e => e.Clasificacion).HasMaxLength(50);
            entity.Property(e => e.EntradaSalida)
                .HasMaxLength(1)
                .IsUnicode(false)
                .IsFixedLength();
            entity.Property(e => e.FechaRegistro).HasColumnType("datetime");
            entity.Property(e => e.IdCcostos).HasColumnName("idCcostos");
            entity.Property(e => e.IdContratistas).HasColumnName("idContratistas");
            entity.Property(e => e.IdGafete).HasColumnName("idGafete");
            entity.Property(e => e.IdGafeteTmp).HasColumnName("idGafeteTmp");
            entity.Property(e => e.IdMarcajes).HasColumnName("idMarcajes");
            entity.Property(e => e.IdMarcajesEntrada).HasColumnName("idMarcajesEntrada");
            entity.Property(e => e.IdTipoPersonal).HasColumnName("idTipoPersonal");
            entity.Property(e => e.Personal).HasMaxLength(50);
        });

        modelBuilder.Entity<View1>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("View_1");

            entity.Property(e => e.Emleado).HasMaxLength(7);
            entity.Property(e => e.Entrada)
                .HasMaxLength(30)
                .IsUnicode(false);
            entity.Property(e => e.Fecha)
                .HasMaxLength(30)
                .IsUnicode(false);
            entity.Property(e => e.Hrs)
                .HasMaxLength(5)
                .IsUnicode(false);
            entity.Property(e => e.Salida)
                .HasMaxLength(30)
                .IsUnicode(false);
        });

        modelBuilder.Entity<ViewMarcajesComedor>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("viewMarcajesComedor");

            entity.Property(e => e.Fecha)
                .HasColumnType("datetime")
                .HasColumnName("fecha");
            entity.Property(e => e.Turno).HasColumnName("turno");
        });

        modelBuilder.Entity<ViewMarcajesComedorSf>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("viewMarcajesComedorSF");

            entity.Property(e => e.Fecha)
                .HasColumnType("datetime")
                .HasColumnName("fecha");
            entity.Property(e => e.Turno).HasColumnName("turno");
        });

        modelBuilder.Entity<Visitante>(entity =>
        {
            entity.HasKey(e => e.IdVisitantes);

            entity.HasIndex(e => e.Rfc, "IX_Visitantes").IsUnique();

            entity.Property(e => e.IdVisitantes).HasColumnName("idVisitantes");
            entity.Property(e => e.ArchivoAdjunto).HasMaxLength(100);
            entity.Property(e => e.DescOrigen).HasMaxLength(150);
            entity.Property(e => e.EmpResponsable).HasMaxLength(150);
            entity.Property(e => e.FechaActualizacion)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("fecha_actualizacion");
            entity.Property(e => e.FechaIndSeguridad).HasColumnType("datetime");
            entity.Property(e => e.FechaIngreso).HasColumnType("datetime");
            entity.Property(e => e.FechaTerminacion).HasColumnType("datetime");
            entity.Property(e => e.Foto).HasColumnType("image");
            entity.Property(e => e.Horas).HasMaxLength(7);
            entity.Property(e => e.IdCcostos).HasColumnName("idCcostos");
            entity.Property(e => e.IdComedor).HasColumnName("idComedor");
            entity.Property(e => e.IdEstatus).HasColumnName("idEstatus");
            entity.Property(e => e.IdGafete).HasColumnName("idGafete");
            entity.Property(e => e.IdOrigen).HasColumnName("idOrigen");
            entity.Property(e => e.IdPlanta).HasColumnName("idPlanta");
            entity.Property(e => e.NombreVisitante).HasMaxLength(150);
            entity.Property(e => e.Notas).HasMaxLength(150);
            entity.Property(e => e.Rfc)
                .HasMaxLength(15)
                .HasColumnName("RFC");

            entity.HasOne(d => d.IdGafeteNavigation).WithMany(p => p.VisitanteIdGafeteNavigations)
                .HasForeignKey(d => d.IdGafete);

            entity.HasOne(d => d.IdEstatusNavigation).WithMany(p => p.Visitantes)
                .HasForeignKey(d => d.IdEstatus);

            entity.HasOne(d => d.IdPlantaNavigation).WithMany(p => p.Visitantes)
                .HasForeignKey(d => d.IdPlanta)
                .HasConstraintName("FK_Visitantes_Planta");
        });

        modelBuilder.Entity<VistaEmpleadosIndo>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("vista_empleados_indo");

            entity.Property(e => e.Centrocostos)
                .HasMaxLength(4)
                .IsUnicode(false)
                .UseCollation("SQL_Latin1_General_CP1_CI_AS")
                .HasColumnName("centrocostos");
            entity.Property(e => e.Centrotrabajo)
                .HasMaxLength(3)
                .IsUnicode(false)
                .UseCollation("SQL_Latin1_General_CP1_CI_AS")
                .HasColumnName("centrotrabajo");
            entity.Property(e => e.Edo).HasColumnName("edo");
            entity.Property(e => e.Empleado).HasColumnName("empleado");
            entity.Property(e => e.Estado)
                .HasMaxLength(6)
                .IsUnicode(false)
                .UseCollation("SQL_Latin1_General_CP1_CI_AS")
                .HasColumnName("estado");
            entity.Property(e => e.Fechamov)
                .HasMaxLength(4000)
                .UseCollation("SQL_Latin1_General_CP1_CI_AS")
                .HasColumnName("fechamov");
            entity.Property(e => e.Materno)
                .HasMaxLength(60)
                .IsUnicode(false)
                .UseCollation("SQL_Latin1_General_CP1_CI_AS")
                .HasColumnName("materno");
            entity.Property(e => e.Nombre)
                .HasMaxLength(60)
                .IsUnicode(false)
                .UseCollation("SQL_Latin1_General_CP1_CI_AS")
                .HasColumnName("nombre");
            entity.Property(e => e.Paterno)
                .HasMaxLength(60)
                .IsUnicode(false)
                .UseCollation("SQL_Latin1_General_CP1_CI_AS")
                .HasColumnName("paterno");
            entity.Property(e => e.Tiponomina).HasColumnName("tiponomina");
        });

        modelBuilder.Entity<VwPermiso>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("vw_Permisos");

            entity.Property(e => e.Accion).HasMaxLength(50);
            entity.Property(e => e.Modulo).HasMaxLength(150);
            entity.Property(e => e.Perfil).HasMaxLength(50);
        });

        modelBuilder.Entity<VwReporteNomina>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("vw_reporte_nomina");

            entity.Property(e => e.Campo)
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasColumnName("campo");
            entity.Property(e => e.Fecha)
                .HasMaxLength(36)
                .IsUnicode(false);
            entity.Property(e => e.Lector)
                .HasMaxLength(3)
                .IsUnicode(false)
                .HasColumnName("lector");
        });

        modelBuilder.Entity<VwReporteNomina1>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("vw_reporte_nomina1");

            entity.Property(e => e.Campo)
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasColumnName("campo");
            entity.Property(e => e.Fecha).HasColumnType("datetime");
            entity.Property(e => e.Lector)
                .HasMaxLength(3)
                .IsUnicode(false)
                .HasColumnName("lector");
            entity.Property(e => e.NumEmpleado).HasMaxLength(7);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
