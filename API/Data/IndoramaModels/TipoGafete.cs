﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class TipoGafete
{
    public int IdTipoGafete { get; set; }

    public string TipoGafete1 { get; set; } = null!;

    public virtual ICollection<Gafete> Gafetes { get; set; } = new List<Gafete>();
}
