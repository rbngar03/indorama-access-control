﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class CamposPerfil
{
    public int Id { get; set; }

    public int ModuloId { get; set; }

    public int PerfilId { get; set; }

    public string NombreCampo { get; set; } = null!;

    public bool IsActivated { get; set; }

    public virtual Modulo Modulo { get; set; } = null!;

    public virtual Perfile Perfil { get; set; } = null!;
}
