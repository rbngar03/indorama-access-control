﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Gafete
{
    public int IdGafete { get; set; }

    public string Gafete1 { get; set; } = null!;

    public DateTime? FechaInicio { get; set; }

    public DateTime? FechaVigencia { get; set; }

    public int IdEstatus { get; set; }

    public int IdTipoGafete { get; set; }

    public int IdTipoPersonal { get; set; }

    public string? GafeteCompleto { get; set; }

    public virtual ICollection<Contratista> ContratistaIdGafeteNavigations { get; set; } = new List<Contratista>();

    public virtual ICollection<Visitante> VisitanteIdGafeteNavigations { get; set; } = new List<Visitante>();

    public virtual ICollection<Contratista> ContratistaNumeroProvisionalGafeteNavigations { get; set; } = new List<Contratista>();

    public virtual ICollection<Empleado> EmpleadoIdGafeteNavigations { get; set; } = new List<Empleado>();

    public virtual ICollection<Empleado> EmpleadoNumeroProvisionalGafeteNavigations { get; set; } = new List<Empleado>();

    public virtual Estatus IdEstatusNavigation { get; set; } = null!;

    public virtual TipoGafete IdTipoGafeteNavigation { get; set; } = null!;

    public virtual Personal IdTipoPersonalNavigation { get; set; } = null!;

    public virtual ICollection<Marcaje> MarcajeIdGafeteNavigations { get; set; } = new List<Marcaje>();

    public virtual ICollection<Marcaje> MarcajeIdGafeteTmpNavigations { get; set; } = new List<Marcaje>();
}
