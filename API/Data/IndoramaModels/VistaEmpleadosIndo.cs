﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class VistaEmpleadosIndo
{
    public int Empleado { get; set; }

    public string? Paterno { get; set; }

    public string? Materno { get; set; }

    public string? Nombre { get; set; }

    public string? Centrocostos { get; set; }

    public int? Tiponomina { get; set; }

    public int? Edo { get; set; }

    public string? Estado { get; set; }

    public string? Fechamov { get; set; }

    public string? Centrotrabajo { get; set; }
}
