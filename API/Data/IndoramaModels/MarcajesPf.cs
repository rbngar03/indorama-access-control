﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class MarcajesPf
{
    public DateTime? FechaRegistro { get; set; }

    public string Gafete { get; set; } = null!;

    public int? IdPlanta { get; set; }

    public int? IdTerminales { get; set; }

    public string NumEmpleado { get; set; } = null!;

    public string ApePaterno { get; set; } = null!;

    public string? ApeMaterno { get; set; }

    public string Nombre { get; set; } = null!;
}
