﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Empleado
{
    public int IdEmpleado { get; set; }

    public string NumEmpleado { get; set; } = null!;

    public string ApePaterno { get; set; } = null!;

    public string? ApeMaterno { get; set; }

    public string Nombre { get; set; } = null!;

    public int? IdCcostos { get; set; }

    public int? IdTipoNomina { get; set; }

    public string NominaProcesada { get; set; } = null!;

    public int IdEstatus { get; set; }

    public int? IdGafete { get; set; }

    public byte[]? Foto { get; set; }

    public string? Notas { get; set; }

    public int? NumeroProvisionalGafete { get; set; }

    public int? IdPlanta { get; set; }

    public DateTime? FechaActualizacion { get; set; }

    public string? Sfid { get; set; }

    public virtual Ccosto? IdCcostosNavigation { get; set; }

    public virtual Estatus IdEstatusNavigation { get; set; } = null!;

    public virtual Gafete? IdGafeteNavigation { get; set; }

    public virtual Plantum? IdPlantaNavigation { get; set; }

    public virtual TipoNomina? IdTipoNominaNavigation { get; set; }

    public virtual ICollection<Marcaje> Marcajes { get; set; } = new List<Marcaje>();

    public virtual Gafete? NumeroProvisionalGafeteNavigation { get; set; }
}
