﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class DtwCaParametro
{
    public int IdParametro { get; set; }

    public string? Nombre { get; set; }

    public string? Descripcion { get; set; }

    public int? Valor { get; set; }

    public string? Unidad { get; set; }
}
