﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class MarcajesInsertarPf
{
    public DateTime? FechaRegistro { get; set; }

    public string Gafete { get; set; } = null!;

    public int NumeroDeLector { get; set; }
}
