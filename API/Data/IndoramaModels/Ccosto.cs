﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Ccosto
{
    public int IdCcostos { get; set; }

    public string? Ccostos { get; set; }

    public string? Descripcion { get; set; }

    public DateTime? FechaActualizacion { get; set; }

    public virtual ICollection<Contratista> Contratista { get; set; } = new List<Contratista>();

    public virtual ICollection<Empleado> Empleados { get; set; } = new List<Empleado>();

    public virtual ICollection<Marcaje> Marcajes { get; set; } = new List<Marcaje>();
}
