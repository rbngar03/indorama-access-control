﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class VEmpleado
{
    public int Numempleado { get; set; }

    public string? Apepaterno { get; set; }

    public string? Apematerno { get; set; }

    public string? Nombre { get; set; }

    public string? Idccostos { get; set; }

    public int? Idtiponomina { get; set; }

    public int? Nominaprocesada { get; set; }

    public string? Idestatus { get; set; }

    public string? FechaActualizacion { get; set; }

    public string? IdPlanta { get; set; }
}
