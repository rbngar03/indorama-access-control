﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class VwReporteNomina
{
    public string? Fecha { get; set; }

    public int? NumEmpleado { get; set; }

    public string? Lector { get; set; }

    public string? Campo { get; set; }
}
