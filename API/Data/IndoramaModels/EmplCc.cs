﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class EmplCc
{
    public string NumEmpleado { get; set; } = null!;

    public string ApePaterno { get; set; } = null!;

    public string? ApeMaterno { get; set; }

    public string Nombre { get; set; } = null!;

    public string? Ccostos { get; set; }

    public string? Descripcion { get; set; }
}
