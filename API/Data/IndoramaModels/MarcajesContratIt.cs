﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class MarcajesContratIt
{
    public string NombreContratista { get; set; } = null!;

    public string ApPaterno { get; set; } = null!;

    public string? ApMaterno { get; set; }

    public DateTime? FechaRegistro { get; set; }

    public string? EntradaSalida { get; set; }
}
