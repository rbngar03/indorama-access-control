﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class VwPermiso
{
    public string? Perfil { get; set; }

    public string? Modulo { get; set; }

    public string Accion { get; set; } = null!;
}
