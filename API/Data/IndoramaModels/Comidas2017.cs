﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Comidas2017
{
    public string NumEmpleado { get; set; } = null!;

    public string? NomEmp { get; set; }

    public DateTime? FechaRegistro { get; set; }

    public string? EntradaSalida { get; set; }

    public string Descripcion { get; set; } = null!;
}
