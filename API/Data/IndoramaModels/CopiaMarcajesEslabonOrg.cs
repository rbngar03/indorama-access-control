﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class CopiaMarcajesEslabonOrg
{
    public DateTime? FechaRegistro { get; set; }

    public int NumEmpleado { get; set; }

    public int? Lector { get; set; }
}
