﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Visitante
{
    public int IdVisitantes { get; set; }

    public DateTime? FechaIndSeguridad { get; set; }

    public DateTime? FechaIngreso { get; set; }
    public string FechaIngresoF => FechaIngreso?.ToString("dd/MM/yyyy") ?? " ";
    public DateTime? FechaTerminacion { get; set; }

    public string? Horas { get; set; }

    public string? NombreVisitante { get; set; }

    public int? IdOrigen { get; set; }

    public string? DescOrigen { get; set; }

    public byte[]? Foto { get; set; }

    public int? IdGafete { get; set; }

    public string? EmpResponsable { get; set; }

    public string? Notas { get; set; }

    public string Rfc { get; set; } = null!;

    public int IdComedor { get; set; }

    public int? IdCcostos { get; set; }

    public string? ArchivoAdjunto { get; set; }

    public int? IdEstatus { get; set; }

    public DateTime? FechaActualizacion { get; set; }

    public int? IdPlanta { get; set; }

    public virtual Plantum? IdPlantaNavigation { get; set; }

    public virtual ICollection<Marcaje> Marcajes { get; set; } = new List<Marcaje>();

    public virtual Gafete? IdGafeteNavigation { get; set; }

    public virtual Estatus IdEstatusNavigation { get; set; } = null!;
}
