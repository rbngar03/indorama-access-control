﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class LogError
{
    public int IdLog { get; set; }

    public DateTime? Fecha { get; set; }

    public string? ErrMsg { get; set; }

    public int? ErrSev { get; set; }

    public int? ErrSt { get; set; }
}
