﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class MarcajesContatista
{
    public string? Nombre { get; set; }

    public DateTime? FechaRegistro { get; set; }

    public string? EntradaSalida { get; set; }

    public string NombreCompañia { get; set; } = null!;
}
