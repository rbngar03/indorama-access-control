﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Comedor2018
{
    public int? IdTerminales { get; set; }

    public DateTime? FechaRegistro { get; set; }

    public string? NumEmpleado { get; set; }

    public string Gafete { get; set; } = null!;

    public decimal? Costo { get; set; }

    public string? Descripcion { get; set; }
}
