﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Terminale
{
    public int IdTerminales { get; set; }

    public string Clave { get; set; } = null!;

    public string Ubicacion { get; set; } = null!;

    public string Ip { get; set; } = null!;

    public string? Nombre { get; set; }

    public int? IdTipoLector { get; set; }

    public DateTime? FechaActualizacion { get; set; }

    public int? Tipo { get; set; }

    public int? Grupo { get; set; }

    public virtual TipoLector? IdTipoLectorNavigation { get; set; }

    public virtual ICollection<Marcaje> Marcajes { get; set; } = new List<Marcaje>();
}
