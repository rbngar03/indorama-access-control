﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class VComidasC
{
    public DateTime? Fecha { get; set; }

    public string? Hora { get; set; }

    public int? IdGafete { get; set; }

    public int Lector { get; set; }

    public int Empresa { get; set; }

    public string Funcion { get; set; } = null!;

    public string? Campo { get; set; }

    public string? Ccostos { get; set; }
}
