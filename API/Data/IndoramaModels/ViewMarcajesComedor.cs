﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class ViewMarcajesComedor
{
    public DateTime? Fecha { get; set; }

    public int? Turno { get; set; }
}
