﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class EmpCsv
{
    public string? Numero { get; set; }

    public string? Apellidop { get; set; }

    public string? Apellidom { get; set; }

    public string? Nombre { get; set; }

    public string? Cc { get; set; }

    public string? Tpnomina { get; set; }

    public string? Nominaproc { get; set; }

    public string? Estatus { get; set; }
}
