﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class CopiaMarcajesEslabon
{
    public DateTime? FechaRegistro { get; set; }

    public string NumEmpleado { get; set; } = null!;

    public int? Lector { get; set; }
}
