﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Ccqro
{
    public double? CeCo { get; set; }

    public string? Descripción { get; set; }

    public string? Responsable { get; set; }
}
