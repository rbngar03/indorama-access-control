﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Accione
{
    public int IdAccion { get; set; }

    public string Nombre { get; set; } = null!;
}
