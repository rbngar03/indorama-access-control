﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class MarcajesLogistica
{
    public string NumEmpleado { get; set; } = null!;

    public string? NomEmp { get; set; }

    public string? Descripcion { get; set; }

    public string? Fecha { get; set; }

    public string? Hora { get; set; }

    public string? EntradaSalida { get; set; }

    public DateTime? FechaRegistro { get; set; }
}
