﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class TipoLector
{
    public int IdTipoLector { get; set; }

    public int TipoLector1 { get; set; }

    public string Descripcion { get; set; } = null!;

    public virtual ICollection<Marcaje> Marcajes { get; set; } = new List<Marcaje>();

    public virtual ICollection<Terminale> Terminales { get; set; } = new List<Terminale>();
}
