﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Companium
{
    public int IdCompanias { get; set; }

    public string? Compania { get; set; }

    public string? Descripcion { get; set; }

    public DateTime? FechaActualizacion { get; set; }

    public virtual ICollection<Contratista> Contratista { get; set; } = new List<Contratista>();

    public virtual ICollection<Marcaje> Marcajes { get; set; } = new List<Marcaje>();
}
