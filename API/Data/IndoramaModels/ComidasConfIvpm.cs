﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class ComidasConfIvpm
{
    public DateTime? FechaRegistro { get; set; }

    public int? IdGafete { get; set; }

    public int? IdEmpleado { get; set; }

    public int? IdCcostos { get; set; }

    public string? NombrePersonal { get; set; }

    public string? NumEmpleado { get; set; }

    public string Personal { get; set; } = null!;

    public int? Idtiponomina { get; set; }
}
