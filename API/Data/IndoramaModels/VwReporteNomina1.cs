﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class VwReporteNomina1
{
    public DateTime? Fecha { get; set; }

    public string NumEmpleado { get; set; } = null!;

    public string? Lector { get; set; }

    public string? Campo { get; set; }
}
