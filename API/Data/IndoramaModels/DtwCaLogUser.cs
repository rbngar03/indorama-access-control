﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class DtwCaLogUser
{
    public int Idlog { get; set; }

    public int Iduser { get; set; }

    public DateTime DateOperation { get; set; }

    public string? MessageOperation { get; set; }

    public virtual Usuario IduserNavigation { get; set; } = null!;
}
