﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Conteocomida
{
    public string? Fecha { get; set; }

    public string? Hora { get; set; }

    public string Ficha { get; set; } = null!;

    public int Lector { get; set; }

    public string Funcion { get; set; } = null!;

    public string? Campo { get; set; }
}
