﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Usuario
{
    public int IdUsuarios { get; set; }

    public string Usuario1 { get; set; } = null!;

    public string Password { get; set; } = null!;

    public string NombreUsuario { get; set; } = null!;

    public int? IdPerfil { get; set; }

    public virtual ICollection<DtwCaLogUser> DtwCaLogUsers { get; set; } = new List<DtwCaLogUser>();

    public virtual Perfile? IdPerfilNavigation { get; set; }
}
