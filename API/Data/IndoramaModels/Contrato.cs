﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Contrato
{
    public string? Contrato1 { get; set; }

    public string? Posicion { get; set; }

    public string? Clase { get; set; }

    public string? Finicio { get; set; }

    public string? Ffin { get; set; }

    public string? Texto { get; set; }

    public string? ClaveC { get; set; }

    public string? Nombre { get; set; }

    public string? Nose { get; set; }
}
