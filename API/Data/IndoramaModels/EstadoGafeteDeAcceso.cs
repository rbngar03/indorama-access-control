﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class EstadoGafeteDeAcceso
{
    public int IdGafete { get; set; }

    public string Gafete { get; set; } = null!;

    public int IdEmpleado { get; set; }

    public string NumEmpleado { get; set; } = null!;

    public string ApePaterno { get; set; } = null!;

    public string? ApeMaterno { get; set; }

    public string Nombre { get; set; } = null!;

    public int? IdTipoNomina { get; set; }

    public int EstatusEmpleado { get; set; }

    public int EstatusGafete { get; set; }
}
