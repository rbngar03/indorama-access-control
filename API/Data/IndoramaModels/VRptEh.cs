﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class VRptEh
{
    public int IdMarcajes { get; set; }

    public int? IdGafete { get; set; }

    public int? IdGafeteTmp { get; set; }

    public int? IdContratistas { get; set; }

    public string? EntradaSalida { get; set; }

    public int? IdClasificacion { get; set; }

    public string? Clasificacion { get; set; }

    public double? SegundosCubiertos { get; set; }

    public int? IdCcostos { get; set; }

    public string? Ccostos { get; set; }

    public DateTime? FechaRegistro { get; set; }

    public int? IdTipoPersonal { get; set; }

    public string Personal { get; set; } = null!;

    public int? IdMarcajesEntrada { get; set; }

    public bool? AccesoOtorgado { get; set; }
}
