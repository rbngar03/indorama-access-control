﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class View1
{
    public string? Fecha { get; set; }

    public string? Entrada { get; set; }

    public string? Salida { get; set; }

    public string? Hrs { get; set; }

    public string Emleado { get; set; } = null!;
}
