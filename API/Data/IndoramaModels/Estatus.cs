﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Estatus
{
    public int IdEstatus { get; set; }

    public string? Estatus1 { get; set; }

    public virtual ICollection<Contratista> Contratista { get; set; } = new List<Contratista>();

    public virtual ICollection<Empleado> Empleados { get; set; } = new List<Empleado>();

    public virtual ICollection<Visitante> Visitantes { get; set; } = new List<Visitante>();

    public virtual ICollection<Gafete> Gafetes { get; set; } = new List<Gafete>();
}
