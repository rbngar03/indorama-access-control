﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Marcaje
{
    public int IdMarcajes { get; set; }

    public int? IdTerminales { get; set; }

    public DateTime? FechaRegistro { get; set; }

    public int? IdGafete { get; set; }

    public int? IdEmpleado { get; set; }

    public int? IdContratistas { get; set; }

    public int? IdVisitantes { get; set; }

    public int? IdTipoPersonal { get; set; }

    public string? EntradaSalida { get; set; }

    public bool? AccesoOtorgado { get; set; }

    public string? Campo { get; set; }

    public int? IdClasificacion { get; set; }

    public double? SegundosCubiertos { get; set; }

    public int? IdTipoLector { get; set; }

    public int? IdGafeteTmp { get; set; }

    public int? IdCompanias { get; set; }

    public int? IdCcostos { get; set; }

    public int? IdMarcajesEntrada { get; set; }

    public string? NombrePersonal { get; set; }

    public string? ResponsableInvista { get; set; }

    public virtual Ccosto? IdCcostosNavigation { get; set; }

    public virtual Clasificacion? IdClasificacionNavigation { get; set; }

    public virtual Companium? IdCompaniasNavigation { get; set; }

    public virtual Contratista? IdContratistasNavigation { get; set; }

    public virtual Empleado? IdEmpleadoNavigation { get; set; }

    public virtual Gafete? IdGafeteNavigation { get; set; }

    public virtual Gafete? IdGafeteTmpNavigation { get; set; }

    public virtual Terminale? IdTerminalesNavigation { get; set; }

    public virtual TipoLector? IdTipoLectorNavigation { get; set; }

    public virtual Personal? IdTipoPersonalNavigation { get; set; }

    public virtual Visitante? IdVisitantesNavigation { get; set; }
}
