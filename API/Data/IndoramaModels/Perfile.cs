﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Perfile
{
    public int IdPerfil { get; set; }

    public string? Clave { get; set; }

    public string? Descripcion { get; set; }

    public virtual ICollection<CamposPerfil> CamposPerfils { get; set; } = new List<CamposPerfil>();

    public virtual ICollection<Usuario> Usuarios { get; set; } = new List<Usuario>();
}
