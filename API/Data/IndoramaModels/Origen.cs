﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Origen
{
    public int IdOrigen { get; set; }

    public string? Origen1 { get; set; }

    public string? DescOrigen { get; set; }

    public DateTime? FechaActualizacion { get; set; }
}
