﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Contratista
{
    public int IdContratistas { get; set; }

    public int IdTipoContratista { get; set; }

    public string NombreContratista { get; set; } = null!;

    public string ApPaterno { get; set; } = null!;

    public string? ApMaterno { get; set; }

    public DateTime FechaIndSeguridad { get; set; }
    public string FechaIndSeguridadF => FechaIndSeguridad.ToString("dd/MM/yyyy");

    public DateTime FechaContrato { get; set; }
    public string FechaContratoF => FechaContrato.ToString("dd/MM/yyyy");

    public DateTime FechaTerminacion { get; set; }
    public string FechaTerminacionF => FechaTerminacion.ToString("dd/MM/yyyy");
    public string Imss { get; set; } = null!;

    public DateTime VigenciaSeguridad { get; set; }

    public DateTime VigenciaImss { get; set; }
    public string VigenciaImssF => VigenciaImss.ToString("dd/MM/yyyy");

    public string NumeroContrato { get; set; } = null!;

    public DateTime PeriodoVigencia { get; set; }

    public int? IdCompanias { get; set; }

    public string NombreCompañia { get; set; } = null!;

    public byte[]? Foto { get; set; }

    public DateTime? FechaBaja { get; set; }

    public string Rfc { get; set; } = null!;

    public int? IdClasificacion { get; set; }

    public string? ResponsableIndorama { get; set; }

    public int? IdCcostos { get; set; }

    public int? IdGafete { get; set; }

    public string? Notas { get; set; }

    public int? NumeroProvisionalGafete { get; set; }

    public int? IdComedor { get; set; }

    public string? ArchivoAdjunto { get; set; }

    public int IdEstatus { get; set; }

    public DateTime? FechaActualizacion { get; set; }

    public int? IdPlanta { get; set; }

    public virtual Ccosto? IdCcostosNavigation { get; set; }

    public virtual Clasificacion? IdClasificacionNavigation { get; set; }

    public virtual Comedor? IdComedorNavigation { get; set; }

    public virtual Companium? IdCompaniasNavigation { get; set; }

    public virtual Estatus IdEstatusNavigation { get; set; } = null!;

    public virtual Gafete? IdGafeteNavigation { get; set; }

    public virtual Plantum? IdPlantaNavigation { get; set; }

    public virtual TipoContratistum IdTipoContratistaNavigation { get; set; } = null!;

    public virtual ICollection<Marcaje> Marcajes { get; set; } = new List<Marcaje>();

    public virtual Gafete? NumeroProvisionalGafeteNavigation { get; set; }
}
