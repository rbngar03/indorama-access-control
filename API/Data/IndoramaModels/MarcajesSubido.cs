﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class MarcajesSubido
{
    public DateTime Fecha { get; set; }

    public int Empleado { get; set; }

    public int Lector { get; set; }
}
