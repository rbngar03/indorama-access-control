﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class MarcajesEmplStafe
{
    public string? Fecha { get; set; }

    public string? Hora { get; set; }

    public string? Cc { get; set; }

    public int Usuario { get; set; }

    public string? Nombre { get; set; }

    public int Lector { get; set; }
}
