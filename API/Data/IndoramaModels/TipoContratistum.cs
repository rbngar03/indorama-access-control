﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class TipoContratistum
{
    public int IdTipoContratista { get; set; }

    public string TipoContratista { get; set; } = null!;

    public string Descripcion { get; set; } = null!;

    public DateTime? FechaVigencia { get; set; }

    public virtual ICollection<Contratista> Contratista { get; set; } = new List<Contratista>();
}
