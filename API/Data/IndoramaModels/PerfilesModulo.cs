﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class PerfilesModulo
{
    public int IdPerfil { get; set; }

    public int IdModulo { get; set; }

    public int IdAccion { get; set; }

    public virtual Accione IdAccionNavigation { get; set; } = null!;

    public virtual Modulo IdModuloNavigation { get; set; } = null!;

    public virtual Perfile IdPerfilNavigation { get; set; } = null!;
}
