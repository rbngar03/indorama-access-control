﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Personal
{
    public int IdTipoPersonal { get; set; }

    public string Personal1 { get; set; } = null!;

    public virtual ICollection<Gafete> Gafetes { get; set; } = new List<Gafete>();

    public virtual ICollection<Marcaje> Marcajes { get; set; } = new List<Marcaje>();
}
