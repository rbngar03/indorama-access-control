﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Comedor
{
    public int IdComedor { get; set; }

    public string? DerechoComedor { get; set; }

    public virtual ICollection<Contratista> Contratista { get; set; } = new List<Contratista>();
}
