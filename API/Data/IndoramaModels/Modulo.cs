﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Modulo
{
    public int IdModulo { get; set; }

    public string? Nombre { get; set; }

    public int? IdPadre { get; set; }

    public string? Archivo { get; set; }

    public int? Orden { get; set; }

    public bool? HasEditableFields { get; set; }

    public virtual ICollection<CamposPerfil> CamposPerfils { get; set; } = new List<CamposPerfil>();
}
