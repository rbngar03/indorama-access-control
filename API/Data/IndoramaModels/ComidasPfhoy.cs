﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class ComidasPfhoy
{
    public string Empleado { get; set; } = null!;

    public DateTime? FechaVisita { get; set; }

    public decimal? Importe { get; set; }
}
