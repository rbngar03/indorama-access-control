﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class Plantum
{
    public int IdPlanta { get; set; }

    public string Clave { get; set; } = null!;

    public string Descripcion { get; set; } = null!;

    public string? Dato1 { get; set; }

    public string? Dato2 { get; set; }

    public string? Dato3 { get; set; }

    public string? Dato4 { get; set; }

    public string? Dato5 { get; set; }

    public string? Dato6 { get; set; }

    public string? Dato7 { get; set; }

    public string? Dato8 { get; set; }

    public virtual ICollection<Contratista> Contratista { get; set; } = new List<Contratista>();

    public virtual ICollection<Empleado> Empleados { get; set; } = new List<Empleado>();

    public virtual ICollection<Visitante> Visitantes { get; set; } = new List<Visitante>();
}
