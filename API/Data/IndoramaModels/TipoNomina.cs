﻿using System;
using System.Collections.Generic;

namespace DW_INDORAMA_API.Data.IndoramaModels;

public partial class TipoNomina
{
    public int IdTipoNomina { get; set; }

    public int TipoNomina1 { get; set; }

    public string Descripcion { get; set; } = null!;

    public DateTime? FechaActualizacion { get; set; }

    public virtual ICollection<Empleado> Empleados { get; set; } = new List<Empleado>();
}
