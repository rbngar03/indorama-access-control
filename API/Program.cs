using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Interfaces;
using DW_INDORAMA_API.Repositories;
using DW_INDORAMA_API.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAutoMapper(typeof(Program));
builder.Services.AddHttpClient();
builder.Services.AddControllersWithViews();
builder.Services.AddHttpContextAccessor();
builder.Services.AddScoped<IPermisosRepository, PermisosRepository>();
builder.Services.AddScoped<IBiostarService, BiostarService>();
builder.Services.AddScoped<ICredentialsRepository, CredentialsRepository>();


//DbContext
builder.Services.AddSqlServer<IndoramaAccessControlContext>(builder.Configuration.GetConnectionString("IndoramaConnection"));

var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
 // {    
  app.UseSwagger();
    app.UseSwaggerUI();    
//}

app.UseCors(x => x
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader());

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
