﻿using AutoMapper;
using DW_INDORAMA_API.Data.IndoramaModels;
using DW_INDORAMA_API.Dtos;

namespace DW_INDORAMA_API.Mapping
{
    public class AutoMapper : Profile
    {
        public AutoMapper()
        {
            CreateMap<PerfilDto, Perfile>().ReverseMap();
            CreateMap<PerfilesModulo, PerfilesModuloAddDeletedto>().ReverseMap();
            CreateMap<Accione, AccionDto>().ReverseMap();
            CreateMap<CamposPerfil,CamposEditablesDto>().ReverseMap();
            CreateMap<Empleado, CredencialResponse>()
               .ForMember(p => p.identificador, x => x.MapFrom(c => c.NumEmpleado))
               .ForMember(p => p.numeroGafete, x => x.MapFrom(c => c.IdGafeteNavigation.Gafete1))
               .ForMember(p => p.NumeroProvisionalGafete, x => x.MapFrom(c => c.NumeroProvisionalGafeteNavigation.Gafete1))
               .ForMember(p => p.fechaVigencia, x => x.MapFrom(c => c.NumeroProvisionalGafeteNavigation.FechaVigencia))
               .ForMember(p => p.apellidos, x => x.MapFrom(c => c.ApeMaterno + " " + c.ApeMaterno))
               .ForMember(p => p.isUserActive, x => x.MapFrom(c => c.IdEstatusNavigation.Estatus1.ToUpper() == "ACTIVO"))
               .ForMember(p => p.isCredentialHabilited, x => x.MapFrom(c => c.IdGafeteNavigation.IdEstatusNavigation.Estatus1 == "ACTIVO"))
               .ForMember(p => p.isCredentialProvisionalHabilited, x => x.MapFrom(c => c.NumeroProvisionalGafeteNavigation.IdEstatusNavigation.Estatus1 == "ACTIVO"));
            CreateMap<Contratista, CredencialResponse>()
                .ForMember(p => p.Nombre, x => x.MapFrom(c => c.NombreContratista))
                .ForMember(p => p.identificador, x => x.MapFrom(c => c.Rfc))
                .ForMember(p => p.numeroGafete, x => x.MapFrom(c => c.IdGafeteNavigation.Gafete1))
                .ForMember(p => p.NumeroProvisionalGafete, x => x.MapFrom(c => c.NumeroProvisionalGafeteNavigation.Gafete1))
                .ForMember(p => p.fechaVigencia, x => x.MapFrom(c => c.NumeroProvisionalGafeteNavigation.FechaVigencia))
                .ForMember(p => p.apellidos, x => x.MapFrom(c => c.ApPaterno + " " + c.ApMaterno))
                .ForMember(p => p.isUserActive, x => x.MapFrom(c => c.IdEstatusNavigation.Estatus1.ToUpper() == "ACTIVO"))
                .ForMember(p => p.isCredentialHabilited, x => x.MapFrom(c => c.IdGafeteNavigation.IdEstatusNavigation.Estatus1.ToUpper() == "ACTIVO"))
                .ForMember(p => p.isCredentialProvisionalHabilited, x => x.MapFrom(c => c.NumeroProvisionalGafeteNavigation.IdEstatusNavigation.Estatus1 == "ACTIVO"));
            CreateMap<Visitante, CredencialResponse>()
                .ForMember(p => p.Nombre, x => x.MapFrom(c => c.NombreVisitante))
                .ForMember(p => p.identificador, x => x.MapFrom(c => c.Rfc))
                .ForMember(p => p.numeroGafete, x => x.MapFrom(c => c.IdGafeteNavigation.Gafete1))
                .ForMember(p => p.isUserActive, x => x.MapFrom(c => c.IdEstatusNavigation.Estatus1.ToUpper() == "ACTIVO"))
                .ForMember(p => p.fechaVigencia, x => x.MapFrom(c => c.IdGafeteNavigation.FechaVigencia));
        }

    }
}
