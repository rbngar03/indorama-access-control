﻿using Azure;
using DW_INDORAMA_API.Dtos;
using DW_INDORAMA_API.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.Text.Json;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace DW_INDORAMA_API.Services
{
    public class BiostarService : IBiostarService
    {
        private readonly HttpClient _httpClient;
        public BiostarService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task LoginAsync(string loginId, string password)
        {
            var loginData = new
            {
                User = new { login_id = loginId, password }
            };
            var json = JsonSerializer.Serialize(loginData);
            var contenido = new StringContent(json, Encoding.UTF8, "application/json");
            var respuesta = await _httpClient.PostAsync("https://192.168.10.20/api/login", contenido);
            if (respuesta.IsSuccessStatusCode)
            {
                var sesionId = respuesta.Headers.GetValues("bs-session-id").FirstOrDefault();
                _httpClient.DefaultRequestHeaders.Add("bs-session-id", sesionId);
            }
            else
            {
                // Maneja el error de autenticación
            }

        }

        public async Task LogoutAsync()
        {
            var respuesta = await _httpClient.PostAsync("https://192.168.10.20/api/logout", null);
            _httpClient.Dispose();
        }

        public Task UserCredential()
        {
            throw new NotImplementedException();
        }

        public async Task UserCredentialAdd(int idUsuario, int idCredential)
        {
            var userData = new
            {
                User = new { cards = new[] { new { id = "7" } } }
            };
            var json = JsonSerializer.Serialize(userData);
            var contenido = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync("https://192.168.10.20/api/users/" + idUsuario, contenido);
            Console.WriteLine("Enpoint para añadir credencial", response.Content);
        }

        public async Task<CardDetail> ViewCardDetail(string query)
        {
            var response = await _httpClient.GetAsync("https://192.168.10.20/api/cards?query=" + query);
            Console.WriteLine(response.Content.ReadFromJsonAsync<CardDetail>());
            var content = response.Content.ReadFromJsonAsync<CardDetail>().Result;
            return null;
        }


    }
}
