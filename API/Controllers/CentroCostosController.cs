﻿using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using Microsoft.AspNetCore.Mvc;

namespace DW_INDORAMA_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CentroCostosController : ControllerBase
    {
        private readonly IndoramaAccessControlContext _context;
        public CentroCostosController(IndoramaAccessControlContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Ccosto> Get()
        {
            return _context.Ccostos.ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Ccosto> GetById(int id)
        {
            var idCcostos = _context.Ccostos.Find(id);

            if (idCcostos is null)
                return StatusCode(StatusCodes.Status404NotFound, new { mensaje = "No se encontro el Centro de Costos" });
            return Ok(idCcostos);
        }
    }
}
