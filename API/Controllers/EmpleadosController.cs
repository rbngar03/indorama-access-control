﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.InkML;
using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.UniversalAccessibility.Drawing;
using System.Collections;
using System.Data;
using System.Net.NetworkInformation;

namespace DW_INDORAMA_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmpleadosController : ControllerBase
    {
        private readonly IndoramaAccessControlContext _context;
        private readonly string cadenaSQL;

        public EmpleadosController(IndoramaAccessControlContext context, IConfiguration configuracion)
        {
            _context = context;
            cadenaSQL = configuracion.GetConnectionString("IndoramaConnection");
        }

        [HttpGet]
        public ActionResult Empleados(int? pag, int? reg, int IdEstatus, string? Planta, string? Ccostos, int IdEmpleado, string? Filtro)
        {
            List<Empleado> empleados = new List<Empleado>();

            try
            {
                using (var conexion = new SqlConnection(cadenaSQL))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("DTW_SP_Empleados", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = IdEstatus;
                    cmd.Parameters.Add("@pPlanta", SqlDbType.VarChar).Value = Planta;
                    cmd.Parameters.Add("@pCentroCostos", SqlDbType.VarChar).Value = Ccostos;
                    cmd.Parameters.Add("@pIdEmpleado", SqlDbType.Int).Value = IdEmpleado;
                    cmd.Parameters.Add("@pFiltro", SqlDbType.VarChar).Value = Filtro;
                    cmd.Parameters.Add("@pPAG", SqlDbType.Int).Value = pag ?? 0;
                    cmd.Parameters.Add("@pREG", SqlDbType.Int).Value = reg ?? 50;
                    cmd.Parameters.Add("@pExportar", SqlDbType.Bit).Value = 0;
                    using (var rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            empleados.Add(new Empleado()
                            {
                                IdEmpleado = Convert.ToInt32(rd["IdEmpleado"]),
                                NumEmpleado = rd["NumEmpleado"].ToString(),
                                IdEstatus = Convert.ToInt32(rd["IdEstatus"]),
                                //Nombre = (rd["Nombre"].ToString() + " " + rd["apePaterno"].ToString() + " " + rd["apeMaterno"].ToString()),
                                Nombre = rd["Nombre"].ToString(),
                                ApePaterno = rd["apePaterno"].ToString(),
                                ApeMaterno = rd["apeMaterno"].ToString(),
                                IdCcostos = Convert.ToInt32(rd["CentroCostos"]),
                                IdGafete = Convert.ToInt32(rd["Gafete"]),
                                Foto = rd.IsDBNull("Foto") ? null : (byte[])rd["Foto"],
                                IdPlanta = Convert.ToInt32(rd["IdPlanta"])
                            });
                        }
                    }
                }
                if (empleados.Count != 0)
                {
                    return Ok(new { response = empleados });

                }

                return StatusCode(StatusCodes.Status404NotFound, new { mensaje = "No se encontraron resulatos" });
            }
            catch (Exception error)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message, response = empleados });
            }
        }

        [HttpGet("{idEmpleado}")]
        public ActionResult Empleado(int idEmpleado)
        {
            List<Empleado> empleados = new List<Empleado>();
            Empleado empleado = new Empleado();

            try
            {
                using (var conexion = new SqlConnection(cadenaSQL))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("DTW_SP_Empleados", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pIdEmpleado", SqlDbType.Int).Value = idEmpleado;
                    using (var rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            empleados.Add(new Empleado()
                            {
                                IdEmpleado = Convert.ToInt32(rd["IdEmpleado"]),
                                NumEmpleado = rd["NumEmpleado"].ToString(),
                                IdEstatus = Convert.ToInt32(rd["IdEstatus"]),
                                Nombre = rd["Nombre"].ToString(),
                                ApePaterno = rd["apePaterno"].ToString(),
                                ApeMaterno = rd["apeMaterno"].ToString(),
                                IdCcostos = Convert.ToInt32(rd["CentroCostos"]),
                                IdGafete = Convert.ToInt32(rd["Gafete"]),
                                Foto = rd.IsDBNull("Foto") ? null : (byte[])rd["Foto"],
                                IdPlanta = Convert.ToInt32(rd["IdPlanta"])
                            }); ;
                        }
                    }
                }
                empleado = empleados.Where(item => item.IdEmpleado == idEmpleado).FirstOrDefault();

                if (empleado != null)
                {
                    return StatusCode(StatusCodes.Status200OK, new { response = empleado });
                }
                return StatusCode(StatusCodes.Status404NotFound, new { mensaje = "No se encontro empleado" });
            }
            catch (Exception error)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message });
            }
        }

        [HttpPut]
        public ActionResult EditarEmpleados(int IdEmpleado, int NumEmpleado, string ApePaterno, string ApeMaterno, string Nombre, int idCcostos,
            int IdEstatus, string Gafete, string? NumeroProvisionalGafete, int IdPlanta)
        {
            //byte[] photo = System.Text.Encoding.UTF8.GetBytes(Foto);
            try
            {

                using (var conexion = new SqlConnection(cadenaSQL))
                {

                    conexion.Open();
                    var cmd = new SqlCommand("DTW_SP_ModificaEmpleado", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pIdEmpleado", SqlDbType.Int).Value = IdEmpleado;
                    cmd.Parameters.Add("@pNumEmpleado", SqlDbType.Int).Value = NumEmpleado;
                    cmd.Parameters.Add("@pApePaterno", SqlDbType.VarChar).Value = ApePaterno;
                    cmd.Parameters.Add("@pApeMaterno", SqlDbType.VarChar).Value = ApeMaterno;
                    cmd.Parameters.Add("@pNombre", SqlDbType.VarChar).Value = Nombre;
                    cmd.Parameters.Add("@pidCcostos", SqlDbType.Int).Value = idCcostos;
                    cmd.Parameters.Add("@pidEstatus", SqlDbType.Int).Value = IdEstatus;
                    cmd.Parameters.Add("@pGafete", SqlDbType.VarChar).Value = Gafete;
                    //cmd.Parameters.Add("@pFoto", SqlDbType.Image).Value = photo is null ? DBNull.Value : photo;
                    cmd.Parameters.Add("@pNumeroProvisionalGafete", SqlDbType.VarChar).Value = NumeroProvisionalGafete is null ? DBNull.Value : NumeroProvisionalGafete;
                    cmd.Parameters.Add("@pidPlanta", SqlDbType.Int).Value = IdPlanta;
                    cmd.Parameters.Add("Response", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteReader();
                    string Response = (string)cmd.Parameters["Response"].Value;
                    if (Response != "Ok")
                    {
                        return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = Response });
                    }
                }
                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Datos actualizados" });
            }
            catch (Exception error)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message });

            }

        }

        [HttpGet("exportar/Excel")]
        public async Task<IActionResult> CatalogoEmpleadosExcel(int IdEstatus, string? Planta, string? Ccostos, int IdEmpleado, string? Filtro)
        {
            List<Empleado> empleados = new List<Empleado> ();
            var ccostos = await _context.Ccostos.ToListAsync();
            var plantas = await _context.Planta.ToArrayAsync();

            try
            {
                using (var conexion = new SqlConnection(cadenaSQL))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("DTW_SP_Empleados", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = IdEstatus;
                    cmd.Parameters.Add("@pPlanta", SqlDbType.VarChar).Value = Planta;
                    cmd.Parameters.Add("@pCentroCostos", SqlDbType.VarChar).Value = Ccostos;
                    cmd.Parameters.Add("@pIdEmpleado", SqlDbType.Int).Value = IdEmpleado;
                    cmd.Parameters.Add("@pFiltro", SqlDbType.VarChar).Value = Filtro;
                    cmd.Parameters.Add("@pPAG", SqlDbType.Int).Value =  0;
                    cmd.Parameters.Add("@pREG", SqlDbType.Int).Value = 0;
                    cmd.Parameters.Add("@pExportar", SqlDbType.Bit).Value = 1;

                    using (var rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            empleados.Add(new Empleado()
                            {
                                IdEmpleado = Convert.ToInt32(rd["IdEmpleado"]),
                                NumEmpleado = rd["NumEmpleado"].ToString(),
                                IdEstatus = Convert.ToInt32(rd["IdEstatus"]),
                                Nombre = rd["Nombre"].ToString(),
                                ApePaterno = rd["apePaterno"].ToString(),
                                ApeMaterno = rd["apeMaterno"].ToString(),
                                IdCcostos = Convert.ToInt32(rd["CentroCostos"]),
                                IdGafete = Convert.ToInt32(rd["Gafete"]),
                                IdPlanta = Convert.ToInt32(rd["IdPlanta"])
                            });
                        }
                    }
                }
                if (empleados.Count != 0)
                {
                    // Se crrea el archivo Excel
                    using (var workbook = new XLWorkbook())
                    {
                        var worksheet = workbook.Worksheets.Add("Empleados");

                        // Se agrean encabezados
                        worksheet.Cell(1, 1).Value = "No. Empleado";
                        worksheet.Cell(1, 2).Value = "Estatus";
                        worksheet.Cell(1, 3).Value = "Nombre Empleado";
                        worksheet.Cell(1, 4).Value = "Centro de Costos";
                        worksheet.Cell(1, 5).Value = "Gafete";
                        worksheet.Cell(1, 6).Value = "Planta";

                        // Se agrega el filtro
                        worksheet.Range("A1:F1").SetAutoFilter();

                        // Se llenan los datos
                        for (int i = 0; i < empleados.Count; i++)
                        {
                            var emp = empleados[i];
                            var ccosto = ccostos.FirstOrDefault(c => c.IdCcostos == emp.IdCcostos);
                            var planta = plantas.FirstOrDefault(p => p.IdPlanta == emp.IdPlanta);

                            worksheet.Cell(i + 2, 1).Value = emp.NumEmpleado;
                            worksheet.Cell(i + 2, 2).Value = emp.IdEstatus == 1 ? "Activo" : "Inactivo";
                            worksheet.Cell(i + 2, 3).Value = $"{emp.Nombre}{emp.ApePaterno}{emp.ApeMaterno}";
                            worksheet.Cell(i + 2, 4).Value = ccosto != null ? ccosto.Ccostos : "";
                            worksheet.Cell(i + 2, 5).Value = emp.IdGafete == 0 ? "" : emp.IdGafete;
                            worksheet.Cell(i + 2, 6).Value = planta != null ? planta.Descripcion : "";
                        }

                        // Se ajusta el tamaño de las columnas
                        worksheet.Columns().AdjustToContents();

                        using (var stream = new MemoryStream())
                        {
                            workbook.SaveAs(stream);
                            var content = stream.ToArray();
                            return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Empleados.xlsx");
                        }
                    }
                }

                return NotFound(new { mensaje = "No se encontraron resultados" });
            }
            catch (Exception error)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message });
            }
        }

        [HttpGet("exportar/PDF")]
        public async Task<IActionResult> CatalogoEmpleadosPDF(int IdEstatus, string? Planta, string? Ccostos, int IdEmpleado, string? Filtro)
        {
            List<Empleado> empleados = new List<Empleado>();
            var ccostos = await _context.Ccostos.ToListAsync();
            var plantas = await _context.Planta.ToArrayAsync();

            try
            {
                using (var conexion = new SqlConnection(cadenaSQL))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("DTW_SP_Empleados", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = IdEstatus;
                    cmd.Parameters.Add("@pPlanta", SqlDbType.VarChar).Value = Planta;
                    cmd.Parameters.Add("@pCentroCostos", SqlDbType.VarChar).Value = Ccostos;
                    cmd.Parameters.Add("@pIdEmpleado", SqlDbType.Int).Value = IdEmpleado;
                    cmd.Parameters.Add("@pFiltro", SqlDbType.VarChar).Value = Filtro;
                    cmd.Parameters.Add("@pPAG", SqlDbType.Int).Value = 0;
                    cmd.Parameters.Add("@pREG", SqlDbType.Int).Value = 0;
                    cmd.Parameters.Add("@pExportar", SqlDbType.Bit).Value = 1;

                    using (var rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            empleados.Add(new Empleado()
                            {
                                IdEmpleado = Convert.ToInt32(rd["IdEmpleado"]),
                                NumEmpleado = rd["NumEmpleado"].ToString(),
                                IdEstatus = Convert.ToInt32(rd["IdEstatus"]),
                                Nombre = rd["Nombre"].ToString(),
                                ApePaterno = rd["apePaterno"].ToString(),
                                ApeMaterno = rd["apeMaterno"].ToString(),
                                IdCcostos = Convert.ToInt32(rd["CentroCostos"]),
                                IdGafete = Convert.ToInt32(rd["Gafete"]),
                                IdPlanta = Convert.ToInt32(rd["IdPlanta"])
                            });
                        }
                    }
                }

                if (empleados.Count != 0)
                {
                    using (var stream = new MemoryStream())
                    {
                        using (var pdfDocument = new PdfDocument())
                        {
                            var font = new XFont("Arial", 8, XFontStyleEx.Regular);
                            var titleFont = new XFont("Verdana", 18, XFontStyleEx.Bold);
                            int recordsPerPage = 35; // Número de registros por página
                            int yPoint = 130; 
                            XGraphics gfx = null;

                            void AddPage()
                            {
                                var page = pdfDocument.AddPage();
                                gfx = XGraphics.FromPdfPage(page);

                                // Se agrega título
                                gfx.DrawString("Catalogo de Empleados", titleFont, XBrushes.Black, new XRect(0, 30, page.Width, page.Height), XStringFormats.TopCenter);

                                // Encabezados
                                gfx.DrawLine(XPens.Black, 20, 100, 580, 100);
                                gfx.DrawString("No. Empleado", font, XBrushes.Black, 25, 113);
                                gfx.DrawString("Estatus", font, XBrushes.Black, 85, 113);
                                gfx.DrawString("Nombre Empleado", font, XBrushes.Black, 230, 113);
                                gfx.DrawString("Centro", font, XBrushes.Black, 415, 109);
                                gfx.DrawString("de Costos", font, XBrushes.Black, 410, 119);
                                gfx.DrawString("Gafete", font, XBrushes.Black, 465, 113);
                                gfx.DrawString("Planta", font, XBrushes.Black, 525, 113);
                                gfx.DrawLine(XPens.Black, 20, 120, 580, 120);

                                yPoint = 130; // Se reinicia yPoint para la nueva página
                            }

                            void DrawVerticalLines(XGraphics gfx, int currentYPoint)
                            {
                                if (currentYPoint > 130) 
                                {
                                    var height = currentYPoint - 9;
                                    gfx.DrawLine(XPens.Black, 20, 100, 20, height);
                                    gfx.DrawLine(XPens.Black, 80, 100, 80, height); 
                                    gfx.DrawLine(XPens.Black, 120, 100, 120, height); 
                                    gfx.DrawLine(XPens.Black, 395, 100, 395, height);
                                    gfx.DrawLine(XPens.Black, 460, 100, 460, height);
                                    gfx.DrawLine(XPens.Black, 497, 100, 497, height);
                                    gfx.DrawLine(XPens.Black, 395, 100, 395, height);
                                    gfx.DrawLine(XPens.Black, 580, 100, 580, height);

                                }
                            }

                            AddPage(); 

                            foreach (var emp in empleados)
                            {
                                if ((yPoint - 130) / 20 >= recordsPerPage)
                                {
                                    DrawVerticalLines(gfx, yPoint);
                                    AddPage(); 
                                }

                                // Se agregan datos
                                gfx.DrawString(emp.NumEmpleado, font, XBrushes.Black, 35, yPoint);
                                gfx.DrawString(emp.IdEstatus == 1 ? "Activo" : "Inactivo", font, XBrushes.Black, 85, yPoint);
                                gfx.DrawString($"{emp.Nombre} {emp.ApePaterno} {emp.ApeMaterno}", font, XBrushes.Black, 125, yPoint);

                                var ccosto = ccostos.FirstOrDefault(c => c.IdCcostos == emp.IdCcostos);
                                string textoCcosto = string.IsNullOrEmpty(ccosto?.Ccostos) ? "" : ccosto.Ccostos.ToString();
                                double textoCcostoAncho = gfx.MeasureString(textoCcosto, font).Width;
                                double xCcostoPos = 400 + (55 - textoCcostoAncho) / 2;
                                gfx.DrawString(textoCcosto, font, XBrushes.Black, xCcostoPos, yPoint);

                                gfx.DrawString(emp.IdGafete == 0 ? "" : emp.IdGafete.ToString(), font, XBrushes.Black, 465, yPoint);

                                var planta = plantas.FirstOrDefault(pl => pl.IdPlanta == emp.IdPlanta);
                                gfx.DrawString(planta != null ? planta.Descripcion : "", font, XBrushes.Black, 500, yPoint);

                                // Línea después de cada registro
                                gfx.DrawLine(XPens.Black, 20, yPoint + 10, 580, yPoint + 10);
                                yPoint += 20;
                            }

                            // Líneas verticales en la última página
                            DrawVerticalLines(gfx, yPoint);

                            // Se guarda el documento PDF en el MemoryStream
                            pdfDocument.Save(stream);
                        }

                        var content = stream.ToArray();
                        return File(content, "application/pdf", "Empleados.pdf");
                    }
                }

                return NotFound(new { mensaje = "No se encontraron resultados" });
            }
            catch (Exception error)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message });
            }
        }
    }
}
