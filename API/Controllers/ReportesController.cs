﻿using ClosedXML.Excel;
using DW_INDORAMA_API.Dtos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp;
using System.Data;

namespace DW_INDORAMA_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReportesController : ControllerBase
    {
        private readonly string cadenaSQL;

        public ReportesController(IConfiguration configuration)
        {
            cadenaSQL = configuration.GetConnectionString("IndoramaConnection");
        }

        private async Task<ActionResult<IEnumerable<ReporteEmpleadoDto>>> ObtenerReporteEmpleado(DateTime StartDate, DateTime EndDate, int idplanta, string? Filtro)
        {
            var resultados = new List<ReporteEmpleadoDto>();

            using (var connection = new SqlConnection(cadenaSQL))
            {
                await connection.OpenAsync();

                using (var command = new SqlCommand("DTW_SP_ReporteEntradasSalidasEmpleados", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@pStartDate", StartDate);
                    command.Parameters.AddWithValue("@pEndDate", EndDate);
                    command.Parameters.AddWithValue("@pIdPlant", idplanta);
                    command.Parameters.AddWithValue("@pNameOrNumber", Filtro ?? (object)DBNull.Value);

                    using (var rd = await command.ExecuteReaderAsync())
                    {
                        try
                        {
                            while (await rd.ReadAsync())
                            {
                                var reporte = new ReporteEmpleadoDto
                                {
                                    Entrada = rd.IsDBNull(rd.GetOrdinal("FechaEntrada")) ? default : rd.GetDateTime(rd.GetOrdinal("FechaEntrada")),
                                    Salida = rd.IsDBNull(rd.GetOrdinal("FechaSalida")) ? default : rd.GetDateTime(rd.GetOrdinal("FechaSalida")),
                                    NombreEmpleado = rd.IsDBNull(rd.GetOrdinal("NombreEmpleado")) ? null : rd.GetString(rd.GetOrdinal("NombreEmpleado")),
                                    NumeroEmpleado = rd.IsDBNull(rd.GetOrdinal("NumEmpleado")) ? null : rd.GetString(rd.GetOrdinal("NumEmpleado")),
                                    Planta = rd.IsDBNull(rd.GetOrdinal("Descripcion")) ? null : rd.GetString(rd.GetOrdinal("Descripcion")),
                                };

                                resultados.Add(reporte);
                            }
                        }
                        catch (Exception ex)
                        {
                            return BadRequest($"Error al procesar los datos: {ex.Message}");
                        }
                    }
                }
            }

            return Ok(resultados);
        }

        [HttpGet("ReporteEmpleados")]
        public async Task<ActionResult<IEnumerable<ReporteEmpleadoDto>>> ReporteEmpleados(DateTime StartDate, DateTime EndDate, int idplanta, string? Filtro)
        {
            return await ObtenerReporteEmpleado(StartDate, EndDate, idplanta, Filtro);
        }


        [HttpGet("ReporteEmpleados/excel")]
        public async Task<IActionResult> ReporteEmpleadosExcel(DateTime StartDate, DateTime EndDate, int idplanta, string? Filtro)
        {
            var resultados = await ObtenerReporteEmpleado(StartDate, EndDate, idplanta, Filtro);

            if (resultados.Result is BadRequestObjectResult badRequestResult)
            {
                return BadRequest(badRequestResult.Value);
            }

            var reportes = resultados.Result as OkObjectResult;
            if (reportes == null || reportes.Value == null)
            {
                return BadRequest("No se encontraron datos.");
            }

            var empleados = reportes.Value as IEnumerable<ReporteEmpleadoDto>;

            var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add("Reporte Empleados");

            // Agregar título y fusionar celdas de A1 a E1
            worksheet.Range("A1:E1").Merge().Value = "Reporte Empleados";
            worksheet.Cell(1, 1).Style.Font.Bold = true;
            worksheet.Cell(1, 1).Style.Font.FontSize = 20;
            worksheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            // Agregar fechas
            worksheet.Cell(2, 1).Value = $"Fecha Inicial: {StartDate:dd/MM/yyyy}";
            worksheet.Cell(3, 1).Value = $"Fecha Final: {EndDate:dd/MM/yyyy}";

            // Agregar encabezados
            worksheet.Cell(5, 1).Value = "Entrada";
            worksheet.Cell(5, 2).Value = "Salida";
            worksheet.Cell(5, 3).Value = "Nombre Empleado";
            worksheet.Cell(5, 4).Value = "No. Empleado";
            worksheet.Cell(5, 5).Value = "Sitio";

            // Se agrega el filtro
            worksheet.Range("A5:E5").SetAutoFilter();

            // Agregar datos
            var row = 6; // Cambia a 6 para dejar espacio para el título y las fechas
            foreach (var item in empleados)
            {
                worksheet.Cell(row, 1).Value = item.Entrada;
                worksheet.Cell(row, 2).Value = item.Salida;
                worksheet.Cell(row, 3).Value = item.NombreEmpleado;
                worksheet.Cell(row, 4).Value = Convert.ToDecimal(item.NumeroEmpleado);
                worksheet.Cell(row, 5).Value = item.Planta;
                row++;
            }

            // Formato de fecha
            worksheet.Column(1).Style.DateFormat.Format = "dd/MM/yyyy HH:mm";
            worksheet.Column(2).Style.DateFormat.Format = "dd/MM/yyyy HH:mm";

            // Autoajustar el ancho de las columnas después de aplicar el formato
            worksheet.Columns().AdjustToContents();

            using (var stream = new MemoryStream())
            {
                workbook.SaveAs(stream);
                var content = stream.ToArray();
                return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ReporteEmpleados.xlsx");
            }
        }


        [HttpGet("ReporteEmpleados/pdf")]
        public async Task<IActionResult> ReporteEmpleadosPDF(DateTime StartDate, DateTime EndDate, int idplanta, string? Filtro)
        {
            var resultados = await ObtenerReporteEmpleado(StartDate, EndDate, idplanta, Filtro);

            if (resultados.Result is BadRequestObjectResult badRequestResult)
            {
                return BadRequest(badRequestResult.Value);
            }

            var reportes = resultados.Result as OkObjectResult;
            if (reportes == null || reportes.Value == null)
            {
                return BadRequest("No se encontraron datos.");
            }

            var empleados = reportes.Value as IEnumerable<ReporteEmpleadoDto>;

            using (var stream = new MemoryStream())
            {
                using (var pdfDocument = new PdfDocument())
                {
                    var font = new XFont("Arial", 9, XFontStyleEx.Regular);
                    var titleFont = new XFont("Verdana", 18, XFontStyleEx.Bold);
                    var titleFont2 = new XFont("Verdana", 9, XFontStyleEx.Bold);
                    int recordsPerPage = 35; // Número de registros por página
                    int yPoint = 130; 
                    const int pageHeight = 840;

                    XGraphics gfx = null;

                    void AddPage()
                    {
                        var page = pdfDocument.AddPage();
                        gfx = XGraphics.FromPdfPage(page);

                        // Se agrega título
                        gfx.DrawString("Reporte Empleados", titleFont, XBrushes.Black, new XRect(0, 30, page.Width, page.Height), XStringFormats.TopCenter);

                        // Se agregan rango de fechas
                        gfx.DrawString($"Fecha Inicial:  {StartDate:dd/MM/yyyy}", titleFont2, XBrushes.Black, 40, 70);
                        gfx.DrawString($"Fecha Final:    {EndDate:dd/MM/yyyy}", titleFont2, XBrushes.Black, 40, 85);

                        // Encabezados
                        gfx.DrawLine(XPens.Black, 30, 100, 560, 100);
                        gfx.DrawString("Entrada", font, XBrushes.Black, 55, 113);
                        gfx.DrawString("Salida", font, XBrushes.Black, 138, 113);
                        gfx.DrawString("Nombre Empleado", font, XBrushes.Black, 275, 113);
                        gfx.DrawString("No.", font, XBrushes.Black, 440, 109);
                        gfx.DrawString("Empleado", font, XBrushes.Black, 428, 117);
                        gfx.DrawString("Sitio", font, XBrushes.Black, 510, 113);
                        gfx.DrawLine(XPens.Black, 30, 120, 560, 120);


                        yPoint = 130; // Se reinicia yPoint para la nueva página
                    }

                    AddPage(); 

                    foreach (var item in empleados)
                    {
                        if ((yPoint - 130) / 20 >= recordsPerPage)
                        {
                            DrawVerticalLines(gfx, yPoint);
                            AddPage();
                        }

                        // Se agregan datos
                        gfx.DrawString(item.Entrada.ToString("dd/MM/yyyy HH:mm"), font, XBrushes.Black, 35, yPoint);
                        gfx.DrawString(item.Salida.ToString("dd/MM/yyyy HH:mm"), font, XBrushes.Black, 115, yPoint);
                        gfx.DrawString(item.NombreEmpleado, font, XBrushes.Black, 193, yPoint);
                        gfx.DrawString(item.NumeroEmpleado, font, XBrushes.Black, 433, yPoint);
                        gfx.DrawString(item.Planta, font, XBrushes.Black, 478, yPoint);

                        // Línea después de cada registro
                        gfx.DrawLine(XPens.Black, 30, yPoint + 10, 560, yPoint + 10);
                        yPoint += 20;
                    }

                    // Líneas verticales en la última página
                    DrawVerticalLines(gfx, yPoint);

                    // Se guarda el documento PDF en el MemoryStream
                    pdfDocument.Save(stream);
                }

                var content = stream.ToArray();
                return File(content, "application/pdf", "ReporteEmpleados.pdf");
            }

            void DrawVerticalLines(XGraphics gfx, int currentYPoint)
            {
                if (currentYPoint > 130) 
                {
                    var height = currentYPoint - 9;
                    gfx.DrawLine(XPens.Black, 30, 100, 30, height); 
                    gfx.DrawLine(XPens.Black, 110, 100, 110, height); 
                    gfx.DrawLine(XPens.Black, 190, 100, 190, height); 
                    gfx.DrawLine(XPens.Black, 425, 100, 425, height); 
                    gfx.DrawLine(XPens.Black, 470, 100, 470, height); 
                    gfx.DrawLine(XPens.Black, 560, 100, 560, height); 
                }
            }
        }


        private async Task<ActionResult<IEnumerable<ReporteContratistaDto>>> ObtenerReporteContratista(string FechaInicial, string FechaFinal, string? Empresa, string? Filtro)
        {
            var resultados = new List<ReporteContratistaDto>();

            using (var connection = new SqlConnection(cadenaSQL))
            {
                await connection.OpenAsync();

                using (var command = new SqlCommand("DTW_SP_ReporteMarcajesContratistas", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@pFechaInicial", FechaInicial);
                    command.Parameters.AddWithValue("@pFechaFinal", FechaFinal);
                    command.Parameters.AddWithValue("@pEmpresa", Empresa ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@pNombreContratista", Filtro ?? (object)DBNull.Value);

                    using (var rd = await command.ExecuteReaderAsync())
                    {
                        try
                        {
                            while (await rd.ReadAsync())
                            {
                                var reporte = new ReporteContratistaDto
                                {
                                    Entrada = rd.IsDBNull(rd.GetOrdinal("FechaEntrada")) ? null : rd.GetDateTime(rd.GetOrdinal("FechaEntrada")).ToString("yyyy-MM-dd HH:mm:ss"),
                                    Salida = rd.IsDBNull(rd.GetOrdinal("FechaSalida")) ? null : rd.GetDateTime(rd.GetOrdinal("FechaSalida")).ToString("yyyy-MM-dd HH:mm:ss"),
                                    EmpresaContratista = rd.IsDBNull(rd.GetOrdinal("EmpresaContratista")) ? null : rd.GetString(rd.GetOrdinal("EmpresaContratista")),
                                    NombreContratista = rd.IsDBNull(rd.GetOrdinal("NombreContratista")) ? null : rd.GetString(rd.GetOrdinal("NombreContratista")),
                                };

                                resultados.Add(reporte);
                            }
                        }
                        catch (Exception ex)
                        {
                            return BadRequest($"Error al procesar los datos: {ex.Message}");
                        }
                    }
                }
            }

            return Ok(resultados);
        }

        [HttpGet("ReporteContratistas")]
        public async Task<ActionResult<IEnumerable<ReporteContratistaDto>>> ReporteContratistas(string FechaInicial, string FechaFinal, string? Empresa, string? Filtro)
        {
            return await ObtenerReporteContratista(FechaInicial, FechaFinal, Empresa, Filtro);
        }

        [HttpGet("ReporteContratistas/excel")]
        public async Task<IActionResult> ReporteContratistasExcel(string FechaInicial, string FechaFinal, string? Empresa, string? Filtro)
        {
            var resultados = await ObtenerReporteContratista(FechaInicial, FechaFinal, Empresa, Filtro);

            if (resultados.Result is BadRequestObjectResult badRequestResult)
            {
                return BadRequest(badRequestResult.Value);
            }

            var reportes = resultados.Result as OkObjectResult;
            if (reportes == null || reportes.Value == null)
            {
                return BadRequest("No se encontraron datos.");
            }

            var empleados = reportes.Value as IEnumerable<ReporteContratistaDto>;

            var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add("Reporte de Contratistas");

            // Agregar título y fusionar celdas de A1 a D1
            worksheet.Range("A1:D1").Merge().Value = "Reporte de Contratistas";
            worksheet.Cell(1, 1).Style.Font.Bold = true;
            worksheet.Cell(1, 1).Style.Font.FontSize = 20;
            worksheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            // Agregar fechas
            worksheet.Cell(2, 1).Value = $"Fecha Inicial: {FechaInicial}";
            worksheet.Cell(3, 1).Value = $"Fecha Final: {FechaFinal}";

            // Agregar encabezados
            worksheet.Cell(5, 1).Value = "Entrada";
            worksheet.Cell(5, 2).Value = "Salida";
            worksheet.Cell(5, 3).Value = "Empresa contratista";
            worksheet.Cell(5, 4).Value = "Nombre contratista";

            // Se agrega el filtro
            worksheet.Range("A5:D5").SetAutoFilter();

            // Agregar datos
            var row = 6; 
            foreach (var item in empleados)
            {
                worksheet.Cell(row, 1).Value = item.Entrada;
                worksheet.Cell(row, 2).Value = item.Salida;
                worksheet.Cell(row, 3).Value = item.EmpresaContratista;
                worksheet.Cell(row, 4).Value = item.NombreContratista;
                row++;
            }

            // Formato de fecha
            worksheet.Column(1).Style.DateFormat.Format = "dd/MM/yyyy HH:mm";
            worksheet.Column(2).Style.DateFormat.Format = "dd/MM/yyyy HH:mm";

            worksheet.Columns().AdjustToContents();

            using (var stream = new MemoryStream())
            {
                workbook.SaveAs(stream);
                var content = stream.ToArray();
                return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ReporteContratistas.xlsx");
            }
        }


        [HttpGet("ReporteContratistas/pdf")]
        public async Task<IActionResult> ReporteContratistasPdf(string FechaInicial, string FechaFinal, string? Empresa, string? Filtro)
        {
            var resultados = await ObtenerReporteContratista(FechaInicial, FechaFinal, Empresa, Filtro);

            if (resultados.Result is BadRequestObjectResult badRequestResult)
            {
                return BadRequest(badRequestResult.Value);
            }

            var reportes = resultados.Result as OkObjectResult;
            if (reportes == null || reportes.Value == null)
            {
                return BadRequest("No se encontraron datos.");
            }

            var contratistas = reportes.Value as IEnumerable<ReporteContratistaDto>;

            using (var stream = new MemoryStream())
            {
                using (var pdfDocument = new PdfDocument())
                {
                    var font = new XFont("Arial", 9, XFontStyleEx.Regular);
                    var titleFont = new XFont("Verdana", 18, XFontStyleEx.Bold);
                    var titleFont2 = new XFont("Verdana", 9, XFontStyleEx.Bold);
                    int recordsPerPage = 23; // Número de registros por página
                    int yPoint = 130; 
                    const int pageHeight = 840; 

                    XGraphics gfx = null;

                    void AddPage()
                    {
                        var page = pdfDocument.AddPage(new PdfPage() { Width = 840, Height = 595 }); 
                        gfx = XGraphics.FromPdfPage(page);

                        // Se agrega título
                        gfx.DrawString("Reporte de Contratistas", titleFont, XBrushes.Black, new XRect(0, 30, page.Width, page.Height), XStringFormats.TopCenter);

                        // Se agregan rango de fechas
                        gfx.DrawString($"Fecha Inicial:  {DateTime.Parse(FechaInicial):dd/MM/yyyy}", titleFont2, XBrushes.Black, 40, 70);
                        gfx.DrawString($"Fecha Final:    {DateTime.Parse(FechaFinal):dd/MM/yyyy}", titleFont2, XBrushes.Black, 40, 85);

                        // Encabezados
                        gfx.DrawLine(XPens.Black, 30, 100, 800, 100);
                        gfx.DrawString("Entrada", font, XBrushes.Black, 55, 113);
                        gfx.DrawString("Salida", font, XBrushes.Black, 138, 113);
                        gfx.DrawString("Empresa Contratista", font, XBrushes.Black, 300, 113);
                        gfx.DrawString("Nombre Contratista", font, XBrushes.Black, 615, 113);
                        gfx.DrawLine(XPens.Black, 30, 120, 800, 120);

                        yPoint = 130; // Se reinicia yPoint para la nueva página
                    }

                    AddPage(); 

                    foreach (var item in contratistas)
                    {
                        if ((yPoint - 130) / 20 >= recordsPerPage)
                        {
                            DrawVerticalLines(gfx, yPoint);
                            AddPage(); 
                        }

                        // Se agregan datos
                        gfx.DrawString(DateTime.Parse(item.Entrada).ToString("dd/MM/yyyy HH:mm"), font, XBrushes.Black, 35, yPoint);
                        gfx.DrawString(DateTime.Parse(item.Salida).ToString("dd/MM/yyyy HH:mm"), font, XBrushes.Black, 115, yPoint);
                        gfx.DrawString(item.EmpresaContratista, font, XBrushes.Black, 193, yPoint);
                        gfx.DrawString(item.NombreContratista, font, XBrushes.Black, 510, yPoint);

                        // Línea después de cada registro
                        gfx.DrawLine(XPens.Black, 30, yPoint + 10, 800, yPoint + 10);
                        yPoint += 20;
                    }

                    // Líneas verticales en la última página
                    DrawVerticalLines(gfx, yPoint);

                    // Se guarda el documento PDF en el MemoryStream
                    pdfDocument.Save(stream);
                }

                var content = stream.ToArray();
                return File(content, "application/pdf", "ReporteContratistas.pdf");
            }

            void DrawVerticalLines(XGraphics gfx, int currentYPoint)
            {
                if (currentYPoint > 130) 
                {
                    var height = currentYPoint - 9;
                    gfx.DrawLine(XPens.Black, 30, 100, 30, height);
                    gfx.DrawLine(XPens.Black, 110, 100, 110, height); 
                    gfx.DrawLine(XPens.Black, 190, 100, 190, height); 
                    gfx.DrawLine(XPens.Black, 500, 100, 500, height); 
                    gfx.DrawLine(XPens.Black, 800, 100, 800, height); 
                }
            }
        }

        private async Task<ActionResult<IEnumerable<ReporteComedorGralDto>>> ObtenerReporteComedorGral(string FechaInicial, string FechaFinal, int? TipoPersonal, int? Planta)
        {
            var resultados = new List<ReporteComedorGralDto>();

            using (var connection = new SqlConnection(cadenaSQL))
            {
                await connection.OpenAsync();

                using (var command = new SqlCommand("DTW_SP_ReporteMarcajesComedorGeneral", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@pFechaInicial", FechaInicial);
                    command.Parameters.AddWithValue("@pFechaFinal", FechaFinal);
                    command.Parameters.AddWithValue("@pTipoPersonal", TipoPersonal ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@pPlanta", Planta ?? (object)DBNull.Value);

                    using (var rd = await command.ExecuteReaderAsync())
                    {
                        try
                        {
                            while (await rd.ReadAsync())
                            {
                                var reporte = new ReporteComedorGralDto
                                {
                                    FechaEntrada = rd.IsDBNull(rd.GetOrdinal("FechaEntrada")) ? null : rd.GetDateTime(rd.GetOrdinal("FechaEntrada")).ToString("yyyy-MM-dd HH:mm:ss"),
                                    Nombre = rd.IsDBNull(rd.GetOrdinal("Nombre")) ? null : rd.GetString(rd.GetOrdinal("Nombre")),
                                    Planta = rd.IsDBNull(rd.GetOrdinal("Planta")) ? null : rd.GetString(rd.GetOrdinal("Planta")),
                                    Compania = rd.IsDBNull(rd.GetOrdinal("Compania")) ? null : rd.GetString(rd.GetOrdinal("Compania"))
                                };

                                resultados.Add(reporte);
                            }
                        }
                        catch (Exception ex)
                        {
                            return BadRequest($"Error al procesar los datos: {ex.Message}");
                        }
                    }
                }
            }

            return Ok(resultados);
        }


        [HttpGet("ReporteComedorGral")]
        public async Task<ActionResult<IEnumerable<ReporteComedorGralDto>>> ReporteComedorGral(string FechaInicial, string FechaFinal, int? TipoPersonal, int? Planta)
        {
            return await ObtenerReporteComedorGral(FechaInicial, FechaFinal, TipoPersonal, Planta);
        }

        [HttpGet("ReporteComedorGral/excel")]
        public async Task<IActionResult> ReporteComedorGralExcel(string FechaInicial, string FechaFinal, int? TipoPersonal, int? Planta)
        {
            var resultados = await ObtenerReporteComedorGral(FechaInicial, FechaFinal, TipoPersonal, Planta);

            if (resultados.Result is BadRequestObjectResult badRequestResult)
            {
                return BadRequest(badRequestResult.Value);
            }

            var reportes = resultados.Result as OkObjectResult;
            if (reportes == null || reportes.Value == null)
            {
                return BadRequest("No se encontraron datos.");
            }

            var empleados = reportes.Value as IEnumerable<ReporteComedorGralDto>;

            var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add("Reporte Comedor");

            // Agregar título y fusionar celdas de A1 a D1
            worksheet.Range("A1:D1").Merge().Value = "Reporte Comedor";
            worksheet.Cell(1, 1).Style.Font.Bold = true;
            worksheet.Cell(1, 1).Style.Font.FontSize = 20;
            worksheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            // Agregar fechas
            worksheet.Cell(2, 1).Value = $"Fecha Inicial: {FechaInicial}";
            worksheet.Cell(3, 1).Value = $"Fecha Final: {FechaFinal}";

            // Agregar encabezados
            worksheet.Cell(5, 1).Value = "Entrada";
            worksheet.Cell(5, 2).Value = "Nombre";
            worksheet.Cell(5, 3).Value = "Sitio";
            worksheet.Cell(5, 4).Value = "Compañia";

            // Se agrega el filtro
            worksheet.Range("A5:D5").SetAutoFilter();

            // Agregar datos
            var row = 6; 
            foreach (var item in empleados)
            {
                worksheet.Cell(row, 1).Value = item.FechaEntrada;
                worksheet.Cell(row, 2).Value = item.Nombre;
                worksheet.Cell(row, 3).Value = item.Planta;
                worksheet.Cell(row, 4).Value = item.Compania;
                row++;
            }

            // Formato de fecha
            worksheet.Column(1).Style.DateFormat.Format = "dd/MM/yyyy HH:mm";
            worksheet.Column(2).Style.DateFormat.Format = "dd/MM/yyyy HH:mm";

            worksheet.Columns().AdjustToContents();

            using (var stream = new MemoryStream())
            {
                workbook.SaveAs(stream);
                var content = stream.ToArray();
                return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ReporteComedor.xlsx");
            }
        }

        [HttpGet("ReporteComedorGral/pdf")]
        public async Task<IActionResult> ReporteComedorGralPdf(string FechaInicial, string FechaFinal, int? TipoPersonal, int? Planta)
        {
            var resultados = await ObtenerReporteComedorGral(FechaInicial, FechaFinal, TipoPersonal, Planta);

            if (resultados.Result is BadRequestObjectResult badRequestResult)
            {
                return BadRequest(badRequestResult.Value);
            }

            var reportes = resultados.Result as OkObjectResult;
            if (reportes == null || reportes.Value == null)
            {
                return BadRequest("No se encontraron datos.");
            }

            var contratistas = reportes.Value as IEnumerable<ReporteComedorGralDto>;

            using (var stream = new MemoryStream())
            {
                using (var pdfDocument = new PdfDocument())
                {
                    var font = new XFont("Arial", 9, XFontStyleEx.Regular);
                    var titleFont = new XFont("Verdana", 18, XFontStyleEx.Bold);
                    var titleFont2 = new XFont("Verdana", 9, XFontStyleEx.Bold);
                    int recordsPerPage = 23; // Número de registros por página
                    int yPoint = 130;
                    const int pageHeight = 840;

                    XGraphics gfx = null;

                    void AddPage()
                    {
                        var page = pdfDocument.AddPage(new PdfPage() { Width = 840, Height = 595 }); 
                        gfx = XGraphics.FromPdfPage(page);

                        // Se agrega título
                        gfx.DrawString("Reporte Comedor", titleFont, XBrushes.Black, new XRect(0, 30, page.Width, page.Height), XStringFormats.TopCenter);

                        // Se agregan rango de fechas
                        gfx.DrawString($"Fecha Inicial:  {DateTime.Parse(FechaInicial):dd/MM/yyyy}", titleFont2, XBrushes.Black, 40, 70);
                        gfx.DrawString($"Fecha Final:    {DateTime.Parse(FechaFinal):dd/MM/yyyy}", titleFont2, XBrushes.Black, 40, 85);

                        // Encabezados
                        gfx.DrawLine(XPens.Black, 30, 100, 800, 100); 
                        gfx.DrawString("Entrada", font, XBrushes.Black, 55, 113);
                        gfx.DrawString("Nombre", font, XBrushes.Black, 230, 113);
                        gfx.DrawString("Sitio", font, XBrushes.Black, 435, 113);
                        gfx.DrawString("Compañia", font, XBrushes.Black, 630, 113);
                        gfx.DrawLine(XPens.Black, 30, 120, 800, 120); 

                        yPoint = 130; 
                    }

                    AddPage(); 

                    foreach (var item in contratistas)
                    {
                        if ((yPoint - 130) / 20 >= recordsPerPage)
                        {
                            DrawVerticalLines(gfx, yPoint);
                            AddPage();
                        }

                        // Se agregan datos
                        gfx.DrawString(DateTime.Parse(item.FechaEntrada).ToString("dd/MM/yyyy HH:mm"), font, XBrushes.Black, 35, yPoint);
                        gfx.DrawString(item.Nombre, font, XBrushes.Black, 115, yPoint);
                        gfx.DrawString(item.Planta, font, XBrushes.Black, 400, yPoint);
                        gfx.DrawString(item.Compania, font, XBrushes.Black, 505, yPoint);

                        // Línea después de cada registro
                        gfx.DrawLine(XPens.Black, 30, yPoint + 10, 800, yPoint + 10);
                        yPoint += 20;
                    }

                    // Líneas verticales en la última página
                    DrawVerticalLines(gfx, yPoint);

                    // Se guarda el documento PDF en el MemoryStream
                    pdfDocument.Save(stream);
                }

                var content = stream.ToArray();
                return File(content, "application/pdf", "ReporteComedor.pdf");
            }

            void DrawVerticalLines(XGraphics gfx, int currentYPoint)
            {
                if (currentYPoint > 130) 
                {
                    var height = currentYPoint - 9;
                    gfx.DrawLine(XPens.Black, 30, 100, 30, height); 
                    gfx.DrawLine(XPens.Black, 110, 100, 110, height); 
                    gfx.DrawLine(XPens.Black, 385, 100, 385, height); 
                    gfx.DrawLine(XPens.Black, 500, 100, 500, height); 
                    gfx.DrawLine(XPens.Black, 800, 100, 800, height); 
                }
            }
        }
        private async Task<ActionResult<IEnumerable<ReporteComidasTurnoDto>>> ObtenerReporteComidasTurno(DateTime StartDate, DateTime EndDate, string idplanta)
        {
            var resultados = new List<ReporteComidasTurnoDto>();

            using (var connection = new SqlConnection(cadenaSQL))
            {
                await connection.OpenAsync();

                using (var command = new SqlCommand("DTW_SP_ConsultaComidasXTurno", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@pStartDate", StartDate);
                    command.Parameters.AddWithValue("@pEndDate", EndDate);
                    command.Parameters.AddWithValue("@pSitio", idplanta);

                    using (var rd = await command.ExecuteReaderAsync())
                    {
                        try
                        {
                            while (await rd.ReadAsync())
                            {
                                var reporte = new ReporteComidasTurnoDto
                                {
                                    // Asignar FechaRegistro como DateTime
                                    FechaRegistro = rd.IsDBNull(rd.GetOrdinal("FechaRegistro")) ? null : rd.GetDateTime(rd.GetOrdinal("FechaRegistro")).ToString("dd-MM-yyyy"),
                                    Descripcion = rd.IsDBNull(rd.GetOrdinal("Descripcion")) ? default : rd.GetString(rd.GetOrdinal("Descripcion")),
                                    Turno = rd.IsDBNull(rd.GetOrdinal("Turno")) ? default : Convert.ToInt32(rd.GetString(rd.GetOrdinal("Turno"))),
                                    TotalRegistros = rd.IsDBNull(rd.GetOrdinal("TotalRegistros")) ? default : rd.GetInt32(rd.GetOrdinal("TotalRegistros")),
                                };

                                resultados.Add(reporte);
                            }
                        }
                        catch (Exception ex)
                        {
                            return BadRequest($"Error al procesar los datos: {ex.Message}");
                        }
                    }
                }
            }

            return Ok(resultados);
        }



        [HttpGet("ReporteComidas")]
        public async Task<ActionResult<IEnumerable<ReporteComidasTurnoDto>>> ReporteComidas(DateTime StartDate, DateTime EndDate, string idplanta)
        {
            return await ObtenerReporteComidasTurno(StartDate, EndDate, idplanta);
        }

        [HttpGet("ReporteComidas/excel")]
        public async Task<IActionResult> ReporteComidasExcel(DateTime StartDate, DateTime EndDate, string idplanta)
        {
            var resultados = await ObtenerReporteComidasTurno(StartDate, EndDate, idplanta);

            if (resultados.Result is BadRequestObjectResult badRequestResult)
            {
                return BadRequest(badRequestResult.Value);
            }

            var reportes = resultados.Result as OkObjectResult;
            if (reportes == null || reportes.Value == null)
            {
                return BadRequest("No se encontraron datos.");
            }

            var comidas = reportes.Value as IEnumerable<ReporteComidasTurnoDto>;

            var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add("Reporte de Comidas por Turno");
            worksheet.Range("A1:D1").Merge().Value = "Reporte de Comidas por Turno";
            worksheet.Cell(1, 1).Style.Font.Bold = true;
            worksheet.Cell(1, 1).Style.Font.FontSize = 20;
            worksheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            // Agregar fechas
            worksheet.Cell(2, 1).Value = $"Fecha Inicial: {StartDate:dd/MM/yyyy}";
            worksheet.Cell(3, 1).Value = $"Fecha Final: {EndDate:dd/MM/yyyy}";

            // Agregar encabezados
            worksheet.Cell(5, 1).Value = "Fecha";
            worksheet.Cell(5, 2).Value = "Descripción";
            worksheet.Cell(5, 3).Value = "Turno";
            worksheet.Cell(5, 4).Value = "Comidas";

            worksheet.Range("A5:D5").SetAutoFilter();

            // Agregar datos y combinar celdas
            var row = 6;
            foreach (var item in comidas)
            {
                worksheet.Cell(row, 1).Value = item.FechaRegistro;
                worksheet.Cell(row, 2).Value = item.Descripcion;
                worksheet.Cell(row, 3).Value = item.Turno;
                worksheet.Cell(row, 4).Value = item.TotalRegistros;

                // Combinar cada tres filas de la columna de Fecha
                if ((row - 6) % 3 == 2) // Cuando estamos en la 3ra fila del grupo
                {
                    worksheet.Range(row - 2, 1, row, 1).Merge().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                }
                // Combinar cada tres filas de la columna de Descripción
                if ((row - 6) % 3 == 2) // Cuando estamos en la 3ra fila del grupo
                {
                    worksheet.Range(row - 2, 2, row, 2).Merge().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                }

                row++;
            }
            worksheet.Columns().AdjustToContents();

            using (var stream = new MemoryStream())
            {
                workbook.SaveAs(stream);
                var content = stream.ToArray();
                return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ReporteComidas.xlsx");
            }
        }

        [HttpGet("ReporteComidas/pdf")]
        public async Task<IActionResult> ReporteComidasPDF(DateTime StartDate, DateTime EndDate, string idplanta)
        {
            var resultados = await ObtenerReporteComidasTurno(StartDate, EndDate, idplanta);

            if (resultados.Result is BadRequestObjectResult badRequestResult)
            {
                return BadRequest(badRequestResult.Value);
            }

            var reportes = resultados.Result as OkObjectResult;
            if (reportes == null || reportes.Value == null)
            {
                return BadRequest("No se encontraron datos.");
            }

            var comidas = reportes.Value as IEnumerable<ReporteComidasTurnoDto>;

            using (var stream = new MemoryStream())
            {
                using (var pdfDocument = new PdfDocument())
                {
                    var font = new XFont("Arial", 9, XFontStyleEx.Regular);
                    var titleFont = new XFont("Verdana", 18, XFontStyleEx.Bold);
                    var titleFont2 = new XFont("Verdana", 9, XFontStyleEx.Bold);
                    int recordsPerPage = 36; // Número de registros por página
                    int yPoint = 130; // Posición inicial vertical
                    const int pageHeight = 840; // Altura total de la página en puntos
                    const int columnWidthFechaRegistro = 120; // Ancho de columna "Fecha Registro"
                    const int columnWidthDescripcion = 200; // Ancho de columna "Descripción"
                    const int columnWidthTurno = 100; // Ancho de columna "Turno"
                    const int columnWidthTotalRegistros = 100; // Ancho de columna "Total Registros"

                    XGraphics gfx = null;

                    void AddPage()
                    {
                        var page = pdfDocument.AddPage();
                        gfx = XGraphics.FromPdfPage(page);

                        // Se agrega título
                        gfx.DrawString("Reporte de Comidas por Turno", titleFont, XBrushes.Black, new XRect(0, 30, page.Width, page.Height), XStringFormats.TopCenter);

                        // Se agregan rango de fechas
                        gfx.DrawString($"Fecha Inicial:  {StartDate:dd/MM/yyyy}", titleFont2, XBrushes.Black, 40, 70);
                        gfx.DrawString($"Fecha Final:    {EndDate:dd/MM/yyyy}", titleFont2, XBrushes.Black, 40, 85);

                        // Encabezados
                        gfx.DrawLine(XPens.Black, 30, 100, 560, 100);
                        DrawCenteredText(gfx, "Fecha", titleFont2, 30, columnWidthFechaRegistro, 113); // Cambiado a titleFont2
                        DrawCenteredText(gfx, "Sitio", titleFont2, 30 + columnWidthFechaRegistro, columnWidthDescripcion, 113); // Cambiado a titleFont2
                        DrawCenteredText(gfx, "Turno", titleFont2, 30 + columnWidthFechaRegistro + columnWidthDescripcion, columnWidthTurno, 113); // Cambiado a titleFont2
                        DrawCenteredText(gfx, "Comidas", titleFont2, 30 + columnWidthFechaRegistro + columnWidthDescripcion + columnWidthTurno, columnWidthTotalRegistros, 113); // Cambiado a titleFont2
                        gfx.DrawLine(XPens.Black, 30, 120, 560, 120);
                        yPoint = 130;
                    }

                    AddPage();

                    for (int i = 0; i < comidas.Count(); i++)
                    {
                        var item = comidas.ElementAt(i);
                        if ((yPoint - 130) / 20 >= recordsPerPage)
                        {
                            DrawVerticalLines(gfx, yPoint);
                            AddPage();
                        }
                        if (i % 3 == 0) 
                        {
                            
                            bool hasFechaRegistro = !string.IsNullOrEmpty(item.FechaRegistro);
                            bool hasDescripcion = !string.IsNullOrEmpty(item.Descripcion);
                            DrawCenteredText(gfx, hasFechaRegistro ? item.FechaRegistro : string.Empty, font, 30, columnWidthFechaRegistro, yPoint);
                            DrawCenteredText(gfx, hasDescripcion ? item.Descripcion : string.Empty, font, 30 + columnWidthFechaRegistro, columnWidthDescripcion, yPoint);
                        }
                        else
                        {
                            DrawCenteredText(gfx, "", font, 30, columnWidthFechaRegistro, yPoint);
                            DrawCenteredText(gfx, "", font, 30 + columnWidthFechaRegistro, columnWidthDescripcion, yPoint);
                        }

                        DrawCenteredText(gfx, item.Turno.ToString(), font, 30 + columnWidthFechaRegistro + columnWidthDescripcion, columnWidthTurno, yPoint);
                        DrawCenteredText(gfx, item.TotalRegistros.ToString(), font, 30 + columnWidthFechaRegistro + columnWidthDescripcion + columnWidthTurno, columnWidthTotalRegistros, yPoint);
                        gfx.DrawLine(XPens.Black, 30 + columnWidthFechaRegistro + columnWidthDescripcion, yPoint + 10, 30 + columnWidthFechaRegistro + columnWidthDescripcion + columnWidthTurno + columnWidthTotalRegistros + 10, yPoint + 10);

                        yPoint += 20; 
                        if (i % 3 == 1) 
                        {
                            gfx.DrawLine(XPens.Black, 30, yPoint + 10, 350, yPoint + 10); 
                        }
                        if (i % 3 == 3) // Al final de cada conjunto de 3, dibujar la línea
                        {
                            gfx.DrawLine(XPens.Black, 30, yPoint + 10, 560, yPoint + 10);
                        }
                    }
                    DrawVerticalLines(gfx, yPoint);
                    pdfDocument.Save(stream);
                }

                var content = stream.ToArray();
                return File(content, "application/pdf", "ReporteComidas.pdf");
            }

            void DrawVerticalLines(XGraphics gfx, int currentYPoint)
            {
                if (currentYPoint > 130)
                {
                    var height = currentYPoint - 9;
                    gfx.DrawLine(XPens.Black, 30, 100, 30, height); // Línea izquierda
                    gfx.DrawLine(XPens.Black, 150, 100, 150, height); // Línea de "Descripción"
                    gfx.DrawLine(XPens.Black, 350, 100, 350, height); // Línea de "Turno"
                    gfx.DrawLine(XPens.Black, 450, 100, 450, height); // Línea de "Total Registros"
                    gfx.DrawLine(XPens.Black, 560, 100, 560, height); // Línea derecha
                }
            }

            void DrawCenteredText(XGraphics gfx, string text, XFont font, int startX, int columnWidth, int startY)
            {
                var textWidth = gfx.MeasureString(text, font).Width;
                var xPos = startX + (columnWidth - textWidth) / 2;
                gfx.DrawString(text, font, XBrushes.Black, xPos, startY);
            }
        }

    }
}
