﻿using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using Microsoft.AspNetCore.Mvc;

namespace DW_INDORAMA_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TerminalesController : ControllerBase
    {
        private readonly IndoramaAccessControlContext _context;

        public TerminalesController(IndoramaAccessControlContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Terminale> GetTerminales()
        {
            return _context.Terminales.ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Terminale> GetById(int id)
        {
            var idTerminal = _context.Terminales.Find(id);

            if (idTerminal is null)
                return StatusCode(StatusCodes.Status404NotFound, new { mensaje = "No se encontro terminal" });

            return Ok(idTerminal);
        }

        [HttpPost]
        public IActionResult Create(Terminale terminal)
        {
            _context.Terminales.Add(terminal);
            _context.SaveChanges();

            return CreatedAtAction(nameof(GetById), new { id = terminal.IdTerminales }, terminal);
        }

        [HttpPut("{idTerminal}")]
        public IActionResult Update(int idTerminal, Terminale terminal)
        {
            if (idTerminal != terminal.IdTerminales)
                return BadRequest();

            var existingTerminal = _context.Terminales.Find(idTerminal);

            if (existingTerminal is null)
                return StatusCode(StatusCodes.Status404NotFound, new { mensaje = "La terminal no existe" });

            existingTerminal.IdTerminales = terminal.IdTerminales;
            existingTerminal.Clave = terminal.Clave;
            existingTerminal.Ubicacion = terminal.Ubicacion;
            existingTerminal.Ip = terminal.Ip;
            existingTerminal.Nombre = terminal.Nombre;
            existingTerminal.IdTipoLector = terminal.IdTipoLector;
            existingTerminal.FechaActualizacion = DateTime.Now;
            existingTerminal.Grupo = terminal.Grupo;
            existingTerminal.Tipo = terminal.Tipo;

            _context.SaveChanges();
            return StatusCode(StatusCodes.Status200OK, new { mensaje = "Datos actualizados" });
        }

        [HttpDelete("{idTerminal}")]
        public IActionResult Delete(int idTerminal)
        {
            var existingTerminal = _context.Terminales.Find(idTerminal);
            if (existingTerminal is null)
                return NotFound();

            _context.Terminales.Remove(existingTerminal);
            _context.SaveChanges();

            return StatusCode(StatusCodes.Status200OK, new { mensaje = "Terminal eliminada" });
        }


    }
}