﻿using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using Microsoft.AspNetCore.Mvc;

namespace DW_INDORAMA_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CompaniasController : ControllerBase
    {
        private readonly IndoramaAccessControlContext _context;

        public CompaniasController(IndoramaAccessControlContext context)
        {
            _context = context; 
        }

        [HttpGet]
        public IEnumerable<Companium> Get()
        { 
            return _context.Compania.ToArray();
        }
    }
}
