﻿using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using Microsoft.AspNetCore.Mvc;


namespace DW_INDORAMA_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ParametrosController : ControllerBase
    {
        private readonly IndoramaAccessControlContext _context;

        public ParametrosController(IndoramaAccessControlContext context)
        {
            _context = context;
        }

        //Obtiene el listado de los parametros 
        [HttpGet]
        public IEnumerable<DtwCaParametro> GetCaParametros()
        {
            return _context.DtwCaParametros.ToList();
        }

        //Edicion de parametros 
        [HttpPut("{idParametro}")]
        public IActionResult Update(int idParametro, DtwCaParametro parametro)
        {
            if (idParametro != parametro.IdParametro)
                return BadRequest();

            var existingParametro = _context.DtwCaParametros.Find(idParametro);

            if (existingParametro is null)
                return StatusCode(StatusCodes.Status404NotFound, new { mensaje = "El parametro no existe" });

            existingParametro.Nombre = parametro.Nombre;
            existingParametro.Descripcion = parametro.Descripcion;
            existingParametro.Valor = parametro.Valor;

            _context.SaveChanges();
            return StatusCode(StatusCodes.Status200OK, new { mensaje = "Datos actualizados" });
        }

    }
}

