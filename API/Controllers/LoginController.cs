﻿using Microsoft.AspNetCore.Mvc;
using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using System.Data;

namespace DW_INDORAMA_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : ControllerBase

    {
        private readonly IndoramaAccessControlContext _context;
        public LoginController(IndoramaAccessControlContext context)
        {
            _context = context;
        }


        [HttpGet("{Usuario}")]
        public ActionResult<Usuario> GetUserByName(string Usuario)
        {
            var user = _context.Usuarios.Where(x => x.Usuario1.Equals(Usuario)).FirstOrDefault();

            if (user is null)
                return NotFound();
            return Ok(user);
        }
    }
}