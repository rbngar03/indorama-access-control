﻿using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using Microsoft.AspNetCore.Mvc;
using DW_INDORAMA_API.Dtos;
using DW_INDORAMA_API.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace DW_INDORAMA_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PermisosController : ControllerBase
    {
        private readonly IndoramaAccessControlContext _context;
        private readonly IPermisosRepository _permisosRepository;
        private readonly IMapper _mapper;

        public PermisosController(IndoramaAccessControlContext context, IPermisosRepository permisosRepository, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
            _permisosRepository = permisosRepository;
        }

        //  Get PerfilesModulo 
        //Obtinene el listado de realcion de los perfiles,modulos y acciones
        [HttpGet("PerfilesModulos")]
        public IEnumerable<PerfilesModulo> Get()
        {
            return _context.PerfilesModulos.ToList();
        }

        //Obtiene la relacion de permisos a los modulos y acciones que tiene un perfil 
        [HttpGet("PerfilesModulos/{idPerfil}")]
        public ActionResult<GenericHttpResponse<PerfilesModuloAddDeletedto>> GetById(int idPerfil)
        {
            GenericHttpResponse<PerfilesModuloAddDeletedto> response = new GenericHttpResponse<PerfilesModuloAddDeletedto>();
            if (_permisosRepository.GetPerfilById(idPerfil) == null)
                return NotFound("No existe perfil consultado");
            response.values = _permisosRepository.GetPermisos(idPerfil);
            return Ok(response);
        }

        [HttpPost("PerfilesModulos")]
        public IResult UpdatePerfilesModulos([FromBody] PermisosRequest request)
        {
            return _permisosRepository.Update(request);
        }

        [HttpPost("Perfiles")]
        public async Task<IActionResult> AddPerfil(PerfilDto perfilRequest)
        {
            //GenericHttpResponse<PerfilDto> response = new GenericHttpResponse<PerfilDto>();
            var search = _context.Perfiles.Where(x => x.Clave == perfilRequest.Clave);
            if (search.Count() != 0)
            {
                /*var perfil = _mapper.Map<PerfilDto, Perfile>(perfilRequest);
                await _permisosRepository.AddPerfil(perfil);
                response.values = _mapper.Map<Perfile, PerfilDto>(perfil);
                response.message = "Perfil añadido";
                return Ok(response);*/
                return BadRequest("Clave de perfil ya existe");
            }
            return Ok(await _permisosRepository.AddPerfil(perfilRequest));
        }

        [HttpDelete("Perfiles/{idPerfil}")]
        public IResult DeletePerfil(int idPerfil)
        {
            var perfil = _permisosRepository.GetPerfilById(idPerfil);
            if (perfil == null)
            {
                return Results.NotFound("El perfil no existe");
            }
            return _permisosRepository.DeletePerfilById(idPerfil);
        }

        //  Get Perfiles 
        //Obtiene un listado de los perfiles
        [HttpGet("Perfiles")]
        public IEnumerable<Perfile> GetPerfiles()
        {
            return _context.Perfiles.ToList();
        }

        //Obtiene la busqueda de un perfil por id
        [HttpGet("Perfiles/{id}")]
        public ActionResult<Perfile> GetByIdPerfil(int id)
        {
            var idPerfil = _context.Perfiles.Find(id);

            if (idPerfil is null)
                return NotFound();
            return Ok(idPerfil);
        }

        //  Get Modulos 
        //Obtiene un listado de los modulos
        [HttpGet("Modulos")]
        public IEnumerable<Modulo> GetModulos()
        {
            return _context.Modulos.ToList();
        }

        //Obtiene la busqueda de un modulo por id
        [HttpGet("Modulos/{id}")]
        public ActionResult<Perfile> GetByIdModulo(int id)
        {
            var idModulo = _context.Modulos.Find(id);

            if (idModulo is null)
                return NotFound();
            return Ok(idModulo);
        }

        //  Get Acciones 
        //Obtiene un listado de las acciones 
        [HttpGet("Acciones")]
        public IEnumerable<Accione> GetAcciones()
        {
            return _context.Acciones.ToList();
        }

        //Obtiene la busqueda de una accion por id 
        [HttpGet("Acciones/{id}")]
        public ActionResult<Accione> GetByIdAccion(int id)
        {
            var idAccion = _context.Acciones.Find(id);

            if (idAccion is null)
                return NotFound();
            return Ok(idAccion);
        }
    }
}