﻿using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Net.NetworkInformation;

namespace DW_INDORAMA_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VisitantesController : ControllerBase
    {
        private readonly IndoramaAccessControlContext _context;
        private readonly string cadenaSQL;

        public VisitantesController(IConfiguration configuracion)
        {
            cadenaSQL = configuracion.GetConnectionString("IndoramaConnection");
        }

        [HttpGet]
        public ActionResult Visitantes(int pag, int reg, int IdEstatus, int IdVisitantes, string? Empresa, string? Ccostos, string? Filtro)
        {
            List<Visitante> visitantes = new List<Visitante>();

            try
            {
                using (var conexion = new SqlConnection(cadenaSQL))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("DTW_SP_Visitantes", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = IdEstatus;
                    cmd.Parameters.Add("@pIdVisitante", SqlDbType.Int).Value = IdVisitantes;
                    cmd.Parameters.Add("@pEmpresaVisitante", SqlDbType.VarChar).Value = Empresa;
                    cmd.Parameters.Add("@pCentroCostos", SqlDbType.VarChar).Value = Ccostos;
                    cmd.Parameters.Add("@pFiltro", SqlDbType.VarChar).Value = Filtro;
                    cmd.Parameters.Add("@pPAG", SqlDbType.Int).Value = pag;
                    cmd.Parameters.Add("@pREG", SqlDbType.Int).Value = reg;
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (var rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            visitantes.Add(new Visitante()
                            {
                                IdVisitantes = Convert.ToInt32(rd["idVisitantes"]),
                                NombreVisitante = rd["NombreVisitante"].ToString(),
                                Rfc = rd["RFC"].ToString(),
                                IdEstatus = Convert.ToInt32(rd["Estatus"]),
                                IdGafete = Convert.ToInt32(rd["Gafete"]),
                                IdCcostos = Convert.ToInt32(rd["idCcostos"]),
                                EmpResponsable = rd["EmpResponsable"].ToString(),
                                DescOrigen = rd["DescOrigen"].ToString(),
                                IdComedor = Convert.ToInt32(rd["DerechoComedor"]),
                                FechaIngreso = Convert.ToDateTime(rd["FechaIngreso"]),
                                Horas = rd["Horas"].ToString(),
                            });
                        }
                    }
                }
                if (visitantes.Count != 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { response = visitantes });

                }

                return StatusCode(StatusCodes.Status404NotFound, new { mensaje = "No se encontraron resulatos" });
            }
            catch (Exception error)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message, response = visitantes });
            }
        }

        [HttpGet("{id}")]
        public ActionResult Visitante(int id)
        {
            List<Visitante> visitantes = new List<Visitante>();
            Visitante visitante = new Visitante();

            try
            {
                using (var conexion = new SqlConnection(cadenaSQL))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("DTW_SP_Visitantes", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pIdVisitante", SqlDbType.Int).Value = id;
                    using (var rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            visitantes.Add(new Visitante()
                            {
                                IdVisitantes = Convert.ToInt32(rd["idVisitantes"]),
                                NombreVisitante = rd["NombreVisitante"].ToString(),
                                Rfc = rd["RFC"].ToString(),
                                IdEstatus = Convert.ToInt32(rd["Estatus"]),
                                IdGafete = Convert.ToInt32(rd["Gafete"]),
                                IdCcostos = Convert.ToInt32(rd["idCcostos"]),
                                EmpResponsable = rd["EmpResponsable"].ToString(),
                                DescOrigen = rd["DescOrigen"].ToString(),
                                IdComedor = Convert.ToInt32(rd["DerechoComedor"]),
                                FechaIngreso = Convert.ToDateTime(rd["FechaIngreso"]),
                                Horas = rd["Horas"].ToString(),
                            }); ;
                        }
                    }
                }
                visitante = visitantes.FirstOrDefault(item => item.IdVisitantes == id);

                if (visitante != null)
                {
                    return StatusCode(StatusCodes.Status200OK, new { response = visitante });
                }
                return StatusCode(StatusCodes.Status404NotFound, new { mensaje = "No se encontro visitante" });
            }
            catch (Exception error)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message, response = visitantes });
            }
        }

        [HttpPut]
        public ActionResult EditarVisitante(int IdEstatus, int IdVisitantes, string? Empresa, string EmpleadoResponsable, int DerechoComedor, string Horas)
        {
            try
            {
                using (var conexion = new SqlConnection(cadenaSQL))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("DTW_SP_ModificaVisitante", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = IdEstatus;
                    cmd.Parameters.Add("@pIdVisitante", SqlDbType.Int).Value = IdVisitantes;
                    cmd.Parameters.Add("@pEmpresaVisitante", SqlDbType.VarChar).Value = Empresa;
                    cmd.Parameters.Add("@pEmpleadoResponsable", SqlDbType.VarChar).Value = EmpleadoResponsable;
                    cmd.Parameters.Add("@pDerechoComedor", SqlDbType.Int).Value = DerechoComedor;
                    cmd.Parameters.Add("@pHoras", SqlDbType.VarChar).Value = Horas;
                    cmd.Parameters.Add("Response", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteReader();
                    string Response = (string)cmd.Parameters["Response"].Value;
                    if (Response != "Ok")
                    {
                        return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = Response });
                    }
                }
                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Datos actualizados" });
            }
            catch (Exception error)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message });
            }
        }

        [HttpPost]
        public ActionResult AltaVisitante(string NombreVisitante, string FehaIngreso, string RFC, string? Empresa, string Horas, string EmpleadoResponsable, int IdEstatus, string Ccostos, int DerechoComedor)
        {
            try
            {
                using (var conexion = new SqlConnection(cadenaSQL))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("DTW_SP_AltaVisitante", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pNombreVisitante", SqlDbType.VarChar).Value = NombreVisitante;
                    cmd.Parameters.Add("@pFechaIngreso", SqlDbType.VarChar).Value = FehaIngreso;
                    cmd.Parameters.Add("@pRFC", SqlDbType.VarChar).Value = RFC;
                    cmd.Parameters.Add("@pEmpresaVisitante", SqlDbType.VarChar).Value = Empresa;
                    cmd.Parameters.Add("@pHoras", SqlDbType.VarChar).Value = Horas;
                    cmd.Parameters.Add("@pEmpleadoResponsable", SqlDbType.VarChar).Value = EmpleadoResponsable;
                    cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = IdEstatus;
                    cmd.Parameters.Add("@pCcostos", SqlDbType.VarChar).Value = Ccostos;
                    cmd.Parameters.Add("@pDerechoComedor", SqlDbType.Int).Value = DerechoComedor;
                    cmd.Parameters.Add("Response", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteReader();
                    string Response = (string)cmd.Parameters["Response"].Value;
                    if (Response != "Ok")
                    {
                        return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = Response });
                    }
                }
                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Visitante registrado" });
            }
            catch (Exception error)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message });
            }
        }

    }
}