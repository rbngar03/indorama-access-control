﻿using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using Microsoft.AspNetCore.Mvc;

namespace DW_INDORAMA_API.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class UsersController : ControllerBase
    {
        private readonly IndoramaAccessControlContext _context;
        public UsersController(IndoramaAccessControlContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Usuario> Get()
        {
            return _context.Usuarios.ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Usuario> GetById(int id)
        {
            var idusuario = _context.Usuarios.Find(id);

            if (idusuario is null)
                return StatusCode(StatusCodes.Status404NotFound, new { mensaje = "No se encontro usuario" });
            return Ok(idusuario);
        }

        [HttpPost]
        public IActionResult Create(Usuario usuario)
        {
            _context.Usuarios.Add(usuario);
            _context.SaveChanges();

            return CreatedAtAction(nameof(GetById), new { id = usuario.IdUsuarios }, usuario);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Usuario usuario)
        {
            if (id != usuario.IdUsuarios)
                return BadRequest();

            var existingIdUsuario = _context.Usuarios.Find(id);

            if (existingIdUsuario is null)
                return StatusCode(StatusCodes.Status404NotFound, new { mensaje = "El usuario no existe" });

            existingIdUsuario.Usuario1 = usuario.Usuario1;
            existingIdUsuario.Password = usuario.Password;
            existingIdUsuario.NombreUsuario = usuario.NombreUsuario;
            existingIdUsuario.IdPerfil = usuario.IdPerfil;

            _context.SaveChanges();

            return StatusCode(StatusCodes.Status200OK, new { mensaje = "Usuario actualizado" });
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var existingIdUsuario = _context.Usuarios.Find(id);
            if (existingIdUsuario is null)
                return NotFound();

            _context.Usuarios.Remove(existingIdUsuario);
            _context.SaveChanges();

            return StatusCode(StatusCodes.Status200OK, new { mensaje = "Usuario eliminado" });
        }

    }
}
