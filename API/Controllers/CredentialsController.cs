﻿using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using Microsoft.AspNetCore.Mvc;
using DW_INDORAMA_API.Dtos;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using DW_INDORAMA_API.Interfaces;

namespace DW_INDORAMA_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CredentialsController : ControllerBase
    {
        private readonly ICredentialsRepository _credentialsRepository;


        public CredentialsController(ICredentialsRepository credentialsRepository)
        {
            _credentialsRepository = credentialsRepository;
        }


        [HttpGet("Employee")]
        public  IActionResult CredentialEmployee(string numEmpleado)
        {
            var credential =  _credentialsRepository.GetEmployeeCredential(numEmpleado);
            return Ok(credential);
        }

        [HttpPut("Employee")]
        public async Task<IActionResult> PutCredencialEmployeeAsync([FromBody] CredentialRequest credencialEmpleado)
        {
            if (ModelState.IsValid)
            {
                string message = await _credentialsRepository.UpdateEmployeeCredential(credencialEmpleado);
                return Ok(new GenericHttpResponse<string> { message= message,values= credencialEmpleado.identificador});
            }
            return BadRequest();
        }

        [HttpGet("Contractor")]
        public IActionResult ContractorCredential(string rfc)
        {
            //await _credentialsRepository.biostarEndpoints();
            var credential =  _credentialsRepository.GetContractorCredential(rfc);
            return Ok(credential);
        }

        [HttpPut("Contractor")]
        public async Task<IActionResult> PutContractorCredencialAsync([FromBody] CredentialRequest contractorCredential)
        {
            if (ModelState.IsValid)
            {
                string message = await _credentialsRepository.UpdateContractorCredential(contractorCredential);
                return Ok(new GenericHttpResponse<string> { message = message, values = contractorCredential.identificador });
            }
            return BadRequest();
        }

        [HttpGet("Visitor")]
        public IActionResult VisitorCredential(string rfc)
        {
            //await _credentialsRepository.biostarEndpoints();
            var credential = _credentialsRepository.GetVisitorCredential(rfc);
            return Ok(credential);
        }

        [HttpPut("Visitor")]
        public async Task<IActionResult> PutVisitorCredencial([FromBody] CredentialRequest visitorCredential)
        {
            if (ModelState.IsValid)
            {
                string message = await _credentialsRepository.UpdateVisitorCredential(visitorCredential);
                return Ok(new GenericHttpResponse<string> { message = message, values = visitorCredential.identificador });
            }
            return BadRequest();
        }

        [HttpPost("Credential")]
        public async Task<IActionResult> PostCredential(CredentialDto perfilRequest)
        {
            return BadRequest();
        }
    }
}
