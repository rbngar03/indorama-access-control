﻿using Azure;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.InkML;
using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System.Data;
using System.Net.NetworkInformation;

namespace DW_INDORAMA_API.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class ContratistasController : ControllerBase
    {
        private readonly IndoramaAccessControlContext _context;
        private readonly string cadenaSQL;

        public ContratistasController(IndoramaAccessControlContext context,IConfiguration configuration)
        {
            _context = context;
            cadenaSQL = configuration.GetConnectionString("IndoramaConnection");
        }

        [HttpGet]
        public ActionResult Contratistas(int? pag, int? reg, int IdEstatus, int IdTipoContratista, string? Empresa, string? Ccostos, string? Clasificacion, int? Comedor, string? Filtro)
        {
            List<Contratista> contratistas = new List<Contratista>();

            try
            {
                using (var conexion = new SqlConnection(cadenaSQL))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("DTW_SP_Contratistas", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = IdEstatus;
                    cmd.Parameters.Add("@pTipoContratista", SqlDbType.Int).Value = IdTipoContratista;
                    cmd.Parameters.Add("@pEmpresa", SqlDbType.VarChar).Value = Empresa;
                    cmd.Parameters.Add("@pCentroCostos", SqlDbType.VarChar).Value = Ccostos;
                    cmd.Parameters.Add("@pClasificacion", SqlDbType.VarChar).Value = Clasificacion;
                    cmd.Parameters.Add("@pComedor", SqlDbType.Int).Value = Comedor;
                    cmd.Parameters.Add("@pFiltro", SqlDbType.VarChar).Value = Filtro;
                    cmd.Parameters.Add("@pPAG", SqlDbType.Int).Value = pag ?? 0;
                    cmd.Parameters.Add("@pREG", SqlDbType.Int).Value = reg ?? 50;
                    cmd.Parameters.Add("@pExportar", SqlDbType.Bit).Value = 0;


                    using (var rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            contratistas.Add(new Contratista()
                            {
                                IdContratistas = Convert.ToInt32(rd["idContratistas"]),
                                IdTipoContratista = Convert.ToInt32(rd["idTipoContratista"]),
                                NombreContratista = rd["NombreContratista"].ToString(),
                                ApPaterno = rd["ApPaterno"].ToString(),
                                ApMaterno = rd["ApMaterno"].ToString(),
                                FechaIndSeguridad = Convert.ToDateTime(rd["FechaIndSeguridad"]),
                                FechaContrato = Convert.ToDateTime(rd["FechaContrato"]),
                                FechaTerminacion = Convert.ToDateTime(rd["FechaTerminacion"]),
                                Imss = rd["IMSS"].ToString(),
                                VigenciaImss = Convert.ToDateTime(rd["VigenciaIMSS"]),
                                NumeroContrato = (rd["NumeroContrato"]).ToString(),
                                NombreCompañia = rd["NombreCompañia"].ToString(),
                                Rfc = rd["RFC"].ToString(),
                                IdClasificacion = Convert.ToInt32(rd["idClasificacion"]),
                                ResponsableIndorama = rd["ResponsableIndorama"].ToString(),
                                IdCcostos = Convert.ToInt32(rd["idCcostos"]),
                                IdGafete = Convert.ToInt32(rd["Gafete"]),
                                NumeroProvisionalGafete = Convert.ToInt32(rd["NumeroProvisionalGafete"]),
                                IdComedor = Convert.ToInt32(rd["DerechoComedor"]),
                                IdEstatus = Convert.ToInt32(rd["IdEstatus"])
                            });
                        }
                    }
                }
                if (contratistas.Count != 0)
                {
                    return Ok(new { response = contratistas });

                }

                return StatusCode(StatusCodes.Status404NotFound, new { mensaje = "No se encontraron resulatos" });
            }
            catch (Exception error)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message, response = contratistas });
            }
        }

        [HttpGet("{idContratista}")]
        public ActionResult Contratista(int idContratista)
        {
            List<Contratista> contratistas = new List<Contratista>();
            Contratista contratista = new Contratista();

            try
            {
                using (var conexion = new SqlConnection(cadenaSQL))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("DTW_SP_Contratistas", conexion);
                    cmd.Parameters.Add("@pIdContratista", SqlDbType.Int).Value = idContratista;
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (var rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            contratistas.Add(new Contratista()
                            {
                                IdContratistas = Convert.ToInt32(rd["idContratistas"]),
                                IdTipoContratista = Convert.ToInt32(rd["idTipoContratista"]),
                                NombreContratista = rd["NombreContratista"].ToString(),
                                ApPaterno = rd["ApPaterno"].ToString(),
                                ApMaterno = rd["ApMaterno"].ToString(),
                                FechaIndSeguridad = Convert.ToDateTime(rd["FechaIndSeguridad"]),
                                FechaContrato = Convert.ToDateTime(rd["FechaContrato"]),
                                FechaTerminacion = Convert.ToDateTime(rd["FechaTerminacion"]),
                                Imss = rd["IMSS"].ToString(),
                                VigenciaImss = Convert.ToDateTime(rd["VigenciaIMSS"]),
                                NumeroContrato = (rd["NumeroContrato"]).ToString(),
                                NombreCompañia = rd["NombreCompañia"].ToString(),
                                Rfc = rd["RFC"].ToString(),
                                IdClasificacion = Convert.ToInt32(rd["idClasificacion"]),
                                ResponsableIndorama = rd["ResponsableIndorama"].ToString(),
                                IdCcostos = Convert.ToInt32(rd["idCcostos"]),
                                IdGafete = Convert.ToInt32(rd["Gafete"]),
                                NumeroProvisionalGafete = Convert.ToInt32(rd["NumeroProvisionalGafete"]),
                                IdComedor = Convert.ToInt32(rd["DerechoComedor"]),
                                IdEstatus = Convert.ToInt32(rd["IdEstatus"])
                            }); ;
                        }
                    }
                }
                contratista = contratistas.Where(item => item.IdContratistas == idContratista).FirstOrDefault();

                if (contratista != null)
                {
                    return StatusCode(StatusCodes.Status200OK, new { response = contratista });
                }
                return StatusCode(StatusCodes.Status404NotFound, new { mensaje = "No se encontro contratista" });
            }
            catch (Exception error)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message });
            }
        }


        [HttpPut]
        public ActionResult EditarContratista(int IdContratistas, int idTipoContratista, int idEstatus, string NombreContratista, string ApPaterno, string ApMaterno, string FechaIndSeguridad,
            string FechaContrato, string IMSS, string RFC, string VigenciaIMSS, string FechaTerminacion, string ResponsableIndorama, int idClasificacion, string NombreCompañia, string NumeroContrato,
            string Gafete, int idComedor, int idCcostos)
        {
            List<Contratista> contratistas = new List<Contratista>();

            try
            {
                using (var conexion = new SqlConnection(cadenaSQL))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("DTW_SP_ModificaContratista", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pidContratistas", SqlDbType.Int).Value = IdContratistas;
                    cmd.Parameters.Add("@pidTipoContratista", SqlDbType.Int).Value = idTipoContratista;
                    cmd.Parameters.Add("@pidEstatus", SqlDbType.Int).Value = idEstatus;
                    cmd.Parameters.Add("@pNombreContratista", SqlDbType.VarChar).Value = NombreContratista;
                    cmd.Parameters.Add("@pApPaterno", SqlDbType.VarChar).Value = ApPaterno;
                    cmd.Parameters.Add("@pApMaterno", SqlDbType.VarChar).Value = ApMaterno;
                    cmd.Parameters.Add("@pFechaIndSeguridad", SqlDbType.VarChar).Value = FechaIndSeguridad;
                    cmd.Parameters.Add("@pFechaContrato", SqlDbType.VarChar).Value = FechaContrato;
                    cmd.Parameters.Add("@pIMSS", SqlDbType.VarChar).Value = IMSS;
                    cmd.Parameters.Add("@pRFC", SqlDbType.VarChar).Value = RFC;
                    cmd.Parameters.Add("@pVigenciaIMSS", SqlDbType.VarChar).Value = VigenciaIMSS;
                    cmd.Parameters.Add("@pFechaTerminacion", SqlDbType.VarChar).Value = FechaTerminacion;
                    cmd.Parameters.Add("@pResponsableIndorama", SqlDbType.VarChar).Value = ResponsableIndorama;
                    cmd.Parameters.Add("@pidClasificacion", SqlDbType.Int).Value = idClasificacion;
                    cmd.Parameters.Add("@pNombreCompañia", SqlDbType.VarChar).Value = NombreCompañia;
                    cmd.Parameters.Add("@pNumeroContrato", SqlDbType.VarChar).Value = NumeroContrato;
                    cmd.Parameters.Add("@pGafete", SqlDbType.VarChar).Value = Gafete;
                    cmd.Parameters.Add("@pidComedor", SqlDbType.Int).Value = idComedor;
                    cmd.Parameters.Add("@pidCcostos", SqlDbType.Int).Value = idCcostos;
                    cmd.Parameters.Add("Response", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteReader();
                    string Response = (string)cmd.Parameters["Response"].Value;
                    if (Response != "Ok")
                    {
                        return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = Response });
                    }
                }
                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Datos actualizados" });
            }
            catch (Exception error)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message });

            }
        }

        [HttpGet("exportar/Excel")]
        public async Task<IActionResult> CatalogoContratistasExcel(int IdEstatus, int IdTipoContratista, string? Empresa, string? Ccostos, string? Clasificacion, int? Comedor, string? Filtro)
        {
            List<Contratista> contratistas = new List<Contratista>();
            var clasificaciones = await _context.Clasificacions.ToListAsync();
            var ccostos = await _context.Ccostos.ToListAsync();
            
            try
            {
                using (var conexion = new SqlConnection(cadenaSQL))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("DTW_SP_Contratistas", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = IdEstatus;
                    cmd.Parameters.Add("@pTipoContratista", SqlDbType.Int).Value = IdTipoContratista;
                    cmd.Parameters.Add("@pEmpresa", SqlDbType.VarChar).Value = Empresa ?? (object)DBNull.Value;
                    cmd.Parameters.Add("@pCentroCostos", SqlDbType.VarChar).Value = Ccostos ?? (object)DBNull.Value;
                    cmd.Parameters.Add("@pClasificacion", SqlDbType.VarChar).Value = Clasificacion ?? (object)DBNull.Value;
                    cmd.Parameters.Add("@pComedor", SqlDbType.Int).Value = Comedor;
                    cmd.Parameters.Add("@pFiltro", SqlDbType.VarChar).Value = Filtro ?? (object)DBNull.Value;
                    cmd.Parameters.Add("@pPAG", SqlDbType.Int).Value = 0; 
                    cmd.Parameters.Add("@pREG", SqlDbType.Int).Value = 0; 
                    cmd.Parameters.Add("@pExportar", SqlDbType.Bit).Value = 1; // Indica que es una exportación

                    using (var rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            contratistas.Add(new Contratista()
                            {
                                IdContratistas = Convert.ToInt32(rd["idContratistas"]),
                                IdTipoContratista = Convert.ToInt32(rd["idTipoContratista"]),
                                NombreContratista = rd["NombreContratista"].ToString(),
                                ApPaterno = rd["ApPaterno"].ToString(),
                                ApMaterno = rd["ApMaterno"].ToString(),
                                FechaIndSeguridad = Convert.ToDateTime(rd["FechaIndSeguridad"]),
                                FechaContrato = Convert.ToDateTime(rd["FechaContrato"]),
                                FechaTerminacion = Convert.ToDateTime(rd["FechaTerminacion"]),
                                Imss = rd["IMSS"].ToString(),
                                VigenciaImss = Convert.ToDateTime(rd["VigenciaIMSS"]),
                                NumeroContrato = (rd["NumeroContrato"]).ToString(),
                                NombreCompañia = rd["NombreCompañia"].ToString(),
                                Rfc = rd["RFC"].ToString(),
                                IdClasificacion = Convert.ToInt32(rd["idClasificacion"]),
                                ResponsableIndorama = rd["ResponsableIndorama"].ToString(),
                                IdCcostos = Convert.ToInt32(rd["idCcostos"]),
                                IdGafete = Convert.ToInt32(rd["Gafete"]),
                                NumeroProvisionalGafete = Convert.ToInt32(rd["NumeroProvisionalGafete"]),
                                IdComedor = Convert.ToInt32(rd["DerechoComedor"]),
                                IdEstatus = Convert.ToInt32(rd["IdEstatus"])
                            });
                        }
                    }
                }
                if (contratistas.Count > 0)
                {
                    // Se crrea el archivo Excel
                    using (var workbook = new XLWorkbook())
                    {
                        var worksheet = workbook.Worksheets.Add("Contratistas");

                        // Se agrean encabezados
                        worksheet.Cell(1, 1).Value = "Tipo Contratista";
                        worksheet.Cell(1, 2).Value = "Nombre Contratista";
                        worksheet.Cell(1, 3).Value = "Fecha Induccion";
                        worksheet.Cell(1, 4).Value = "Fecha Inicio Contrato";
                        worksheet.Cell(1, 5).Value = "Fecha Terminación";
                        worksheet.Cell(1, 6).Value = "No. IMSS";
                        worksheet.Cell(1, 7).Value = "Vigencia IMSS";
                        worksheet.Cell(1, 8).Value = "No. Contrato";
                        worksheet.Cell(1, 9).Value = "Empresa";
                        worksheet.Cell(1, 10).Value = "RFC";
                        worksheet.Cell(1, 11).Value = "Clasificación";
                        worksheet.Cell(1, 12).Value = "Responsable Indorama";
                        worksheet.Cell(1, 13).Value = "Centro de Costos";
                        worksheet.Cell(1, 14).Value = "Gafete";
                        worksheet.Cell(1, 15).Value = "Comedor";
                        worksheet.Cell(1, 16).Value = "Estatus";

                        // Se agrega el filtro
                        worksheet.Range("A1:P1").SetAutoFilter();

                        // Se llenan los datos
                        for (int i = 0; i < contratistas.Count; i++)
                        {
                            var contr = contratistas[i];
                            var clasificacion = clasificaciones.FirstOrDefault(c => c.IdClasificacion == contr.IdClasificacion);
                            var ccosto = ccostos.FirstOrDefault(c => c.IdCcostos == contr.IdCcostos);
                                                        
                            worksheet.Cell(i + 2, 1).Value = contr.IdTipoContratista == 1 ? "Corto Plazo" : "Residente";
                            worksheet.Cell(i + 2, 2).Value = $"{contr.NombreContratista}{contr.ApPaterno}{contr.ApMaterno}";
                            worksheet.Cell(i + 2, 3).Value = contr.FechaIndSeguridad;
                            worksheet.Cell(i + 2, 4).Value = contr.FechaContrato;
                            worksheet.Cell(i + 2, 5).Value = contr.FechaTerminacion;
                            worksheet.Cell(i + 2, 6).Value = contr.Imss;
                            worksheet.Cell(i + 2, 7).Value = contr.VigenciaImss;
                            worksheet.Cell(i + 2, 8).Value = contr.NumeroContrato;
                            worksheet.Cell(i + 2, 9).Value = contr.NombreCompañia;
                            worksheet.Cell(i + 2, 10).Value = contr.Rfc;
                            worksheet.Cell(i + 2, 11).Value = clasificacion != null ? Convert.ToDecimal(clasificacion.Clasificacion1) : "";
                            worksheet.Cell(i + 2, 12).Value = contr.ResponsableIndorama;
                            worksheet.Cell(i + 2, 13).Value = ccosto != null ? ccosto.Ccostos : "";
                            worksheet.Cell(i + 2, 14).Value = contr.IdGafete == 0 ? "" : contr.IdGafete;
                            worksheet.Cell(i + 2, 15).Value = contr.IdComedor == 1 ? "SI" : "NO";
                            worksheet.Cell(i + 2, 16).Value = contr.IdEstatus == 1 ? "Activo" : "Inactivo";
                        }

                        // Se ajusta el tamaño de las columnas
                        worksheet.Columns().AdjustToContents();

                        using (var stream = new MemoryStream())
                        {
                            workbook.SaveAs(stream);
                            var content = stream.ToArray();
                            return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Contratistas.xlsx");
                        }
                    }
                }

                return NotFound(new { mensaje = "No se encontraron resultados" });
            }
            catch (Exception error)
            {          
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message });
            }
        }

        [HttpGet("exportar/PDF")]
        public async Task<IActionResult> CatalogoContratistasPdf(int IdEstatus, int IdTipoContratista, string? Empresa, string? Ccostos, string? Clasificacion, int? Comedor, string? Filtro)
        {
            List<Contratista> contratistas = new List<Contratista>();
            var clasificaciones = await _context.Clasificacions.ToListAsync();
            var ccostos = await _context.Ccostos.ToListAsync();

            try
            {
                using (var conexion = new SqlConnection(cadenaSQL))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("DTW_SP_Contratistas", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = IdEstatus;
                    cmd.Parameters.Add("@pTipoContratista", SqlDbType.Int).Value = IdTipoContratista;
                    cmd.Parameters.Add("@pEmpresa", SqlDbType.VarChar).Value = Empresa ?? (object)DBNull.Value;
                    cmd.Parameters.Add("@pCentroCostos", SqlDbType.VarChar).Value = Ccostos ?? (object)DBNull.Value;
                    cmd.Parameters.Add("@pClasificacion", SqlDbType.VarChar).Value = Clasificacion ?? (object)DBNull.Value;
                    cmd.Parameters.Add("@pComedor", SqlDbType.Int).Value = Comedor;
                    cmd.Parameters.Add("@pFiltro", SqlDbType.VarChar).Value = Filtro ?? (object)DBNull.Value;
                    cmd.Parameters.Add("@pPAG", SqlDbType.Int).Value = 0;
                    cmd.Parameters.Add("@pREG", SqlDbType.Int).Value = 0;
                    cmd.Parameters.Add("@pExportar", SqlDbType.Bit).Value = 1;

                    using (var rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            contratistas.Add(new Contratista()
                            {
                                IdContratistas = Convert.ToInt32(rd["idContratistas"]),
                                IdTipoContratista = Convert.ToInt32(rd["idTipoContratista"]),
                                NombreContratista = rd["NombreContratista"].ToString(),
                                ApPaterno = rd["ApPaterno"].ToString(),
                                ApMaterno = rd["ApMaterno"].ToString(),
                                FechaIndSeguridad = Convert.ToDateTime(rd["FechaIndSeguridad"]),
                                FechaContrato = Convert.ToDateTime(rd["FechaContrato"]),
                                FechaTerminacion = Convert.ToDateTime(rd["FechaTerminacion"]),
                                Imss = rd["IMSS"].ToString(),
                                VigenciaImss = Convert.ToDateTime(rd["VigenciaIMSS"]),
                                NumeroContrato = (rd["NumeroContrato"]).ToString(),
                                NombreCompañia = rd["NombreCompañia"].ToString(),
                                Rfc = rd["RFC"].ToString(),
                                IdClasificacion = Convert.ToInt32(rd["idClasificacion"]),
                                ResponsableIndorama = rd["ResponsableIndorama"].ToString(),
                                IdCcostos = Convert.ToInt32(rd["idCcostos"]),
                                IdGafete = Convert.ToInt32(rd["Gafete"]),
                                NumeroProvisionalGafete = Convert.ToInt32(rd["NumeroProvisionalGafete"]),
                                IdComedor = Convert.ToInt32(rd["DerechoComedor"]),
                                IdEstatus = Convert.ToInt32(rd["IdEstatus"])
                            });
                        }
                    }
                }

                if (contratistas.Count > 0)
                {
                    // Se crea el documento PDF 
                    using (var document = new PdfDocument())
                    {
                        var page = document.AddPage();
                        page.Width = XUnit.FromMillimeter(297); 
                        page.Height = XUnit.FromMillimeter(210); 
                        XGraphics gfx = XGraphics.FromPdfPage(page);
                        XFont font = new XFont("Arial", 7, XFontStyleEx.Regular);
                        const int maxRowsPerPage = 24; // Límite de filas por página
                        double yPoint = 80; 
                        double lineHeight = 20; 

                        void DrawTableHeader(XGraphics gfx)
                        {
                            gfx.DrawString("Catálogo de Contratistas", new XFont("Verdana", 18, XFontStyleEx.Bold), XBrushes.Black, new XPoint(300, 50));
                            gfx.DrawLine(XPens.Black, 40, yPoint - 10, 800, yPoint - 10); 

                            // Encabezados de columnas
                            gfx.DrawString("Tipo", font, XBrushes.Black, new XPoint(55, yPoint + 4));
                            gfx.DrawString("Contratista", font, XBrushes.Black, new XPoint(45, yPoint + 15));
                            gfx.DrawString("Nombre Contratista", font, XBrushes.Black, new XPoint(150, yPoint + 13));
                            gfx.DrawString("Fecha", font, XBrushes.Black, new XPoint(300, yPoint + 4));
                            gfx.DrawString("Inducción", font, XBrushes.Black, new XPoint(295, yPoint + 15));
                            gfx.DrawString("Fecha Inicio", font, XBrushes.Black, new XPoint(332, yPoint + 4));
                            gfx.DrawString("Contrato", font, XBrushes.Black, new XPoint(338, yPoint + 15));
                            gfx.DrawString("Fecha", font, XBrushes.Black, new XPoint(382, yPoint + 4));
                            gfx.DrawString("Terminación", font, XBrushes.Black, new XPoint(373, yPoint + 15));
                            gfx.DrawString("No. IMSS", font, XBrushes.Black, new XPoint(423, yPoint + 12));
                            gfx.DrawString("Vigencia", font, XBrushes.Black, new XPoint(472, yPoint + 4));
                            gfx.DrawString("IMSS", font, XBrushes.Black, new XPoint(477, yPoint + 15));
                            gfx.DrawString("No.", font, XBrushes.Black, new XPoint(520, yPoint + 4));
                            gfx.DrawString("Contrato", font, XBrushes.Black, new XPoint(515, yPoint + 15));
                            gfx.DrawString("Empresa", font, XBrushes.Black, new XPoint(663, yPoint + 10));
                            gfx.DrawLine(XPens.Black, 40, yPoint + 20, 800, yPoint + 20); // Línea debajo de encabezados
                            yPoint += 30; // Espacio después de los encabezados
                        }

                        DrawTableHeader(gfx);

                        double lineStartY = yPoint - 40;

                        for (int i = 0; i < contratistas.Count; ++i)
                        {
                            if (i > 0 && i % maxRowsPerPage == 0)
                            {
                                page = document.AddPage();
                                page.Width = XUnit.FromMillimeter(297);
                                page.Height = XUnit.FromMillimeter(210);
                                yPoint = 80; 
                                gfx = XGraphics.FromPdfPage(page);
                                DrawTableHeader(gfx);
                            }

                            // Lineas verticales
                            gfx.DrawLine(XPens.Black, 40, lineStartY, 40, yPoint + lineHeight / 2); 
                            gfx.DrawLine(XPens.Black, 85, lineStartY, 85, yPoint + lineHeight / 2); 
                            gfx.DrawLine(XPens.Black, 288, lineStartY, 288, yPoint + lineHeight / 2); 
                            gfx.DrawLine(XPens.Black, 330, lineStartY, 330, yPoint + lineHeight / 2); 
                            gfx.DrawLine(XPens.Black, 372, lineStartY, 372, yPoint + lineHeight / 2); 
                            gfx.DrawLine(XPens.Black, 414, lineStartY, 414, yPoint + lineHeight / 2); 
                            gfx.DrawLine(XPens.Black, 465, lineStartY, 465, yPoint + lineHeight / 2); 
                            gfx.DrawLine(XPens.Black, 507, lineStartY, 507, yPoint + lineHeight / 2); 
                            gfx.DrawLine(XPens.Black, 550, lineStartY, 550, yPoint + lineHeight / 2); 
                            gfx.DrawLine(XPens.Black, 800, lineStartY, 800, yPoint + lineHeight / 2); 

                            var contr = contratistas[i];
                            var clasificacion = clasificaciones.FirstOrDefault(c => c.IdClasificacion == contr.IdClasificacion);
                            var ccosto = ccostos.FirstOrDefault(c => c.IdCcostos == contr.IdCcostos);

                            gfx.DrawString(contr.IdTipoContratista == 1 ? "Corto Plazo" : "Residente", font, XBrushes.Black, new XRect(45, yPoint, page.Width, page.Height), XStringFormats.TopLeft);
                            gfx.DrawString($"{contr.NombreContratista} {contr.ApPaterno} {contr.ApMaterno}", font, XBrushes.Black, new XRect(90, yPoint, page.Width, page.Height), XStringFormats.TopLeft);
                            gfx.DrawString(contr.FechaIndSeguridad.ToShortDateString(), font, XBrushes.Black, new XRect(290, yPoint, page.Width, page.Height), XStringFormats.TopLeft);
                            gfx.DrawString(contr.FechaContrato.ToShortDateString(), font, XBrushes.Black, new XRect(332, yPoint, page.Width, page.Height), XStringFormats.TopLeft);
                            gfx.DrawString(contr.FechaTerminacion.ToShortDateString(), font, XBrushes.Black, new XRect(375, yPoint, page.Width, page.Height), XStringFormats.TopLeft);
                            gfx.DrawString(string.IsNullOrEmpty(contr.Imss) ? "" : Convert.ToDecimal(contr.Imss).ToString(), font, XBrushes.Black, new XRect(418, yPoint, page.Width, page.Height), XStringFormats.TopLeft);
                            gfx.DrawString(contr.VigenciaImss.ToShortDateString(), font, XBrushes.Black, new XRect (468, yPoint, page.Width, page.Height), XStringFormats.TopLeft);
                            gfx.DrawString(string.IsNullOrEmpty(contr.NumeroContrato) ? "" : (contr.NumeroContrato).ToString(), font, XBrushes.Black, new XRect(509, yPoint, page.Width, page.Height), XStringFormats.TopLeft);
                            gfx.DrawString(string.IsNullOrEmpty(contr.NombreCompañia) ? "" : contr.NombreCompañia.ToString(), font, XBrushes.Black, new XRect(555, yPoint, page.Width, page.Height), XStringFormats.TopLeft);


                            gfx.DrawLine(XPens.Black, 40, yPoint + lineHeight / 2, 800, yPoint + lineHeight / 2); // Línea de separación
                            yPoint += lineHeight; // Aumentar el espacio entre filas
                            
                        }

                        using (var stream = new MemoryStream())
                        {
                            document.Save(stream, false);
                            var content = stream.ToArray();
                            return File(content, "application/pdf", "Contratistas.pdf");
                        }
                    }
                }

                return NotFound(new { mensaje = "No se encontraron resultados" });
            }
            catch (Exception error)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message });
            }
        }

    }
}
