﻿using DW_INDORAMA_API.Data.IndoramaModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Data;

namespace DW_INDORAMA_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LogOutController : ControllerBase
    {
        private readonly string cadenaSQL;
        public LogOutController(IConfiguration configuracion)
        {
            cadenaSQL = configuracion.GetConnectionString("IndoramaConnection");
        }

        [HttpPost]
        public IActionResult AgregarPeticion([FromBody] DtwCaLogUser logUser)
        {
            try
            {
                using (var conexion = new SqlConnection(cadenaSQL))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("[DTW_SP_LogOut]", conexion);
                    cmd.Parameters.AddWithValue("pIdUser", logUser.Iduser);
                    cmd.Parameters.AddWithValue("pMessageOperation", logUser.MessageOperation);
                    cmd.Parameters.Add("Response", SqlDbType.VarChar, 2).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteReader();
                    string Response = (string)cmd.Parameters["Response"].Value;
                    if (Response != "Ok")
                    {
                        return StatusCode(StatusCodes.Status401Unauthorized, new { mensaje = "No" });
                    }
                }
                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Ok" });
            }
            catch (Exception error)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = error.Message });

            }
        }

    }
}
