﻿using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using Microsoft.AspNetCore.Mvc;

namespace DW_INDORAMA_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PlantasController : ControllerBase
    {
        private readonly IndoramaAccessControlContext _context;
        public PlantasController(IndoramaAccessControlContext context) 
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Plantum> Get()
        {
            return _context.Planta.ToList();
        }
    }
}
