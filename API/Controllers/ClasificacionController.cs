﻿using DW_INDORAMA_API.Data;
using DW_INDORAMA_API.Data.IndoramaModels;
using Microsoft.AspNetCore.Mvc;

namespace DW_INDORAMA_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClasificacionController : ControllerBase
    {
        private readonly IndoramaAccessControlContext _context;
        public ClasificacionController(IndoramaAccessControlContext context)
        {
           _context = context;
        }

        [HttpGet]
        public IEnumerable<Clasificacion> Get()
        {
            return _context.Clasificacions.ToList();    
        }
    }
}
