import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {Md5} from 'ts-md5';

import { User } from '../../login/interfaces/user.interface';
import { Router } from '@angular/router';
import { AppConfigService } from '../../shared/services/appConfig.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private encrypter: Md5 = new Md5();

  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();

  public users: User[] = [];

  public user: User = {
    idUsuarios:         0,
    usuario1:           '',
    password:           '',
    nombreUsuario:      '',
    idPerfil:           0,
    idPerfilNavigation: null
  }

  constructor(
    private http: HttpClient,
    private router: Router,
    public snackbar: MatSnackBar
    ) { }

  public getAllUsers(): void {
    this.http.get<User[]>(`${this.serviceUrl}/Users`)
      .subscribe(
        resp => {
          this.users = resp;
        },
        error => {

        }
      );
  }

  public getUserById(idUser: number): void {
    if(idUser == 0){
      this.user = {
        idUsuarios:         0,
        usuario1:           '',
        password:           '',
        nombreUsuario:      '',
        idPerfil:           0,
        idPerfilNavigation: null
      }
    } else {
      this.http.get<User>(`${this.serviceUrl}/Users/${idUser}`)
      .subscribe(
        resp => {
          this.user = resp;
        }
      )
    }
  }

  public deleteUser(idUser: number): void {
    this.http.delete(`${this.serviceUrl}/Users/${idUser}`)
      .subscribe(
        resp => {
          alert("Usuario eliminado exitosamente");
          location.reload();
        }
      );
  }

  public createUser(user: User): void {
    const body = {
      "idUsuarios": user.idUsuarios,
      "usuario1":   user.usuario1,
      "password":   this.encryptPassword(user.password),
      "nombreUsuario": user.nombreUsuario,
      "idPerfil":   user.idPerfil,
      "dtwCaLogUsers": [],
    }
    this.http.post(`${this.serviceUrl}/Users`, body)
    .subscribe(
      resp =>{
        if (resp) {
          this.openSnackBar("Usuario creado exitosamente", "Aceptar");
          //alert("Usuario creado exitosamente");
          this.goToRoute('catalogues/users')
        }
      }
    )
  }

  public editUser(user: User): void {
    const body = {
      "idUsuarios": user.idUsuarios,
      "usuario1":   user.usuario1,
      "password":   user.password,
      "nombreUsuario": user.nombreUsuario,
      "idPerfil":   user.idPerfil,
      "dtwCaLogUsers": []
    }
    console.log("Edit user");
    console.log(body);
    this.http.put(`${this.serviceUrl}/Users/${user.idUsuarios}`, body)
      .subscribe(
        resp => {
          if (resp == null) {
            this.openSnackBar("Usuario actualizado con éxito", "Aceptar")
            //alert("Usuario actualizado con éxito");
            this.goToRoute('catalogues/users');
          }
        }
      )
  }

  public encryptPassword(password: string): string | undefined {
    this.encrypter.start();
    this.encrypter.appendStr(password);
    let encryptedPassword = this.encrypter.end()?.toString();
    return encryptedPassword;
  }

  public validateUserExistence(userValue: string): boolean {
    var userArray: string[] = [];
    console.log(this.users);
    this.users.forEach(item => {
      userArray.push(item.usuario1);
    })

    var userExists = userArray.includes(userValue);
    return userExists;
  }

  public goToRoute(route: string){
    this.router.navigate([route]);
  }

  openSnackBar(message: string, action: string): void {
    this.snackbar.open(message, action, {
      duration: 3000
    })
  }

}
