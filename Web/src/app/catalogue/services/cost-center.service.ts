import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from '../../shared/services/appConfig.service';
import { CostCenter } from '../interfaces/cost-center.interface';

@Injectable({
  providedIn: 'root'
})
export class CostCenterService {

  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();

  public costCenter: CostCenter = {
      idCcostos:          0,
      ccostos:            '',
      descripcion:        '',
      fechaActualizacion: new Date(),
      contratista:        [],
      empleados:          [],
      marcajes:           []
  };

  public costCenters: CostCenter[] = [];

  constructor(
    private http: HttpClient,
    ) { }

  public getAllCostCenters(): void {
    this.http.get<CostCenter[]>(`${this.serviceUrl}/CentroCostos`)
      .subscribe(
        resp => {
          this.costCenters = resp;
        }
      );
  }

  public getCostCenterById(idCostCenter: number){
    this.http.get<CostCenter>(`${this.serviceUrl}/CentroCostos/${idCostCenter}`)
      .subscribe(
        resp => {
          this.costCenter = resp;
        }
      )
  }

}
