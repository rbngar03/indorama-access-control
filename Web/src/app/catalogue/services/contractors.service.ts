import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Contractor, Contractors } from '../interfaces/contractor.interface';
import { AppConfigService } from '../../shared/services/appConfig.service';
import { Observable } from 'rxjs/internal/Observable';
import { GenericHttpResponse } from '../../shared/interfaces/generic-http-response';

@Injectable({
  providedIn: 'root'
})
export class ContractorsService {

  //private serviceUrl: string = 'http://192.168.10.20:9090';
  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();
  //private serviceUrl : string = 'https://localhost:44305';

  public contractors: Contractors = {
    response: []
  };

  public contractor: Contractor[] = [];

  constructor(
    private http: HttpClient,
    ) { }

  public getAllContractors(pag : number, reg : number, idEstatus? : number, idTipoContratista? : string, empresa? : string, cCostos? : string, clasificacion? : string, comedor? : string, filtro? : string): void {

    let filtros : string = `?pag=${pag}&reg=${reg}`;
    let filtrosArray: string[] = [`pag=${pag}`, `reg=${reg}`];

    if (idEstatus) filtrosArray.push(`idEstatus=${idEstatus}`);
    if (idTipoContratista) filtrosArray.push(`idTipoContratista=${idTipoContratista}`);
    if (empresa) filtrosArray.push(`Empresa=${empresa}`);
    if (cCostos) filtrosArray.push(`Ccostos=${cCostos}`);
    if (clasificacion) filtrosArray.push(`Clasificacion=${clasificacion}`);
    if (comedor) filtrosArray.push(`Comedor=${comedor}`);
    if (filtro) filtrosArray.push(`Filtro=${filtro}`);

    filtros = filtrosArray.join('&');

    this.http.get<Contractors>(`${this.serviceUrl}/Contratistas?${filtros}`)
    .subscribe(
      resp => {
        this.contractors = resp;
      },
      error => {
        console.error(error);
        this.contractors = { response:[] };
      }
    );
  }

  public getContractorById(idContractor: number): Observable<Contractor> {
    return this.http.get<Contractor>(`${this.serviceUrl}/Contratistas/${idContractor}`);
  }

  public putContractor(contractor: Contractor) {
    let contractorString = `?idContratistas=${contractor.idContratistas}&idTipoContratista=${contractor.idTipoContratista}&idEstatus=${contractor.idEstatus}&NombreContratista=${contractor.nombreContratista}&ApPaterno=${contractor.apPaterno}&ApMaterno=${contractor.apMaterno}&FechaIndSeguridad=${contractor.fechaIndSeguridad}&FechaContrato=${contractor.fechaContrato}&IMSS=${contractor.imss}&RFC=${contractor.rfc}&VigenciaIMSS=${contractor.vigenciaImss}&FechaTerminacion=${contractor.fechaTerminacion}&ResponsableIndorama=${contractor.responsableIndorama}&idClasificacion=${contractor.idClasificacion}&NombreCompañia=${contractor.nombreCompania}&NumeroContrato=${contractor.numeroContrato}&Gafete=${contractor.idGafete}&idComedor=${contractor.idComedor}&idCcostos=${contractor.idCcostos}`;
    return this.http.put<GenericHttpResponse<Contractor>>(`${this.serviceUrl}/Contratistas${contractorString}`, {});
  }
}
