import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from '../../shared/services/appConfig.service';
import { Observable } from 'rxjs/internal/Observable';
import { CCosto } from '../interfaces/ccosto.interface';

@Injectable({
  providedIn: 'root'
})
export class CCostosService {

  //private serviceUrl: string = 'http://192.168.10.20:9090';
  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();
  //private serviceUrl : string = 'http://localhost:5282';

  constructor(
    private http: HttpClient,
    ) { }

  public getAllCentrosCostos(): Observable<CCosto[]> {
    return this.http.get<CCosto[]>(`${this.serviceUrl}/CentroCostos`);
  }
}
