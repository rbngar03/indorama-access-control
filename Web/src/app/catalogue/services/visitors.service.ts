import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Visitor, VisitorByID } from '../interfaces/visitor.interface';
import { Visitors } from '../interfaces/visitor.interface';
import { AppConfigService } from '../../shared/services/appConfig.service';

@Injectable({
  providedIn: 'root'
})
export class VisitorsService {

  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();

  public visitors: Visitors = {
    response: []
  };

  public visitor: Visitor = {
        idVisitantes:       0,
        fechaIndSeguridad:  null,
        fechaIngreso:       new Date(),
        fechaIngresoF:      '',
        fechaTerminacion:   null,
        horas:              '',
        nombreVisitante:    '',
        idOrigen:           null,
        descOrigen:         '',
        foto:               null,
        idGafete:           0,
        empResponsable:     '',
        notas:              null,
        rfc:                '',
        idComedor:          0,
        idCcostos:          0,
        archivoAdjunto:     null,
        idEstatus:          1,
        fechaActualizacion: null,
        idPlanta:           null,
        idPlantaNavigation: null,
        marcajes:           []
  }

  constructor(
    private http: HttpClient,
    ) { }

  public getAllVisitors(pag: number, reg: number, idStatus: number, idVisitor: number, enterprise: string, idCostCenter: number, keyword: string): void {
    this.http.get<Visitors>(`${this.serviceUrl}/Visitantes?pag=${pag}&reg=${reg}&IdEstatus=${idStatus}&IdVisitantes=${idVisitor}&Empresa=${enterprise}&Ccostos=${idCostCenter}&Filtro=${keyword}`)
      .subscribe(
        resp => {
          this.visitors = resp;
        }
      );
  }

  public getVisitorById(idVisitor: number){
    if (idVisitor == 0) {
      var visitor: Visitor = {
        idVisitantes:       0,
        fechaIndSeguridad:  null,
        fechaIngreso:       new Date(),
        fechaIngresoF:      '',
        fechaTerminacion:   null,
        horas:              '',
        nombreVisitante:    '',
        idOrigen:           null,
        descOrigen:         '',
        foto:               null,
        idGafete:           0,
        empResponsable:     '',
        notas:              null,
        rfc:                '',
        idComedor:          0,
        idCcostos:          0,
        archivoAdjunto:     null,
        idEstatus:          1,
        fechaActualizacion: null,
        idPlanta:           null,
        idPlantaNavigation: null,
        marcajes:           []
      }
      this.visitor = visitor;
    } else {
      this.http.get<VisitorByID>(`${this.serviceUrl}/Visitantes/${idVisitor}`)
      .subscribe(
        resp => {
          this.visitor = resp.response;
        }
      )
    }
  }

  public addVisitor(visitor: Visitor): void {
    const body = '';
    this.http.post<any>(`${this.serviceUrl}/Visitantes?NombreVisitante=${visitor.nombreVisitante}&FehaIngreso=${visitor.fechaIngresoF}&RFC=${visitor.rfc}&Empresa=${visitor.descOrigen}&Horas=${visitor.horas}&EmpleadoResponsable=${visitor.empResponsable}&IdEstatus=${visitor.idEstatus}&Ccostos=${visitor.idCcostos}&DerechoComedor=${visitor.idComedor}`, body)
    .subscribe(
      resp => {
        console.log(resp);
      }
    )
  }

  public editVisitor(visitor: Visitor): void {
    const body = '';
    this.http.put<any>(`${this.serviceUrl}/Visitantes?IdEstatus=${visitor.idEstatus}&IdVisitantes=${visitor.idVisitantes}&Empresa=${visitor.descOrigen}&EmpleadoResponsable=${visitor.empResponsable}&DerechoComedor=${visitor.idComedor}&Horas=${visitor.horas}`, body)
    .subscribe(
      resp => {
        console.log(resp);
      }
    )
  }

}
