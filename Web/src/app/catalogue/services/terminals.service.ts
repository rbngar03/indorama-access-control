import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Terminal } from '../interfaces/terminal.interface';
import { AppConfigService } from '../../shared/services/appConfig.service';
import { Observable } from 'rxjs/internal/Observable';
import { GenericHttpResponse } from '../../shared/interfaces/generic-http-response';

@Injectable({
  providedIn: 'root'
})
export class TerminalsService {

  //private serviceUrl: string = 'http://192.168.10.46:9090';
  //private serviceUrl: string = 'http://localhost:5282';
  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();

  public terminals: Terminal[] = [];

  constructor(
    private http: HttpClient,
    ) { }

  public getAllTerminals(): void {
    this.http.get<Terminal[]>(`${this.serviceUrl}/Terminales`)
      .subscribe(
        resp => {
          this.terminals = resp;
        },
        error => {
        }
      );
  }

  public getTerminalById(idTerminal: number): Observable<Terminal> {
    return this.http.get<Terminal>(`${this.serviceUrl}/Terminales/${idTerminal}`);
  }

  public postTerminal(terminal: Terminal) {
    return this.http.post<GenericHttpResponse<Terminal>>(`${this.serviceUrl}/Terminales`, terminal);
  }

  public putTerminal(terminal: Terminal, idTerminal : number) {
    return this.http.put<GenericHttpResponse<Terminal>>(`${this.serviceUrl}/Terminales/${idTerminal}`, terminal);
  }

  /*public getTerminalById(idTerminal: number): void{
    this.http.get<Terminal[]>(`${this.serviceUrl}/Terminales/${idTerminal}`)
      .subscribe(
        resp => {
          this.terminals = resp;
          //console.log(this.terminals);
        }
      )
  }*/
}
