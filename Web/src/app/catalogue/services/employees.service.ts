import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employees, Employee, EmployeeByID } from '../interfaces/employee.interface';
import { Observable, filter } from 'rxjs';
import { AppConfigService } from '../../shared/services/appConfig.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();

  public employees: Employees = {
    response: []
  };

   public employee: Employee = {
    idEmpleado: 0,
    numEmpleado: '',
    apePaterno: '',
    apeMaterno: '',
    nombre:                            '',
    idCcostos:                         0,
    idTipoNomina:                      0,
    nominaProcesada:                   '',
    idEstatus:                         0,
    idGafete:                          0,
    foto:                              '',
    notas:                             null,
    numeroProvisionalGafete:           0,
    idPlanta:                          0,
    fechaActualizacion:                null,
    idCcostosNavigation:               null,
    idEstatusNavigation:               null,
    idGafeteNavigation:                null,
    idPlantaNavigation:                null,
    idTipoNominaNavigation:            null,
    marcajes:                          [],
    numeroProvisionalGafeteNavigation: null,
  }

  constructor(
    private http: HttpClient,
    private router: Router
    ) { }

  public getAllEmployees(pag: number, reg: number, idStatus: number, idPlant: number, idCostCenter: number, idEmployee: number, sFilter: string): void {
    this.http.get<Employees>(`${this.serviceUrl}/Empleados?pag=${pag}&reg=${reg}&IdEstatus=${idStatus}&IdPlanta=${idPlant}&IdCcostos=${idCostCenter}&IdEmpleado=${idEmployee}&Filtro=${sFilter}`)
      .subscribe(
        resp => {
          this.employees = resp;
        },
        error => {
          //this.handleError();
        }
      );
  }

  public getEmployeeById(idEmployee: number){
    this.http.get<EmployeeByID>(`${this.serviceUrl}/Empleados/${idEmployee}`)
      .subscribe(
        resp => {
          this.employee = resp.response;
        }
      )
  }

  public getEmployees(idEmployee: number): Observable<Employee[]> {
    return this.http.get<Employee[]>(`${this.serviceUrl}/Empleados/${idEmployee}`);
  }

  public putEmployee(employee: Employee): void{
    const body = "";
    this.http.put(`${this.serviceUrl}/Empleados?IdEmpleado=${employee.idEmpleado}&NumEmpleado=${employee.numEmpleado}&ApePaterno=${employee.apePaterno}&ApeMaterno=${employee.apeMaterno}&Nombre=${employee.nombre}&idCcostos=${employee.idCcostos}&IdEstatus=${employee.idEstatus}&Gafete=${employee.idGafete}&IdPlanta=${employee.idPlanta}`, body)
      .subscribe(
        resp => {
          if (resp == null) {
            alert("Empleado actualizado con éxito");
            this.goToRoute('catalogues/employees');
          }
        }
      )
  }

  public goToRoute(route: string){
    this.router.navigate([route]);
  }

}
