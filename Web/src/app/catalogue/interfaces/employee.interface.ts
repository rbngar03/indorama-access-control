export interface Employees {
  response: Employee[];
}

export interface EmployeeByID {
  response: Employee;
}

export interface Employee {
  idEmpleado:                        number;
  numEmpleado:                       string;
  apePaterno:                        string;
  apeMaterno:                        string;
  nombre:                            string;
  idCcostos:                         number;
  idTipoNomina:                      number;
  nominaProcesada:                   string;
  idEstatus:                         number;
  idGafete:                          number;
  foto:                              string;
  notas:                             null;
  numeroProvisionalGafete:           number;
  idPlanta:                          number;
  fechaActualizacion:                null;
  idCcostosNavigation:               null;
  idEstatusNavigation:               null;
  idGafeteNavigation:                null;
  idPlantaNavigation:                null;
  idTipoNominaNavigation:            null;
  marcajes:                          any[];
  numeroProvisionalGafeteNavigation: null;
}
