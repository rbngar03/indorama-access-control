export interface Visitors {
  response: Visitor[];
}

export interface VisitorByID {
  response: Visitor;
}

export interface Visitor {
    idVisitantes:       number;
    fechaIndSeguridad:  null;
    fechaIngreso:       Date;
    fechaIngresoF:      string;
    fechaTerminacion:   null;
    horas:              string;
    nombreVisitante:    string;
    idOrigen:           null;
    descOrigen:         string;
    foto:               null;
    idGafete:           number;
    empResponsable:     string;
    notas:              null;
    rfc:                string;
    idComedor:          number;
    idCcostos:          number;
    archivoAdjunto:     null;
    idEstatus:          number;
    fechaActualizacion: null;
    idPlanta:           null;
    idPlantaNavigation: null;
    marcajes:           any[];
}


