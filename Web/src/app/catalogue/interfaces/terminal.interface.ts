export interface Terminal {
  idTerminales:           number;
  clave:                  string;
  ubicacion:              string;
  ip:                     string;
  nombre:                 string;
  idTipoLector:           number | null;
  fechaActualizacion:     Date;
  tipo:                   number;
  grupo:                  number;
  idTipoLectorNavigation: null;
  marcajes:               any[];
}
