export interface CostCenter {
  idCcostos:          number;
  ccostos:            string;
  descripcion:        string;
  fechaActualizacion: Date;
  contratista:        any[];
  empleados:          any[];
  marcajes:           any[];
}
