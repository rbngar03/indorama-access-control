export interface Contractors {
  response: Contractor[];
}

export interface Contractor {
  idContratistas:                    number;
  idTipoContratista:                 number;
  nombreContratista:                 string;
  apPaterno:                         string;
  apMaterno:                         string;
  fechaIndSeguridad:                 string;
  fechaContrato:                     string;
  fechaTerminacion:                  string;
  imss:                              string;
  vigenciaSeguridad:                 string;
  vigenciaImss:                      string;
  numeroContrato:                    string;
  periodoVigencia:                   string;
  idCompanias:                       number;
  nombreCompania:                    string;
  foto:                              string;
  fechaBaja:                         null;
  rfc:                               string;
  idClasificacion:                   number;
  responsableIndorama:               string;
  idCcostos:                         number;
  idGafete:                          number;
  notas:                             string;
  numeroProvisionalGafete:           number;
  idComedor:                         number;
  archivoAdjunto:                    string;
  idEstatus:                         number;
  fechaActualizacion:                null;
  idPlanta:                          null;
  idCcostosNavigation:               null;
  idClasificacionNavigation:         null;
  idComedorNavigation:               null;
  idCompaniasNavigation:             null;
  idEstatusNavigation:               null;
  idGafeteNavigation:                null;
  idPlantaNavigation:                null;
  idTipoContratistaNavigation:       null;
  marcajes:                          any[];
  numeroProvisionalGafeteNavigation: null;
}
