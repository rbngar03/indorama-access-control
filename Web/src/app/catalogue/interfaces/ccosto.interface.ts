export interface CCosto {
  idCcostos:          number,
  ccostos:            string,
  descripcion:        string,
  fechaActualizacion: Date,
  contratista:        null,
  empleados:          null,
  marcajes:           null
}
