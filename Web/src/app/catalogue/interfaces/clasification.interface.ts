export interface Clasification {
  idClasificacion:  number;
  clasificacion1:   string;
  descripcion:      string;
}
