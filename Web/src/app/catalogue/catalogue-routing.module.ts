import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersPageComponent } from './pages/users-catalogue-page/users-page.component';
import { EmployeesCataloguePageComponent } from './pages/employees-catalogue-page/employees-catalogue-page.component';
import { ContractorsCataloguePageComponent } from './pages/contractors-catalogue-page/contractors-catalogue-page.component';
import { VisitorsCataloguePageComponent } from './pages/visitors-catalogue-page/visitors-catalogue-page.component';
import { TerminalsCataloguePageComponent } from './pages/terminals-catalogue-page/terminals-catalogue-page.component';
import { TerminalDetailsPageComponent } from './pages/terminal-details-catalogue-page/terminal-details-page.component';
import { UserDetailsPageComponent } from './pages/user-details-catalogue-page/user-details-page.component';
import { EmployeeDetailsPageComponent } from './pages/employee-details-page/employee-details-page.component';
import { VisitorDetailsPageComponent } from './pages/visitor-details-page/visitor-details-page.component';
import { ContractorDetailsPageComponent } from './pages/contractor-details-catalogue-page/contractor-details-catalogue-page.component';

const routes: Routes = [
    { path: 'users', component: UsersPageComponent },
    { path: 'employees', component: EmployeesCataloguePageComponent},
    { path: 'contractors', component: ContractorsCataloguePageComponent},
    { path: 'visitors', component: VisitorsCataloguePageComponent},
    { path: 'terminals', component: TerminalsCataloguePageComponent},
    { path: 'userDetails/:idUser/:idProfile', component: UserDetailsPageComponent},
    { path: 'employeeDetails/:idEmployee', component: EmployeeDetailsPageComponent},
    { path: 'terminalDetails/:idTerminal', component: TerminalDetailsPageComponent},
    { path: 'visitorDetails/:idVisitor', component: VisitorDetailsPageComponent},
    { path: 'contractorDetails/:idContractor', component: ContractorDetailsPageComponent},
    { path: '**', redirectTo: 'login' },

  /*
  { path: 'not-found', component: NotFoundComponent },
  { path: '**',redirectTo: 'not-found' }*/
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogueRoutingModule { }
