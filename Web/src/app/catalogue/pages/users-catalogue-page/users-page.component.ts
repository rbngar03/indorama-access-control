import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { User } from '../../../login/interfaces/user.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'catalogues-users-page',
  templateUrl: './users-page.component.html',
  styleUrl: './users-page.component.css'
})
export class UsersPageComponent implements OnInit{

  constructor(
    private router: Router,
    private userService: UsersService
    ) {}

  ngOnInit(): void {
    this.userService.getAllUsers();
  }

  get users(): User[]{
    return this.userService.users;
  }

  public refresh(): void{
    this.userService.getAllUsers();
  }

  public createUser(){

  }

  goToRoute(route: string){
    this.router.navigate([route, 0, 0]);
  }
}
