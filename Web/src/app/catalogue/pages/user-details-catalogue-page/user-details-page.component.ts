import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UsersService } from '../../services/users.service';
import { User } from '../../../login/interfaces/user.interface';
import { PermissionsService } from '../../../login/services/permissions.service';
import { Profile } from '../../../login/interfaces/profile.interface';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'catalogue-user-details-page',
  templateUrl: './user-details-page.component.html',
  styleUrl: './user-details-page.component.css'
})
export class UserDetailsPageComponent implements OnInit{

  public title : string = '';
  public idUser: number = -1;
  public idProfile: number = 0;
  public isPasswordChanged = false;

  constructor(public snackbar: MatSnackBar,
              private route: ActivatedRoute,
              private permissionsService: PermissionsService,
              private userService: UsersService){
  }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.idUser = params["idUser"];
        this.idProfile = params["idProfile"];
      }
    );

    if (this.idUser == 0) {
      this.title = "Crear usuario";
      this.userService.getUserById(this.idUser);
    } else if (this.idUser >= 1) {
      this.title = "Editar usuario";
      this.userService.getUserById(this.idUser);
      this.permissionsService.getUserProfileById(this.idProfile);
    }
    this.permissionsService.getAllProfiles();

  }

  get user(): User{
    return this.userService.user;
  }

  get profile(): Profile {
    return this.permissionsService.profile;
  }

  get profiles(): Profile[] {
    return this.permissionsService.profiles;
  }

  public saveUser(user: User){
    if (user.idUsuarios == 0){
      var userExists= this.validateUniqueUser(user);
      if (!userExists)
        this.userService.createUser(user);
      else
        this.openSnackBar("El usuario ya existe.", "Aceptar");
    }
    if (user.idUsuarios > 0){
      if(this.isPasswordChanged) {
        const encryptedPassword: string | any = this.userService.encryptPassword(this.user.password);
        this.user.password = encryptedPassword;
      }
      this.userService.editUser(user);
    }
  }

  public savePassword(isPasswordChanged: boolean){
    this.isPasswordChanged = isPasswordChanged;
    console.log(this.isPasswordChanged);
  }

  public validateUniqueUser(user: User): boolean {
    var userExits: boolean = false;
    userExits = this.userService.validateUserExistence(user.usuario1);
    return userExits;
  }

  openSnackBar(message: string, action: string): void {
    this.snackbar.open(message, action, {
      duration: 3000
    })
  }

}
