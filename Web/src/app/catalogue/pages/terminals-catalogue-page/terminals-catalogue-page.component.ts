import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TerminalsService } from '../../services/terminals.service';
import { Terminal } from '../../interfaces/terminal.interface';

@Component({
  selector: 'app-terminals-catalogue-page',
  templateUrl: './terminals-catalogue-page.component.html',
  styleUrl: './terminals-catalogue-page.component.css'
})
export class TerminalsCataloguePageComponent implements OnInit{

  constructor(private terminalsService: TerminalsService, private router: Router) {}

  ngOnInit(): void {
    this.terminalsService.getAllTerminals();
  }

  get terminals(): Terminal[]{
    return this.terminalsService.terminals;
  }

  public refresh(): void{
    this.terminalsService.getAllTerminals();
  }

  goToRoute(route: string){
    this.router.navigate([route, 0]);
  }
}
