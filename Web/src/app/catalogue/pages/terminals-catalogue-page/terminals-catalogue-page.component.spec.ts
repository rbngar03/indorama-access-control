import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminalsCataloguePageComponent } from './terminals-catalogue-page.component';

describe('TerminalsCataloguePageComponent', () => {
  let component: TerminalsCataloguePageComponent;
  let fixture: ComponentFixture<TerminalsCataloguePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TerminalsCataloguePageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TerminalsCataloguePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
