import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesCataloguePageComponent } from './employees-catalogue-page.component';

describe('EmployeesCataloguePageComponent', () => {
  let component: EmployeesCataloguePageComponent;
  let fixture: ComponentFixture<EmployeesCataloguePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmployeesCataloguePageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(EmployeesCataloguePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
