import { Component, OnInit } from '@angular/core';
import { Employees } from '../../interfaces/employee.interface';
import { EmployeesService } from '../../services/employees.service';
import { PageEvent } from '@angular/material/paginator';
import { CostCenterService } from '../../services/cost-center.service';
import { CostCenter } from '../../interfaces/cost-center.interface';

@Component({
  selector: 'app-employees-catalogue-page',
  templateUrl: './employees-catalogue-page.component.html',
  styleUrl: './employees-catalogue-page.component.css',
})
export class EmployeesCataloguePageComponent implements OnInit {

  public pag: number = 0;
  public reg: number = 100;
  public idStatus: number = 1;
  public idPlant: number = 0;
  public idCostCenter: number = 0;
  public idEmployee: number = 0;
  public sFilter: string = '';

  constructor(private employeesService: EmployeesService,
              private costCenterService: CostCenterService) {}

  ngOnInit(): void {
    this.costCenterService.getAllCostCenters();
    this.employeesService.getAllEmployees(this.pag, this.reg, this.idStatus, this.idPlant, this.idCostCenter, this.idEmployee, this.sFilter);
  }

  get employees(): Employees{
    return this.employeesService.employees;
  }

  get costCenters(): CostCenter[] {
    return this.costCenterService.costCenters;
  }

  public refresh(): void{
    this.employeesService.getAllEmployees(this.pag, this.reg, this.idStatus, this.idPlant, this.idCostCenter, this.idEmployee, this.sFilter);
  }

  public changePlant(selectedPlant: string): void {
    if(selectedPlant == 'Todos') this.idPlant = 0;
    if(selectedPlant == 'Querétaro PET') this.idPlant = 1;
    if(selectedPlant == 'Santa Fe') this.idPlant = 2;
    if(selectedPlant == 'Querétaro FT') this.idPlant = 3;
    this.employeesService.getAllEmployees(this.pag, this.reg, this.idStatus, this.idPlant, this.idCostCenter, this.idEmployee, this.sFilter);
  }

  public changeCostCenter(selectedCostCenter: string): void {
    if(selectedCostCenter == 'Todos'){
      this.idCostCenter = 0;
    } else {
      let data: string[] = selectedCostCenter.split("-");
      this.idCostCenter = Number(data[0]);
    }
    this.employeesService.getAllEmployees(this.pag, this.reg, this.idStatus, this.idPlant, this.idCostCenter, this.idEmployee, this.sFilter);
  }

  public changeStatus(selectedStatus: string): void {
    if(selectedStatus == 'Todos') this.idStatus = 0;
    if(selectedStatus == 'Activo') this.idStatus = 1;
    if(selectedStatus == 'Inactivo') this.idStatus = 2;
    this.employeesService.getAllEmployees(this.pag, this.reg, this.idStatus, this.idPlant, this.idCostCenter, this.idEmployee, this.sFilter);
  }

  public changeFilter(searchCriteria: string): void {
    this.sFilter = searchCriteria;
    this.employeesService.getAllEmployees(this.pag, this.reg, this.idStatus, this.idPlant, this.idCostCenter, this.idEmployee, this.sFilter);
  }

  public changePage(pageEvent: PageEvent): void{
    this.reg = pageEvent.pageSize;
    this.employeesService.getAllEmployees(this.pag, this.reg, this.idStatus, this.idPlant, this.idCostCenter, this.idEmployee, this.sFilter);
  }
}
