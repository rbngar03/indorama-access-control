import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TerminalsService } from '../../services/terminals.service';

@Component({
  selector: 'catalogue-terminal-details-page',
  templateUrl: './terminal-details-page.component.html',
  styleUrl: './terminal-details-page.component.css'
})
export class TerminalDetailsPageComponent implements OnInit{

  public title : string = '';
  public idTerminal: number = -1;

  constructor(private route: ActivatedRoute,
              private terminalService: TerminalsService){
  }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.idTerminal = params["idTerminal"];
      }
    );

    if (this.idTerminal == 0) {
      this.title = "Agregar Terminal";

    } else if (this.idTerminal >= 1) {
      this.title = "Editar Terminal";
      this.terminalService.getTerminalById(this.idTerminal);
    }
  }

  /*get terminal(): Terminal{
    return this.terminalService.terminal;
  }*/

}
