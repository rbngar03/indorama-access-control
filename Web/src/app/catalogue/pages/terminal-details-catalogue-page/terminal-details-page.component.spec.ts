import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminalDetailsPageComponent } from './terminal-details-page.component';

describe('TerminalDetailsPageComponent', () => {
  let component: TerminalDetailsPageComponent;
  let fixture: ComponentFixture<TerminalDetailsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TerminalDetailsPageComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TerminalDetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
