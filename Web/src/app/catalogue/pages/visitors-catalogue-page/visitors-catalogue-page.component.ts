import { Component, OnInit } from '@angular/core';
import { VisitorsService } from '../../services/visitors.service';
import { Visitors } from '../../interfaces/visitor.interface';
import { PageEvent } from '@angular/material/paginator';
import { CostCenter } from '../../interfaces/cost-center.interface';
import { CostCenterService } from '../../services/cost-center.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-visitors-catalogue-page',
  templateUrl: './visitors-catalogue-page.component.html',
  styleUrl: './visitors-catalogue-page.component.css'
})
export class VisitorsCataloguePageComponent implements OnInit{

  public pag: number = 0;
  public reg: number = 100;
  public idStatus: number = 1;
  public enterprise: string = '';
  public idCostCenter: number = 0;
  public idVisitor: number = 0;
  public keyword: string = '';

  constructor(
    private costCenterService: CostCenterService,
    private router: Router,
    private visitorService: VisitorsService) {}

  ngOnInit(): void {
    this.costCenterService.getAllCostCenters();
    this.visitorService.getAllVisitors(this.pag, this.reg, this.idStatus, this.idVisitor, this.enterprise, this.idCostCenter, this.keyword);
  }

  get visitors(): Visitors{
    return this.visitorService.visitors;
  }

  get costCenters(): CostCenter[] {
    return this.costCenterService.costCenters;
  }

  public refresh(): void{
    this.visitorService.getAllVisitors(this.pag, this.reg, this.idStatus, this.idVisitor, this.enterprise, this.idCostCenter, this.keyword);
  }

  changeEnterprise(enterprise: string) {
    this.enterprise = enterprise;
    this.visitorService.getAllVisitors(this.pag, this.reg, this.idStatus, this.idVisitor, this.enterprise, this.idCostCenter, this.keyword);
  }

  public changeCostCenter(selectedCostCenter: string): void {
    if(selectedCostCenter == 'Todos'){
      this.idCostCenter = 0;
    } else {
      let data: string[] = selectedCostCenter.split("-");
      this.idCostCenter = Number(data[0]);
    }
    this.visitorService.getAllVisitors(this.pag, this.reg, this.idStatus, this.idVisitor, this.enterprise, this.idCostCenter, this.keyword);
  }

  public changeStatus(selectedStatus: string): void {
    if(selectedStatus == 'Todos') this.idStatus = 0;
    if(selectedStatus == 'Activo') this.idStatus = 1;
    if(selectedStatus == 'Inactivo') this.idStatus = 2;
    this.visitorService.getAllVisitors(this.pag, this.reg, this.idStatus, this.idVisitor, this.enterprise, this.idCostCenter, this.keyword);
  }

  public changeFilter(keyword: string): void {
    this.keyword = keyword;
    this.visitorService.getAllVisitors(this.pag, this.reg, this.idStatus, this.idVisitor, this.enterprise, this.idCostCenter, this.keyword);
  }

  public changePage(pageEvent: PageEvent): void{
    this.reg = pageEvent.pageSize;
    this.visitorService.getAllVisitors(this.pag, this.reg, this.idStatus, this.idVisitor, this.enterprise, this.idCostCenter, this.keyword);
  }

  goToRoute(route: string){
    this.router.navigate([route, 0]);
  }


}
