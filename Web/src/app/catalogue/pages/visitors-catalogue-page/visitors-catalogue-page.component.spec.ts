import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorsCataloguePageComponent } from './visitors-catalogue-page.component';

describe('VisitorsCataloguePageComponent', () => {
  let component: VisitorsCataloguePageComponent;
  let fixture: ComponentFixture<VisitorsCataloguePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VisitorsCataloguePageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(VisitorsCataloguePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
