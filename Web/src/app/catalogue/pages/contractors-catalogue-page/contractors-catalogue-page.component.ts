import { Component, OnInit } from '@angular/core';
import { ContractorsService } from '../../services/contractors.service';
import { Contractors } from '../../interfaces/contractor.interface';
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-contractors-catalogue-page',
  templateUrl: './contractors-catalogue-page.component.html',
  styleUrl: './contractors-catalogue-page.component.css'
})
export class ContractorsCataloguePageComponent implements OnInit{

  public pag               : number = 0;
  public reg               : number = 100;
  public idEstatus         : number = 1;
  public idTipoContratista : string = '';
  public empresa           : string = '';
  public cCostos           : string = '';
  public clasificacion     : string = '';
  public filtro            : string = '';

  constructor(private contractorsService: ContractorsService, private router: Router) {}

  ngOnInit(): void {
    this.contractorsService.getAllContractors(this.pag, this.reg, this.idEstatus, this.idTipoContratista, this.empresa, this.cCostos, this.clasificacion, undefined, this.filtro);
  }

  get contractors(): Contractors{
    return this.contractorsService.contractors;
  }

  public refresh(): void{
    this.contractorsService.getAllContractors(this.pag, this.reg, this.idEstatus, this.idTipoContratista, this.empresa, this.cCostos, this.clasificacion, undefined, this.filtro);
  }

  public onSearch(){
    this.contractorsService.getAllContractors(this.pag, this.reg, this.idEstatus, this.idTipoContratista, this.empresa, this.cCostos, this.clasificacion, undefined, this.filtro);
  }

  public changePage(pageEvent: PageEvent): void{
    this.reg = pageEvent.pageSize;
    this.contractorsService.getAllContractors(this.pag, this.reg, this.idEstatus, this.idTipoContratista, this.empresa, this.cCostos, this.clasificacion, undefined, this.filtro);
  }

  goToRoute(route: string){
    this.router.navigate([route, 0]);
  }
}
