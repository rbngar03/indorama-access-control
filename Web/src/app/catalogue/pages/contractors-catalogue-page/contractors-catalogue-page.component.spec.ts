import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorsCataloguePageComponent } from './contractors-catalogue-page.component';

describe('ContractorsCataloguePageComponent', () => {
  let component: ContractorsCataloguePageComponent;
  let fixture: ComponentFixture<ContractorsCataloguePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ContractorsCataloguePageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ContractorsCataloguePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
