import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorDetailsCataloguePageComponent } from './contractor-details-catalogue-page.component';

describe('ContractorDetailsCataloguePageComponent', () => {
  let component: ContractorDetailsCataloguePageComponent;
  let fixture: ComponentFixture<ContractorDetailsCataloguePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ContractorDetailsCataloguePageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ContractorDetailsCataloguePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
