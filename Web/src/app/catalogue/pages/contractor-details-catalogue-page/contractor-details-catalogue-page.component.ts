import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ContractorsService } from '../../services/contractors.service';

@Component({
  selector: 'catlogue-contractor-details-page',
  templateUrl: './contractor-details-catalogue-page.component.html',
  styleUrl: './contractor-details-catalogue-page.component.css'
})
export class ContractorDetailsPageComponent implements OnInit{
  public title : string = '';
  public idContractor: number = -1;

  constructor(private route: ActivatedRoute,
              private ContractorsService: ContractorsService){
  }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.idContractor = params["idContractor"];
      }
    );

    if (this.idContractor == 0) {
      this.title = "Agregar Contratista";

    } else if (this.idContractor >= 1) {
      this.title = "Editar Contratista";
      this.ContractorsService.getContractorById(this.idContractor);
    }
  }
}
