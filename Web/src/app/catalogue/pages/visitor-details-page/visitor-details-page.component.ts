import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { VisitorsService } from '../../services/visitors.service';
import { Visitor } from '../../interfaces/visitor.interface';

@Component({
  selector: 'app-visitor-details-page',
  templateUrl: './visitor-details-page.component.html',
  styleUrl: './visitor-details-page.component.css'
})
export class VisitorDetailsPageComponent implements OnInit{

  idVisitor: number = 0;
  title: string = '';

  constructor(private route: ActivatedRoute,
              private visitorService: VisitorsService) {}

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.idVisitor = params["idVisitor"];
        console.log(this.idVisitor);
      }
    );

    if (this.idVisitor == 0) {
      this.title = "Crear visitante";
      this.visitorService.getVisitorById(this.idVisitor);
    } else if (this.idVisitor >= 1) {
      this.title = "Editar visitante";
      this.visitorService.getVisitorById(this.idVisitor);
    }
  }

  get visitor(): Visitor{
    return this.visitorService.visitor;
  }

  public saveVisitor(visitor: Visitor): void {
    if(this.idVisitor == 0){
      this.visitorService.addVisitor(visitor);
    } else {
      this.visitorService.editVisitor(visitor);
    }
  }

}
