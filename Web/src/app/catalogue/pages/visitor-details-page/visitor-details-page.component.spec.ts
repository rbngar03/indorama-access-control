import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorDetailsPageComponent } from './visitor-details-page.component';

describe('VisitorDetailsPageComponent', () => {
  let component: VisitorDetailsPageComponent;
  let fixture: ComponentFixture<VisitorDetailsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VisitorDetailsPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(VisitorDetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
