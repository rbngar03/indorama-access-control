import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Employee } from '../../interfaces/employee.interface';

@Component({
  selector: 'catalogue-employee-details-page',
  templateUrl: './employee-details-page.component.html',
  styleUrl: './employee-details-page.component.css'
})
export class EmployeeDetailsPageComponent implements OnInit{

  private idEmployee: number = 0;

  constructor(private route: ActivatedRoute,
              private employeesService: EmployeesService){

  }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.idEmployee = params["idEmployee"];
      }
    );
    this.employeesService.getEmployeeById(this.idEmployee);
  }

  get employee(): Employee {
    return this.employeesService.employee;
  }

  public saveEmployee(employee: Employee): void {
    this.employeesService.putEmployee(employee);
  }
}
