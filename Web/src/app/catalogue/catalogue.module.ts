import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersPageComponent } from './pages/users-catalogue-page/users-page.component';
import { CatalogueRoutingModule } from './catalogue-routing.module';
import { TableComponent } from './components/table-users/table.component';
import { EmployeesCataloguePageComponent } from './pages/employees-catalogue-page/employees-catalogue-page.component';
import { ContractorsCataloguePageComponent } from './pages/contractors-catalogue-page/contractors-catalogue-page.component';
import { VisitorsCataloguePageComponent } from './pages/visitors-catalogue-page/visitors-catalogue-page.component';
import { TerminalsCataloguePageComponent } from './pages/terminals-catalogue-page/terminals-catalogue-page.component';
import { TableEmployeesComponent } from './components/table-employees/table-employees.component';
import { TableContractorsComponent } from './components/table-contractors/table-contractors.component';
import { TableVisitorsComponent } from './components/table-visitors/table-visitors.component';
import { TableTerminalsComponent } from './components/table-terminals/table-terminals.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable, MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { ModalDeleteUserComponent } from './components/modal-delete-user/modal-delete-user.component';
import { UserDetailsPageComponent } from './pages/user-details-catalogue-page/user-details-page.component';
import { FormDetailsUserComponent } from './components/form-details-user/form-details-user.component';
import { FormsModule } from '@angular/forms';
import { EmployeeDetailsPageComponent } from './pages/employee-details-page/employee-details-page.component';
import { FormDetailsEmployeeComponent } from './components/form-details-employee/form-details-employee.component';
import { TerminalDetailsPageComponent } from './pages/terminal-details-catalogue-page/terminal-details-page.component';
import { FormDetailsTerminalComponent } from './components/form-details-terminal/form-details-terminal.component';
import { MatButton, MatIconButton } from '@angular/material/button';
import { ModalDetailUserComponent } from './components/modal-detail-user/modal-detail-user.component';
import { ModalInfoComponent } from './components/modal-info/modal-info.component';
import { ModalDetailEmployeeComponent } from './components/modal-detail-employee/modal-detail-employee.component';
import { VisitorDetailsPageComponent } from './pages/visitor-details-page/visitor-details-page.component';
import { FormDetailsVisitorComponent } from './components/form-details-visitor/form-details-visitor.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { provideNativeDateAdapter } from '@angular/material/core';
import { FormDetailsContractorComponent } from './components/form-details-contractor/form-details-contractor.component';
import { ContractorDetailsPageComponent } from './pages/contractor-details-catalogue-page/contractor-details-catalogue-page.component';

@NgModule({
  declarations: [
    UsersPageComponent,
    TableComponent,
    EmployeesCataloguePageComponent,
    ContractorsCataloguePageComponent,
    VisitorsCataloguePageComponent,
    TerminalsCataloguePageComponent,
    TerminalDetailsPageComponent,
    TableEmployeesComponent,
    TableContractorsComponent,
    TableVisitorsComponent,
    TableTerminalsComponent,
    ModalDeleteUserComponent,
    UserDetailsPageComponent,
    FormDetailsUserComponent,
    EmployeeDetailsPageComponent,
    FormDetailsEmployeeComponent,
    FormDetailsTerminalComponent,
    ModalDetailUserComponent,
    ModalInfoComponent,
    ModalDetailEmployeeComponent,
    VisitorDetailsPageComponent,
    FormDetailsVisitorComponent,
    FormDetailsContractorComponent,
    ContractorDetailsPageComponent
  ],
  imports: [
    CommonModule,
    CatalogueRoutingModule,
    FormsModule,
    MatPaginator,
    MatTable,
    MatTableModule,
    MatIconModule,
    MatButton,
    MatIconButton,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule
  ],
  providers: [
    provideNativeDateAdapter()
  ]
})
export class CatalogueModule { }
