import { Component, Input, Output, SimpleChanges, ViewChild, EventEmitter } from '@angular/core';
import { Employee, Employees } from '../../interfaces/employee.interface';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'catalogue-table-employees',
  templateUrl: './table-employees.component.html',
  styleUrl: './table-employees.component.css'
})
export class TableEmployeesComponent {

  public idEmployee: number = 0;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  filtro: string = '';
  columns: string[] = [ 'ID', 'Número de empleado', 'Estatus', 'Nombre',
                        'Centro de costos',	'Tipo de nómina','Nómina procesada',
                        'Gafete',	'Gafete provisional',	'Planta', 'Editar'
                      ];
  dbColumns: string[] = [ 'idEmpleado','numEmpleado','idEstatus','nombre',
                          'idCcostos','idTipoNomina', 'nominaProcesada',
                          'idGafete', 'numeroProvisionalGafete', 'idPlanta'
                        ];

  datasource: MatTableDataSource<Employee> = new MatTableDataSource();

  @Input()
  public employees: Employees = {
    response: []
  };

  @Output()
  public onEmitPage: EventEmitter<PageEvent> = new EventEmitter();

  constructor(private router: Router){

  }

  ngAfterViewInit(): void {
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.datasource.data = this.employees.response;
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
  }

  public editEmployee(idEmployee: number): void {
    this.idEmployee = idEmployee;
    this.goToRoute('catalogues/employeeDetails', this.idEmployee);
  }

  goToRoute(route: string, idEmployee: number){
    this.router.navigate([route, idEmployee]);
  }

  changeNumberOfRegisters(pageEvent: PageEvent): void {
    this.onEmitPage.emit(pageEvent);
  }
}
