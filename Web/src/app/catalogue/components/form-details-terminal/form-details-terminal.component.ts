import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TerminalsService } from '../../services/terminals.service';
import { Terminal } from './../../interfaces/terminal.interface';
import { Planta } from './../../interfaces/planta.interface';
import { NgForm } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { PLANTAS_CONSTANTES } from '../../data/plantas.constants';
import { ModalInfoComponent } from '../modal-info/modal-info.component';

@Component({
  selector: 'catalogue-form-details-terminal',
  templateUrl: './form-details-terminal.component.html',
  styleUrl: './form-details-terminal.component.css'
})
export class FormDetailsTerminalComponent {

  @Input()
  public idTerminal: number = -1;

  @ViewChild(ModalInfoComponent) modalInfoComponent?: ModalInfoComponent;

  public selectedTipo  : string | null = null;
  public selectedGrupo : number = 0;

  public tituloModal   : string = '';
  public mensajeModal  : string = '';
  public redirectModal : string = '';

  public terminal: Terminal = {
    idTerminales:           0,
    clave:                  '',
    ubicacion:              '',
    ip:                     '',
    nombre:                 '',
    idTipoLector:           null,
    fechaActualizacion:     new Date(),
    tipo:                   0,
    grupo:                  0,
    idTipoLectorNavigation: null,
    marcajes:               []
  }

  public plantas : Planta[] = [];
  public terminales : Terminal[] = [];
  public terminalReady: boolean = false;

  constructor(private router: Router, private terminalsService: TerminalsService){
  }

  ngOnInit() : void {
    this.getTerminal();
    this.plantas = PLANTAS_CONSTANTES;
    this.terminales = this.terminalsService.terminals;
  }

  public getTerminal() : void{
    if(this.idTerminal > 0){
      this.terminalsService.getTerminalById(this.idTerminal).subscribe((response)=>{
        this.terminal = response;
        this.checkRadio();
        this.setSelectPlanta();
      });
    }
  }

  public addTerminal()
  {
    this.terminalsService.postTerminal(this.terminal)
        .subscribe({
          next: (response : any ) => {
            if(response.hasOwnProperty('idTerminales')) {
              /* this.setModal('Mensaje',
                            `La terminal con clave "${response.clave}" e IP "${response.ip}" fue agregada correctamente.`,
                            'catalogues/terminals'); */
                  this.open('Mensaje',
                  `La terminal con clave "${response.clave}" e IP "${response.ip}" fue agregada correctamente.`,
                  'catalogues/terminals');
            }
            else
            {
             /*  this.setModal('Error',
                            `Ocurrió un error. Intente nuevamente.`,
                            ''); */
                  this.open('Error', "Ocurrió un error. Intente nuevamente.", '');
            }
          },
          error: (error: HttpErrorResponse) => {
            /* this.setModal('Error',
                          `Ocurrió un error. Contacte a su administrador de servicios.`,
                          ''); */
            console.error(error);
            this.open('Error', "Ocurrió un error. Contacte a su administrador de servicios.", '')
          }
        });
  }

  public updateTerminal()
  {
    this.terminalsService.putTerminal(this.terminal, this.terminal.idTerminales)
        .subscribe({
          next: (response : any) => {
            if(response.hasOwnProperty('mensaje')) {
              //this.setModal('Mensaje', response.mensaje, 'catalogues/terminals');
              this.open('Mensaje', response.mensaje, 'catalogues/terminals');
            }
            else
            {
              /* this.setModal('Error',
                            `Ocurrió un error. Intente nuevamente.`,
                            ''); */
              this.open('Error', "Ocurrió un error. Intente nuevamente.", '')
            }
          },
          error: (error : HttpErrorResponse) => {
            /* this.setModal('Error',
                          `Ocurrió un error. Contacte a su administrador de servicios.`,
                          ''); */
            this.open('Error', "Ocurrió un error. Contacte a su administrador de servicios.", '');
            console.error(error);
          }
        });
  }

  public isTerminalDuplicated() : boolean {
    this.terminalsService.getAllTerminals();
    this.terminales = this.terminalsService.terminals;

    let claveExists : boolean = false;
    let ipExists    : boolean = false;

    claveExists = this.terminales.find( t => t.clave === this.terminal.clave
                                          && t.idTerminales !== this.terminal.idTerminales) !== undefined;

    ipExists    = this.terminales.find( t => t.ip === this.terminal.ip
                                          && t.idTerminales !== this.terminal.idTerminales) !== undefined;

    if(claveExists){
      /* this.setModal('Mensaje',
                    `La Terminal con la Clave '"${this.terminal.clave}"' ya existe.`,
                    ''); */
      this.open('Mensaje', `La Terminal con la Clave '"${this.terminal.clave}"' ya existe.`,'');
    }
    if(ipExists){
      /* this.setModal('Mensaje',
                    `La Terminal con la IP '"${this.terminal.ip}"' ya existe.`,
                    ''); */
      this.open('Mensaje', `La Terminal con la IP '"${this.terminal.ip}"' ya existe.`, '')
    }

    return claveExists || ipExists;
  }

  public onSubmit( terminalForm : NgForm) : void{
    this.setModal('', '', '');
    if(terminalForm.valid && !this.isTerminalDuplicated()){
      if(this.terminal.idTerminales > 0){
        this.updateTerminal();
      }else{
        this.addTerminal();
      }
    }
    //this.modalInfoComponent.showModal();
  }

  public checkRadio() : void{
    if(this.terminal.tipo == 1){
      this.selectedTipo = "flexRadioComedor";
    }else if(this.terminal.tipo == 2){
      this.selectedTipo = "flexRadioGimnasio";
    }
  }

  public uncheckRadio() : void {
    this.selectedTipo = null;
    this.terminal.tipo = 0;
  }

  public onTipoChange(event : any) : void {
    let newTipo = event.target as HTMLInputElement;
    this.terminal.tipo = newTipo.value == "flexRadioComedor" ? 1 : 2;
  }

  public setSelectPlanta() : void {
    if(this.terminal.grupo != null && this.terminal.grupo != 0){
      this.selectedGrupo = this.terminal.grupo;
    }else{
      this.selectedGrupo = 0;
      this.terminal.grupo = 0;
    }
  }

  public onGrupoChange(event : any) : void{
    let newGrupo = event.target as HTMLInputElement;
    this.terminal.grupo = Number(newGrupo.value);
  }

  public goToRoute(route: string) : void{
    this.router.navigate([route]);
  }

  public setModal( tituloModal : string, mensajeModal : string, redirectModal : string) : void {
    this.tituloModal  = tituloModal;
    this.mensajeModal = mensajeModal;
    this.redirectModal = redirectModal;
  }

  public open(title: string, message: string, redirectTo: string){
    this.modalInfoComponent?.openModal(title, message, redirectTo);
  }
}
