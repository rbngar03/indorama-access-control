import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormDetailsTerminalComponent } from './form-details-terminal.component';

describe('FormDetailsTerminalComponent', () => {
  let component: FormDetailsTerminalComponent;
  let fixture: ComponentFixture<FormDetailsTerminalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormDetailsTerminalComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormDetailsTerminalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
