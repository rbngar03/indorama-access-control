import { Component, ElementRef, ViewChild } from '@angular/core';

//@ts-ignore
const $: any = window['$'];

@Component({
  selector: 'catalogue-modal-detail-employee',
  templateUrl: './modal-detail-employee.component.html',
  styleUrl: './modal-detail-employee.component.css'
})
export class ModalDetailEmployeeComponent {

  @ViewChild('modal') modal?: ElementRef;

  message: string = '';

  openModal(message: string){
    console.log("Entre");
    this.message = message;
    $(this.modal?.nativeElement).modal('show');
  }
}
