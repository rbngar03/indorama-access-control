import { Component, ElementRef, ViewChild } from '@angular/core';

//@ts-ignore
const $: any = window['$'];

@Component({
  selector: 'catalogue-modal-detail-user',
  templateUrl: './modal-detail-user.component.html',
  styleUrl: './modal-detail-user.component.css'
})
export class ModalDetailUserComponent {

  @ViewChild('modal') modal?: ElementRef;

  message: string = '';

  openModal(message: string){
    this.message = message;
    $(this.modal?.nativeElement).modal('show');
  }
}
