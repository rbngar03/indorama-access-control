import { Component, EventEmitter, Input, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Visitor, Visitors } from '../../interfaces/visitor.interface';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'catalogue-table-visitors',
  templateUrl: './table-visitors.component.html',
  styleUrl: './table-visitors.component.css'
})
export class TableVisitorsComponent {

  @Input()
  public visitors: Visitors = {
    response: []
  };

  @Output()
  public onEmitPage: EventEmitter<PageEvent> = new EventEmitter();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  filtro: string = '';
  columns: string[] = [ 'ID del visitante',	'Fecha de inducción de seguridad',
                        'Fecha de ingreso',	'Fecha de terminación', 'Horas',
                        'Nombre',	'Origen',	'Gafete',	'Empleado responsable',
                        'Notas', 'RFC', 'Derecho a comedor', 'Centro de costos',
                        'Archivo adjunto', 'Estatus',	'Editar'
                      ];
  dbColumns: string[] = [ 'idVisitantes','fechaIndSeguridad','fechaIngreso',
                          'fechaTerminacion','horas', 'nombreVisitante', 'idOrigen',
                          'idGafete', 'empResponsable', 'notas', 'rfc', 'idComedor',
                          'idCcostos', 'archivoAdjunto', 'idEstatus'
                        ];

  datasource: MatTableDataSource<Visitor> = new MatTableDataSource();

  constructor(private router : Router){}

  public idVisitor : number = 0;

  ngAfterViewInit(): void {
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.datasource.data = this.visitors.response;
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
  }

  changeNumberOfRegisters(pageEvent: PageEvent): void {
    this.onEmitPage.emit(pageEvent);
  }

  editVisitor(idVisitor: number): void {
    this.goToRoute("catalogues/visitorDetails", idVisitor);
  }

  goToRoute(route: string,  idVisitor: number){
    this.router.navigate([route, idVisitor]);
  }
}
