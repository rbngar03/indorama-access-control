import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableContractorsComponent } from './table-contractors.component';

describe('TableContractorsComponent', () => {
  let component: TableContractorsComponent;
  let fixture: ComponentFixture<TableContractorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TableContractorsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TableContractorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
