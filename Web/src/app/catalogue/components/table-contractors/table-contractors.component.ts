import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Contractor, Contractors } from './../../interfaces/contractor.interface';
import { Component, EventEmitter, Input, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ClasificationsService } from '../../services/clasifications.service';
import { Clasification } from '../../interfaces/clasification.interface';
import { CCostosService } from '../../services/ccostos.service';
import { CCosto } from '../../interfaces/ccosto.interface';

@Component({
  selector: 'catalogue-table-contractors',
  templateUrl: './table-contractors.component.html',
  styleUrl: './table-contractors.component.css'
})
export class TableContractorsComponent {

  @Input() contractors: Contractors = {response: []};

  @Output()
  public onEmitPage: EventEmitter<PageEvent> = new EventEmitter();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  filtro: string = '';
  columns: string[] = [ 'Tipo Contratista','Nombre Contratista',
                        'Fecha Inducción','Fecha Inicio Contrato',
                        'Fecha Terminación', 'No. IMSS', 'Vigencia IMSS',
                        'No. Contrato', 'Empresa','RFC', 'Clasificación',
                        'Responsable Indorama', 'Centro de Costos', 'Gafete',
                        'Derecho a Comedor','Estatus','Editar'
                      ];
  dbColumns: string[] = [ 'idTipoContratista','nombreContratista',
                          'fechaIndSeguridadF','fechaContratoF',
                          'fechaTerminacionF', 'imss', 'vigenciaImssF',
                          'numeroContrato','nombreCompañia','rfc', 'idClasificacion',
                          'responsableIndorama', 'idCcostos', 'idGafete',
                          'idComedor','idEstatus'
                        ];

  datasource: MatTableDataSource<Contractor> = new MatTableDataSource();

  public idContractor   : number = 0;
  public clasifications :  Clasification[] = [];
  public cCostos        :  CCosto[] = [];

  public contractorsLoaded : boolean = false;

  constructor(private router : Router, private clasificationsService : ClasificationsService, private cCostosService :CCostosService){}

  ngOnInit(): void{
    this.getClasifications();
    this.getCentrosCostos();
  }

  ngAfterViewInit(): void {
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.contractorsLoaded = false;
  }

  ngOnChanges(changes: SimpleChanges): void {
   if (changes['contractors'] && changes['contractors'].currentValue) {
      this.datasource.data = this.contractors.response;
      this.datasource.paginator = this.paginator;
      this.datasource.sort = this.sort;
      this.contractorsLoaded = true;
    }
  }

  getCellValue(column: string, row: any): string {
    const columnHandlers: { [key: string]: () => string } = {
      'Nombre Contratista': () => this.getFullContractorName(row),
      'Tipo Contratista': () => this.getContractorType(row),
      'Clasificación': () => this.getClasification(row),
      'Centro de Costos': () => this.getCCosto(row),
      'Derecho a Comedor': () => this.getComedor(row),
      'Estatus': () => this.getStatus(row),
      'Gafete': () => this.getGafete(row)
    };

    return columnHandlers[column]?.() || row[this.dbColumns[this.columns.indexOf(column)]];
  }

  getFullContractorName(row: Contractor): string {
    return `${row['nombreContratista']} ${row['apPaterno']} ${row['apMaterno']}`;
  }

  getContractorType(row: Contractor) : string{
    return row['idTipoContratista'] == 1 ? 'CORTO PLAZO' :  'RESIDENTE';
  }

  getClasification(row: Contractor) : string {
    let clasificacion = this.clasifications.find(c => c.idClasificacion === row['idClasificacion']);
    return clasificacion?.clasificacion1 ?? 'N/A';
  }

  getCCosto(row: Contractor) : string{
    let ccostos = this.cCostos.find(c => c.idCcostos === row['idCcostos']);
    return ccostos?.ccostos ?? 'N/A';
  }

  getComedor(row: Contractor) : string{
    return row['idComedor'] == 1 ? 'SÍ' : 'NO';
  }

  getStatus(row: Contractor) : string{
    return row['idEstatus'] == 1 ? 'ACTIVO' : 'INACTIVO';
  }

  getGafete(row : Contractor) : string{
    return row['idGafete'] == 0 ? 'N/A' : row['idGafete'].toString();
  }

  goToRoute(route: string, idContractor: number){
    this.router.navigate([route, idContractor]);
  }

  changeNumberOfRegisters(pageEvent: PageEvent): void {
    this.onEmitPage.emit(pageEvent);
  }

  public getClasifications(){
    this.clasificationsService.getAllClasifications()
          .subscribe(
          {
            next: (response : Clasification[]) => {
              this.clasifications = response;
            },
            error:(error) => {
              console.error(error);
              this.clasifications = [];
            }
          });
  }

  public getCentrosCostos(){
    this.cCostosService.getAllCentrosCostos()
          .subscribe(
            {
              next: (response : CCosto[]) => {
                this.cCostos = response;
              },
              error:(error) => {
                console.error(error);
                this.cCostos = [];
              }
            });
  }

}
