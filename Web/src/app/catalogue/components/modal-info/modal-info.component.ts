import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
//import { Modal } from '@types/bootstrap';

//@ts-ignore
const $: any = window['$'];

@Component({
  selector: 'catalogue-modal-info',
  templateUrl: './modal-info.component.html',
  styleUrl: './modal-info.component.css'
})
export class ModalInfoComponent {
  @Input()
  mensaje  : string = '';
  @Input()
  titulo   : string = '';
  @Input()
  redirect : string = '';

  message: string = '';
  title: string = '';
  redirectTo: string = '';

  @ViewChild('modal') modalInfo?: ElementRef;

  //private modalInstance: Modal | undefined;

  constructor(private router: Router, private el: ElementRef){}

  ngAfterViewInit(): void {
    const modalElement = this.el.nativeElement.querySelector('#simpleModal');
    if (modalElement) {
      //this.modalInstance = new Modal(modalElement);
    }
  }

  public showModal(): void {
    /* if (this.modalInstance) {
      this.modalInstance.show();
    } */
  }

  public hideModal(): void {
    /* if (this.modalInstance) {
      this.modalInstance.hide();
    } */
  }

  public onClick(){
    this.goToRoute();
  }

  public goToRoute() : void{
    if(this.redirectTo != ''){
      this.router.navigate([this.redirectTo]);
    }
    this.hideModal();
  }

  openModal(title: string, message: string, redirectTo: string){
    this.message = message;
    this.title = title;
    this.redirectTo = redirectTo;
    $(this.modalInfo?.nativeElement).modal('show');
  }
}
