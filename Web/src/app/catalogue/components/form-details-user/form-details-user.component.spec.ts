import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDetailsUserComponent } from './form-details-user.component';

describe('FormDetailsUserComponent', () => {
  let component: FormDetailsUserComponent;
  let fixture: ComponentFixture<FormDetailsUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormDetailsUserComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FormDetailsUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
