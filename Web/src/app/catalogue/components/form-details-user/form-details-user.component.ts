import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { Router } from '@angular/router';
import { User } from '../../../login/interfaces/user.interface';
import { Profile } from '../../../login/interfaces/profile.interface';
import { Validators } from '@angular/forms';
import { ModalDetailUserComponent } from '../modal-detail-user/modal-detail-user.component';

@Component({
  selector: 'catalogue-form-details-user',
  templateUrl: './form-details-user.component.html',
  styleUrl: './form-details-user.component.css'
})
export class FormDetailsUserComponent {

  public isPasswordChanged =  false;

  @Input()
  public idUser: number = -1;

  @Input()
  public user: User = {
    idUsuarios:         0,
    usuario1:           '',
    password:           '',
    nombreUsuario:      '',
    idPerfil:           0,
    idPerfilNavigation: null
  }

  @Input()
  public profile: Profile = {
    idPerfil:    0,
    clave:       '',
    descripcion: '',
    usuarios:    []
  }

  @Input()
  public listProfiles: Profile[] = [];

  @Output()
  public onEmitUser: EventEmitter<User> = new EventEmitter();

  @Output()
  public onEmitPassword: EventEmitter<boolean> = new EventEmitter();

  @ViewChild(ModalDetailUserComponent) modalDetailUser?: ModalDetailUserComponent;

  constructor(
    private router: Router){

  }

  public saveUser(): void{
    if (this.user.usuario1 == '' || this.user.password == '' ||
        this.user.idPerfil == 0) this.open("Por favor llene todos los campos");
    else{
      var isValid = this.validatePassword(this.user.password, this.user.idPerfil);
      if(isValid) {
        this.onEmitPassword.emit(this.isPasswordChanged);
        this.onEmitUser.emit(this.user);
      } else {
        if(this.user.idPerfil == 9) this.open("Contraseña no válida. Asegurese de que contiene entre 5 y 15 carácteres, incluye al menos una letra mayúscula, una letra minúscula, un número, un carácter especial y no contiene espacios en blanco.")
        else this.open("Contraseña no válida. Asegurese de que contiene al menos 6 cáracteres alfanúmericos y no contiene caracteres especiales ni espacios en blanco");
      }
    }

  }

  public changeProfile(value: string){
    this.listProfiles.forEach(element => {
      if (element.clave == value)
        this.user.idPerfil = element.idPerfil;

    });
  }

  public goToRoute(route: string){
    this.router.navigate([route]);
  }

  public savePassword(){
    this.isPasswordChanged = true;
  }

  public validatePassword(password: string, typeUser: number): boolean {
    var isValid: boolean = false;
    var regex;
    if(typeUser == 9){
      regex = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{5,15}$/;
    } else {
      regex = /^[A-Za-z\d]{6,}$/;
    }
    console.log(typeUser);
    console.log(password);
    isValid = regex.test(password);
    console.log(isValid);
    return isValid;
  }

  public open(message: string){
    this.modalDetailUser?.openModal(message);
  }

}
