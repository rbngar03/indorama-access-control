import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {provideNativeDateAdapter} from '@angular/material/core';

import { Visitor } from '../../interfaces/visitor.interface';
import { Router } from '@angular/router';
import { ModalDetailUserComponent } from '../modal-detail-user/modal-detail-user.component';

@Component({
  selector: 'catalogue-form-details-visitor',
  templateUrl: './form-details-visitor.component.html',
  styleUrl: './form-details-visitor.component.css',
})
export class FormDetailsVisitorComponent {

  @ViewChild(ModalDetailUserComponent) modalDetailVisitor?: ModalDetailUserComponent;

  @Input()
  visitor: Visitor = {
      idVisitantes:       0,
        fechaIndSeguridad:  null,
        fechaIngreso:       new Date(),
        fechaIngresoF:      '',
        fechaTerminacion:   null,
        horas:              '',
        nombreVisitante:    '',
        idOrigen:           null,
        descOrigen:         '',
        foto:               null,
        idGafete:           0,
        empResponsable:     '',
        notas:              null,
        rfc:                '',
        idComedor:          0,
        idCcostos:          0,
        archivoAdjunto:     null,
        idEstatus:          1,
        fechaActualizacion: null,
        idPlanta:           null,
        idPlantaNavigation: null,
        marcajes:           []
  };

  @Output()
  public onEmitVisitor: EventEmitter<Visitor> = new EventEmitter();

  constructor(private router: Router){

  }

  public saveVisitor(): void {
    if(this.visitor.nombreVisitante == '' || this.visitor.fechaIngreso == null ||
       this.visitor.rfc == '' || this.visitor.descOrigen == '' || this.visitor.horas == '' ||
       this.visitor.empResponsable == '' || this.visitor.idEstatus == 0 ||
       this.visitor.idCcostos == 0 || this.visitor.idComedor == 0) {
        this.open("Por favor llene todos los campos");
    } else {
      this.onEmitVisitor.emit(this.visitor);
    }
  }

  public changeStatus(value: string): void {
    if(value == 'Activo') this.visitor.idEstatus = 1;
    if(value == 'Inactivo') this.visitor.idEstatus = 2;
  }

  public goToRoute(route: string): void {
    this.router.navigate([route]);
  }

  public open(message: string){
    this.modalDetailVisitor?.openModal(message);
  }
}
