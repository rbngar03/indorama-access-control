import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDetailsVisitorComponent } from './form-details-visitor.component';

describe('FormDetailsVisitorComponent', () => {
  let component: FormDetailsVisitorComponent;
  let fixture: ComponentFixture<FormDetailsVisitorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormDetailsVisitorComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FormDetailsVisitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
