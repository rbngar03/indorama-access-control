import { Component, Input, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Contractor } from '../../interfaces/contractor.interface';
import { ContractorsService } from '../../services/contractors.service';
import { Clasification } from '../../interfaces/clasification.interface';
import { CCosto } from '../../interfaces/ccosto.interface';
import { CCostosService } from '../../services/ccostos.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ClasificationsService } from '../../services/clasifications.service';
import { ModalInfoComponent } from '../modal-info/modal-info.component';

@Component({
  selector: 'catalogue-form-details-contractor',
  templateUrl: './form-details-contractor.component.html',
  styleUrl: './form-details-contractor.component.css'
})
export class FormDetailsContractorComponent {

  @Input()
  public idContractor: number = -1;

  @ViewChild(ModalInfoComponent) modalInfoComponent?: ModalInfoComponent;

  constructor(private router: Router, private contractorsService : ContractorsService, private cCostosService : CCostosService, private clasificationsService : ClasificationsService ){}

  public tituloModal   : string = '';
  public mensajeModal  : string = '';
  public redirectModal : string = '';

  public fechaIndSeguridad     : string = '';
  public fechaContrato         : string = '';
  public fechaVigenciaImss     : string = '';
  public fechaTerminacion      : string = '';

  public nombreClasificacion?  : string = '';
  public selectedClasificacion : string = '';
  public selectedCentroCostos  : string = '';
  public selectedEstatus       : string = '';
  public selectedTipo          : string = '';
  public selectedComedor       : string = '';

  public clasifications : Clasification[] = [];
  public cCostos : CCosto[] = [];
  public contractors : Contractor[] = [];
  public contractorReady: boolean = false;

  public contractor : Contractor = {
    idContratistas:                    0,
    idTipoContratista:                 0,
    nombreContratista:                 '',
    apPaterno:                         '',
    apMaterno:                         '',
    fechaIndSeguridad:                 '',
    fechaContrato:                     '',
    fechaTerminacion:                  '',
    imss:                              '',
    vigenciaSeguridad:                 '',
    vigenciaImss:                      '',
    numeroContrato:                    '',
    periodoVigencia:                   '',
    idCompanias:                       0,
    nombreCompania:                    '',
    foto:                              '',
    fechaBaja:                         null,
    rfc:                               '',
    idClasificacion:                   0,
    responsableIndorama:               '',
    idCcostos:                         0,
    idGafete:                          0,
    notas:                             '',
    numeroProvisionalGafete:           0,
    idComedor:                         0,
    archivoAdjunto:                    '',
    idEstatus:                         0,
    fechaActualizacion:                null,
    idPlanta:                          null,
    idCcostosNavigation:               null,
    idClasificacionNavigation:         null,
    idComedorNavigation:               null,
    idCompaniasNavigation:             null,
    idEstatusNavigation:               null,
    idGafeteNavigation:                null,
    idPlantaNavigation:                null,
    idTipoContratistaNavigation:       null,
    marcajes:                          [],
    numeroProvisionalGafeteNavigation: null,
  }

  ngOnInit() : void {
    this.getCentrosCostos();
    this.getContractor();
  }

  public getContractor() : void{
    if(this.idContractor > 0){
      this.contractorsService.getContractorById(this.idContractor)
          .subscribe({
            next: (response: any) => {
              this.contractor = response.response;
              this.contractor.nombreCompania = response.response.nombreCompañia;

              this.selectedClasificacion = response.response.idClasificacion ?? '';
              this.selectedComedor       = response.response.idComedor ?? '';
              this.selectedEstatus       = response.response.idEstatus ?? '';
              this.selectedTipo          = response.response.idTipoContratista ?? '';

              this.fechaIndSeguridad = this.formatDateToDDMMYYYY(response.response.fechaIndSeguridad);
              this.fechaContrato     = this.formatDateToDDMMYYYY(response.response.fechaContrato);
              this.fechaVigenciaImss = this.formatDateToDDMMYYYY(response.response.vigenciaImss);
              this.fechaTerminacion  = this.formatDateToDDMMYYYY(response.response.fechaTerminacion);

              this.onDateChange(0);
              this.getClasifications(response.response.idClasificacion);
            },
            error: (error: HttpErrorResponse) => {
              console.error('Error al obtener el contratista: ', error);
            }
          });
    }
  }

  public getCentrosCostos(){
    this.cCostosService.getAllCentrosCostos()
          .subscribe({
            next: (response: any) => {
              this.cCostos = response;
            },
            error: (error: HttpErrorResponse) => {
              console.error('Error al obtener el centros de costos: ', error);
              this.cCostos = [];
            }
          });
  }

  public getClasifications(idClasification : number){
    this.clasificationsService.getAllClasifications()
          .subscribe({
            next: (response: any) => {
              this.clasifications = response;
              if(idClasification != 0){
                this.setNombreClasificacion(idClasification);
              }
            },
            error: (error: HttpErrorResponse) => {
              console.error('Error al obtener clasificaciones: ', error);
              this.clasifications = [];
            }
          });
  }

  public updateContractor() : void{
    console.log(this.contractor);
    this.contractorsService.putContractor(this.contractor)
    .subscribe({
      next: (response : any) => {
        if(response.hasOwnProperty('mensaje')) {
          this.open('Mensaje',
            `El Contratista ${this.contractor.nombreContratista} ${this.contractor.apPaterno} ${this.contractor.apMaterno?? this.contractor.apMaterno} fue actualizado correctamente.`,
            'catalogues/contractors');
        }
        else
        {
          this.open('Error', "Ocurrió un error. Intente nuevamente.", '');
        }
      },
      error: (error : HttpErrorResponse) => {
        console.error(error);
      }
    });
  }

  public onSubmit(contractorForm : NgForm) : void{
    this.setModal('', '', '');
    if(contractorForm.valid && this.validateDates()){
      this.updateContractor();
    }
  }

  public validateDates() : boolean{
    let validDates : boolean = true;
    if(this.fechaIndSeguridad != this.fechaContrato){
      this.open('Mensaje', 'La Fecha de Inducción y la Fecha de Inicio Contrato son diferentes.', '');
      validDates = false;
    }
    if(this.contractor.idEstatus == 1 && this.contractor.vigenciaImss){
      const fechaActual = new Date();
      const fechaVigenciaImssDate = new Date(this.fechaVigenciaImss);
      const fechaTerminacionDate = new Date(this.fechaTerminacion);

      if (fechaActual > fechaVigenciaImssDate || fechaActual > fechaTerminacionDate) {
        this.open('Mensaje', 'La Fecha de vigencia IMSS y la Fecha de Terminación Contrato no pueden ser menores a la fecha actual para usuarios activos.', '');
        validDates = false;
      }
    }
    return validDates;
  }

  public formatDateToDDMMYYYY(dateString : string) : string {
    const date = new Date(dateString);
    const dia = String(date.getDate()).padStart(2, '0');
    const mes = String(date.getMonth() + 1).padStart(2, '0');
    const anio = date.getFullYear();

    return `${anio}-${mes}-${dia}`;
  }

  public onDateChange(event? : any) : void{
    const updateDateField = (fieldName: string, dateValue: string) => {
      const [year, month, day] = dateValue.split('-');
      const formattedDate = `${day}/${month}/${year}`;
      switch (fieldName) {
        case 'FechaInduccion':
          this.contractor.fechaIndSeguridad = formattedDate;
          if(event !== 0){
            this.contractor.fechaContrato = formattedDate;
            this.fechaContrato = this.fechaIndSeguridad;
          }
          break;
        case 'FechaContrato':
          this.contractor.fechaContrato = formattedDate;
          if(event !== 0){
            this.contractor.fechaIndSeguridad = formattedDate;
            this.fechaIndSeguridad = this.fechaContrato;
          }
          break;
        case 'VigenciaIMSS':
          this.contractor.vigenciaImss = formattedDate;
          break;
        case 'TerminacionContrato':
          this.contractor.fechaTerminacion = formattedDate;
          break;
      }
    };

    if (event === 0) {
      updateDateField('FechaInduccion', this.fechaIndSeguridad);
      updateDateField('FechaContrato', this.fechaContrato);
      updateDateField('VigenciaIMSS', this.fechaVigenciaImss);
      updateDateField('TerminacionContrato', this.fechaTerminacion);
    } else {
      const newDate = event.target as HTMLInputElement;
      updateDateField(newDate.name, newDate.value);
    }
    console.log(this.contractor);
  }

  public onSelectChange(event? : any) : void{
    const newEvent = event.target as HTMLInputElement;
    const value = newEvent.value === '' ? 0 : Number(newEvent.value);

    switch (newEvent.name) {
      case 'tipoSelect':
        this.contractor.idTipoContratista = value;
        break;
      case 'estatusSelect':
        this.contractor.idEstatus = value;
        break;
      case 'clasificacionSelect':
        this.contractor.idClasificacion = value;
        this.setNombreClasificacion(value);
        break;
      case 'comedorSelect':
        this.contractor.idComedor = value;
        break;
    }
  }

  public setNombreClasificacion(idClasificacion: number): void{
    let clasificacion = this.clasifications.find(c => c.idClasificacion === idClasificacion);
    this.nombreClasificacion = clasificacion?.descripcion;
  }

  public setModal( tituloModal : string, mensajeModal : string, redirectModal : string) : void {
    this.tituloModal  = tituloModal;
    this.mensajeModal = mensajeModal;
    this.redirectModal = redirectModal;
  }

  public open(title: string, message: string, redirectTo: string){
    this.modalInfoComponent?.openModal(title, message, redirectTo);
  }

  public goToRoute(route: string) : void{
    this.router.navigate([route]);
  }
}
