import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDetailsContractorComponent } from './form-details-contractor.component';

describe('FormDetailsContractorsComponent', () => {
  let component: FormDetailsContractorComponent;
  let fixture: ComponentFixture<FormDetailsContractorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormDetailsContractorComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormDetailsContractorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
