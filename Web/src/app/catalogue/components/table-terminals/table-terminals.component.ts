import { Component, Input, SimpleChanges, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Terminal } from '../../interfaces/terminal.interface';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Planta } from '../../interfaces/planta.interface';
import { PLANTAS_CONSTANTES } from '../../data/plantas.constants';

@Component({
  selector: 'catalogue-table-terminals',
  templateUrl: './table-terminals.component.html',
  styleUrl: './table-terminals.component.css'
})
export class TableTerminalsComponent {

  @Input() terminals: Terminal[] = [];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  filtro: string = '';
  columns: string[] = ['clave', 'ubicacion','ip', 'nombre', 'grupo', 'editar'];
  datasource: MatTableDataSource<Terminal> = new MatTableDataSource();

  public idTerminal : number = 0;
  public plantas    : Planta[] = [];

  public getDescripcionPlanta(idPlanta: number): string {
    if (idPlanta == null || idPlanta === 0) {
      return 'N/A';
    }
    const planta = this.plantas.find(p => p.idPlanta === idPlanta);
    return planta ? planta.descripcion : '';
  }

  constructor(private router : Router){}

  ngOnInit() : void {
    this.plantas = PLANTAS_CONSTANTES;
  }

  ngAfterViewInit(): void {
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.datasource.data = this.terminals;
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
  }

  goToRoute(route: string, idTerminal: number){
    this.router.navigate([route, idTerminal]);
  }
}
