import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableTerminalsComponent } from './table-terminals.component';

describe('TableTerminalsComponent', () => {
  let component: TableTerminalsComponent;
  let fixture: ComponentFixture<TableTerminalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TableTerminalsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TableTerminalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
