import { Component, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { User } from '../../../login/interfaces/user.interface';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'catalogue-components-table',
  templateUrl: './table.component.html',
  styleUrl: './table.component.css'
})
export class TableComponent {

  @Input()
  public users: User[] = [];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  filtro: string = '';
  columns: string[] = ['ID', 'Usuario', 'Nombre de usuario', 'Perfil', 'Acciones'];
  dbColumns: string[] = ['idUsuarios', 'usuario1', 'nombreUsuario', 'idPerfil'];
  datasource: MatTableDataSource<User> = new MatTableDataSource();

  public idUsuario: number = 0;
  public usuario:   string = "";
  public idPerfil:  number = 0;

  constructor(private router: Router){ }

  ngAfterViewInit(): void {
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.datasource.data = this.users;
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
  }

  public deleteUser(idUser: number, user: string): void {
    this.idUsuario = idUser;
    this.usuario = user;
  }

  public editUser(idUser: number, idPerfil: number){
    this.idUsuario = idUser;
    this.idPerfil = idPerfil;
    this.goToRoute('catalogues/userDetails', this.idUsuario, this.idPerfil);
  }

  goToRoute(route: string, idUser: number, idProfile: number){
    this.router.navigate([route, idUser, idProfile]);
  }
}
