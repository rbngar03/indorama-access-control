import { Component, Input } from '@angular/core';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'catalogue-modal-delete-user',
  templateUrl: './modal-delete-user.component.html',
  styleUrl: './modal-delete-user.component.css'
})
export class ModalDeleteUserComponent {

  @Input()
  public user: string = "";

  @Input()
  public idUser: number = 0;

  constructor(private userService: UsersService){

  }

  public deleteUser(idUser: number): void {
    this.userService.deleteUser(idUser);
  }

}
