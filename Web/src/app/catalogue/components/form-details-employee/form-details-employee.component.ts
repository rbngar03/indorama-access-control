import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../../interfaces/employee.interface';
import { ModalDetailUserComponent } from '../modal-detail-user/modal-detail-user.component';
import { ModalDetailEmployeeComponent } from '../modal-detail-employee/modal-detail-employee.component';

@Component({
  selector: 'catalogue-form-details-employee',
  templateUrl: './form-details-employee.component.html',
  styleUrl: './form-details-employee.component.css'
})
export class FormDetailsEmployeeComponent {

  @ViewChild(ModalDetailEmployeeComponent) modalDetailEmployee?: ModalDetailEmployeeComponent;

  @Output()
  public onEmitEmployee: EventEmitter<Employee> = new EventEmitter();

  @Input()
  public employee: Employee = {
    idEmpleado: 0,
    numEmpleado: '',
    apePaterno: '',
    apeMaterno: '',
    nombre:                            '',
    idCcostos:                         0,
    idTipoNomina:                      0,
    nominaProcesada:                   '',
    idEstatus:                         0,
    idGafete:                          0,
    foto:                              '',
    notas:                             null,
    numeroProvisionalGafete:           0,
    idPlanta:                          0,
    fechaActualizacion:                null,
    idCcostosNavigation:               null,
    idEstatusNavigation:               null,
    idGafeteNavigation:                null,
    idPlantaNavigation:                null,
    idTipoNominaNavigation:            null,
    marcajes:                          [],
    numeroProvisionalGafeteNavigation: null,
  };

  constructor(private router: Router){

  }

  public saveEmployee(): void {
    console.log(this.employee);
    if (this.employee.numEmpleado == '' || this.employee.apePaterno == '' || this.employee.apeMaterno == '' ||
        this.employee.nombre == '' || this.employee.idGafete == 0 || this.employee.idCcostos == 0 ||
        this.employee.idPlanta == 0) {
          this.open("Verifique que todos los campos contengan algún valor");
    } else {
      this.onEmitEmployee.emit(this.employee);
    }
  }

  public goToRoute(route: string){
    this.router.navigate([route]);
  }

  public open(message: string){
    console.log(this.modalDetailEmployee);
    this.modalDetailEmployee?.openModal(message);
  }
}
