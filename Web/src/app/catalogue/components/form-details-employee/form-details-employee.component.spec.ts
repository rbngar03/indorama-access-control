import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDetailsEmployeeComponent } from './form-details-employee.component';

describe('FormDetailsEmployeeComponent', () => {
  let component: FormDetailsEmployeeComponent;
  let fixture: ComponentFixture<FormDetailsEmployeeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormDetailsEmployeeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FormDetailsEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
