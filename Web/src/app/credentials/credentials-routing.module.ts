import { VisitantesComponent } from './pages/visitantes/visitantes.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmpleadosPermanentesComponent } from './pages/empleados-permanentes/empleados-permanentes.component';
import { ContratistasComponent } from './pages/contratistas/contratistas.component';

const routes: Routes = [
  { path: 'employee/permanent', component: EmpleadosPermanentesComponent },
  { path: 'employee/provisional', component: EmpleadosPermanentesComponent },
  { path: 'contractor/permanent', component: ContratistasComponent },
  { path: 'contractor/provisional', component: ContratistasComponent },
  { path: 'contractor/shorterTerm', component: ContratistasComponent },
  { path: 'contractor/longerTerm', component: ContratistasComponent },
  { path: 'visitors', component: VisitantesComponent },
  { path: '**', redirectTo: 'home' }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CredentialsRoutingModule { }
