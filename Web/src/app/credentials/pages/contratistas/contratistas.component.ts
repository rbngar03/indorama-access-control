import { Component } from '@angular/core';
import { CredentialResponse } from './../../interfaces/CredencialResponse';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CredentialsService } from '../../services/credentials.service';
import { HttpParams } from '@angular/common/http';
import jsPDF from 'jspdf';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ConfirmacionDialogComponent } from '../../components/confirmacion-dialog/confirmacion-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
@Component({
  selector: 'app-contratistas',
  templateUrl: './contratistas.component.html',
  styleUrl: './contratistas.component.css'
})
export class ContratistasComponent {
  empleadoForm = new FormGroup({
    identificador:         new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-Z0-9\-]*$') ]),
    numeroGafete:          new FormControl('', [ Validators.pattern('^[0-9]*$') ]),
    nombre:                new FormControl(''),
    isCredentialHabilited: new FormControl(false),
    fechaVigencia:         new FormControl(),
    credentialType:        new FormControl(0),
  });

  credential!:          CredentialResponse;
  pdfSrc:               SafeResourceUrl;
  pdf:                  jsPDF               = new jsPDF();
  assignedDays:         Date                = new Date();
  btnDescargarDisabled: boolean             = true;
  btnGuardarDisabled:   boolean             = true;
  tipoCredencial:       string              = "Residentes";
  leyendaGafete:        string              = "Numero de gafete"

  constructor( private CS: CredentialsService, private sanitizer: DomSanitizer, private dialog: MatDialog, private router: Router) {
    this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl('');
    if(this.router.url.includes("provisional")){
      this.tipoCredencial = "Provisional";
      this.leyendaGafete = "Numero de gafete provisional";
      this.empleadoForm.controls.credentialType.setValue(1);
    }else
      if(!this.router.url.includes("permanent")){
        this.tipoCredencial = this.router.url.includes("longerTerm") ? "Corto Plazo" : "Corto Plazo (Menor a 3 días)";
        this.empleadoForm.controls.credentialType.setValue(this.router.url.includes("longerTerm") ? 3 : 2);
        //this.assignedDays.setDate(this.router.url.includes("longerTerm") ? new Date().getDate() + 3 : new Date().getDate() + 5);
      }
    this.empleadoForm.controls.isCredentialHabilited.disable();
    this.empleadoForm.controls.numeroGafete.disable();
    this.empleadoForm.controls.nombre.disable();
  }

  getEmployee() {
    if(this.empleadoForm.controls.identificador.valid){
      const params = new HttpParams().set('rfc', this.empleadoForm.controls.identificador.value! );
      this.CS.getUserCredential(params, "Contractor").subscribe({
        next: (v) => {
          if (v.values != null) 
            if(v.values.idTipoContratista == 2 && this.empleadoForm.controls.credentialType.value! > 1){
              this.openConfirmDialog("El contratista no es de tipo Corto Plazo");
              this.limpiar();
            }
            else
            if(v.values.idTipoContratista == 1 && this.empleadoForm.controls.credentialType.value! < 2){
              this.openConfirmDialog("El contratista no es de tipo Residente");
              this.limpiar();
            }
            else{
              const datediff = (new Date(v.values.fechaTerminacion).getTime() - new Date(v.values.fechaContrato).getTime())/ (1000 * 60 * 60 * 24);
              if(datediff > 3 && this.empleadoForm.controls.credentialType.value == 2)
              {
                this.openConfirmDialog("El contrato relacionado a este RFC es mayor a 3 días, tiene que ser menor");
                this.limpiar();
              }
              else
                if(datediff < 3 && this.empleadoForm.controls.credentialType.value == 3){
                  this.openConfirmDialog("El contrato relacionado a este RFC es menor a 3 días, tiene que ser mayor");
                  this.limpiar()
                }
                else
                  this.onSuccesGetContractor(v.values);
                }

          else {
            this.openConfirmDialog(v.message);
            this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl('');
            this.empleadoForm.patchValue({
              numeroGafete: '',
              isCredentialHabilited: false,
              nombre: '',
            });
          }
        }
      });
    }
  }

  onSuccesGetContractor(credential: CredentialResponse) {
    let numeroGafete: number;

    if (credential.isUserActive) {
      this.empleadoForm.controls.isCredentialHabilited.enable();
      this.empleadoForm.controls.numeroGafete.enable();
      this.btnGuardarDisabled = false;
      this.empleadoForm.controls.nombre.patchValue( credential.nombre + ' ' + credential.apellidos );
      numeroGafete = this.tipoCredencial != "Provisional" ? credential.numeroGafete : credential.numeroProvisionalGafete;
      if (numeroGafete != null && numeroGafete != undefined) {
        if(this.tipoCredencial != "Provisional")
          this.empleadoForm.patchValue({
            isCredentialHabilited: credential.isCredentialHabilited,
            numeroGafete: credential.numeroGafete.toString()
          });
        else
          this.empleadoForm.patchValue({
            isCredentialHabilited: credential.isCredentialProvisionalHabilited,
            numeroGafete: credential.numeroProvisionalGafete.toString(),
            fechaVigencia: credential.fechaVigencia
          });
        this.btnDescargarDisabled = false;
        this.credential = credential;
        this.generarCredencial();
      } else {
        this.btnDescargarDisabled = true;
        this.CS.openSnackBar( this.tipoCredencial != "Provisional" ? 'No tiene número de gafete asignado' : 'No tiene número de gafete provisional asignado','Aceptar');
        this.empleadoForm.patchValue({
          numeroGafete: '',
          isCredentialHabilited: false,
          fechaVigencia: this.assignedDays
        });
        this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl('');
      }
    } else {
      this.limpiar();
      this.CS.openSnackBar('Estatus Inactivo','Aceptar');
    }
  }

  onSubmit() {
    if( this.empleadoForm.controls.numeroGafete.value =='' ){
      this.empleadoForm.controls.numeroGafete.setErrors({ required: true });
    }else
    {
      this.CS.updateUserCredential(this.empleadoForm.value, "Contractor").subscribe({
        next: (v) => {
          if (v.message == 'Registro actualizado correctamente') {
            this.CS.openSnackBar(v.message,'Aceptar');
            this.getEmployee();
            this.btnDescargarDisabled = false;
          }else{
            this.CS.openSnackBar(v.message,'Aceptar');
            this.empleadoForm.patchValue({
              numeroGafete: '',
              isCredentialHabilited: false,
            });
          }
        },
      });
    }
  }

  generarCredencial( ) {
      this.pdf = this.CS.generarCredenciaContratista(this.credential, this.empleadoForm.controls.credentialType.value! < 2);
    const pdfContent = this.pdf.output('dataurlstring');
    this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl(pdfContent);
  }

  openConfirmDialog(mensaje: string) {
    const dialogRef = this.dialog.open(ConfirmacionDialogComponent, {
      width: '25%',
      data: {
        message: mensaje,
      },
    });
  }

  getControlError(control: FormControl) {
    return control?.hasError('required')
      ? 'Campo obligatorio'
      : control?.hasError('pattern') 
      ? 'El campo debe contener solamente numeros' 
      :'El campo no puede contener caracteres especiales';
  }

  descargar() {
    const credentialData = this.credential;
    this.pdf.save(`${credentialData!.nombre + credentialData!.apellidos}.pdf`);
  }

  limpiar(){
    this.empleadoForm.controls.isCredentialHabilited.disable();
    this.empleadoForm.controls.numeroGafete.disable();
    this.btnGuardarDisabled = true;
    this.btnDescargarDisabled = true;
    this.empleadoForm.patchValue({
      nombre: '',
      numeroGafete: '',
      isCredentialHabilited: false
    });
    this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl('');
  }
}
