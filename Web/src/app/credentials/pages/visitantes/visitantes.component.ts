import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import jsPDF from 'jspdf';
import { ConfirmacionDialogComponent } from '../../components/confirmacion-dialog/confirmacion-dialog.component';
import { CredentialResponse } from '../../interfaces/CredencialResponse';
import { CredentialsService } from '../../services/credentials.service';

@Component({
  selector: 'app-visitantes',
  templateUrl: './visitantes.component.html',
  styleUrl: './visitantes.component.css'
})
export class VisitantesComponent {

  visitorForm = new FormGroup({
    identificador:         new FormControl(''),
    numeroGafete:          new FormControl('', [ Validators.pattern('^[0-9]*$') ]),
    nombre:                new FormControl(''),
    isCredentialHabilited: new FormControl(false),
    fechaVigencia:         new FormControl(),
    credentialType:        new FormControl(4),
  });

  pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl('');
  pdf: jsPDF = new jsPDF();
  credential!: CredentialResponse ;
  btnDescargarDisabled: boolean = true;
  btnGuardarDisabled: boolean = true;
  leyendaGafete: string = "NÚmero de Gafete Provisional"

  constructor( private CS: CredentialsService, private sanitizer: DomSanitizer, private dialog: MatDialog, private router: Router ) {
    this.visitorForm.controls.isCredentialHabilited.disable();
    this.visitorForm.controls.numeroGafete.disable();
    this.visitorForm.controls.nombre.disable();
  }

  getEmployee() {
    if(this.visitorForm.controls.identificador.valid){
      const params = new HttpParams().set('rfc', this.visitorForm.controls.identificador.value! );
      this.CS.getUserCredential(params,'Visitor').subscribe({
        next: (v) => {
          if (v.values != null) 
            this.onSuccesGetEmployee(v.values);
          else {
            this.openConfirmDialog(v.message);
            this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl('');
            this.visitorForm.patchValue({
              numeroGafete: '',
              isCredentialHabilited: false,
              nombre: '',
            });
          }
        }
      });
    }
  }

  onSuccesGetEmployee(credential: CredentialResponse) {
    let numeroGafete: number;
    if (credential.isUserActive) {
      this.visitorForm.controls.isCredentialHabilited.enable();
      this.visitorForm.controls.numeroGafete.enable();
      this.btnGuardarDisabled = false;
      this.visitorForm.controls.nombre.patchValue( credential.nombre );
      numeroGafete = credential.numeroGafete;
      if (numeroGafete != null && numeroGafete != undefined) {
          this.visitorForm.patchValue({
            isCredentialHabilited: credential.isCredentialHabilited,
            numeroGafete: credential.numeroGafete.toString(),
            fechaVigencia: credential.fechaVigencia
        })
        this.btnDescargarDisabled = false;
        this.credential = credential;
        this.generarCredencial();
      } else {
        this.btnDescargarDisabled = true;
        this.CS.openSnackBar('No tiene número de gafete provisional asignado','Aceptar');
        this.visitorForm.patchValue({
          numeroGafete: '',
          isCredentialHabilited: false,
          fechaVigencia: new Date()
        });
        this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl('');
      }
    } else {
      this.visitorForm.controls.isCredentialHabilited.disable();
      this.visitorForm.controls.numeroGafete.disable();
      this.btnGuardarDisabled = true;
      this.btnDescargarDisabled = true;
      this.visitorForm.patchValue({
        nombre: '',
        numeroGafete: '',
        isCredentialHabilited: false
      });
      this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl('');
      this.CS.openSnackBar('Estatus Inactivo','Aceptar');
    }
  }

  onSubmit() {
    if( this.visitorForm.controls.numeroGafete.value =='' ){
      this.visitorForm.controls.numeroGafete.setErrors({ required: true });
    }else
    this.CS.updateUserCredential(this.visitorForm.value, "Visitor").subscribe({
      next: (v) => {
        if (v.message == 'Registro actualizado correctamente') {
          this.CS.openSnackBar(v.message,'Aceptar');
          this.getEmployee();
          this.btnDescargarDisabled = false;
        }else{
          this.CS.openSnackBar(v.message,'Aceptar');
          this.visitorForm.patchValue({
            numeroGafete: '',
            isCredentialHabilited: false,
          });
        }
      },
    });
  }

  generarCredencial( ) {
    this.pdf = this.CS.generarCredenciaVisitante(this.credential);
    const pdfContent = this.pdf.output('dataurlstring');
    this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl(pdfContent);
  }

  openConfirmDialog(mensaje: string) {
    const dialogRef = this.dialog.open(ConfirmacionDialogComponent, {
      width: '25%',
      data: {
        message: mensaje,
      },
    });
  }

  getControlError(control: FormControl) {
    return control?.hasError('required')
      ? 'Campo obligatorio'
      : control?.hasError('pattern') 
      ? 'El campo debe contener solamente numeros' 
      :'El campo no puede contener caracteres especiales';
  }

  descargar() {
    const credentialData = this.credential;
    this.pdf.save(`${credentialData!.nombre + credentialData!.apellidos}.pdf`);
  }

}
