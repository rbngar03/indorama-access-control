import { CredentialResponse } from './../../interfaces/CredencialResponse';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CredentialsService } from '../../services/credentials.service';
import { HttpParams } from '@angular/common/http';
import jsPDF from 'jspdf';
import { DomSanitizer } from '@angular/platform-browser';
import { ConfirmacionDialogComponent } from '../../components/confirmacion-dialog/confirmacion-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-empleados-permanentes',
  templateUrl: './empleados-permanentes.component.html',
  styleUrl: './empleados-permanentes.component.css',
})

export class EmpleadosPermanentesComponent {
  
  empleadoForm = new FormGroup({
    identificador:         new FormControl('', [ Validators.required, Validators.pattern('^[0-9]*$') ]),
    numeroGafete:          new FormControl('', [ Validators.pattern('^[0-9]*$') ]),
    nombre:                new FormControl(''),
    isCredentialHabilited: new FormControl(false),
    fechaVigencia:         new FormControl(),
    credentialType:        new FormControl(0),
  });

  pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl('');
  pdf: jsPDF = new jsPDF();
  credential!: CredentialResponse ;
  btnDescargarDisabled: boolean = true;
  btnGuardarDisabled: boolean = true;
  tipoCredencial: string = "Permanentes";
  leyendaGafete: string = "Numero de gafete"

  constructor( private CS: CredentialsService, private sanitizer: DomSanitizer, private dialog: MatDialog, private router: Router ) {
    if(this.router.url.includes("provisional")){
      this.tipoCredencial = "Provisional";
      this.leyendaGafete = "Numero de gafete provisional";
      this.empleadoForm.controls.credentialType.setValue(1);
    }
    this.empleadoForm.controls.isCredentialHabilited.disable();
    this.empleadoForm.controls.numeroGafete.disable();
    this.empleadoForm.controls.nombre.disable();
  }

  getEmployee() {
    if(this.empleadoForm.controls.identificador.valid){
      const params = new HttpParams().set('numEmpleado', this.empleadoForm.controls.identificador.value! );
      this.CS.getUserCredential(params, "Employee").subscribe({
        next: (v) => {
          if (v.values != null) 
            this.onSuccesGetEmployee(v.values);
          else {
            this.openConfirmDialog(v.message);
            this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl('');
            this.empleadoForm.patchValue({
              numeroGafete: '',
              isCredentialHabilited: false,
              nombre: '',
            });
          }
        }
      });
    }
  }

  onSuccesGetEmployee(credential: CredentialResponse) {
    let numeroGafete: number;
    if (credential.isUserActive) {
      this.empleadoForm.controls.isCredentialHabilited.enable();
      this.empleadoForm.controls.numeroGafete.enable();
      this.btnGuardarDisabled = false;
      this.empleadoForm.controls.nombre.patchValue( credential.nombre + ' ' + credential.apellidos );
      numeroGafete = this.tipoCredencial == "Permanentes" ? credential.numeroGafete : credential.numeroProvisionalGafete;
      if (numeroGafete != null && numeroGafete != undefined) {
        if(this.tipoCredencial == "Permanentes")
          this.empleadoForm.patchValue({
            isCredentialHabilited: credential.isCredentialHabilited,
            numeroGafete: credential.numeroGafete.toString()
          });
        else
        this.empleadoForm.patchValue({
          isCredentialHabilited: credential.isCredentialProvisionalHabilited,
          numeroGafete: credential.numeroProvisionalGafete.toString(),
          fechaVigencia: credential.fechaVigencia
        })
        this.btnDescargarDisabled = false;
        this.credential = credential;
        this.generarCredencial();
      } else {
        this.btnDescargarDisabled = true;
        this.CS.openSnackBar( this.tipoCredencial == "Permanentes" ? 'No tiene número de gafete asignado' : 'No tiene número de gafete provisional asignado','Aceptar');
        this.empleadoForm.patchValue({
          numeroGafete: '',
          isCredentialHabilited: false,
          fechaVigencia: new Date()
        });
        this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl('');
      }
    } else {
      this.empleadoForm.controls.isCredentialHabilited.disable();
      this.empleadoForm.controls.numeroGafete.disable();
      this.btnGuardarDisabled = true;
      this.btnDescargarDisabled = true;
      this.empleadoForm.patchValue({
        nombre: '',
        numeroGafete: '',
        isCredentialHabilited: false
      });
      this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl('');
      this.CS.openSnackBar('Estatus Inactivo','Aceptar');
    }
  }

  onSubmit() {
    if( this.empleadoForm.controls.numeroGafete.value =='' ){
      this.empleadoForm.controls.numeroGafete.setErrors({ required: true });
    }else
    this.CS.updateUserCredential(this.empleadoForm.value, "Employee").subscribe({
      next: (v) => {
        if (v.message == 'Registro actualizado correctamente') {
          this.CS.openSnackBar(v.message,'Aceptar');
          this.getEmployee();
          this.btnDescargarDisabled = false;
        }else{
          this.CS.openSnackBar(v.message,'Aceptar');
          this.empleadoForm.patchValue({
            numeroGafete: '',
            isCredentialHabilited: false,
          });
        }
      },
    });
  }

  generarCredencial( ) {
    this.pdf = this.CS.generarCredencialEmpleadoPerm(this.credential);
    const pdfContent = this.pdf.output('dataurlstring');
    this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl(pdfContent);
  }

  openConfirmDialog(mensaje: string) {
    const dialogRef = this.dialog.open(ConfirmacionDialogComponent, {
      width: '25%',
      data: {
        message: mensaje,
      },
    });
  }

  getControlError(control: FormControl) {
    return control?.hasError('required')
      ? 'Campo obligatorio'
      : control?.hasError('pattern') 
      ? 'El campo debe contener solamente numeros' 
      :'El campo no puede contener caracteres especiales';
  }

  descargar() {
    const credentialData = this.credential;
    this.pdf.save(`${credentialData!.nombre + credentialData!.apellidos}.pdf`);
  }
}
