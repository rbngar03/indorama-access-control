import { CredentialResponse } from './../interfaces/CredencialResponse';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { GenericHttpResponse } from '../../shared/interfaces/generic-http-response';
import { DomSanitizer } from '@angular/platform-browser';
import jsPDF from 'jspdf';
import { Router } from '@angular/router';
import { AppConfigService } from '../../shared/services/appConfig.service';

@Injectable({
  providedIn: 'root'
})
export class CredentialsService {

  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();

  constructor(private router: Router, private http: HttpClient, public snackBar: MatSnackBar) { }

  getUserCredential(params: HttpParams, type: string): Observable<GenericHttpResponse<CredentialResponse>> {
    return this.http.get<GenericHttpResponse<CredentialResponse>>(`https://localhost:44305/Credentials/${type}`, { params: params });
  }

  updateUserCredential(credentialRequest: any, type: string): Observable<GenericHttpResponse<string>> {
    return this.http.put<GenericHttpResponse<string>>(`https://localhost:44305/Credentials/${type}`, credentialRequest);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: Infinity,
    });
  }

  generarCredencialEmpleadoPerm (credential: CredentialResponse) : jsPDF {
    let pdf = new jsPDF({ format: [54.21, 87.01] });
    const credentialData = credential;
    const frente = new Image();
    const trasera = new Image();
    const logo = new Image();
    const foto = new Image();
    frente.src = './../../../../assets/images/FrenteEmpPerNuevo_leyenda.png';
    trasera.src ='./../../../../assets/images/Atras_empleado_nuevo.png';
    logo.src = './../../../../assets/logos/Indorama_logo.png';
    foto.src = `data:image/jpeg;base64,${credentialData?.foto}`;
    pdf.setFontSize(9);
    pdf.addImage(frente, 'PNG', 0, 0, 0, 0, '', 'NONE');
    pdf.addImage(logo, 'PNG', 0, 8, 54.21, 12);
    if(this.router.url.includes("permanent")){
      pdf.addImage(foto, 'PNG', 12, 23, 30, 28);
      pdf.text(credentialData!.nombre, 27.1, 58, { align: 'center' });
      pdf.text(credentialData!.apellidos, 27.1, 62, { align: 'center' });
    }
    pdf.addPage();
    pdf.addImage(trasera, 'PNG', 0, 0, 0, 0, '', 'NONE');
    pdf.setFontSize(7);
    pdf.setFont("helvetica",'bold')
    pdf.setTextColor('Dark blue');
    pdf.text('Indorama Ventures', 17.1, 22);
    pdf.text('Complejo Querétaro', 17.1, 26);
    pdf.setTextColor('Gray');
    pdf.text('Acceso IV No. 202', 17.1, 30);
    pdf.text('Parque Ind. "Benito Juárez"', 17.1, 33);
    pdf.text('Querétaro, Qro', 17.1, 36);
    pdf.text('Tel: (442) 2112700', 17.1, 39);
    pdf.setFontSize(5);
    pdf.text('ESTA CREDENCIAL ES PROPIEDAD DE ', 19.1, 45);
    pdf.text('LA COMPAÑÍA', 19.1, 48);
    pdf.text('ES UNICAMENTE IDENTIFICACIÓN Y', 19.1, 51);
    pdf.text('DEBERÁ DEVOLVERSE A SOLICITUD  ', 19.1, 54);
    pdf.text('DE LA MISMA.', 19.1, 57);
    return pdf;
  }

  generarCredenciaContratista (credential: CredentialResponse, isshortTerm: boolean = false) : jsPDF {
    let pdf = new jsPDF({ format: [54.21, 87.01] });
    const credentialData = credential;
    const frente = new Image();
    const foto = new Image();
    frente.src = isshortTerm ? 
      './../../../../assets/images/FrenteContratista.png':
      './../../../../assets/images/FrenteContCPNuevo.png';
    foto.src = `data:image/jpeg;base64,${credentialData?.foto}`;
    pdf.setFontSize(8);
    pdf.setFont("helvetica",'bold')
    pdf.addImage(frente, 'PNG', 0, 0, 0, 0, '', 'NONE');
    pdf.text("Credencial", 27.1, 8, { align: 'center' });
    pdf.text("de Identificación", 27.1, 13, { align: 'center' });
    pdf.text(credentialData.nombreCompañia, 27.1, 18, { align: 'center' });
    if(this.router.url.includes("permanent") || this.router.url.includes("longerTerm")){
      pdf.addImage(foto, 'PNG', 12, 23, 30, 26);
      pdf.setFontSize(10);
      pdf.text(credentialData!.nombre, 27.1, 58, { align: 'center' });
      pdf.setFont("helvetica",'normal')
      pdf.text(credentialData!.apellidos, 27.1, 62, { align: 'center' });
    }
    pdf.setFont("helvetica",'bold')
    pdf.setTextColor('White');
    pdf.setFontSize(16);
    pdf.text("Contratista", 27.1, 78, { align: 'center' });
    pdf.setFontSize(12);
    pdf.text("QUERÉTARO", 27.1, 84, { align: 'center' });
    pdf.addPage();
    pdf.setFont("helvetica",'bold')
    pdf.setTextColor('Gray');
    pdf.setFontSize(7);
    pdf.text('ESTA CREDENCIAL ES ', 27.1, 22, { align: 'center' });
    pdf.text('PROPIEDAD DE LA COMPAÑÍA', 27.1, 25, { align: 'center' });
    pdf.text('ES ÚNICAMENTE PARA QUE EL ', 27.1, 28, { align: 'center' });
    pdf.text('VISITANTE SE IDENTIFIQUE', 27.1, 31, { align: 'center' });
    pdf.text('DURANTE SU ESTANCIA DENTRO', 27.1, 34, { align: 'center' });
    pdf.text('DE LAS INSTALACIOENES', 27.1, 37, { align: 'center' });
    pdf.text('ESTA CREDENCIAL DEBERÁ', 27.1, 40, { align: 'center' });
    pdf.text('DEVOLVERSE A SOLICITUD DE LA', 27.1, 43, { align: 'center' });
    pdf.text('DE LA MISMA.', 27.1, 46, { align: 'center' });
    return pdf;
  }

  generarCredenciaVisitante (credential: CredentialResponse) : jsPDF {
    let pdf = new jsPDF({ format: [54.21, 87.01] });
    const credentialData = credential;
    const frente = new Image();
    frente.src = './../../../../assets/images/frenteVisProvNew.png';
    pdf.setFontSize(8);
    pdf.setFont("helvetica",'bold')
    pdf.addImage(frente, 'PNG', 0, 0, 0, 0, '', 'NONE');
    pdf.text("Credencial", 27.1, 8, { align: 'center' });
    pdf.text("de Identificación", 27.1, 13, { align: 'center' });
    pdf.text(credentialData!.nombre, 27.1, 58, { align: 'center' });
    pdf.setFont("helvetica",'bold')
    pdf.setTextColor('White');
    pdf.setFontSize(16);
    pdf.text("Visitante", 27.1, 78, { align: 'center' });
    pdf.setFontSize(12);
    pdf.text("QUERÉTARO", 27.1, 84, { align: 'center' });
    pdf.addPage();
    pdf.setFont("helvetica",'bold')
    pdf.setTextColor('Gray');
    pdf.setFontSize(7);
    pdf.text('ESTA CREDENCIAL ES ', 27.1, 22, { align: 'center' });
    pdf.text('PROPIEDAD DE LA COMPAÑÍA', 27.1, 25, { align: 'center' });
    pdf.text('ES ÚNICAMENTE PARA QUE EL ', 27.1, 28, { align: 'center' });
    pdf.text('VISITANTE SE IDENTIFIQUE', 27.1, 31, { align: 'center' });
    pdf.text('DURANTE SU ESTANCIA DENTRO', 27.1, 34, { align: 'center' });
    pdf.text('DE LAS INSTALACIOENES', 27.1, 37, { align: 'center' });
    pdf.text('ESTA CREDENCIAL DEBERÁ', 27.1, 40, { align: 'center' });
    pdf.text('DEVOLVERSE A SOLICITUD DE LA', 27.1, 43, { align: 'center' });
    pdf.text('DE LA MISMA.', 27.1, 46, { align: 'center' });
    return pdf;
    
  }
  
}
