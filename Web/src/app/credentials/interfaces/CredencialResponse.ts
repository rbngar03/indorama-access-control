export interface CredentialResponse {
  isCredentialHabilited:            boolean;
  isCredentialProvisionalHabilited: boolean;
  isUserActive:                     boolean;
  fechaVigencia:                    Date;
  fechaContrato:                    Date;
  fechaTerminacion:                 Date;
  numeroGafete:                     number;
  numeroProvisionalGafete:          number;
  idTipoContratista:                number;
  identificador:                    string;
  nombre:                           string;
  apellidos:                        string;
  nombreCompañia:                   string;
  foto:                             string;
}
