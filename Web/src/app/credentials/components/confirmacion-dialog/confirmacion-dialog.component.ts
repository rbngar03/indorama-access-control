import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CredentialsService } from '../../services/credentials.service';

@Component({
  selector: 'app-confirmacion-dialog',
  templateUrl: './confirmacion-dialog.component.html',
  styleUrl: './confirmacion-dialog.component.css'
})
export class ConfirmacionDialogComponent {
  isAccepted: boolean = false;
  constructor(public dialogRef: MatDialogRef<ConfirmacionDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data:any, private CS: CredentialsService, 
   ){ 
   }

  
}
