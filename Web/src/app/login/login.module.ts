import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FormLoginComponent } from './components/form-login/form-login.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { ModalLoginComponent } from './components/modal-login/modal-login.component';




@NgModule({
  declarations: [
    FormLoginComponent,
    LoginPageComponent,
    ModalLoginComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    LoginPageComponent
  ]
})
export class LoginModule { }
