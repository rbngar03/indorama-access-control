import { ElementRef, Injectable, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {Md5} from 'ts-md5';

import { User } from '../interfaces/user.interface';
import { Router } from '@angular/router';
import { PermissionsService } from './permissions.service';
import { AppConfigService } from '../../shared/services/appConfig.service';
import { ModalLoginComponent } from '../components/modal-login/modal-login.component';
import { FormLoginComponent } from '../components/form-login/form-login.component';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private encrypter: Md5 = new Md5();
  //private serviceUrl: string = 'http://192.168.10.20:9090';
  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();

  public isLoginSuccessfull: boolean = false;

  constructor(
    private http: HttpClient,
    private router: Router,
    private permissionsService: PermissionsService,
    ) { }

  public verifyCredentials(user: User, modalLogin: ModalLoginComponent | undefined): void {
    let encryptedPassword = this.encryptPassword(user.password)?.toString();
    this.http.get<User>(`${this.serviceUrl}/Login/${user.usuario1}`)
      .subscribe(
        resp => {
          this.handleResult(user, encryptedPassword,  resp, modalLogin);
        },
        error => {
          this.handleError(user);
          modalLogin?.openModal("Usuario o contraseña no válido.")
        }
      );
  }

  private handleResult(user: User, encyptedPassword: string | undefined, resp: User, modalLogin?: ModalLoginComponent | undefined): void{
    if (user.usuario1 === resp.usuario1 && encyptedPassword === resp.password) {

      console.log(modalLogin);
      user.usuario1 = '';
      user.password = '';
      sessionStorage.setItem('usuario',resp.usuario1);

      this.permissionsService.getAllUserPermissions(resp.idPerfil);

      this.router.navigate(['/home']);
    }  else {
      modalLogin?.openModal("Usuario o contraseña no válido.")
      user.usuario1 = '';
      user.password = '';
    }
  }

  checkAuthentication():boolean {
    return !!sessionStorage.getItem('usuario');
  }

  checkLoginSuccessfull(): boolean {
    return this.isLoginSuccessfull;
  }

  public logout(): void {
    sessionStorage.clear();
    this.router.navigate(['/login/']);
  }

  private handleError(user: User): void{
    //alert("Usuario no registrado, ingrese datos correctos.");
    user.usuario1 = '';
    user.password = '';
  }

  private encryptPassword(password: string): string | undefined {
    this.encrypter.start();
    this.encrypter.appendStr(password);
    let encryptedPassword = this.encrypter.end()?.toString();
    return encryptedPassword;
  }

}
