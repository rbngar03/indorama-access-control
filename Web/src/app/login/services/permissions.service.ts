import { Permission } from './../interfaces/permission.interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Profile } from '../interfaces/profile.interface';
import { AppConfigService } from '../../shared/services/appConfig.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {

  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();

  public profile: Profile = {
    idPerfil:    0,
    clave:       '',
    descripcion: '',
    usuarios:    []
  }

  public profiles: Profile[] = [];

  constructor(
    private http: HttpClient,
    ) { }

  public getAllUserPermissions(idProfile: number): void {
    this.http.get<Permission>(`${this.serviceUrl}/Permisos/PerfilesModulos/${idProfile}`)
    .subscribe(
      response => {
        response.values.moduloAccion.forEach(modulesActions => {
          let actions: string = "";
          modulesActions.acciones.forEach(acciones => {
            if(acciones.isActivated)
              actions += acciones.nombre + ';';
          });
          sessionStorage.setItem(modulesActions.nombre, actions);
        });
      }
    )
  }

  public getUserProfileById(idProfile: number): void{
    this.http.get<Profile>(`${this.serviceUrl}/Permisos/Perfiles/${idProfile}`)
    .subscribe(
      response => {
        this.profile = response;
      }
    )
  }

  public getAllProfiles(): void {
    this.http.get<Profile[]>(`${this.serviceUrl}/Permisos/Perfiles`)
    .subscribe(
      response => {
        this.profiles = response;
      }
    )
  }

  readPermissionsByModule(moduleName: string, action: string): boolean {
    var permissions: string[] = [];
    var stringPermissions: string | null = sessionStorage.getItem(moduleName);
    if (stringPermissions)
      permissions = stringPermissions.split(";");

    var isContained = permissions.includes(action);

    return isContained;
  }

}
