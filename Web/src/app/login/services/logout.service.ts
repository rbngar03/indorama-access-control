import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../interfaces/user.interface';
import { Router } from '@angular/router';
import { Observable, catchError, map, of, tap } from 'rxjs';
import { ResponseTransaction } from '../interfaces/response-transaction';
import { AppConfigService } from '../../shared/services/appConfig.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  //private serviceUrl: string = 'http://192.168.10.20:9090';
  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();

  private responseTransaction: ResponseTransaction = {
    result: ""
  };

  constructor(
    private http: HttpClient,
    private router: Router
    ) { }

  public saveTransaction(idUsuario: number, message: string): void {
    /*this.http.get<ResponseTransaction>(`${this.serviceUrl}/Login/${}`)
      .subscribe(
        resp => {

        },
        error => {

        }
      );*/
  }

  private handleResult(user: User, encyptedPassword: string | undefined, resp: User): void{

  }

  checkAuthentication():boolean {
    if ( !localStorage.getItem('usuario') ) return false;
    //const user = localStorage.getItem('usuario');
    return true
  }

  public logout(): void {
    localStorage.clear();
    this.router.navigate(['/login/']);
  }

  private handleError(): void{

  }

}
