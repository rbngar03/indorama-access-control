export interface User {
  idUsuarios:         number;
  usuario1:           string;
  password:           string;
  nombreUsuario:      string;
  idPerfil:           number;
  idPerfilNavigation: null;
}
