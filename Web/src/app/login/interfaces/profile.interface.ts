export interface Profile {
  idPerfil:    number;
  clave:       string;
  descripcion: string;
  usuarios:    any[];
}
