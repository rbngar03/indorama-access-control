export interface Permission {
  values:   Values;
  menssaje: null;
}

export interface Values {
  idPerfil:     number;
  moduloAccion: ModuloAccion[];
}

export interface ModuloAccion {
  idModulo: number;
  nombre:   string;
  acciones: Accione[];
}

export interface Accione {
  idAccion:    number;
  nombre:      Nombre;
  isActivated: boolean;
}

export enum Nombre {
  Agregar = "AGREGAR",
  Editar = "EDITAR",
  Eliminar = "ELIMINAR",
  Exportar = "EXPORTAR",
  Filtrar = "FILTRAR",
  Ver = "VER",
}
