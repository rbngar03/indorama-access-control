import { Component, ElementRef, ViewChild } from '@angular/core';

//@ts-ignore
const $: any = window['$'];

@Component({
  selector: 'app-modal-login',
  templateUrl: './modal-login.component.html',
  styleUrl: './modal-login.component.css'
})
export class ModalLoginComponent {

  @ViewChild('modal') modalLogin?: ElementRef;

  message: string = '';

  openModal(message: string){
    this.message = message;
    $(this.modalLogin?.nativeElement).modal('show');
  }

}
