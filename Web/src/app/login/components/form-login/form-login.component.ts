import { Component, ViewChild } from '@angular/core';
import { User } from '../../interfaces/user.interface';
import { LoginService } from '../../services/login.service.service';
import { Router } from '@angular/router';
import { ModalLoginComponent } from '../modal-login/modal-login.component';

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrl: './form-login.component.css'
})
export class FormLoginComponent {

  @ViewChild(ModalLoginComponent) modalLogin?: ModalLoginComponent;

  constructor(
    private loginService: LoginService,
    private router: Router) {
    }

  public user: User = {
    idUsuarios: 0,
    usuario1: '',
    password: '',
    nombreUsuario: '',
    idPerfil: 0,
    idPerfilNavigation: null
  }

  public validateUser(): void{
    if (this.user.usuario1.length == 0 || this.user.password?.length == 0) {
      this.open("Los campos de usuario y contraseña no pueden estar vacíos.");
    } else {
      this.loginService.verifyCredentials(this.user, this.modalLogin);
    }
  }

  public open(message: string){
    this.modalLogin?.openModal(message);
  }

}
