import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { LoginService } from '../services/login.service.service';

export const auhtGuard: CanActivateFn = (route, state) => {
  const loginService = inject(LoginService);
  const router = inject(Router);
  if(!loginService.checkAuthentication())
    router.navigate(['./login']);
  return loginService.checkAuthentication();
};
