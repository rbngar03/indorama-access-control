import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPageComponent } from './login/pages/login-page/login-page.component';
import { MainLayoutComponent } from './shared/components/main-layout/main-layout.component';
import { auhtGuard } from './login/guards/auht.guard';
import { HomeViewComponent } from './shared/components/home-view/home-view.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: '',
    component: MainLayoutComponent,
    canActivate: [auhtGuard],
    canActivateChild: [auhtGuard],
    children : [
      {
        path: "", redirectTo: "/home", pathMatch: "full"
      },
      {
        path: 'home', component: HomeViewComponent
      },
      {
        path: 'catalogues', loadChildren: () =>
        import("./catalogue/catalogue.module").then((m) => m.CatalogueModule)
      },
      {
        path: 'parameters', loadChildren: () =>
        import("./parameters/parameters.module").then((m) => m.ParametersModule)
      },
      {
        path: 'profiles', loadChildren : () =>
          import("./profiles/profiles.module").then((m) => m.ProfilesModule)
      },
      {
        path: 'credentials', loadChildren : () =>
          import("./credentials/credentials.module").then((m) => m.CredentialsModule)
      },
      {
        path: 'reports', loadChildren : () =>
          import("./reports/reports.module").then((m) => m.ReportsModule)
      },
    ],

  },
  {
    path: '**',
    redirectTo: '/home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
