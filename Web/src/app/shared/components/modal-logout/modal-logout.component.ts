import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { delay } from 'rxjs';
import { LoginService } from '../../../login/services/login.service.service';

@Component({
  selector: 'shared-modal-logout',
  templateUrl: './modal-logout.component.html',
  styleUrl: './modal-logout.component.css'
})
export class ModalLogoutComponent {

  constructor(
    private loginService : LoginService
    ) { }

  public logout(): void {
    this.loginService.logout();
  }
}
