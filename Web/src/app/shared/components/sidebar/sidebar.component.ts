import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { PermissionsService } from '../../../login/services/permissions.service';

@Component({
  selector: 'app-shared-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.css'
})
export class SidebarComponent {

  constructor(private router: Router,
              private permissionServie: PermissionsService){
  }

  goToRoute( route:string ){
    this.router.navigate([route]);
  }

  checkPermission(moduleName: string, action: string): boolean {
    var isModuleEnable = false;
    isModuleEnable = this.permissionServie.readPermissionsByModule(moduleName, action);
    return isModuleEnable;
  }
}
