import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';

@Component({
  selector: 'shared-login-menu',
  templateUrl: './login-menu.component.html',
  styleUrl: './login-menu.component.css'
})
export class LoginMenuComponent implements OnInit{

  public userName : string = "";

  constructor(
    private activatedRoute: ActivatedRoute
  ){

  }

  ngOnInit(){
    this.userName = sessionStorage.getItem('usuario') ?? "";
  }

}
