import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { LoginMenuComponent } from './components/login-menu/login-menu.component';
import { ModalLogoutComponent } from './components/modal-logout/modal-logout.component';
import { RouterModule } from '@angular/router';

import { HomeViewComponent } from './components/home-view/home-view.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { AppRoutingModule } from '../app-routing.module';


@NgModule({
  declarations: [
    SidebarComponent,
    LoginMenuComponent,
    ModalLogoutComponent,
    HomeViewComponent,
    MainLayoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatSidenavModule,
    AppRoutingModule,
  ],
  exports:[
    LoginMenuComponent,
    ModalLogoutComponent,
    SidebarComponent
  ]
})
export class SharedModule { }
