export interface GenericHttpResponse<T> {
    values:  T;
    message: string;
}
