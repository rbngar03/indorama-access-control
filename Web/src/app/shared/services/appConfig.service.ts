import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IAppConfig } from '../interfaces/IAppConfig';
import { environment } from '../../../environments/environment';

@Injectable()
export class AppConfigService {

  public static settings: IAppConfig | undefined;

  constructor(private http: HttpClient) { }

  load() {
    const jsonFile = `assets/config/config.${environment.name}.json`;
    return new Promise<void>((resolve, reject) => {
      this.http.get<IAppConfig>(jsonFile).toPromise().then((response) => {
        AppConfigService.settings = response;
        resolve();
      }).catch((response: any) => {
        reject(`Could not load file '${jsonFile}': ${JSON.stringify(response)}`);
      });
    });
  }
}
