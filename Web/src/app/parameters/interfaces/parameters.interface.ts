export interface Parameter {
  idParametro: number,
  nombre:      string,
  descripcion: string,
  valor:       number,
  unidad:      string
}

