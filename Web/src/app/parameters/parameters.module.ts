import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParametersPageComponent } from './pages/parameters-page/parameters-page.component';
import { ParametersRoutingModule } from './parameters-routing.module';
import { ParametersTableComponent } from './components/parameters-table/parameters-table.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ParametersPageComponent,
    ParametersTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ParametersRoutingModule
  ]
})
export class ParametersModule { }
