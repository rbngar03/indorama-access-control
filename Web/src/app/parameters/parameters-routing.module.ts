import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ParametersPageComponent } from './pages/parameters-page/parameters-page.component';

//localhost/catalogos/
const routes: Routes = [
    { path: 'parameters', component: ParametersPageComponent },
    { path: '**', redirectTo: 'login' }
  /*
  { path: 'not-found', component: NotFoundComponent },
  { path: '**',redirectTo: 'not-found' }*/
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParametersRoutingModule { }
