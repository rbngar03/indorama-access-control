import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Parameter } from '../interfaces/parameters.interface';
import { AppConfigService } from '../../shared/services/appConfig.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class ParametersService {

  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();

  public parameters: Parameter[] = [];

  constructor(
    private http: HttpClient,
    public snackbar: MatSnackBar
    ) { }

  public getAllParameters(): void {
    this.http.get<Parameter[]>(`${this.serviceUrl}/Parametros`)
      .subscribe(
        resp => {
          this.parameters = resp;
          console.log(this.parameters);
        }
      );
  }

  public editAllParameters(parameters: Parameter[]): void {

    var countOK: number = 0;

    parameters.forEach(parameter => {
      const bodyRequest = {
        "idParametro": parameter.idParametro,
        "nombre": parameter.nombre,
        "descripcion": parameter.descripcion,
        "valor": parameter.valor,
        "unidad": parameter.unidad
      }

      this.http.put<any>(`${this.serviceUrl}/Parametros/${parameter.idParametro}`, bodyRequest)
        .subscribe(
          resp => {
            if(resp !== null) countOK += 1;
            if(countOK == 4) this.openSnackBar("Parámetros actualizados correctamente", "Aceptar")
          }
      )
    });
  }

  openSnackBar(message: string, action: string): void {
    this.snackbar.open(message, action, {
      duration: 3000
    })
  }


}
