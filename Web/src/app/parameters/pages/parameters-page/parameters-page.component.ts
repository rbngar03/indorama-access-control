import { Component, OnInit } from '@angular/core';
import { ParametersService } from '../../services/parameters.service';
import { Parameter } from '../../interfaces/parameters.interface';

@Component({
  selector: 'app-parameters-page',
  templateUrl: './parameters-page.component.html',
  styleUrl: './parameters-page.component.css'
})
export class ParametersPageComponent implements OnInit{

  constructor(private parametersService: ParametersService){

  }

  ngOnInit(): void {
    this.parametersService.getAllParameters();
  }

  get parameters(): Parameter[]{
    return this.parametersService.parameters;
  }

  onEditParameter(parameters: Parameter[]): void {
    parameters.forEach(parameter => {
      if (parameter.valor <= 0) alert("Los valores no pueden ser menores o iguales a 0.");
    });
    this.parametersService.editAllParameters(parameters);
  }
}
