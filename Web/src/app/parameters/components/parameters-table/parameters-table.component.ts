import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Parameter } from '../../interfaces/parameters.interface';

@Component({
  selector: 'parameters-table',
  templateUrl: './parameters-table.component.html',
  styleUrl: './parameters-table.component.css'
})
export class ParametersTableComponent {

  @Input()
  public parameters: Parameter[] = [];

  @Output()
  public onEditParameters: EventEmitter<Parameter[]> = new EventEmitter();

  emitParameter(){
    if (this.parameters.length <= 0) return
    this.onEditParameters.emit(this.parameters);
    console.log('EditParameters');
    console.log(this.parameters);
  }

}
