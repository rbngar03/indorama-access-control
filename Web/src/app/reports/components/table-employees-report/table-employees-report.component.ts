import { Component, Input, SimpleChanges, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { EmployeeReport } from '../../interfaces/employeeReport.interface';

@Component({
  selector: 'reports-table-employees-report',
  templateUrl: './table-employees-report.component.html',
  styleUrl: './table-employees-report.component.css'
})
export class TableEmployeesReportComponent {

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  @Input()
  employeesReport: EmployeeReport[] = [];

  filtro: string = '';
  columns: string[] = ['Entrada', 'Salida', 'Nombre del empleado', 'Número del empleado', 'Sitio'];
  dbColumns: string[] = ['entrada', 'salida', 'nombreEmpleado', 'numeroEmpleado', 'planta']
  datasource: MatTableDataSource<EmployeeReport> = new MatTableDataSource();

  public isEmployeesLoaded : boolean = false;

  ngOnInit(): void{
    //this.datasource.data = ELEMENT_DATA;
  }

  ngAfterViewInit(): void {
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.datasource.data = this.employeesReport;
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
  }
}
