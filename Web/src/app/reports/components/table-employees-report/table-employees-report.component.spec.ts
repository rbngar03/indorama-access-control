import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableEmployeesReportComponent } from './table-employees-report.component';

describe('TableEmployeesReportComponent', () => {
  let component: TableEmployeesReportComponent;
  let fixture: ComponentFixture<TableEmployeesReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TableEmployeesReportComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TableEmployeesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
