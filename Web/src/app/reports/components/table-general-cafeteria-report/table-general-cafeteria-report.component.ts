import { Component, Input, SimpleChanges, ViewChild } from '@angular/core';
import { ReportGeneralCafeteria } from '../../interfaces/report-general-cafeteria';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'reports-table-general-cafeteria-report',
  templateUrl: './table-general-cafeteria-report.component.html',
  styleUrl: './table-general-cafeteria-report.component.css'
})
export class TableGeneralCafeteriaReportComponent {

  @Input() reportGeneralCafeteria : ReportGeneralCafeteria[] = [];
  @Input() reportLoading          : boolean = false;
  @Input() reportLoaded           : boolean = false;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  //@ViewChild(MatSort) sort!: MatSort;

  //@Output()
  //public onEmitPage: EventEmitter<PageEvent> = new EventEmitter();

  columns: string[] = ['Entrada', 'Nombre', 'Planta', 'Compañía'];
  dbColumns: string[] = ['fechaEntrada', 'nombre', 'planta', 'compania'];
  datasource: MatTableDataSource<ReportGeneralCafeteria> = new MatTableDataSource();

  constructor(){}

  ngAfterViewInit(): void {
    this.datasource.paginator = this.paginator;
    //this.datasource.sort = this.sort;
  }

  ngOnChanges(changes: SimpleChanges): void {
   if (changes['reportGeneralCafeteria'] && changes['reportGeneralCafeteria'].currentValue) {
      this.datasource.data = this.reportGeneralCafeteria;
      this.datasource.paginator = this.paginator;
      //this.datasource.sort = this.sort;
    }
  }

  //public changeNumberOfRegisters(pageEvent: PageEvent): void {
  //  this.onEmitPage.emit(pageEvent);
  //}

  public getCellValue(column: string, row: any): string {
    const columnHandlers: { [key: string]: () => string } = {
        'Entrada': () => this.getDateToCustomFormat(row, 'entrada'),
    };
    return columnHandlers[column]?.() || row[this.dbColumns[this.columns.indexOf(column)]];
  }

  public getDateToCustomFormat(row : ReportGeneralCafeteria, entradaSalida : string) : string {

    let date: Date = new Date(row['fechaEntrada']);

    const dia = String(date.getUTCDate()).padStart(2, '0');
    const mes = String(date.getUTCMonth() + 1).padStart(2, '0');
    const anio = date.getUTCFullYear();

    let horas: string | number = date.getUTCHours();
    const minutos: string = String(date.getUTCMinutes()).padStart(2, '0');
    const segundos: string = String(date.getUTCSeconds()).padStart(2, '0');
    const ampm: string = horas >= 12 ? 'p.m.' : 'a.m.';

    horas = horas % 12;
    horas = horas ? String(horas).padStart(2, '0') : '12';

    return `${dia}/${mes}/${anio} ${horas}:${minutos}:${segundos} ${ampm}`;
  }
}
