import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableGeneralCafeteriaReportComponent } from './table-general-cafeteria-report.component';

describe('TableGeneralCafeteriaReportComponent', () => {
  let component: TableGeneralCafeteriaReportComponent;
  let fixture: ComponentFixture<TableGeneralCafeteriaReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TableGeneralCafeteriaReportComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TableGeneralCafeteriaReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
