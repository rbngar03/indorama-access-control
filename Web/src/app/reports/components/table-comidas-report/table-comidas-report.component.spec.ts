import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableComidasReportComponent } from './table-comidas-report.component';

describe('TableComidasReportComponent', () => {
  let component: TableComidasReportComponent;
  let fixture: ComponentFixture<TableComidasReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TableComidasReportComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TableComidasReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
