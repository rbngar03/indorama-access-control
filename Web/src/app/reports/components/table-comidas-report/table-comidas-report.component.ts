import { Component, Input, SimpleChanges, ViewChild, AfterViewInit, OnChanges } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Comedor } from '../../interfaces/comedor.interface';

@Component({
  selector: 'app-table-comidas-report',
  templateUrl: './table-comidas-report.component.html',
  styleUrls: ['./table-comidas-report.component.css']
})
export class TableComidasReportComponent implements OnChanges, AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  @Input() comedorReport: Comedor[] = [];
  filtro: string = '';
  columns: string[] = ['Fecha', 'Sitio', 'Turno', 'Comidas'];
  datasource: MatTableDataSource<Comedor> = new MatTableDataSource();

  public isComedorLoaded: boolean = false;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['comedorReport']) {
      this.datasource.data = this.comedorReport;
      if (this.datasource.paginator) {
        this.datasource.paginator.firstPage();
      }
    }
  }

  ngAfterViewInit(): void {
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
  
    // Configurar el ordenamiento
    this.datasource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'Fecha':
          return new Date(item.fechaRegistro.split('-').reverse().join('-')).getTime(); 
        case 'Sitio':
          return item.descripcion.toLowerCase(); 
        case 'Turno':
          return item.turno;
        case 'Comidas':
          return item.totalRegistros;
        default:
          return '';
      }
    };
  }
  
  

  // Método para convertir la fecha cuando se visualiza
  public convertStringToDate(dateString: string): string {
    const parts = dateString.split('-'); 
    const day = parts[0].padStart(2, '0'); 
    const month = parts[1].padStart(2, '0'); 
    const year = parts[2]; 
    return `${day}/${month}/${year}`; 
  }
  

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.datasource.filter = filterValue.trim().toLowerCase();

    if (this.datasource.paginator) {
      this.datasource.paginator.firstPage();
    }
  }
  
}
