import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableContractorsReportComponent } from './table-contractors-report.component';

describe('TableContractorsReportComponent', () => {
  let component: TableContractorsReportComponent;
  let fixture: ComponentFixture<TableContractorsReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TableContractorsReportComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TableContractorsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
