import { Component, EventEmitter, Input, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
//import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Contractor } from '../../../catalogue/interfaces/contractor.interface';
import { ReportByCompany } from '../../interfaces/report-by-company.interface';

@Component({
  selector: 'reports-table-contractors-report',
  templateUrl: './table-contractors-report.component.html',
  styleUrl: './table-contractors-report.component.css'
})
export class TableContractorsReportComponent {

  @Input() reportByCompany : ReportByCompany[] = [];
  @Input() reportLoading   : boolean = false;
  @Input() reportLoaded    : boolean = false;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  //@ViewChild(MatSort) sort!: MatSort;

  //@Output()
  //public onEmitPage: EventEmitter<PageEvent> = new EventEmitter();

  columns: string[] = ['Entrada', 'Salida', 'Empresa Contratista', 'Nombre Contratista'];
  dbColumns: string[] = ['entrada', 'salida', 'empresaContratista', 'nombreContratista'];
  datasource: MatTableDataSource<ReportByCompany> = new MatTableDataSource();

  constructor(){}

  ngAfterViewInit(): void {
    this.datasource.paginator = this.paginator;
    //this.datasource.sort = this.sort;
  }

  ngOnChanges(changes: SimpleChanges): void {
   if (changes['reportByCompany'] && changes['reportByCompany'].currentValue) {
      this.datasource.data = this.reportByCompany;
      this.datasource.paginator = this.paginator;
      //this.datasource.sort = this.sort;
    }
  }

  //public changeNumberOfRegisters(pageEvent: PageEvent): void {
  //  this.onEmitPage.emit(pageEvent);
  //}

  public getCellValue(column: string, row: any): string {
    const columnHandlers: { [key: string]: () => string } = {
        'Entrada': () => this.getDateToCustomFormat(row, 'entrada'),
        'Salida': () => this.getDateToCustomFormat(row, 'salida'),
    };
    return columnHandlers[column]?.() || row[this.dbColumns[this.columns.indexOf(column)]];
  }

  public getDateToCustomFormat(row : ReportByCompany, entradaSalida : string) : string {

    let date: Date = new Date(entradaSalida === 'entrada' ? row['entrada'] : row['salida']);

    const dia = String(date.getUTCDate()).padStart(2, '0');
    const mes = String(date.getUTCMonth() + 1).padStart(2, '0');
    const anio = date.getUTCFullYear();

    let horas: string | number = date.getUTCHours();
    const minutos: string = String(date.getUTCMinutes()).padStart(2, '0');
    const segundos: string = String(date.getUTCSeconds()).padStart(2, '0');
    const ampm: string = horas >= 12 ? 'p.m.' : 'a.m.';

    horas = horas % 12;
    horas = horas ? String(horas).padStart(2, '0') : '12';

    return `${dia}/${mes}/${anio} ${horas}:${minutos}:${segundos} ${ampm}`;
  }
}
