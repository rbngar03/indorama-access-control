import { Planta } from "../interfaces/planta.interface";

export const PLANTAS_CONSTANTES : Planta[] = [
  { idPlanta:1, clave:'001', descripcion:'QUERÉTARO PET' },
  { idPlanta:2, clave:'002', descripcion:'SANTA FE' },
  { idPlanta:3, clave:'003', descripcion:'QUERÉTARO FT'}
];
