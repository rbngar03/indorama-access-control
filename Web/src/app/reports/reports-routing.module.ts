import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContractorsReportPageComponent } from './pages/contractors-report-page/contractors-report-page.component';
import { GeneralCafeteriaReportPageComponent } from './pages/general-cafeteria-report-page/general-cafeteria-report-page.component';
import { EmployeesReportPageComponent } from './pages/employees-report-page/employees-report-page.component';
import { ComedorReportPageComponent } from './pages/comedor-report-page/comedor-report-page.component';


const routes: Routes = [
    { path: 'contractors', component: ContractorsReportPageComponent },
    { path: 'general-cafeteria', component: GeneralCafeteriaReportPageComponent },
    { path: 'employees', component: EmployeesReportPageComponent },
    { path: '**', redirectTo: 'login' },
    { path: 'no-comidas', component: ComedorReportPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
