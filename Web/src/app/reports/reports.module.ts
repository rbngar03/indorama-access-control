import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContractorsReportPageComponent } from './pages/contractors-report-page/contractors-report-page.component';
import { provideNativeDateAdapter } from '@angular/material/core';
import { ReportsRoutingModule } from './reports-routing.module';
import { MatButton, MatIconButton } from '@angular/material/button';
import { TableContractorsReportComponent } from './components/table-contractors-report/table-contractors-report.component';
import { MatTable, MatTableModule } from '@angular/material/table';
import { MatPaginator, MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { EmployeesReportPageComponent } from './pages/employees-report-page/employees-report-page.component';
import { TableEmployeesReportComponent } from './components/table-employees-report/table-employees-report.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { TableComidasReportComponent } from './components/table-comidas-report/table-comidas-report.component';
import { ComedorReportPageComponent } from './pages/comedor-report-page/comedor-report-page.component';
import { getSpanishPaginatorIntl } from './services/spanish-paginator-intl'
import { TableGeneralCafeteriaReportComponent } from './components/table-general-cafeteria-report/table-general-cafeteria-report.component';
import { GeneralCafeteriaReportPageComponent } from './pages/general-cafeteria-report-page/general-cafeteria-report-page.component';

@NgModule({
  declarations: [
    ContractorsReportPageComponent,
    TableContractorsReportComponent,
    TableGeneralCafeteriaReportComponent,
    GeneralCafeteriaReportPageComponent,
    EmployeesReportPageComponent,
    TableEmployeesReportComponent,
    TableComidasReportComponent,
    ComedorReportPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReportsRoutingModule,
    MatAutocompleteModule,
    MatButton,
    MatIconButton,
    MatInputModule,
    MatFormFieldModule,
    MatPaginator,
    MatTable,
    FormsModule,
    MatButtonModule,
    MatIconButton,
    MatPaginatorModule,
    MatTableModule,
    MatTableModule,
    MatSortModule,
    MatMenuModule,
    MatButtonModule
  ],
  providers: [
    provideNativeDateAdapter(),
    {provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl()}
  ]
})
export class ReportsModule { }
