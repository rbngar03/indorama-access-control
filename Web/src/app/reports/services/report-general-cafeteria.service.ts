import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from '../../shared/services/appConfig.service';
import { Observable } from 'rxjs/internal/Observable';
import { ReportGeneralCafeteria } from '../interfaces/report-general-cafeteria';

@Injectable({
  providedIn: 'root'
})
export class ReportGeneralCafeteriaService {

  //private serviceUrl: string = 'http://192.168.10.20:9090';
  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();
  //private serviceUrl : string = 'https://localhost:44305';

  constructor(
    private http: HttpClient,
    ) { }

  public getAllReportRegisters(fechaInicial : string, fechaFinal : string, tipoPersonal : number, sitio : number): Observable<ReportGeneralCafeteria[]> {
    let filtros : string = ``;
    let filtrosArray: string[] = [`FechaInicial=${fechaInicial}`, `FechaFinal=${fechaFinal}`];

    if (tipoPersonal != 0) filtrosArray.push(`TipoPersonal=${tipoPersonal}`);
    if (sitio != 0) filtrosArray.push(`Planta=${sitio}`);

    filtros = filtrosArray.join('&');
    return this.http.get<ReportGeneralCafeteria[]>(`${this.serviceUrl}/Reportes/ReporteComedorGral?${filtros}`);
  }

  public downloadReportDocument(type: string, fechaInicial : string, fechaFinal : string, tipoPersonal : number, sitio : number): Observable<Blob> {
    let filtros : string = ``;
    let filtrosArray: string[] = [`FechaInicial=${fechaInicial}`, `FechaFinal=${fechaFinal}`];

    if (tipoPersonal != 0) filtrosArray.push(`TipoPersonal=${tipoPersonal}`);
    if (sitio != 0) filtrosArray.push(`Planta=${sitio}`);

    filtros = filtrosArray.join('&');
    return this.http.get(`${this.serviceUrl}/Reportes/ReporteComedorGral/${type}?${filtros}`, {
      responseType: 'blob'
    });
  }
}
