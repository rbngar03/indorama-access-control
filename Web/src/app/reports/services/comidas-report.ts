import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from '../../shared/services/appConfig.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Comedor } from '../interfaces/comedor.interface';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class comidas {

  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();
  //private serviceUrl : string = 'https://localhost:44305';
//private serviceUrl: string = 'http://192.168.10.20:9090';
  comidasReport: Comedor[] = [];

  constructor(
    private http: HttpClient,
    public snackbar: MatSnackBar
    ) { }

    getReport(startDate: string, endDate: string, site: string): Observable<Comedor[]> {
      return this.http.get<Comedor[]>(`${this.serviceUrl}/Reportes/ReporteComidas?StartDate=${startDate}&EndDate=${endDate}&idplanta=${site}`);
    }
    
    getExcelReport(startDate: string, endDate: string, site: string): Observable<Blob> {
      return this.http.get(`${this.serviceUrl}/Reportes/ReporteComidas/excel?StartDate=${startDate}&EndDate=${endDate}&idplanta=${site}`, { responseType: 'blob' });
    }
    
    getPDFReport(startDate: string, endDate: string, site: string): Observable<Blob> {
      return this.http.get(`${this.serviceUrl}/Reportes/ReporteComidas/pdf?StartDate=${startDate}&EndDate=${endDate}&idplanta=${site}`, { responseType: 'blob' });
    }
    
  openSnackBar(message: string, action: string): void {
    this.snackbar.open(message, action, {
      duration: 3000
    })
  }


}
