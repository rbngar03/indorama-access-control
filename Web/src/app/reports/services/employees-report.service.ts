import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppConfigService } from '../../shared/services/appConfig.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EmployeeReport } from '../interfaces/employeeReport.interface';

@Injectable({
  providedIn: 'root'
})
export class EmployeesReportService {

  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();

  employeesReport: EmployeeReport[] = [];

  constructor(
    private http: HttpClient,
    public snackbar: MatSnackBar
    ) { }

  getReport(startDate: string, endDate: string, site: number, keyword: string): void {
    this.http.get<EmployeeReport[]>(`${this.serviceUrl}/Reportes/ReporteEmpleados?StartDate=${startDate}&EndDate=${endDate}&idplanta=${site}&Filtro=${keyword}`)
      .subscribe(
        resp => {
          this.employeesReport = resp;
        }
      )
  }

  getExcelReport(startDate: string, endDate: string, site: number, keyword: string): void {
    this.http.get(`${this.serviceUrl}/Reportes/ReporteEmpleados/excel?StartDate=${startDate}&EndDate=${endDate}&idplanta=${site}&Filtro=${keyword}`, {responseType: 'blob'})
      .subscribe(
        resp => {
          window.open(URL.createObjectURL(resp), '_blank');
        }
      )
  }

  getPDFReport(startDate: string, endDate: string, site: number, keyword: string): void {
    this.http.get(`${this.serviceUrl}/Reportes/ReporteEmpleados/pdf?StartDate=${startDate}&EndDate=${endDate}&idplanta=${site}&Filtro=${keyword}`, {responseType: 'blob'})
      .subscribe(
        resp => {
          window.open(URL.createObjectURL(resp), '_blank');
        }
      )
  }

  openSnackBar(message: string, action: string): void {
    this.snackbar.open(message, action, {
      duration: 3000
    })
  }


}
