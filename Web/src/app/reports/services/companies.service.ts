import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from '../../shared/services/appConfig.service';
import { Observable } from 'rxjs/internal/Observable';
import { Company } from '../interfaces/company.interface';

@Injectable({
  providedIn: 'root'
})
export class CompaniesService {

  //private serviceUrl: string = 'http://192.168.10.20:9090';
  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();
  //private serviceUrl : string = 'https://localhost:44305';

  constructor(
    private http: HttpClient,
    ) { }

  public getAllCompanies(): Observable<Company[]> {
    return this.http.get<Company[]>(`${this.serviceUrl}/Companias`);
  }
}
