import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from '../../shared/services/appConfig.service';
import { Observable } from 'rxjs/internal/Observable';
import { ReportByCompany } from '../interfaces/report-by-company.interface';

@Injectable({
  providedIn: 'root'
})
export class ReportByCompanyService {

  //private serviceUrl: string = 'http://192.168.10.20:9090';
  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();
  //private serviceUrl : string = 'https://localhost:44305';

  constructor(
    private http: HttpClient,
    ) { }

  public getAllReportRegisters(fechaInicial : string, fechaFinal : string, empresa? : string, nombreContratista? : string): Observable<ReportByCompany[]> {
    let filtros : string = ``;
    let filtrosArray: string[] = [`FechaInicial=${fechaInicial}`, `FechaFinal=${fechaFinal}`];

    if (empresa) filtrosArray.push(`Empresa=${empresa}`);
    if (nombreContratista && nombreContratista.length > 0) filtrosArray.push(`Filtro=${nombreContratista}`);

    filtros = filtrosArray.join('&');
    return this.http.get<ReportByCompany[]>(`${this.serviceUrl}/Reportes/ReporteContratistas?${filtros}`);
  }

  public downloadReportDocument(type : string, fechaInicial: string, fechaFinal: string, empresa?: string, nombreContratista? : string): Observable<Blob> {
    let filtros : string = ``;
    let filtrosArray: string[] = [`FechaInicial=${fechaInicial}`, `FechaFinal=${fechaFinal}`];

    if (empresa) filtrosArray.push(`Empresa=${empresa}`);
    if (nombreContratista && nombreContratista.length > 0) filtrosArray.push(`Filtro=${nombreContratista}`);

    filtros = filtrosArray.join('&');
    return this.http.get(`${this.serviceUrl}/Reportes/ReporteContratistas/${type}?${filtros}`, {
      responseType: 'blob'
    });
  }
}
