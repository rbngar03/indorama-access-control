export interface ReportByCompany {
  entrada            : Date,
  salida             : Date,
  empresaContratista : string,
  nombreContratista  : string
}
