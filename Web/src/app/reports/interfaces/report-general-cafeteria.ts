
export interface ReportGeneralCafeteria {
  fechaEntrada : Date,
  nombre       : string,
  planta       : string,
  compania     : string
}
