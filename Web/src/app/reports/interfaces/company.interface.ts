export interface Company {
  idCompanias        : number,
  compania           : string,
  descripcion        : string,
  fechaActualizacion : Date,
  contratista        : null,
  marcajes           : null
}
