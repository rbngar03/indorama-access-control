export interface EmployeeReport {
  entrada:        Date;
  salida:         Date;
  nombreEmpleado: string;
  numeroEmpleado: string;
  planta:         number;
}
