export interface Planta {
  idPlanta:    number;
  clave:       string;
  descripcion: string;
}
