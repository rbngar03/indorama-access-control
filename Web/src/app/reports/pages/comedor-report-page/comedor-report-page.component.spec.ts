import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComedorReportPageComponent } from './comedor-report-page.component';

describe('ComedorReportPageComponent', () => {
  let component: ComedorReportPageComponent;
  let fixture: ComponentFixture<ComedorReportPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ComedorReportPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ComedorReportPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
