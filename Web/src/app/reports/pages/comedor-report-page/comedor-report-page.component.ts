import { Component, Input, ElementRef, ViewChild, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { comidas } from '../../services/comidas-report';
import { Comedor } from '../../interfaces/comedor.interface';

@Component({
  selector: 'app-comedor-report-page',
  templateUrl: './comedor-report-page.component.html',
  styleUrls: ['./comedor-report-page.component.css']
})
export class ComedorReportPageComponent implements OnInit{
  @ViewChild('selectSite') selectSite!: ElementRef<HTMLSelectElement>;
  @ViewChild(comidas) comidasReportComponent!: comidas; // Inyectar el componente

  @Input() startDate: string = '';
  @Input() endDate: string = '';
  @Input() site: string = 'Todas';

  isTableEmpty: boolean = true;
  loading: boolean = false;

  constructor(
    public comidasReportService: comidas,
    public snackbar: MatSnackBar
  ) {
  }


  ngOnInit(): void {
    this.cleanInputs();
  }

  searchComidaT(valueSite: string): void {
    if (this.startDate === '' || this.endDate === '' ) {
      this.openSnackBar("Los criterios de búsqueda son requeridos", "Aceptar");
    } else {
      const start = new Date(this.startDate);
      const end = new Date(this.endDate);

      if (start > end) {
        this.openSnackBar("La fecha inicial debe ser menor a la fecha final", "Aceptar");
        return;
      }

      this.loading = true; // Activar el loading

      this.comidasReportService.getReport(this.startDate, this.endDate, this.site).subscribe(
        (response) => {
          this.comidasReportService.comidasReport = response; 
          this.loading = false; // Desactivar el loading

          if (this.comidasReportService.comidasReport.length === 0) {
            this.isTableEmpty = true; 
            this.openSnackBar("No se encontraron resultados", "Aceptar");
          } else {
            this.isTableEmpty = false; 
          }
        },
        (error) => {
          this.loading = false; // Desactivar el loading en caso de error
          this.openSnackBar("Ocurrió un error al obtener los datos", "Aceptar");
        }
      );
    }
  }

  downloadExcel(): void {
    if (this.startDate === '' || this.endDate === '') {
      this.openSnackBar("Los criterios de búsqueda son requeridos", "Aceptar");
    } else {
      this.loading = true; // Activar el loading
      this.comidasReportService.getExcelReport(this.startDate, this.endDate, this.site).subscribe({
        next: (data) => {
          const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
          const url = URL.createObjectURL(blob);
          
          const a = document.createElement('a');
          a.href = url;
          a.download = 'reporte_comidas_por_turno.xlsx'; 
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a); 
          URL.revokeObjectURL(url);
          this.loading = false; // Desactivar el loading
        },
        error: (err) => {
          this.loading = false; // Desactivar el loading en caso de error
          this.openSnackBar("Error al descargar el archivo", "Aceptar");
        }
      });
    }
  }
  
  downloadPDF(): void {
    if (this.startDate === '' || this.endDate === '') {
      this.openSnackBar("Los criterios de búsqueda son requeridos", "Aceptar");
    } else {
      this.loading = true; // Activar el loading
      this.comidasReportService.getPDFReport(this.startDate, this.endDate, this.site).subscribe({
        next: (data) => {
          const blob = new Blob([data], { type: 'application/pdf' });
          const url = URL.createObjectURL(blob);
          
          const a = document.createElement('a');
          a.href = url;
          a.download = 'reporte_comidas_por_turno.pdf';
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a); 
          URL.revokeObjectURL(url);
          this.loading = false; // Desactivar el loading
        },
        error: (err) => {
          this.loading = false; // Desactivar el loading en caso de error
          this.openSnackBar("Error al descargar el archivo", "Aceptar");
        }
      });
    }
  }
  

  get comidasReport(): Comedor[] {
    return this.comidasReportService.comidasReport;
  }

  disblesearch(){
    if (this.startDate === '' || this.endDate === ''){
      return true;
      
    }else{
      return false
    }
  }
  cleanInputs(): void {
    this.startDate = '';
    this.endDate = '';
    this.comidasReportService.comidasReport = [];
    this.isTableEmpty = true;
    this.site = 'Todas';
  }

  openSnackBar(message: string, action: string): void {
    this.snackbar.open(message, action, {
      duration: 3000
    });
  }
}
