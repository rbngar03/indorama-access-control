import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorsReportPageComponent } from './contractors-report-page.component';

describe('ContractorsReportPageComponent', () => {
  let component: ContractorsReportPageComponent;
  let fixture: ComponentFixture<ContractorsReportPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ContractorsReportPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ContractorsReportPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
