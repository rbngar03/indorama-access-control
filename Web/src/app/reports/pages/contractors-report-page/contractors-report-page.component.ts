import { Component, OnInit } from '@angular/core';
import { CompaniesService } from '../../services/companies.service';
import { Company } from '../../interfaces/company.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { ReportByCompany } from '../../interfaces/report-by-company.interface';
import { NgForm } from '@angular/forms';
import { ReportByCompanyService } from '../../services/report-by-company.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'reports-contractors-report-page',
  templateUrl: './contractors-report-page.component.html',
  styleUrl: './contractors-report-page.component.css'
})
export class ContractorsReportPageComponent implements OnInit {

  public companies         : Company[] = [];
  public filteredCompanies : Company[] = [];
  public searchTerm        : string = '';
  public reportByCompany   : ReportByCompany[] = [];
  public reportLoading     : boolean = false;
  public reportLoaded      : boolean = false;

  public fechaInicial      : string = '';
  public fechaFinal        : string = '';
  public empresa           : string = '';
  public nombreContratista : string = '';

  constructor( private companiesService : CompaniesService,
               private reportByCompanyService : ReportByCompanyService,
               private snackBar: MatSnackBar ) {}

  ngOnInit() : void {
    this.getCompanies();
  }

  public onInputChange(event: any) {
    const searchTarget = event.target as HTMLInputElement;
    this.searchTerm = searchTarget.value;

    if (this.searchTerm.length > 0) {
      this.filteredCompanies = this.companies.filter(company =>
        company.descripcion.toLowerCase().includes(this.searchTerm.toLowerCase())
      );
    } else {
      this.filteredCompanies = this.companies.slice(0, 30);
    }
  }

  public formatDateToDDMMYYYY(dateString : string) : string {
    const date = new Date(dateString);

    const dia = String(date.getUTCDate()).padStart(2, '0');
    const mes = String(date.getUTCMonth() + 1).padStart(2, '0');
    const anio = date.getUTCFullYear();

    return `${dia}/${mes}/${anio}`;
  }

  public onSubmit(contractorReportForm : NgForm) : void{
    this.reportLoading = true;
    this.reportByCompany = [];
    if(contractorReportForm.valid){
      this.getReportByCompany();
    }
  }

  public limpiarFormulario(form: NgForm) : void{
    form.resetForm();
    this.filteredCompanies = this.companies.slice(0,30);
    this.reportLoaded = false;
    this.reportByCompany = [];
  }

  public getReportByCompany() : void {
    this.reportByCompanyService.getAllReportRegisters(
      this.formatDateToDDMMYYYY(this.fechaInicial),
      this.formatDateToDDMMYYYY(this.fechaFinal),
      this.empresa,
      this.nombreContratista
    )
    .subscribe({
      next: (response: any) => {
        this.reportByCompany = response;
        this.reportLoaded = true;
        this.reportLoading = false;
      },
      error: (error: HttpErrorResponse) => {
        console.error('Error al obtener el reporte: ', error);
        this.reportByCompany = [];
        this.reportLoaded = false;
        this.reportLoading = false;
      }
    });
  }

  public downloadDocument(type : string) : void {
    this.snackBar.open('Iniciando descarga del documento...', 'Cerrar', { duration: 3000 });

    this.reportByCompanyService.downloadReportDocument(
        type,
        this.formatDateToDDMMYYYY(this.fechaInicial),
        this.formatDateToDDMMYYYY(this.fechaFinal),
        this.empresa,
        this.nombreContratista
      )
      .subscribe({
        next: (response : Blob) => {
          const url = window.URL.createObjectURL(response);
          const a = document.createElement('a');

          a.href = url;
          a.download = `ReporteContratistas.${type != 'pdf' ? 'xlsx' : 'pdf'}`;
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
          window.URL.revokeObjectURL(url);

          this.snackBar.open('Descarga finalizada', 'Cerrar', { duration: 3000 });
        },
      error : (error: HttpErrorResponse) => {
        console.error(`Error al descargar el archivo (${type})`, error);
        this.snackBar.open('Error al descargar el documento', 'Cerrar', { duration: 5000 });
      }
    });
  }

  public getCompanies(){
    this.companiesService.getAllCompanies()
    .subscribe({
      next: (response: any) => {
        this.companies = response;
        this.filteredCompanies = this.companies.slice(0, 30);
      },
      error: (error: HttpErrorResponse) => {
        console.error('Error al obtener las compañías: ', error);
        this.companies = [];
      }
    });
  }
}
