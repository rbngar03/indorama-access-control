import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralCafeteriaReportPageComponent } from './general-cafeteria-report-page.component';

describe('GeneralCafeteriaReportPageComponent', () => {
  let component: GeneralCafeteriaReportPageComponent;
  let fixture: ComponentFixture<GeneralCafeteriaReportPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GeneralCafeteriaReportPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(GeneralCafeteriaReportPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
