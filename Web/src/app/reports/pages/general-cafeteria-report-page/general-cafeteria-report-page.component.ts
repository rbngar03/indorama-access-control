import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Planta } from '../../interfaces/planta.interface';
import { PLANTAS_CONSTANTES } from '../../data/plantas.constants';
import { ReportGeneralCafeteria } from '../../interfaces/report-general-cafeteria';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ReportGeneralCafeteriaService } from '../../services/report-general-cafeteria.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'reports-general-cafeteria-report-page',
  templateUrl: './general-cafeteria-report-page.component.html',
  styleUrl: './general-cafeteria-report-page.component.css'
})
export class GeneralCafeteriaReportPageComponent {

  public fechaInicial         : string = '';
  public fechaFinal           : string = '';
  public selectedSitio        : number = 0;
  public selectedTipoPersonal : number = 0;

  public reportGeneralCafeteria : ReportGeneralCafeteria[] = [];
  public reportLoading          : boolean = false;
  public reportLoaded           : boolean = false;

  public plantas : Planta[] = [];

  constructor( private reportGeneralCafeteriaService : ReportGeneralCafeteriaService,
               private snackBar: MatSnackBar ) {}

  ngOnInit() : void {
    this.plantas = PLANTAS_CONSTANTES;
  }

  public onSubmit(generalCafeteriaReportForm : NgForm) : void{
    this.reportLoading = true;
    this.reportGeneralCafeteria = [];
    if(generalCafeteriaReportForm.valid){
      this.getReportGeneralCafeteria();
    }
  }

  public limpiarFormulario(form: NgForm) : void{
    form.resetForm({
      fechaInicial: '',
      fechaFinal: '',
      tipoPersonalSelect: 0,
      sitioSelect: 0
    });
    this.reportLoaded = false;
    this.reportGeneralCafeteria = [];
  }

  public getReportGeneralCafeteria() : void {
    this.reportGeneralCafeteriaService.getAllReportRegisters(
      this.formatDateToDDMMYYYY(this.fechaInicial),
      this.formatDateToDDMMYYYY(this.fechaFinal),
      this.selectedTipoPersonal,
      this.selectedSitio
    )
    .subscribe({
      next: (response: any) => {
        this.reportGeneralCafeteria = response;
        console.log(this.reportGeneralCafeteria);
        this.reportLoaded = true;
        this.reportLoading = false;
      },
      error: (error: HttpErrorResponse) => {
        console.error('Error al obtener el reporte: ', error);
        this.reportGeneralCafeteria = [];
        this.reportLoaded = false;
        this.reportLoading = false;
      }
    });
  }

  public downloadDocument(type : string) : void {
    this.snackBar.open('Iniciando descarga del documento...', 'Cerrar', { duration: 3000 });

    this.reportGeneralCafeteriaService.downloadReportDocument(
        type,
        this.formatDateToDDMMYYYY(this.fechaInicial),
        this.formatDateToDDMMYYYY(this.fechaFinal),
        this.selectedTipoPersonal,
        this.selectedSitio
      )
      .subscribe({
        next: (response : Blob) => {
          const url = window.URL.createObjectURL(response);
          const a = document.createElement('a');

          a.href = url;
          a.download = `ReporteContratistas.${type != 'pdf' ? 'xlsx' : 'pdf'}`;
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
          window.URL.revokeObjectURL(url);

          this.snackBar.open('Descarga finalizada', 'Cerrar', { duration: 3000 });
        },
      error : (error: HttpErrorResponse) => {
        console.error(`Error al descargar el archivo (${type})`, error);
        this.snackBar.open('Error al descargar el documento', 'Cerrar', { duration: 5000 });
      }
    });
  }

  public formatDateToDDMMYYYY(dateString : string) : string {
    const date = new Date(dateString);

    const dia = String(date.getUTCDate()).padStart(2, '0');
    const mes = String(date.getUTCMonth() + 1).padStart(2, '0');
    const anio = date.getUTCFullYear();

    return `${dia}/${mes}/${anio}`;
  }
}
