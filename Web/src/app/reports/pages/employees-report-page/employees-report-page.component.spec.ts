import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesReportPageComponent } from './employees-report-page.component';

describe('EmployeesReportPageComponent', () => {
  let component: EmployeesReportPageComponent;
  let fixture: ComponentFixture<EmployeesReportPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmployeesReportPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(EmployeesReportPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
