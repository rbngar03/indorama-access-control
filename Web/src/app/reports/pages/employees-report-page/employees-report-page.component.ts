import { Component, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EmployeesReportService } from '../../services/employees-report.service';
import { EmployeeReport } from '../../interfaces/employeeReport.interface';

@Component({
  selector: 'reports-employees-report-page',
  templateUrl: './employees-report-page.component.html',
  styleUrl: './employees-report-page.component.css'
})
export class EmployeesReportPageComponent {

  @Input()
  startDate: string = '';

  @Input()
  endDate:   string = '';

  @Input()
  site:      number = 0;

  @Input()
  keyword:   string = '';

  constructor(
    public employeesReportService: EmployeesReportService,
    public snackbar: MatSnackBar){}

  searchEmployees(valueSite: string): void {
    this.site = Number(valueSite);
    if (this.startDate == '' || this.endDate == '')
      this.openSnackBar("Las fechas de inicio y fin son requeridas", "Aceptar");
    else
      this.employeesReportService.getReport(this.startDate, this.endDate, this.site, this.keyword);
  }

  downloadExcel(valueSite: string): void {
    this.site = Number(valueSite);
    if (this.startDate == '' || this.endDate == '')
      this.openSnackBar("Las fechas de inicio y fin son requeridas", "Aceptar");
    else
      this.employeesReportService.getExcelReport(this.startDate, this.endDate, this.site, this.keyword);
  }

  downloadPDF(valueSite: string): void {
    this.site = Number(valueSite);
    if (this.startDate == '' || this.endDate == '')
      this.openSnackBar("Las fechas de inicio y fin son requeridas", "Aceptar");
    else
      this.employeesReportService.getPDFReport(this.startDate, this.endDate, this.site, this.keyword);
  }

  get employeesReport(): EmployeeReport[] {
    return this.employeesReportService.employeesReport;
  };

  cleanInputs(): void {
    this.startDate = '';
    this.endDate   = '';
    this.site      = 0;
    this.keyword   = '';
  }

  openSnackBar(message: string, action: string): void {
    this.snackbar.open(message, action, {
      duration: 3000
    })
  }
}
