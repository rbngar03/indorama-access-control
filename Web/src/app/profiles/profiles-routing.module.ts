import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainProfilesComponent } from './pages/main-profiles/main-profiles.component';
const routes: Routes = [
  { path: 'main', component: MainProfilesComponent },
  { path: '**', redirectTo: 'main' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfilesRoutingModule { }
