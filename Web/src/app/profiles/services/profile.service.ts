import { GenericHttpResponse } from './../../shared/interfaces/generic-http-response';
import { Permiso } from './../interfaces/permiso';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Accion } from '../interfaces/accion';
import { HttpClient } from '@angular/common/http';
import { Modulo } from '../interfaces/modulo';
import { Perfil } from '../interfaces/perfil';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppConfigService } from '../../shared/services/appConfig.service';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {

  private serviceUrl: string = AppConfigService.settings!.apiUrl.toString();

  constructor(private http: HttpClient, public snackBar: MatSnackBar) {}

  getAcciones(): Observable<Accion[]> {
    return this.http.get<Accion[]>(`${this.serviceUrl}/Permisos/Acciones`);
  }

  getModulos(): Observable<Modulo[]> {
    return this.http.get<Modulo[]>(`${this.serviceUrl}/Permisos/Modulos`);
  }

  getPerfiles(): Observable<Perfil[]> {
    return this.http.get<Perfil[]>(`${this.serviceUrl}/Permisos/Perfiles`);
  }

  getPerfilesPermisos(id: number): Observable<GenericHttpResponse<Permiso>> {
    return this.http.get<GenericHttpResponse<Permiso>>(
      `${this.serviceUrl}/Permisos/PerfilesModulos/${id}`
    );
  }

  postPerfil(perfil: Perfil) {
    return this.http.post<GenericHttpResponse<Perfil>>(`${this.serviceUrl}/Permisos/Perfiles`,perfil);
  }

  postPermisos(modulosAcciones: Permiso) {
    return this.http.post<GenericHttpResponse<Permiso>>(
      `${this.serviceUrl}/Permisos/PerfilesModulos/`,modulosAcciones
    );
  }

  deletePerfil(id: number) {
    return this.http.delete<GenericHttpResponse<Permiso>>(
      `https://localhost:44305/Permisos/Perfiles/${id}`
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: Infinity,
    });
  }
}
