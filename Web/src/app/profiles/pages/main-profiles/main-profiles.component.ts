import { ConfirmacionDialogComponent } from './../../components/confirmacion-dialog/confirmacion-dialog.component';
import { Component, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Perfil } from '../../interfaces/perfil';
import { ProfileService } from '../../services/profile.service';
import { MatDialog } from '@angular/material/dialog';
import { AddPerfilDialogComponent } from '../add-perfil-dialog/add-perfil-dialog.component';
import { HttpErrorResponse } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { GenericHttpResponse } from '../../../shared/interfaces/generic-http-response';
import { Permiso } from '../../interfaces/permiso';
import { ModulosAccionesComponent } from '../modulos-acciones/modulos-acciones.component';

@Component({
  selector: 'app-main-profiles',
  templateUrl: './main-profiles.component.html',
  styleUrl: './main-profiles.component.css',
})
export class MainProfilesComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  columns: string[] = ['clave', 'descripcion','acciones'];
  datasource: MatTableDataSource<Perfil> = new MatTableDataSource();
  
  constructor(private profileService: ProfileService, private dialog: MatDialog) {  }
  
  ngOnInit(): void {
    this.getPerfiles();
  }
  
  ngAfterViewInit() {
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
  }
  
  getPerfiles(){
    this.profileService.getPerfiles().subscribe({
      next:(v)=>{ this.datasource.data = v }
    });
  }

  addPerfil(){
    const dialogRef = this.dialog.open(
      AddPerfilDialogComponent, {
        width: '35%', 
      });
    dialogRef.afterClosed().subscribe( ()=> {
      this.getPerfiles();
    });
  }

  deletePerfil(perfil: Perfil){
    const dialogRef = this.dialog.open(
      ConfirmacionDialogComponent, {
        width: '25%',
        data: {
          perfil,
        }
      });
    dialogRef.afterClosed().subscribe(
      (resp: boolean | number) => {
        resp == 1 ? this.getPerfiles():
        resp? this.deleteRequest(perfil.idPerfil) :
        this.profileService.openSnackBar("Operación cancelada","Aceptar");
    });
    
  }

  deleteRequest(id: number){
    this.profileService.deletePerfil(id).subscribe(
      {
        next: (v) => {
          this.getPerfiles();
          this.profileService.openSnackBar(v.message,"Aceptar");
        },
        error: (e: HttpErrorResponse) => this.profileService.openSnackBar(e.error,"Aceptar")
    });
  }

  getPermisos(perfil: Perfil){
    this.profileService.getPerfilesPermisos(perfil.idPerfil).subscribe((response)=>{
      this.openDialog(response, perfil.descripcion)
    });
  }

  openDialog(response: GenericHttpResponse<Permiso>, perfil: string) {
    const dialogRef = this.dialog.open(
      ModulosAccionesComponent, {
        width: '50%',
        data: {
          response,
          perfil
        }
      });
    dialogRef.afterClosed().subscribe(
      () => {
        this.profileService.getPerfiles().subscribe();
    });
  }

}