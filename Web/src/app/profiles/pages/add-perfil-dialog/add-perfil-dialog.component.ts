import { Perfil } from './../../interfaces/perfil';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProfileService } from '../../services/profile.service';
import { MatDialogRef } from '@angular/material/dialog';
import { GenericHttpResponse } from '../../../shared/interfaces/generic-http-response';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-add-perfil-dialog',
  templateUrl: './add-perfil-dialog.component.html',
  styleUrl: './add-perfil-dialog.component.css',
})
export class AddPerfilDialogComponent {
  profileForm = new FormGroup({
    idPerfil: new FormControl( 0 ),
    clave: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9]*$'),
    ]),
    descripcion: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9ÁÉÍÓÚáéíóúÑñ_]*$'),
    ]),
  });

  constructor(
    private profileService: ProfileService,
    private dialogRef: MatDialogRef<AddPerfilDialogComponent>
  ) {}

  onSubmit() {
    this.profileForm.controls.descripcion.markAsTouched();
    if (this.profileForm.valid)
      this.profileService
        .postPerfil(this.profileForm.value as Perfil)
        .subscribe({
          next: (v) => {
            this.profileService.openSnackBar(v.message, 'Aceptar');
            this.dialogRef.close();
          },
          error: (e: HttpErrorResponse) => {
            this.profileService.openSnackBar(e.error, 'Aceptar');
          },
        });
  }

  getControlError(control: FormControl){
    return control?.hasError('required')? 'Campo obligatorio' : 'El campo no puede contener caracteres especiales';
  }

}
