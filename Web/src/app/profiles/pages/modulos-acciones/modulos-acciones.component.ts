import { Component, ElementRef, inject, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ProfileService } from '../../services/profile.service';
import { Accione, ModuloAccion, Permiso } from '../../interfaces/permiso';
import { GenericHttpResponse } from '../../../shared/interfaces/generic-http-response';

@Component({
  selector: 'app-modulos-acciones',
  templateUrl: './modulos-acciones.component.html',
  styleUrl: './modulos-acciones.component.css',
})
export class ModulosAccionesComponent {
  modulos: ModuloAccion[] = [];
  counter: number = 0;
  allComplete: boolean = false;
  editablefields: boolean = false;

  constructor(public service: ProfileService,  @Inject(MAT_DIALOG_DATA) public data: Data,
  public dialogRef: MatDialogRef<ModulosAccionesComponent> ) {
    this.modulos = data.response.values.moduloAccion;
    this.updateAllComplete();
  }

  updateAllComplete() {
    let count = 0;
    this.modulos.forEach(x =>{ 
      if(x.acciones.find((accion) => accion.nombre == "EDITAR"))
        this.editablefields = x.acciones.find((accion) => accion.nombre == "EDITAR")!.isActivated;
      if (x.acciones.every(y => y.isActivated))
        count ++;
    });
    this.allComplete = count == this.modulos.length;
  }

  someComplete(): boolean {
    let isSomeChequed = false;
    if (this.modulos[0].acciones == null) 
      return false;
    this.modulos.forEach((modulo) => {
      if ( modulo.acciones.filter((t) => t.isActivated).length > 0 && !this.allComplete )
        isSomeChequed = true;
    });
    return isSomeChequed;
  }

  setAll(completed: boolean) {
    this.allComplete = completed;
    if (this.modulos[0].acciones == null) 
      return;
    this.modulos.forEach((t) =>
      t.acciones.forEach((x) => (x.isActivated = completed))
    );
  }

  updatePermisos(){
    let modulos: Permiso = {
      idPerfil: this.data.response.values.idPerfil,
      moduloAccion: this.modulos
    }
    this.service.postPermisos(modulos).subscribe((resp)=>this.service.openSnackBar(resp.message,"Aceptar"));
    this.dialogRef.close();
  }

  findAccion(accion: Accione) {
    return accion.nombre === "EDITAR";
  }
}

interface Data {
  response: GenericHttpResponse<Permiso>;
  perfil: string;
}
