import { Perfil } from './../../interfaces/perfil';
import { Component, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ModulosAccionesComponent } from '../../pages/modulos-acciones/modulos-acciones.component';
import { ProfileService } from '../../services/profile.service';
import { GenericHttpResponse } from '../../../shared/interfaces/generic-http-response';
import { Permiso } from '../../interfaces/permiso';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrl: './table.component.css'
})
export class TableComponent implements OnChanges{

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @Input() filtro?: string;
  @Input() columns?: string[];
  @Input() datasource! : MatTableDataSource<any>;

  constructor( private dialog: MatDialog, public service : ProfileService) {  }

  ngOnChanges(changes: SimpleChanges): void {
    this.datasource.filter = this.filtro!.trim().toLowerCase();
    if (this.datasource.paginator) {
      this.datasource.paginator.firstPage();
    }
  }

  ngAfterViewInit() {
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
  }

  deletePerfil(perfil: Perfil){
    this.service.deletePerfil(perfil.idPerfil).subscribe(
      {
        next: (v) => this.service.openSnackBar(v.message,"Aceptar"),
        error: (e: HttpErrorResponse) => this.service.openSnackBar(e.error,"Aceptar"),
        complete: () => console.info('complete')
    });
  }

  getPermisos(perfil: Perfil){
    this.service.getPerfilesPermisos(perfil.idPerfil).subscribe((response)=>{
      this.openDialog(response, perfil.descripcion)
    });
  }
  openDialog(response: GenericHttpResponse<Permiso>, perfil: string) {
    
    const dialogRef = this.dialog.open(
      ModulosAccionesComponent, {
        width: '50%',
        data: {
          response,
          perfil
        }
      });

    dialogRef.afterClosed().subscribe(
      () => {
        this.service.getPerfiles().subscribe();
    });
  }
  
}
