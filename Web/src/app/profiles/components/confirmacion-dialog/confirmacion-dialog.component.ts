import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Perfil } from '../../interfaces/perfil';
import { HttpErrorResponse } from '@angular/common/http';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'app-confirmacion-dialog',
  templateUrl: './confirmacion-dialog.component.html',
  styleUrl: './confirmacion-dialog.component.css'
})
export class ConfirmacionDialogComponent {
  isAccepted: boolean = false;
  constructor(public dialogRef: MatDialogRef<ConfirmacionDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data:any, private profileService: ProfileService, 
   ){ }


  deleteRequest(/*id: number*/){
    this.profileService.deletePerfil(this.data.perfil.idPerfil).subscribe(
      {
        next: (v) => {
          this.profileService.openSnackBar(v.message,"Aceptar");
          this.dialogRef.close(1);
        },
        error: (e: HttpErrorResponse) => this.profileService.openSnackBar(e.error,"Aceptar")
    });
  }

  
}
