
export interface CamposEditables {
    id:          number;
    nombreCampo: string;
    isActivated: boolean;
}