import { CamposEditables } from './camposEditables';
export interface Permiso {
    idPerfil:     number;
    moduloAccion: ModuloAccion[];
}

export interface ModuloAccion {
    idModulo: number;
    nombre:   string;
    acciones: Accione[];
    camposEditables: CamposEditables[];
}

export interface Accione {
    idAccion:    number;
    nombre:      string;
    isActivated: boolean;
}