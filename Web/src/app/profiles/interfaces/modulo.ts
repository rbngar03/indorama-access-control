export interface Modulo {
    idPerfil:           number;
    idModulo:           number;
    idAccion:           number;
    idAccionNavigation: null;
    idModuloNavigation: null;
    idPerfilNavigation: null;
}

