export interface Perfil {
    idPerfil:    number;
    clave:       string;
    descripcion: string;
}