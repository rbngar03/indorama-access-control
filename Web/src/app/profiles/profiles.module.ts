import { NgModule } from '@angular/core';
import { CommonModule, JsonPipe } from '@angular/common';
import { MainProfilesComponent } from './pages/main-profiles/main-profiles.component';
import { TableComponent } from './components/table/table.component';
import { ProfilesRoutingModule } from './profiles-routing.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltip } from '@angular/material/tooltip';
import { ModulosAccionesComponent } from './pages/modulos-acciones/modulos-acciones.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDialogModule} from '@angular/material/dialog';
import { AddPerfilDialogComponent } from './pages/add-perfil-dialog/add-perfil-dialog.component';
import {MatCardModule} from '@angular/material/card';
import { getSpanishPaginatorIntl } from './services/spanish-paginator-intl';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { ConfirmacionDialogComponent } from './components/confirmacion-dialog/confirmacion-dialog.component';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

@NgModule({
  declarations: [
    MainProfilesComponent,
    ModulosAccionesComponent,
    TableComponent,
    AddPerfilDialogComponent,
    ConfirmacionDialogComponent
  ],
  providers:[
    { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() }],
  imports: [
    CommonModule,
    ProfilesRoutingModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatInputModule, 
    MatTableModule, 
    MatSortModule, 
    MatPaginatorModule,
    MatDatepickerModule,
    FormsModule,
    ReactiveFormsModule,
    JsonPipe,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatTooltip,
    MatCheckboxModule,
    MatDialogModule,
    MatCardModule,
    MatSnackBarModule,
    MatChipsModule,
    MatAutocompleteModule
  ]
})
export class ProfilesModule { }
