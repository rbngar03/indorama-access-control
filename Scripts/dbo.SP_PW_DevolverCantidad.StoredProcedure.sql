USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_DevolverCantidad]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Héctor Romero Badillo
-- ALTER date: 01/11/2010 12:59
-- Description:	Devolver cantidad de entradas de contratistas
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_DevolverCantidad] 
@p_idClasificacion int,
@p_fecha nvarchar(10), 
@p_cantidad int output	
AS
declare @v_fecha datetime
declare @v_fechaComp varchar(10)
BEGIN
	

set @v_fecha = Convert(datetime ,@p_fecha,103)
set @v_fechaComp = CONVERT(varchar(10),@v_fecha, 103)
set @p_cantidad =  
		(select count(M.idMarcajes) from Marcajes as M where 
			CONVERT(varchar(10),M.fecharegistro, 103)=@v_fechaComp
			and M.entradasalida='E' and M.accesoOtorgado=1 and M.idClasificacion=@p_idClasificacion)

END

GO
