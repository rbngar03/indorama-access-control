USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[spGetMarcajesCont]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetMarcajesCont]
	-- Add the parameters for the stored procedure here
	@FechaIni datetime,
	@Fechafin datetime,
	@idcont int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select m.Fecharegistro,e.idContratistas, 'Torniquete' as Lector from
Marcajes m
INNER JOIN Contratistas e on m.IdContratistas = e.IdContratistas
INNER JOIN Terminales t on m.idTerminales = t.IdTerminales
WHERE
m.AccesoOtorgado = 1 
and 
m.fecharegistro between @FechaIni and @FechaFin 
and 
m.IdTipoPersonal = 2 
and m.IdContratistas = @idcont
and m.entradaSalida in ('E','S')
END
GO
