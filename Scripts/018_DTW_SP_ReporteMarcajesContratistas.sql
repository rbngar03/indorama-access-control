IF EXISTS (	SELECT 1
			FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_ReporteMarcajesContratistas' )
  BEGIN
	DROP PROCEDURE DTW_SP_ReporteMarcajesContratistas
  END
GO
-- =============================================
-- Author:		Alejandra Gpe. Marín Ascencio
-- Create date: 03/10/24
-- Description:	Script que obtiene los marcajes para generar reporte por contratista
-- History:
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_ReporteMarcajesContratistas]
@pFechaInicial 		VARCHAR (20),
@pFechaFinal 		VARCHAR (20),
@pEmpresa 			VARCHAR (150) = NULL,
@pNombreContratista VARCHAR (150) = NULL
AS
BEGIN
	DECLARE @vFechaInicial		DATETIME,
			@vFechaFinal		DATETIME,
			@vEmpresa			VARCHAR(150),
			@vNombreContratista VARCHAR(150)
			
	BEGIN TRY 
		SET @vFechaInicial		= CONVERT(DATETIME, @pFechaInicial, 103)
		SET @vFechaFinal		= CONVERT(DATETIME, @pFechaFinal, 103)
		SET @vEmpresa			= UPPER(TRANSLATE(@pEmpresa, 'ÁÉÍÓÚÑ', 'AEIOUN'))
		SET @vNombreContratista = UPPER(TRANSLATE(@pNombreContratista, 'ÁÉÍÓÚÑ', 'AEIOUN'))

		SELECT T1.FechaRegistro  AS FechaEntrada,
			   T2.FechaRegistro  AS FechaSalida,
			   UPPER(T3.NombreCompañia) AS EmpresaContratista,
			   UPPER(T2.NombrePersonal) AS NombreContratista
		FROM Marcajes T1
		LEFT JOIN Marcajes T2
			ON T1.idMarcajes = T2.idMarcajesEntrada
		INNER JOIN Contratistas T3
			ON T1.idContratistas = T3.idContratistas
		WHERE T1.idContratistas IS NOT NULL
			AND T1.EntradaSalida != 'C' AND T1.EntradaSalida != 'S'
			AND (CAST(T1.FechaRegistro AS DATE) >= @vFechaInicial AND CAST(T1.FechaRegistro AS DATE) <= @vFechaFinal)
			AND (CAST(T2.FechaRegistro AS DATE) >= @vFechaInicial AND CAST(T2.FechaRegistro AS DATE) <= @vFechaFinal)
			AND (@pEmpresa IS NULL OR (@vEmpresa LIKE CONCAT('%', UPPER(TRANSLATE(T3.NombreCompañia, 'ÁÉÍÓÚÑ', 'AEIOUN')), '%')))
			AND (@pNombreContratista IS NULL OR ( UPPER(TRANSLATE(T2.NombrePersonal, 'ÁÉÍÓÚÑ', 'AEIOUN')) LIKE CONCAT('%', @vNombreContratista, '%')))
		ORDER BY T1.FechaRegistro
		
	END TRY
	BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRAN;
			DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
			SET @ErrMsg = ERROR_MESSAGE()
			SET @ErrSev = ERROR_SEVERITY()
			SET @ErrSt = ERROR_STATE()
			RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
 END
