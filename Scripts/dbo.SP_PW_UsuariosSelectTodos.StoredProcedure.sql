USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_UsuariosSelectTodos]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_UsuariosSelectTodos] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT idUsuarios, Usuario, Password, NombreUsuario,clave
FROM Usuarios,Perfiles
WHERE Usuarios.id_perfil=Perfiles.id_perfil
END

GO
