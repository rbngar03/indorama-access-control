USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_Control_Accesos]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Leopoldo Rangel>
-- Create date: <04/11/2010>
-- Description:	<SP que registra los accesos en las terminales>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_Control_Accesos]
@p_tipo_usuario INT = NULL,
@p_periodo_ini DATETIME,
@p_periodo_fin DATETIME,
@p_centro_costos INT =	NULL
AS

BEGIN
	DECLARE @v_query Nvarchar(MAX);
	
	SET @v_query  = 'SELECT DISTINCT MARCAJES.idTipoPersonal,
		  CASE MARCAJES.idTipoPersonal WHEN 1 THEN MARCAJES.IDEMPLEADO WHEN 3 THEN MARCAJES.IDVISITANTES ELSE MARCAJES.IDCONTRATISTAS END AS IDPERSONAL,
		  CASE MARCAJES.idTipoPersonal
          WHEN 1 THEN ''EMPLEADO''
          WHEN 3 THEN ''VISITANTE''
          ELSE ''CONTRATISTA''
       END
          AS TIPO_PERSONAL,
       CASE Marcajes.idTipoPersonal
          WHEN 1
          THEN
                ( (  ISNULL (Empleado.NOMBRE, "")+ " "
                   + ISNULL (Empleado.APEPATERNO, "")
                   + " "
                   + ISNULL (Empleado.APEMATERNO, "")))
          WHEN 3
          THEN
             ISNULL (Visitantes.NOMBREVISITANTE, "")
          ELSE
            
                ( (  ISNULL (Contratistas.NOMBRECONTRATISTA, "")
                   + " "
                   + ISNULL (Contratistas.APPATERNO, "")
                   + " "
                   + ISNULL (Contratistas.APMATERNO, "")))
       END
          AS PERSONAL_NOMBRE,
       ccostos.descripcion
	   FROM (((dbInvista_accesos.dbo.Marcajes Marcajes
          LEFT OUTER JOIN dbInvista_accesos.dbo.ccostos ccostos
             ON (Marcajes.idCcostos = ccostos.idCcostos))
         LEFT OUTER JOIN dbInvista_accesos.dbo.Empleado Empleado
            ON (Empleado.idCcostos = ccostos.idCcostos)
               AND (Marcajes.idEmpleado = Empleado.idEmpleado))
        LEFT OUTER JOIN dbInvista_accesos.dbo.Contratistas Contratistas
           ON (Marcajes.idContratistas = Contratistas.idContratistas)
              AND (Contratistas.idCcostos = ccostos.idCcostos))
       LEFT OUTER JOIN dbInvista_accesos.dbo.Visitantes Visitantes
          ON (Marcajes.idVisitantes = Visitantes.idVisitantes)';
	
	SET @V_QUERY = REPLACE (@v_query,'"','''' );
	
	SET @V_QUERY = @V_QUERY + ' WHERE CONVERT(VARCHAR,FechaRegistro,101) BETWEEN ''' + CONVERT(VARCHAR,@p_periodo_ini,101) + ''' AND ''' + CONVERT(VARCHAR,@p_periodo_fin,101) + ''''; 	
	
	SET @V_QUERY = @V_QUERY + ' AND Marcajes.IdTipoPersonal IS NOT NULL AND Marcajes.NombrePersonal IS NOT NULL ';
	
	IF @p_tipo_usuario > 0 
	BEGIN
		SET @V_QUERY = @V_QUERY + ' AND Marcajes.IdTipoPersonal = ' + CAST(@p_tipo_usuario AS VARCHAR);
	END;
	IF @p_centro_costos  > 0
	BEGIN
		SET @V_QUERY = @V_QUERY + '  AND Marcajes.idCcostos = ' + CAST(@p_centro_costos AS VARCHAR);
	END;
	

	EXEC sp_executesql @V_QUERY; 
END;
GO
