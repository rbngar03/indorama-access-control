USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_ContResidGafetePermanente]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Héctor Romero Badillo>
-- Create date: <29/10/2010,>
-- Description:	<Credenciales Permanenete de Empleados>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_ContResidGafetePermanente] 
	-- Add the parameters for the stored procedure here
@pidContratistas int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select 
	c.foto,
	c.NombreContratista,
	rtrim(c.apPaterno + ' ' + isnull(c.apMaterno,' ')) as apellido,
    (select gafete from gafete where 
	 idgafete=(select idGafete 
	 from contratistas where idContratistas=@pidContratistas)) as gafete,
	(select p.descripcion from planta as p where p.idPlanta = c.idPlanta) as planta,
	c.NombreCompañia as Comp
from Contratistas as c
WHERE c.idContratistas=@pidContratistas
END

GO
