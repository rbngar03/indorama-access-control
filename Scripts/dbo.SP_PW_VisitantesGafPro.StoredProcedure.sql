USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_VisitantesGafPro]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Hector Romero Badillo>
-- ALTER date: <01/11/2010>
-- Description:	<Gafete Visitantes Provisional>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_VisitantesGafPro] 
	-- Add the parameters for the stored procedure here
@pRFCVisitante nvarchar(15)
AS
DECLARE @idVisitantes int
BEGIN
BEGIN TRY

set @idVisitantes=(select idVisitantes from visitantes where RFC=@pRFCVisitante)

if (@idVisitantes is null)
	begin
		RAISERROR('No existe Visitante', 16, 1)
		return 
	end
else
begin
  if((select idestatus from visitantes where idVisitantes=@idVisitantes)=2)
	begin
		RAISERROR('El Visitante esta Inactivo', 16, 1)
		return 
	end
   else
	begin 
		select v.idVisitantes,
			(select gafete from gafete where idgafete=v.idgafete and idTipoGafete=2 and idtipopersonal=3) as gafete,
			(select idestatus from gafete where idgafete=v.idGafete and idTipoGafete=2 and idtipopersonal=3) as estatusgafete,
			(select convert(VARCHAR(10),FechaVigencia,105) from gafete where idgafete=v.idGafete and idTipoGafete=2 and idtipopersonal=3) as fechaVigencia,
			v.NombreVisitante
		from visitantes as v where v.idVisitantes=@idVisitantes
	end
end
END TRY
BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrSev = ERROR_SEVERITY()
		SET @ErrSt = ERROR_STATE()
		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
		return
END CATCH
END

GO
