USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_UpdateGafeteEmpleado]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez
-- ALTER date: 08/11/2010 01:56 pm
-- Description:	Actualizacion de gafete para empleados.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_UpdateGafeteEmpleado] 
	
AS
	declare @NumEmpleado nvarchar(7)
    declare @idEstatus int
    declare @NumeroProvisionalGafete int
    declare @Gafete nvarchar(5)
    --declare @fecha datetime
BEGIN
	
	-- SE OBTIENE EL NUMERO DEL EMPLEADO Y EL ESTATUS DE ESTE MISMO
    --Set @fecha=(convert(varchar(10),getdate()-1,103))
    declare  conteo Cursor For
	
    SELECT DISTINCT NumEmpleado,idEstatus
    FROM  Empleado
    --WHERE fecha_actualizacion between @fecha + ' 00:00:00' and @fecha + ' 23:59:59'
    ORDER BY NumEmpleado
   
	Open conteo

	fetch next from conteo
	into @NumEmpleado,@idEstatus

	while @@FETCH_STATUS=0
    BEGIN
       if (@idEstatus=2)
       BEGIN
		--Actualizar lo campos del Gafete
		 Set @Gafete=(SELECT Gafete FROM Gafete as gaf,Empleado as emp WHERE emp.NumEmpleado=@NumEmpleado and gaf.idGafete=emp.idGafete and gaf.idTipoGafete=1 and gaf.idTipoPersonal=1)
          if (@Gafete is not null)
			  BEGIN
				--Actualiza la fecha de vigencia a null y el estatus a inactivo del gafete
				UPDATE Gafete set FechaVigencia=null,idEstatus=2 WHERE Gafete=@Gafete
			  END
		--Actualizar los campos del Numero Provisional Gafete
         Set @NumeroProvisionalGafete=(SELECT Gafete FROM Empleado as emp,Gafete as gaf WHERE emp.NumEmpleado=@NumEmpleado and emp.NumeroProvisionalGafete=gaf.idGafete)
	      if (@NumeroProvisionalGafete is not null)
             BEGIN
				--Actualiza la fecha de vigencia la fecha inicio a null y el estatus a inactivo del gafete
				UPDATE Gafete set FechaVigencia=null,FechaInicio=null,idEstatus=2 WHERE Gafete=@NumeroProvisionalGafete
                --Actualiza el campo Numero Provisional de Gafete a null para el empleado
				UPDATE Empleado set NumeroProvisionalGafete=null WHERE NumEmpleado=@NumEmpleado
			  END
       END
      fetch next from conteo
	  into @NumEmpleado,@idEstatus
    END
	Close conteo
	deallocate conteo
END
GO
