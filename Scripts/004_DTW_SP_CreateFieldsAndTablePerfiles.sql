IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_CreateFieldsAndTablePerfiles')
  BEGIN
	DROP PROCEDURE DTW_SP_CreateFieldsAndTablePerfiles
  END
GO
-- =============================================
-- Author:		Alonso Sanchez
-- Create date: 02/08/2024
-- Description:	Creación y modificación de tablas para campos editables
-- History:
-- <14/10/2024> <r.garcia> <Se agrega formato de store procedure.>
-- =============================================
CREATE PROCEDURE DTW_SP_CreateFieldsAndTablePerfiles
AS
BEGIN
	IF NOT EXISTS( SELECT 1	FROM SYSOBJECTS	WHERE Name = N'CamposPerfil') 
	BEGIN
		CREATE TABLE dbo.CamposPerfil (
			[Id] [INT] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
			[ModuloId] [INT] NOT NULL,
			[PerfilId] [INT] NOT NULL,
			[NombreCampo] [VARCHAR] (40) NOT NULL,
			[IsActivated] [BIT] NOT NULL,
			CONSTRAINT FK_Modulo_Id FOREIGN KEY (ModuloId) REFERENCES Modulos(id_modulo),
			CONSTRAINT FK_Perfil_Id FOREIGN KEY (PerfilId) REFERENCES Perfiles(id_perfil) ON DELETE CASCADE ON UPDATE CASCADE
		)	
	END

	IF NOT EXISTS( 
		SELECT column_name 
		FROM information_schema.columns 
		WHERE table_schema='dbo' 
  		AND table_name='Modulos' 
  		AND column_name='HasEditableFields')
	BEGIN
		ALTER TABLE	[dbo].[Modulos] ADD	[HasEditableFields] BIT
	END
	
	UPDATE [dbo].[Modulos] SET [HasEditableFields] = 1
END
GO

EXEC DTW_SP_CreateFieldsAndTablePerfiles