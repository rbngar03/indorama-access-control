USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_ConResGafPro]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Hector Romero Badillo>
-- ALTER date: <01/11/2010>
-- Description:	<Gafete empleados Provisional>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_ConResGafPro] 
	-- Add the parameters for the stored procedure here
@pRFCResidente nvarchar(15)
AS
DECLARE @idContratistas int
BEGIN
BEGIN TRY
 
set @idContratistas=(select idContratistas from contratistas where RFC=@pRFCResidente and idtipocontratista=2)

if (@idContratistas is null)
	begin
		RAISERROR('No existe Residente', 16, 1)
		return 
	end
else
begin
  if((select idestatus from contratistas where idContratistas=@idContratistas)=2)
	begin
		RAISERROR('El Residente esta Inactivo', 16, 1)
		return 
	end
   else
	begin 
		select c.idContratistas,
			(select gafete from gafete where idgafete=c.NumeroProvisionalGafete and idTipoGafete=2 and idtipopersonal=2) as gafete,
			(select idestatus from gafete where idgafete=c.NumeroProvisionalGafete and idTipoGafete=2 and idtipopersonal=2) as estatusgafete ,
			(select convert(VARCHAR(10),FechaVigencia,105) from gafete where idgafete=c.NumeroProvisionalGafete and idTipoGafete=2 and idtipopersonal=2) as fechaVigencia,
		rtrim(c.NombreContratista + ' ' + c.apPaterno + ' ' + isnull(c.apmaterno,' ')) as Nombre
		from contratistas as c where c.idContratistas=@idContratistas
	end
end
END TRY
BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrSev = ERROR_SEVERITY()
		SET @ErrSt = ERROR_STATE()
		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
		return
END CATCH
END

GO
