USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_TerminalesInsertar]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Debi Sanchez
-- ALTER date: 27-10-2010
-- Description:	Inserta un registro nuevo de terminales.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_TerminalesInsertar]

@Clave VARCHAR(50),
@Ubicacion VARCHAR(50),
@IP VARCHAR(50),
@Nombre VARCHAR(50)

AS
BEGIN
	INSERT INTO Terminales(Clave,Ubicacion,IP,Nombre)
	VALUES (@Clave,@Ubicacion,@IP,@Nombre)
END
GO
