USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_ContratistasSelect1]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez V.
-- Create date: 01/11/2010 4:04 pm
-- Description:	Extrae la información de contratistas de una persona especifica.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_ContratistasSelect1] 
	-- Add the parameters for the stored procedure here
@pidContratistas int
AS
BEGIN
	
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

    SELECT  con.idTipoContratista,con.idEstatus,
     con.NombreContratista,con.ApPaterno,con.ApMaterno,convert(VARCHAR(10),con.FechaIndSeguridad,105),
	convert(VARCHAR(10),con.FechaContrato,105),con.Foto,con.IMSS,convert(VARCHAR(10),con.FechaTerminacion,105),convert(VARCHAR(10),con.VigenciaIMSS,105),
	convert(VARCHAR(10),con.VigenciaSeguridad,105),con.NumeroContrato,convert(VARCHAR(10),con.PeriodoVigencia,105),
	con.idCompanias,con.NombreCompañia,con.RFC,con.idClasificacion,con.idCcostos,con.ResponsableInvista,
	(SELECT gaf.gafete FROM Gafete AS gaf WHERE gaf.idGafete=con.idGafete),
    (SELECT gaf1.gafete FROM Gafete AS gaf1 WHERE gaf1.idGafete=con.NumeroProvisionalGafete),
   -- con.NumeroProvisionalGafete,
    con.idComedor,con.Notas,convert(VARCHAR(10),con.FechaBaja,105),
    con.ArchivoAdjunto,
   (SELECT clas.Descripcion FROM clasificacion AS clas WHERE clas.idClasificacion=con.idClasificacion),
	con.idPlanta
	FROM Contratistas as con
	WHERE con.idContratistas=@pidContratistas

END

GO
