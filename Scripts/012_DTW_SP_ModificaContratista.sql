IF EXISTS (	SELECT 1
			FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_ModificaContratista' )
  BEGIN
	DROP PROCEDURE DTW_SP_ModificaContratista
  END
GO
-- =============================================
-- Author:		Karla Ramirez
-- Create date: 27/08/24
-- Description:	Script que actualiza datos de los Contratistas
-- History:
-- <26/09/2024> <ag.marin> <Se agrega actualización de estatus.>
-- <26/09/2024> <ag.marin> <Se agrega condición a validación de status cuando pasa de activo a inactivo>
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_ModificaContratista]
	@pidContratistas INT,
	@pidTipoContratista INT,
	@pidEstatus INT,
	@pNombreContratista VARCHAR(150),
	@pApPaterno VARCHAR(50),
	@pApMaterno VARCHAR(50),
	@pFechaIndSeguridad VARCHAR(20),
	@pFechaContrato VARCHAR(20),
	--@pFoto IMAGE,
	@pIMSS VARCHAR(50),
	@pRFC VARCHAR(15),
	@pVigenciaIMSS VARCHAR(20),
	@pFechaTerminacion VARCHAR(20),
	@pResponsableIndorama VARCHAR(150),
	@pidClasificacion INT,
	@pNombreCompañia VARCHAR(50),
	@pNumeroContrato VARCHAR(10),
	@pGafete VARCHAR(5),
	@pidComedor INT,
	@pidCcostos INT,
	@Response VARCHAR(100) OUTPUT

AS
BEGIN
	DECLARE @IdEstatusAct INT,
			@IdCcostosAct INT,
			@IdGafeteAct INT,
            @GafeteAct INT,
			@IdGafete INT
            

	BEGIN TRY 
		IF @pGafete = 0 SET @pGafete = NULL

		BEGIN TRAN

		-- Se obtienen valores actuales 
		SET @IdEstatusAct = (SELECT idEstatus FROM Contratistas WHERE idContratistas = @pidContratistas)
		SET @IdCcostosAct = (SELECT idCcostos FROM Contratistas WHERE  idContratistas = @pidContratistas) 
		SET @GafeteAct = (SELECT idGafete FROM Contratistas WHERE idContratistas = @pidContratistas)
		SET @IdGafete = (SELECT idGafete FROM Gafete WHERE Gafete = @pGafete)

		   -- Validaciones y actualizaciones
		IF @pidEstatus = 2 AND @IdEstatusAct = 1
		BEGIN
			UPDATE Gafete
            SET FechaVigencia = NULL, idEstatus = 2
            WHERE idGafete = @GafeteAct
			
			UPDATE Contratistas 
			SET idEstatus = @pidEstatus, 
			--Foto = isnull(@pFoto,Foto), 
			idGafete = NULL
			WHERE idContratistas = @pidContratistas  
		END
		ELSE IF @pidEstatus = 1
	         -- Actualizacion de gafete --
			 IF(@pGafete IS NOT NULL)
		     BEGIN
		        IF (@IdGafete IS NULL)
			    BEGIN 
					SET @Response = 'No existe el gafete';
					RAISERROR('No existe gafete', 16, 1)
					ROLLBACK TRAN;
					RETURN
			    END
		 
				IF EXISTS (SELECT 1 FROM Gafete WHERE Gafete = @pGafete AND idEstatus = 1) AND 
				   NOT EXISTS (SELECT 1 FROM Contratistas WHERE  idGafete = @IdGafete AND  idContratistas <> @pidContratistas
				   OR idContratistas = @pidContratistas ) 
		           BEGIN
				   SET @Response = 'El gafete ya esta asignado';
				   RAISERROR('El gafete ya esta asignado', 16, 1)
				   ROLLBACK TRAN;
				   RETURN
				END
		 	
				IF  (@pGafete IS NOT NULL AND @pGafete <> @GafeteAct)
				 BEGIN
					SET @IdGafeteAct = (SELECT idGafete FROM Gafete WHERE idGafete IN (SELECT idGafete FROM Contratistas WHERE idContratistas = @pidContratistas))
					UPDATE Gafete SET idEstatus = 2 WHERE idGafete = @IdGafeteAct
					UPDATE Gafete SET idEstatus = 1, FechaInicio = GETDATE() WHERE idGafete = @IdGafete
					UPDATE Contratistas SET idGafete = @IdGafete WHERE idContratistas = @pidContratistas
				 END
			 END

		  --Actualizacion de Centro de Costos 
		  IF @pidCcostos <> @IdCcostosAct
		  BEGIN
			IF EXISTS (SELECT 1 FROM ccostos WHERE  idCcostos = @pidCcostos)
			UPDATE Contratistas SET idCcostos = @pidCcostos WHERE idContratistas = @pidContratistas
			ELSE
			BEGIN
				SET @Response = 'El centro de Costos no existe';
				RAISERROR('El centro de Costos no existe', 16, 1)
			    ROLLBACK TRAN;
			    RETURN
			END
		  END 

		-- Actualizacion de datos generales --
		UPDATE [dbo].[Contratistas]
	       SET [idTipoContratista] = @pidTipoContratista,
			   [idEstatus] = @pidEstatus,
			   [NombreContratista] = @pNombreContratista,
			   [ApPaterno] = @pApPaterno,
			   [ApMaterno] = ISNULL(@pApMaterno,NULL),
			   [FechaIndSeguridad] =  CONVERT(DATETIME, @pFechaIndSeguridad, 103),
			   [FechaContrato] = CONVERT(DATETIME, @pFechaContrato, 103),
			   [IMSS] = @pIMSS,
			   [VigenciaIMSS] = CONVERT(DATETIME, @pVigenciaIMSS, 103),
			   [NumeroContrato] = @pNumeroContrato,
			   [NombreCompañia] = @pNombreCompañia,
			   -- [Foto] = ISNULL(@pFoto,Foto),
			   [RFC] = @pRFC,
			   [FechaTerminacion] = CONVERT(DATETIME, @pFechaTerminacion, 103),
			   [idClasificacion] = @pidClasificacion,
			   [ResponsableIndorama] = @pResponsableIndorama,
			   [Notas] = NULL,
			   [idComedor] = @pidComedor,
			   [ArchivoAdjunto] = NULL,
			   [fecha_actualizacion] = GETDATE()
			WHERE idContratistas = @pidContratistas
		 
	 COMMIT TRAN
	 SET @Response = 'Ok'; 
	
   END TRY
   BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRAN;
			DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
			SET @ErrMsg = ERROR_MESSAGE()
			SET @ErrSev = ERROR_SEVERITY()
			SET @ErrSt = ERROR_STATE()
			RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
 END
