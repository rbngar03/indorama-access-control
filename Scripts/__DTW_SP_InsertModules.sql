INSERT INTO
    Perfiles (Clave, Descripcion)
VALUES
    ('ADMIN', 'ADMINISTRADOR'),
    ('RH', 'RECURSOS HUMANOS'),
    ('VIGILANCIA', 'VIGILANCIA')
GO

INSERT INTO
    Modulos (nombre, orden, HasEditableFields)
VALUES
    ('Catálogo de usuarios', 1, 1),
    ('Catálogo de contratistas', 1, 1),
    ('Catálogo de perfiles', 1, 1),
    ('Catálogo de empleados', 1, 1),
    ('Catálogo de terminales', 1, 1),
    ('Catálogo de visitantes', 1, 1)
GO

INSERT INTO
    CamposPerfil(ModuloId, PerfilId, NombreCampo, IsActivated)
VALUES
    ()