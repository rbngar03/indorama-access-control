USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[spGetMarcajesComidas]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetMarcajesComidas]
	-- Add the parameters for the stored procedure here
	@FechaIni datetime,
	@FechaFin datetime,
	@Sitio int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if @Sitio = 0
		Begin
			select Fecha, turno, count(Fecha) as Comidas, 'QRO' as Sitio from viewMarcajesComedor
			where fecha between @FechaIni and @FechaFin
			Group by fecha, turno
			Union All
			select Fecha, turno, count(Fecha) as Comidas, 'STA FE' as Sitio from viewMarcajesComedorSF
			where fecha between @FechaIni and @FechaFin
			Group by fecha, turno
			order by sitio, fecha, turno
		End
	if @Sitio = 1
		Begin
			select Fecha, turno, count(Fecha) as Comidas, 'QRO' as Sitio from viewMarcajesComedor
			where fecha between @FechaIni and @FechaFin
			Group by fecha, turno
			order by fecha, turno
		End
	if @Sitio=2
		Begin
			select Fecha, turno, count(Fecha) as Comidas, 'STA FE' as Sitio from viewMarcajesComedorSF
			where fecha between @FechaIni and @FechaFin
			Group by fecha, turno
			order by sitio, fecha, turno
		End
END
GO
