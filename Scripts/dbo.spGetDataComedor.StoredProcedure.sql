USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[spGetDataComedor]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetDataComedor]
	-- Add the parameters for the stored procedure here
	@FechaIni datetime,
	@Fechafin datetime,
	@Tipo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if @Tipo = 1
		Begin
			Select e.NumEmpleado, Nombre + ' ' + ApePaterno +' ' +ApeMaterno as Nombre, m.Fecharegistro  ,
			Case When e.idCCostos = 459 Then 'PF' else 'IVL' END as Empresa, 'Empleado' as Tipo, Case when idTerminales = 1 then 'Queretaro' When idterminales=18 then 'Santa Fé' end as Sitio
			from marcajes m
			INNER JOIN Empleado e on m.idEmpleado = e.idEmpleado 
			where EntradaSalida = 'C' and idTipoPersonal = 1
			and FechaRegistro between @FechaIni and @FechaFin
			Union All
			Select e.idContratistas, NombreContratista + ' ' + ApPaterno +' ' + ApMaterno as Nombre, m.Fecharegistro  ,
			NombreCompañia as Empresa, 'Contratista' as Tipo, Case when idTerminales = 1 then 'Queretaro' When idterminales=18 then 'Santa Fé' end as Sitio
			from marcajes m
			INNER JOIN Contratistas e on m.idContratistas = e.idContratistas 
			where EntradaSalida = 'C' and idTipoPersonal = 2
			and FechaRegistro between @FechaIni and @FechaFin
			Order by FechaRegistro
		End
	if @Tipo = 2  --Solo Empleados todos los sitios
		Begin
			Select e.NumEmpleado, Nombre + ' ' + ApePaterno +' ' +ApeMaterno as Nombre, m.Fecharegistro  ,
			Case When e.idCCostos = 459 Then 'PF' else 'IVL' END as Empresa, 'Empleado' as Tipo, Case when idTerminales = 1 then 'Queretaro' When idterminales=18 then 'Santa Fé' end as Sitio
			from marcajes m
			INNER JOIN Empleado e on m.idEmpleado = e.idEmpleado 
			where EntradaSalida = 'C' and idTipoPersonal = 1
			and FechaRegistro between @FechaIni and @FechaFin
			Order by FechaRegistro
		End
	if @Tipo = 3 -- Solo Contratistas 
		Begin
			Select e.idContratistas, NombreContratista + ' ' + ApPaterno +' ' + ApMaterno as Nombre, m.Fecharegistro  ,
			NombreCompañia as Empresa, 'Contratista' as Tipo, Case when idTerminales = 1 then 'Queretaro' When idterminales=18 then 'Santa Fé' end as Sitio
			from marcajes m
			INNER JOIN Contratistas e on m.idContratistas = e.idContratistas 
			where EntradaSalida = 'C' and idTipoPersonal = 2
			and FechaRegistro between @FechaIni and @FechaFin
			Order by FechaRegistro
		End
	if @Tipo = 4 -- Empleados Queretaro
		Begin
			Select e.NumEmpleado, Nombre + ' ' + ApePaterno +' ' +ApeMaterno as Nombre, m.Fecharegistro  ,
			Case When e.idCCostos = 459 Then 'PF' else 'IVL' END as Empresa, 'Empleado' as Tipo, Case when idTerminales = 1 then 'Queretaro' When idterminales=18 then 'Santa Fé' end as Sitio
			from marcajes m
			INNER JOIN Empleado e on m.idEmpleado = e.idEmpleado 
			where EntradaSalida = 'C' and idTipoPersonal = 1 and idTerminales = 1
			and FechaRegistro between @FechaIni and @FechaFin
			Order by FechaRegistro
		End
	if @Tipo = 5 -- Contratistas Queretaro
		Begin
			Select e.idContratistas, NombreContratista + ' ' + ApPaterno +' ' + ApMaterno as Nombre, m.Fecharegistro  ,
			NombreCompañia as Empresa, 'Contratista' as Tipo, Case when idTerminales = 1 then 'Queretaro' When idterminales=18 then 'Santa Fé' end as Sitio
			from marcajes m
			INNER JOIN Contratistas e on m.idContratistas = e.idContratistas 
			where EntradaSalida = 'C' and idTipoPersonal = 2 and idTerminales = 1
			and FechaRegistro between @FechaIni and @FechaFin
			Order by FechaRegistro
		End
	if @Tipo = 6 -- Empleados Sta Fe
		Begin
			Select e.NumEmpleado, Nombre + ' ' + ApePaterno +' ' +ApeMaterno as Nombre, m.Fecharegistro  ,
			Case When e.idCCostos = 459 Then 'PF' else 'IVL' END as Empresa, 'Empleado' as Tipo, Case when idTerminales = 1 then 'Queretaro' When idterminales=18 then 'Santa Fé' end as Sitio
			from marcajes m
			INNER JOIN Empleado e on m.idEmpleado = e.idEmpleado 
			where EntradaSalida = 'C' and idTipoPersonal = 1 and idTerminales = 18
			and FechaRegistro between @FechaIni and @FechaFin
			Order by FechaRegistro
		End
	if @Tipo = 7 -- Contratistas Sta Fe
		Begin
			Select e.idContratistas, NombreContratista + ' ' + ApPaterno +' ' + ApMaterno as Nombre, m.Fecharegistro  ,
			NombreCompañia as Empresa, 'Contratista' as Tipo, Case when idTerminales = 1 then 'Queretaro' When idterminales=18 then 'Santa Fé' end as Sitio
			from marcajes m
			INNER JOIN Contratistas e on m.idContratistas = e.idContratistas 
			where EntradaSalida = 'C' and idTipoPersonal = 2 and idTerminales = 18
			and FechaRegistro between @FechaIni and @FechaFin
			Order by FechaRegistro
		End
	if @Tipo = 8 -- Todos Queretaro
		Begin
			Select e.NumEmpleado, Nombre + ' ' + ApePaterno +' ' +ApeMaterno as Nombre, m.Fecharegistro  ,
			Case When e.idCCostos = 459 Then 'PF' else 'IVL' END as Empresa, 'Empleado' as Tipo, Case when idTerminales = 1 then 'Queretaro' When idterminales=18 then 'Santa Fé' end as Sitio
			from marcajes m
			INNER JOIN Empleado e on m.idEmpleado = e.idEmpleado 
			where EntradaSalida = 'C' and idTipoPersonal = 1 and idTerminales = 1
			and FechaRegistro between @FechaIni and @FechaFin
			Union All
			Select e.idContratistas, NombreContratista + ' ' + ApPaterno +' ' + ApMaterno as Nombre, m.Fecharegistro  ,
			NombreCompañia as Empresa, 'Contratista' as Tipo, Case when idTerminales = 1 then 'Queretaro' When idterminales=18 then 'Santa Fé' end as Sitio
			from marcajes m
			INNER JOIN Contratistas e on m.idContratistas = e.idContratistas 
			where EntradaSalida = 'C' and idTipoPersonal = 2 and idTerminales = 1
			and FechaRegistro between @FechaIni and @FechaFin
			Order by FechaRegistro
		End
	if @Tipo = 9 -- Todos Sta Fe
		Begin
			Select e.NumEmpleado, Nombre + ' ' + ApePaterno +' ' +ApeMaterno as Nombre, m.Fecharegistro  ,
			Case When e.idCCostos = 459 Then 'PF' else 'IVL' END as Empresa, 'Empleado' as Tipo, Case when idTerminales = 1 then 'Queretaro' When idterminales=18 then 'Santa Fé' end as Sitio
			from marcajes m
			INNER JOIN Empleado e on m.idEmpleado = e.idEmpleado 
			where EntradaSalida = 'C' and idTipoPersonal = 1 and idTerminales = 18
			and FechaRegistro between @FechaIni and @FechaFin
			Union All
			Select e.idContratistas, NombreContratista + ' ' + ApPaterno +' ' + ApMaterno as Nombre, m.Fecharegistro  ,
			NombreCompañia as Empresa, 'Contratista' as Tipo, Case when idTerminales = 1 then 'Queretaro' When idterminales=18 then 'Santa Fé' end as Sitio
			from marcajes m
			INNER JOIN Contratistas e on m.idContratistas = e.idContratistas 
			where EntradaSalida = 'C' and idTipoPersonal = 2 and idTerminales = 18
			and FechaRegistro between @FechaIni and @FechaFin
			Order by FechaRegistro
		End
END
GO
