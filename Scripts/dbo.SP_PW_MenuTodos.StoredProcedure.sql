USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_MenuTodos]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_MenuTodos] 
	-- Add the parameters for the stored procedure here
@idUsuarios int
AS
declare @idperfil int
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
set @idperfil=(select id_perfil from usuarios where idUsuarios=@idUsuarios)

SELECT	MODULOS.NOMBRE,
		MODULOS.ID_MODULO,
		MODULOS.ID_PADRE,
		MODULOS.ARCHIVO,
		MODULOS.ORDEN
		FROM MODULOS 
		WHERE 
		((SELECT COUNT(*) FROM PerfilesModuloS WHERE PerfilesModuloS.Id_Modulo = Modulos.ID_Modulo 
       AND PerfilesModuloS.Id_PERFIL = @idperfil) > 0 OR ARCHIVO IS NULL)
       ORDER by MODULOS.ID_MODULO,orden;

END

GO
