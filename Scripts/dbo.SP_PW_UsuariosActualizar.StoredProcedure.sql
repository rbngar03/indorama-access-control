USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_UsuariosActualizar]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Debi Sanchez
-- ALTER date: 28-10-2010
-- Description: Actualiza un registro nuevo de usuario.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_UsuariosActualizar]

@idUsuarios INT,
@Usuario VARCHAR(50),
@Password VARCHAR(50),
@NombreUsuario VARCHAR(50),
@id_perfil INT

AS
BEGIN
    
    UPDATE Usuarios set Usuario=@Usuario, Password=isnull(@Password,Password),
--Password=(CASE WHEN @Password IS NULL THEN Password ELSE @Password END ), 
NombreUsuario=@NombreUsuario, id_perfil=@id_perfil
    WHERE (idUsuarios = @idUsuarios)
END
GO
