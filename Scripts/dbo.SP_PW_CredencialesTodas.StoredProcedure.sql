USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_CredencialesTodas]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- <02/07/2024>	<Rubén García> <Se agrega claúsula TOP 1 a los CASE WHEN para evitar la duplicación de registros.>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_CredencialesTodas] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select	g.idgafete,
		g.gafete,
		g.fechainicio,
		g.fechavigencia,
		g.idestatus,
		(select e.estatus from estatus as e where e.idestatus=g.idestatus) as estatus_Gafete,
		g.idtipogafete,
		(select tg.tipogafete from tipogafete as tg where tg.idtipogafete=g.idtipogafete) as tipo_Gafete,
		g.idTipoPersonal,
		(select tp.personal from personal as tp where tp.idTipoPersonal=g.idTipoPersonal) as tipo_Personal,
		'Nombre' = 
      CASE 
         WHEN g.idTipoPersonal =  1 and g.idtipogafete=1 THEN (select TOP 1 RTRIM(emp.nombre + ' ' + emp.apepaterno + ' ' + isnull(emp.apematerno,' ')) from empleado as emp where emp.idgafete=g.idgafete)
		 WHEN g.idTipoPersonal =  1 and g.idtipogafete=2 THEN (select TOP 1 RTRIM(emp.nombre + ' ' + emp.apepaterno + ' ' + isnull(emp.apematerno,' ')) from empleado as emp where emp.NumeroProvisionalGafete=g.idgafete)
		 WHEN g.idTipoPersonal =  2 and g.idtipogafete=1 THEN (select TOP 1 RTRIM(c1.NombreContratista + ' ' + c1.appaterno + ' ' + isnull(c1.apmaterno,' ')) from contratistas as c1 where c1.idgafete=g.idgafete)
		 WHEN g.idTipoPersonal =  2 and g.idtipogafete=2 THEN (select TOP 1 RTRIM(c1.NombreContratista + ' ' + c1.appaterno + ' ' + isnull(c1.apmaterno,' ')) from contratistas as c1 where c1.NumeroProvisionalGafete=g.idgafete)
		 WHEN g.idTipoPersonal =  4 and g.idtipogafete=1 THEN (select TOP 1 RTRIM(c1.NombreContratista + ' ' + c1.appaterno + ' ' + isnull(c1.apmaterno,' ')) from contratistas as c1 where c1.idgafete=g.idgafete)
		 WHEN g.idTipoPersonal =  4 and g.idtipogafete=2 THEN (select TOP 1 RTRIM(c1.NombreContratista + ' ' + c1.appaterno + ' ' + isnull(c1.apmaterno,' ')) from contratistas as c1 where c1.NumeroProvisionalGafete=g.idgafete)
		 WHEN g.idTipoPersonal =  3 and g.idtipogafete=2 THEN (select TOP 1 RTRIM(v1.NombreVisitante) from visitantes as v1 where v1.idgafete=g.idgafete)
      END,
	'EstatusEmpleado' = 
      CASE 
         WHEN g.idTipoPersonal =  1 and g.idtipogafete=1 THEN (select TOP 1 (select es.estatus from estatus as es where es.idestatus=emp.idestatus) from empleado as emp where emp.idgafete=g.idgafete)
		 WHEN g.idTipoPersonal =  1 and g.idtipogafete=2 THEN (select TOP 1 (select es.estatus from estatus as es where es.idestatus=emp.idestatus) from empleado as emp where emp.NumeroProvisionalGafete=g.idgafete)
		 WHEN g.idTipoPersonal =  2 and g.idtipogafete=1 THEN (select TOP 1 (select es.estatus from estatus as es where es.idestatus=c1.idestatus) from contratistas as c1 where c1.idgafete=g.idgafete)
		 WHEN g.idTipoPersonal =  2 and g.idtipogafete=2 THEN (select TOP 1 (select es.estatus from estatus as es where es.idestatus=c1.idestatus) from contratistas as c1 where c1.NumeroProvisionalGafete=g.idgafete)
		 WHEN g.idTipoPersonal =  4 and g.idtipogafete=1 THEN (select TOP 1 (select es.estatus from estatus as es where es.idestatus=c1.idestatus) from contratistas as c1 where c1.idgafete=g.idgafete)
		 WHEN g.idTipoPersonal =  4 and g.idtipogafete=2 THEN (select TOP 1 (select es.estatus from estatus as es where es.idestatus=c1.idestatus) from contratistas as c1 where c1.NumeroProvisionalGafete=g.idgafete)
		 WHEN g.idTipoPersonal =  3 and g.idtipogafete=2 THEN (select TOP 1 (select es.estatus from estatus as es where es.idestatus=v1.idestatus) from visitantes as v1 where v1.idgafete=g.idgafete)
      END
from gafete as g
order by g.fechainicio,g.idestatus,g.idtipogafete,g.idTipoPersonal

END

GO
