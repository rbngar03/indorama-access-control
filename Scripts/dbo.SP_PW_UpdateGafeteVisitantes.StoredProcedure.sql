USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_UpdateGafeteVisitantes]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez
-- ALTER date: 09/11/2010 10:17 am
-- Description:	Actualizacion de gafete para visitantes
--              en base a su estatus y fecha de terminacion.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_UpdateGafeteVisitantes] 
	
AS
	declare @RFC nvarchar(15)
    declare @idEstatus int
    declare @Gafete nvarchar(5)
    declare @FechaTerminacion varchar(10)
	--declare @fecha datetime
BEGIN
	
	-- SE OBTIENE EL RFC DEL VISITANTE Y SU ESTATUS DE ESTE MISMO 
	--Set @fecha=(convert(varchar(10),getdate()-1,103))
    declare  conteo Cursor For
	
    SELECT DISTINCT RFC,idEstatus
    FROM  Visitantes
	--WHERE fecha_actualizacion between @fecha + ' 00:00:00' and @fecha + ' 23:59:59'
    ORDER BY RFC

	Open conteo

	fetch next from conteo
	into @RFC,@idEstatus

	while @@FETCH_STATUS=0
    BEGIN
	    Set @Gafete=(SELECT Gafete FROM Gafete as gaf,Visitantes as vis WHERE vis.RFC=@RFC and vis.idGafete=gaf.idGafete and gaf.idTipoGafete=2 and gaf.idTipoPersonal=3)
       if (@idEstatus=2) 
       BEGIN
		--Actualizar lo campos del Gafete
		   if (@Gafete is not null)
			  BEGIN
				--Se actualiza la fecha inicio y fecha vigencia a null,el estatus del gafete del visitante cambia a inactivo
				UPDATE Gafete  set FechaInicio=null,FechaVigencia=null,idEstatus=2 WHERE Gafete=@Gafete
			    --Se actualiza el idGafete a nulo para el visitante
				UPDATE Visitantes set idGafete=null WHERE RFC=@RFC   
			  END 
       END
	   --Si la fecha de terminacion ha sido vencida este bloque aplica
	   Set @FechaTerminacion=(SELECT CONVERT(VARCHAR(10),FechaTerminacion, 103) AS [DD/MM/YYYY] FROM Visitantes WHERE RFC=@RFC)
	   if (@FechaTerminacion< getdate())
			 BEGIN
				--Se actualiza la fecha inicio y la fecha vigencia null, el estatus del gafete del visitantes cambia a inactivo
				UPDATE Gafete  set FechaInicio=null,FechaVigencia=null,idEstatus=2 WHERE Gafete=@Gafete
			    --Se actualiza el estatus a inactivo para el visitante
			    UPDATE Visitantes set idEstatus=2,idGafete=null WHERE RFC=@RFC   
			 END
      fetch next from conteo
	   into @RFC,@idEstatus
    END
	
	Close conteo
	deallocate conteo
   
END
GO
