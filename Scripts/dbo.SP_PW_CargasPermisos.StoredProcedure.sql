USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_CargasPermisos]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_CargasPermisos] 
	-- Add the parameters for the stored procedure here
@archivo nvarchar(150),
@id_perfil int
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT  Acciones.Nombre
        FROM Acciones 
		INNER JOIN PerfilesModulos ON Acciones.id_accion = PerfilesModulos.id_accion 
		INNER JOIN Modulos ON PerfilesModulos.Id_Modulo = Modulos.Id_Modulo
	    WHERE PerfilesModulos.Id_Perfil = @id_perfil 
        AND Modulos.Archivo = @archivo;

END

GO
