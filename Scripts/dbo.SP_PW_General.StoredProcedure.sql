USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_General]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez V.
-- ALTER date: 16/11/2010 06:10 p.m.
-- Description:	Reporte de Credenciales pago a comedor (comidas) 
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_General] 
	-- Add the parameters for the stored procedure here

@fecha_inicio varchar(30),
@fecha_fin varchar(30),
@tipo_usuario varchar(40)=null,
@compania varchar(50)=null,
@nombre varchar(200)=null,
@marcaje bit=null
AS
   declare @consulta nvarchar(max)
   declare @idCompanias varchar(20)
   declare @fecha1 varchar(50)
   declare @fecha2 varchar(50)
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	 SET NOCOUNT ON;
   SET @idCompanias=''
   Set @fecha1=convert(varchar(10),@fecha_inicio,103)
   Set @fecha2=convert(varchar(10),@fecha_fin,103)

  Set @fecha1=@fecha1 + ' 00:00:00'
  Set @fecha2=@fecha2 + ' 23:59:59'


   IF (@tipo_usuario='Empleados')
       BEGIN
			Set @consulta=('SELECT CONVERT(varchar(10), mar.FechaRegistro, 103) AS Fecha, CONVERT(varchar(10), mar.FechaRegistro, 108) AS Hora, tp.TipoLector, 
                            gaf.Gafete,mar.nombrepersonal as Nombre,mar.entradasalida as ''Tipo de Acceso''
                            FROM Marcajes mar,terminales ter,gafete gaf,tipolector tp WHERE mar.idTerminales = ter.idTerminales and
                             mar.idGafete = gaf.idGafete and mar.idTipoLector = tp.idTipoLector and mar.entradasalida <>''C'' and mar.AccesoOtorgado=1 and FechaRegistro between ''' + @fecha1 + ''' and ''' + @fecha2 + '''')
      END
 ELSE if (@tipo_usuario='Visitantes')
       BEGIN
           Set @consulta=('SELECT CONVERT(varchar(10), mar.FechaRegistro, 103) AS Fecha, CONVERT(varchar(10), mar.FechaRegistro, 108) AS Hora, tp.TipoLector, 
                           gaf.Gafete,mar.nombrepersonal as Nombre,mar.entradasalida as ''Tipo de Acceso'',mar.ResponsableInvista
                           FROM Marcajes mar,terminales ter,gafete gaf,tipolector tp
                           WHERE mar.idTerminales = ter.idTerminales and mar.idGafetetmp = gaf.idGafete and
                                 mar.idTipoLector = tp.idTipoLector and mar.entradasalida <>''C'' and mar.AccesoOtorgado=1 and FechaRegistro between ''' + @fecha1 + ''' and ''' + @fecha2 + '''')
       END
   ELSE 
       BEGIN
           Set @consulta=('SELECT CONVERT(varchar(10), mar.FechaRegistro, 103) AS Fecha, CONVERT(varchar(10), mar.FechaRegistro, 108) AS Hora, tp.TipoLector, 
                           gaf.Gafete,mar.nombrepersonal as Nombre,mar.entradasalida as ''Tipo de Acceso'',com.Compania,mar.ResponsableInvista
                           FROM Marcajes mar,terminales ter,gafete gaf,tipolector tp,compania com
                           WHERE mar.idTerminales = ter.idTerminales and mar.idGafete = gaf.idGafete and
                                 mar.idTipoLector = tp.idTipoLector and mar.idCompanias=com.idCompanias and mar.entradasalida <>''C'' and mar.AccesoOtorgado=1 and FechaRegistro between ''' + @fecha1 + ''' and ''' + @fecha2 + '''')
       END
   
    IF (SELECT @marcaje)=1
        BEGIN
            -- print @marcaje
			 SET @consulta = (@consulta + 'and idmarcajes not in (select idmarcajesentrada from marcajes WHERE FechaRegistro between ''' + @fecha1 + ''' and ''' + @fecha2 + ''' and idmarcajesentrada is not null) AND (mar.entradasalida<>''S'')')
        END
        

    IF (SELECT @tipo_usuario)='Empleados'
        BEGIN
            SET @consulta = (@consulta + ' AND (mar.idTipoPersonal=1)')
        END
    ELSE IF (SELECT @tipo_usuario)='Contratistas Residentes'
		BEGIN
            SELECT @consulta = @consulta + ' AND (mar.idTipoPersonal=2)' 
        END
    ELSE IF (SELECT @tipo_usuario)='Contratistas Corto Plazo'
		BEGIN
            SELECT @consulta = @consulta + ' AND (mar.idTipoPersonal=4)' 
        END
    ELSE IF (SELECT @tipo_usuario)='Visitantes'
		BEGIN
            SELECT @consulta = @consulta + ' AND (mar.idTipoPersonal=3)' 
        END

    IF (SELECT @compania) <>''
        BEGIN
			set @idCompanias=(SELECT idCompanias FROM Compania WHERE Compania=@compania)
            SELECT @consulta=@consulta + ' AND (mar.idCompanias=''' + @idCompanias + ''')'
        END
 
	 IF (SELECT @nombre) <>''
        BEGIN
            SELECT @consulta = @consulta + 'AND mar.NombrePersonal LIKE ''%' + @nombre + '%''' 
        END
    
     exec (@consulta)
       
END

GO
