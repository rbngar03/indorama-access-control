IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_UpdateFieldsIndorama')
  BEGIN
	DROP PROCEDURE DTW_SP_UpdateFieldsIndorama
  END
GO
-- =============================================
-- Author:	Rub�n Garc�a Garc�a
-- Create date: 14/10/2024
-- Description:	Actualiza los campos de algunas tablas
--				para que sean compatibles con la nueva aplicaci�n.
-- <17/10/2024> <r.garcia> <Se agrega actualiac�n de tabla Planta.>
-- =============================================

CREATE PROCEDURE DTW_SP_UpdateFieldsIndorama 	
AS
BEGIN
	
	BEGIN TRY

		ALTER TABLE Empleado
		ALTER COLUMN NumEmpleado NVARCHAR(10);

		ALTER TABLE Empleado
		ALTER COLUMN ApePaterno NVARCHAR(100);

		ALTER TABLE Empleado
		ALTER COLUMN ApeMaterno NVARCHAR(100);

		ALTER TABLE Empleado
		ALTER COLUMN Nombre NVARCHAR(100);

		IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'SFID' AND TABLE_NAME = 'Empleado')
		BEGIN
			ALTER TABLE Empleado
			ADD SFID NVARCHAR(10);
		END

		IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'Tipo' AND TABLE_NAME = 'Terminales')
		BEGIN
			ALTER TABLE Terminales
			ADD Tipo INT;
		END

		IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'Grupo' AND TABLE_NAME = 'Terminales')
		BEGIN
			ALTER TABLE Terminales
			ADD Grupo INT;
		END
		
		UPDATE Planta SET Descripcion = 'QUER�TARO FT' WHERE idPlanta = 3

	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
  		SET @ErrMsg = ERROR_MESSAGE()
  		SET @ErrSev = ERROR_SEVERITY()
  		SET @ErrSt  = ERROR_STATE()
  		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
END
GO

EXEC DTW_SP_UpdateFieldsIndorama