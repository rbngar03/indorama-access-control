USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_PagoComedor]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez V.
-- ALTER date: 16/11/2010 06:10 p.m.
-- Description:	Reporte de Credenciales pago a comedor (comidas) 
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_PagoComedor] 
	-- Add the parameters for the stored procedure here

@fecha_inicio varchar(30),
@fecha_fin varchar(30),
@tipo_usuario varchar(40)=null,
@compania varchar(50)=null,
@ccostos varchar(50)=null
AS
   declare @consulta nvarchar(max)
   declare @idCompanias varchar(20)
    declare @idCcostos varchar(20)
   declare @fecha1 varchar(50)
    declare @fecha2 varchar(50)
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	 SET NOCOUNT ON;
   SET @idCompanias=''
   SET @idCcostos=''
   Set @fecha1=convert(varchar(10),@fecha_inicio,103)
   Set @fecha2=convert(varchar(10),@fecha_fin,103)

  Set @fecha1=@fecha1 + ' 00:00:00'
  Set @fecha2=@fecha2 + ' 23:59:59'

  

   Set @consulta=('SELECT count(EntradaSalida) as ''Total de Comidas'' FROM Marcajes WHERE EntradaSalida=''C'' and AccesoOtorgado=1  and FechaRegistro between ''' + @fecha1 + ''' and ''' + @fecha2 + '''')

    IF (SELECT @tipo_usuario)='Empleados'
        BEGIN
            SET @consulta = (@consulta + ' AND (idTipoPersonal=1)')
        END
    ELSE IF (SELECT @tipo_usuario)='Contratistas'
		BEGIN
            SELECT @consulta = @consulta + ' AND (idTipoPersonal=2 or idTipoPersonal=4)' 
        END
    ELSE IF (SELECT @tipo_usuario)='Visitantes'
		BEGIN
            SELECT @consulta = @consulta + ' AND (idTipoPersonal=3)' 
        END

    IF (SELECT @compania) <>''
        BEGIN
			set @idCompanias=(SELECT idCompanias FROM Compania WHERE Compania=@compania)
            SELECT @consulta=@consulta + ' AND (idCompanias=''' + @idCompanias + ''')'
        END
 
    IF (SELECT @ccostos) <> ''
        BEGIN
			Set @idCcostos=(SELECT idCcostos FROM ccostos WHERE Ccostos=@ccostos)
			print @idCcostos
            SELECT @consulta=@consulta + ' AND (idCcostos=''' + @idCcostos + ''')'
		END
     exec (@consulta)
             
END

GO
