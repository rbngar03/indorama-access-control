USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_Comedor]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez V.
-- Create date: 19/11/2010 09:23 a.m.
-- Description:	Reporte de comedor 
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_Comedor] 
	-- Add the parameters for the stored procedure here

@fecha_inicio varchar(30),
@fecha_fin varchar(30),
@tipo_usuario varchar(40)=null,
@ccostos varchar(50)=null
AS
   declare @consulta nvarchar(max)
   declare @consulta2 varchar(100)
    declare @idCcostos varchar(20)
   declare @fecha varchar
   declare @fecha1 varchar(50)
    declare @fecha2 varchar(50)
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	 SET NOCOUNT ON;
   SET @idCcostos=''
   Set @fecha1=convert(varchar(10),@fecha_inicio,103)
   Set @fecha2=convert(varchar(10),@fecha_fin,103)

  Set @fecha1=@fecha1 + ' 00:00:00'
  Set @fecha2=@fecha2 + ' 23:59:59'

     Set @consulta=('SELECT convert(varchar(10),CAST(FLOOR(CAST(fecharegistro AS float)) AS datetime),105) fecha,nombrepersonal as ''Usuario'',count(entradasalida) as ''Cantidaddecomidas'',cc.Ccostos as ''Centro de Costos'' FROM MARCAJES mar,ccostos cc
                     WHERE mar.idCcostos=cc.idCcostos  and mar.EntradaSalida=''C'' and mar.AccesoOtorgado=1  and Fecharegistro between ''' + @fecha1 + ''' and ''' + @fecha2 + '''')
				     
	 Set @consulta2=('GROUP BY CAST(FLOOR(CAST(fecharegistro AS float)) AS datetime),nombrepersonal,ccostos')

    IF (SELECT @tipo_usuario)='Empleados'
        BEGIN
            SET @consulta = (@consulta + ' AND (idTipoPersonal=1)')
        END
    ELSE IF (SELECT @tipo_usuario)='Contratistas Residentes'
		BEGIN
            SELECT @consulta = @consulta + ' AND (idTipoPersonal=2)' 
        END
    ELSE IF (SELECT @tipo_usuario)='Contratistas Corto Plazo'
		BEGIN
            SELECT @consulta = @consulta + ' AND (idTipoPersonal=4)' 
        END
    ELSE IF (SELECT @tipo_usuario)='Visitantes'
		BEGIN
            SELECT @consulta = @consulta + ' AND (idTipoPersonal=3)' 
        END

     IF (SELECT @ccostos) <> ''
        BEGIN
			Set @idCcostos=(SELECT idCcostos FROM ccostos WHERE Ccostos=@ccostos)
			print @idCcostos
            SELECT @consulta=@consulta + ' AND (mar.idCcostos=''' + @idCcostos + ''')'
		END
 
     SELECT @consulta = @consulta + @consulta2 
     exec (@consulta)
             
END

GO
