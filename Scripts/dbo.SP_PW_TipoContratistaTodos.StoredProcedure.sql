USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_TipoContratistaTodos]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez
-- ALTER date: 01/11/2010 04:15 p.m.
-- Description:	Selección del catalogo de TipoContratista
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_TipoContratistaTodos] 
	
AS
BEGIN
	SET NOCOUNT ON;
    -- Insert statements for procedure here
SELECT idTipoContratista,TipoContratista
FROM TipoContratista

END

GO
