USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_TerminalesActualizar]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Debi Sanchez
-- ALTER date: 28-10-2010
-- Description: Actualiza un registro nuevo de usuario.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_TerminalesActualizar]

@idTerminales INT,
@Clave VARCHAR(50),
@Ubicacion VARCHAR(50),
@IP VARCHAR(50),
@Nombre VARCHAR(50)


AS
BEGIN

    UPDATE Terminales set Clave=@Clave, Ubicacion=@Ubicacion, IP=@IP,Nombre=@Nombre
    WHERE (idTerminales = @idTerminales)
END
GO
