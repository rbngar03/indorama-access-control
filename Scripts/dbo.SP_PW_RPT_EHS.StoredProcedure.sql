USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_RPT_EHS]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Héctor Romero
-- ALTER date: 29-10-2010
-- Description: Modificacion de empleados.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_RPT_EHS]
@fechaInicio datetime,
@fechaFinal datetime,
@clasificacion int
AS
BEGIN
SELECT     Clasificacion, Ccostos, COUNT(idContratistas) AS num_contratistas, CAST(SUM(SegundosCubiertos) / 3600 AS decimal(18, 2)) 
          AS horas_cubiertas
FROM         v_rpt_ehs
WHERE     (FechaRegistro BETWEEN @fechaInicio AND DATEADD (minute,1439 , @fechaFinal)) 
	and (idclasificacion=@clasificacion)
GROUP BY Clasificacion, Ccostos
END
GO
