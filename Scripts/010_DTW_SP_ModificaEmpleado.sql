IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_ModificaEmpleado')
  BEGIN
	DROP PROCEDURE DTW_SP_ModificaEmpleado
  END
GO
-- =============================================
-- Author:		Karla Ramirez
-- Create date: 27/08/24
-- Description:	Script que actualiza datos de los Empleados
-- History:
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_ModificaEmpleado]
	@pIdEmpleado INT,
	@pNumEmpleado VARCHAR(10), 
    @pApePaterno VARCHAR(100),
	@pApeMaterno VARCHAR(100),
	@pNombre VARCHAR(100),
	@pidCcostos INT,
	@pidEstatus INT,
	@pGafete VARCHAR(5),
	--@pFoto IMAGE,
	@pNumeroProvisionalGafete VARCHAR(5),
	@pidPlanta INT,
	@Response VARCHAR(100) OUTPUT

AS
BEGIN
	DECLARE @IdEstatusAct INT, @NumEmpleadoAct VARCHAR(10),
            @CcostosAct INT, @GafeteAct INT, @IdGafete INT,
			@IdGafeteAct INT, @GafeteProvAct INT, @IdGafeteProv INT

	BEGIN TRY 
		IF @pGafete = 0 SET @pGafete = NULL
		IF @pNumeroProvisionalGafete IS NOT NULL
			SET @pNumeroProvisionalGafete = CONCAT('%', @pNumeroProvisionalGafete, '%')
       

		BEGIN TRAN

		-- Se obtienen valores actuales 
		SET @IdEstatusAct = (SELECT idEstatus FROM Empleado WHERE idEmpleado = @pIdEmpleado)
		SET @NumEmpleadoAct = (SELECT NumEmpleado FROM Empleado WHERE idEmpleado = @pIdEmpleado)
		SET @CcostosAct = (SELECT idCcostos FROM Empleado WHERE idEmpleado = @pIdEmpleado)--CENTRO DE COSTOS ACTUAL 
		SET @GafeteAct = (SELECT idGafete FROM Empleado WHERE idEmpleado = @pIdEmpleado)--NUMERO DE GAFETE
		SET @IdGafete = (SELECT idGafete FROM Gafete WHERE Gafete = @pGafete AND idTipoGafete = 1 AND idTipoPersonal = 1)--ID
		SET @GafeteProvAct = (SELECT NumeroProvisionalGafete FROM Empleado WHERE idEmpleado = @pIdEmpleado)--NUMERO DE GAFETE
		SET @IdGafeteProv = (SELECT idGafete FROM Gafete WHERE Gafete = @pNumeroProvisionalGafete AND idTipoGafete = 2 AND idTipoPersonal = 1)--ID

		   -- Validaciones y actualizaciones
		IF @pidEstatus = 2
		BEGIN
			UPDATE Gafete
            SET FechaVigencia = NULL, idEstatus = 2
            WHERE idGafete IN (@GafeteAct, @IdGafeteProv);
			
			UPDATE Empleado 
			SET idEstatus = @pidEstatus, 
			--Foto = isnull(@pFoto,Foto), 
			NumeroProvisionalGafete = NULL
			WHERE idEmpleado = @pIdEmpleado   
		END
	    ELSE IF @pidEstatus = 1
	    BEGIN
			IF EXISTS (SELECT 1 FROM Empleado WHERE NumEmpleado = @pNumEmpleado AND idEmpleado <> @pIdEmpleado)
            BEGIN
                SET @Response = 'El numero de empleado esta asignado a otro empleado';
                RAISERROR('El numero de empleado esta asignado a otro Empleado', 16, 1);
                ROLLBACK TRAN;
                RETURN;
            END

	         -- Actualizacion de gafete --
			 IF(@pGafete IS NOT NULL)
		     BEGIN
		        IF (@IdGafete IS NULL)
			    BEGIN 
					SET @Response = 'No existe el gafete';
					RAISERROR('No existe gafete', 16, 1)
					ROLLBACK TRAN;
					RETURN
			    END
		 
				IF EXISTS (SELECT 1 FROM Gafete WHERE Gafete = @pGafete AND idEstatus = 1) AND 
				   NOT EXISTS (SELECT 1 FROM Empleado  WHERE  idGafete = @IdGafete AND  idEmpleado <> @pIdEmpleado 
				   OR idEmpleado = @pIdEmpleado ) 
		           BEGIN
				   SET @Response = 'El gafete esta asignado a otro empleado';
				   RAISERROR('El gafete esta asignado a otro Empleado', 16, 1)
				   ROLLBACK TRAN;
				   RETURN
				END
		 	
				IF  (@pGafete IS NOT NULL AND @pGafete <> @GafeteAct)
				 BEGIN
					SET @IdGafeteAct = (SELECT idGafete FROM Gafete WHERE idGafete IN (SELECT idGafete FROM Empleado WHERE idEmpleado = @pIdEmpleado))
					UPDATE Gafete SET idEstatus = 2 WHERE idGafete = @IdGafeteAct
					UPDATE Gafete SET idEstatus = 1, FechaInicio = GETDATE() WHERE idGafete = @IdGafete
					UPDATE Empleado SET idGafete = @IdGafete WHERE idEmpleado = @pIdEmpleado
				 END
			 END

		    -- Actualizacion de gafete Provisional --
		IF (@pNumeroProvisionalGafete IS NOT NULL)
		BEGIN
		   IF EXISTS (SELECT 1 FROM Gafete WHERE Gafete = 2170 and idEstatus = 1) AND
		      EXISTS (SELECT 1 FROM Empleado WHERE  NumeroProvisionalGafete = @pNumeroProvisionalGafete AND idEmpleado <> @pIdEmpleado 
			  AND idEmpleado = @pIdEmpleado)
		   BEGIN
			   SET @Response = 'Gafete provisional ya esta asignado a otro empleado';
			   RAISERROR('Gafete provisional ya esta asignado a otro empleado', 16, 1)
			   ROLLBACK TRAN;
			   RETURN
		   END

		   IF  (@pNumeroProvisionalGafete IS NOT NULL AND @pNumeroProvisionalGafete <> @GafeteProvAct)
			 BEGIN
				DECLARE @IdNumProvGafeteNew INT 
				SET @IdNumProvGafeteNew = (SELECT idGafete FROM Gafete WHERE Gafete = @pNumeroProvisionalGafete)
				UPDATE Gafete SET idEstatus = 2 WHERE idGafete = @IdGafeteProv
				UPDATE Gafete SET idEstatus = 1, FechaInicio = GETDATE() WHERE idGafete = @IdNumProvGafeteNew
				UPDATE Empleado SET NumeroProvisionalGafete = @IdNumProvGafeteNew WHERE idEmpleado = @pIdEmpleado
			END
	      END
		 -- Actualizacion de Centro de Costos 
		  IF @pidCcostos <> @CcostosAct
		  BEGIN
			IF EXISTS (SELECT 1 FROM ccostos WHERE IdCcostos = @pidCcostos)
			UPDATE Empleado SET idCcostos = @pidCcostos WHERE idEmpleado = @pIdEmpleado
			ELSE
			BEGIN
				SET @Response = 'El centro de Costos no existe';
				RAISERROR('El centro de Costos no existe', 16, 1)
			    ROLLBACK TRAN;
			    RETURN
			END
		  END 
		END

		-- Actualizacion de datos generales --
		UPDATE [dbo].[Empleado]
		   SET [ApePaterno] = @pApePaterno,
			   [ApeMaterno] = ISNULL(@pApeMaterno,NULL),
			   [Nombre] = @pNombre,
			   [idTipoNomina] = NULL,
			   [NominaProcesada] = '',
			   [idEstatus] = @pidEstatus,
			   -- [Foto] = ISNULL(@pFoto,Foto),
			   [Notas] = NULL,
			   [idPlanta] = @pidPlanta,
			   [fecha_actualizacion] = GETDATE(),
			   [SFID] = NULL
		 WHERE idEmpleado = @pIdEmpleado
		 
	 COMMIT TRAN
	 SET @Response = 'Ok'; 
	
   END TRY
   BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRAN;
			DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
			SET @ErrMsg = ERROR_MESSAGE()
			SET @ErrSev = ERROR_SEVERITY()
			SET @ErrSt = ERROR_STATE()
			RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
 END
