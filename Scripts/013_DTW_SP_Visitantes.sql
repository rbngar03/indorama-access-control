IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_Visitantes')
  BEGIN
	DROP PROCEDURE DTW_SP_Visitantes
  END
GO

-- =============================================
-- Author:		Karla Ramirez
-- Create date: 10/06/24
-- Description:	Script que obtiene la consulta de Visitantes
-- History:
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_Visitantes](
@pStatus INT = NULL, -- {1-Activo / 2-Inactivo}
@pIdVisitante INT = NULL,
@pEmpresaVisitante VARCHAR (150) = NULL,
@pCentroCostos VARCHAR(50) = NULL,
@pFiltro VARCHAR(100) = NULL,
@pPAG INT = 0, -- Pagina inicial (0 para la primera pagina)
@pREG INT = 50  -- Numero de registros por pagina
)
AS
BEGIN 
	BEGIN TRY 
	IF @pStatus = 0
	SET @pStatus = NULL

	IF @pIdVisitante = 0
	SET @pIdVisitante = NULL

	IF @pEmpresaVisitante IS NOT NULL
	SET @pEmpresaVisitante = CONCAT('%', @pEmpresaVisitante, '%')

	IF @pCentroCostos IS NOT NULL
	SET @pCentroCostos = CONCAT('%', @pCentroCostos, '%')

	IF @pFiltro = ''
		SET @pFiltro = NULL

	IF @pPAG <> 0
		SET @pPAG = @pPAG

	IF @pREG = 0
		SET @pREG = 50

    IF @pFiltro IS NOT NULL
		SET @pFiltro = CONCAT('%', @pFiltro, '%')

	SELECT T0.idVisitantes, T0.NombreVisitante, T0.RFC, T5.idEstatus AS Estatus, ISNULL(T2.Gafete,0) AS Gafete, T4.idCcostos, T0.EmpResponsable, T0.DescOrigen, T0.idComedor AS DerechoComedor, 
	FORMAT(CAST(T0.FechaIngreso AS datetime), 'dd/MM/yyyy') AS FechaIngreso, ISNULL(T0.Horas,' ') AS Horas
	FROM Visitantes AS T0
	LEFT JOIN Origen AS T1 ON T1.idOrigen = T0.idOrigen
	LEFT JOIN Comedor AS T3 ON T3.idComedor = T0.idComedor
	LEFT JOIN ccostos AS T4 ON T4.idCcostos = T0.idCcostos
	LEFT JOIN Estatus AS T5 ON T5.idEstatus = T0.idEstatus
	LEFT JOIN Gafete AS T2 ON T2.idGafete = T0.idGafete
	WHERE (@pStatus IS NULL OR (T0.idEstatus = @pStatus))
	AND (@pIdVisitante IS NULL OR (T0.idVisitantes = @pIdVisitante))
	AND (@pEmpresaVisitante IS NULL OR (T0.DescOrigen LIKE @pEmpresaVisitante))
	AND (@pCentroCostos IS NULL OR (T4.Ccostos LIKE @pCentroCostos))
	AND (@pFiltro IS NULL
			OR T0.NombreVisitante LIKE @pFiltro
			OR T0.RFC LIKE @pFiltro
			OR T5.Estatus LIKE @pFiltro
			OR T2.Gafete LIKE @pFiltro
			OR T4.Ccostos LIKE @pFiltro
			OR T0.EmpResponsable LIKE @pFiltro
			OR T0.DescOrigen LIKE @pFiltro)
	ORDER BY T0.idEstatus ASC, T0.idVisitantes DESC
	OFFSET @pPAG * @pREG ROWS
	FETCH NEXT @pREG ROWS ONLY


END TRY
	BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
  		SET @ErrMsg = ERROR_MESSAGE()
  		SET @ErrSev = ERROR_SEVERITY()
  		SET @ErrSt  = ERROR_STATE()
  		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
END
