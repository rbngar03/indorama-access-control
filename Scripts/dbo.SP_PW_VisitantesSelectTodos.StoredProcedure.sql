USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_VisitantesSelectTodos]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez V.
-- ALTER date: 29/10/2010 8:30 pm
-- Description:	Se crea el sp para consulta de los visitantes
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_VisitantesSelectTodos] 
AS
BEGIN
	SET NOCOUNT ON;
    -- Insert statements for procedure here



SELECT  idVisitantes,FechaIndSeguridad,FechaIngreso,FechaTerminacion,Horas,NombreVisitante,
        (SELECT Origen
         FROM Origen 
         WHERE (idOrigen = vis.idOrigen)) AS Origen,Foto,
        (SELECT Gafete
          FROM Gafete 
          WHERE(idGafete = vis.idGafete)) AS Gafete,EmpResponsable,Notas,RFC,
         (SELECT DerechoComedor
          FROM Comedor
          WHERE (idComedor = vis.idComedor)) AS DerechoComedor,
         (SELECT Ccostos
          FROM ccostos 
          WHERE (idCcostos = vis.idCcostos)) AS Ccostos,ArchivoAdjunto,
          (SELECT Estatus
           FROM Estatus 
           WHERE (idEstatus = vis.idEstatus)) AS Estatus
FROM Visitantes AS vis

END
GO
