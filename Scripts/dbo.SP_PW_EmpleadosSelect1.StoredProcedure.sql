USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_EmpleadosSelect1]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_EmpleadosSelect1] 
	-- Add the parameters for the stored procedure here
@pidEmpleado int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select em.NumEmpleado, em.idEstatus,em.apePaterno,em.apematerno,
em.nombre,em.foto,
(select t1.tiponomina from tiponomina as t1 where t1.idtiponomina=em.idtiponomina) as TipoNomina,
(select cc1.ccostos from ccostos as cc1 where cc1.idccostos=em.idccostos) as CentroCostos,
em.NominaProcesada,
(select g1.Gafete from gafete as g1 where g1.idgafete=em.idgafete) as Gafete,
(select g1.Gafete from gafete as g1 where g1.idgafete=em.NumeroProvisionalGafete) as GafeteProvisional,
em.Notas,
(select p.Descripcion from Planta as p where p.idPlanta=em.idPlanta) as Planta
from empleado as em
WHERE em.idEmpleado=@pidEmpleado
END

GO
