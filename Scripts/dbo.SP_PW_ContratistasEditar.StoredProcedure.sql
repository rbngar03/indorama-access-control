USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_ContratistasEditar]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Héctor Romero Badillo.
-- Create date: 02/11/2010
-- Description: Modificacion de Contratistas.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_ContratistasEditar]

@idContratistas int,
@idEstatus int,
@FechaIndSeguridad datetime,
@Foto image= null,
@RFC nvarchar(15),
@idClasificacion int= null,
@idCcostos int= null,
@ResponsableInvista nvarchar(150)= null,
@Gafete nvarchar(5)=null,
@NumeroProvisionalGafete nvarchar(7)= null,
@idComedor int =null,
@Notas nvarchar(50)= null,
@FechaBaja datetime= null,
@ArchivoAdjunto nvarchar(100)= null,
@VigenciaIMSS datetime,
@VigenciaSeguridad datetime,
@idPlanta int

AS
DECLARE @idTipoContratista int
DECLARE @idGafete int
DECLARE @idGafeteAnte int
DECLARE @idGafeteP int
DECLARE @idGafeteAnteP int

BEGIN
BEGIN TRY
 BEGIN TRAN;
----------Gafetes
set @idTipoContratista=(select idTipoContratista from Contratistas where idContratistas=@idContratistas)

if (@idTipoContratista=1)
begin
	set @idGafete=(select idgafete from gafete where gafete=@Gafete and idtipogafete=1 and idTipoPersonal=4)
	set @idGafeteP=(select idgafete from gafete where gafete=@NumeroProvisionalGafete and idtipogafete=2 and idTipoPersonal=4)
end
else if (@idTipoContratista=2)
begin
	set @idGafete=(select idgafete from gafete where gafete=@Gafete and idtipogafete=1 and idTipoPersonal=2)
	set @idGafeteP=(select idgafete from gafete where gafete=@NumeroProvisionalGafete and idtipogafete=2 and idTipoPersonal=2)
end
set @idGafeteAnte=(select idGafete from Contratistas  where idContratistas = @idContratistas)
set @idGafeteAnteP=(select idGafete from Contratistas  where idContratistas = @idContratistas)

----------Registro de RFC del Contratista
 --if ((SELECT count(RFC) FROM Contratistas WHERE (RFC=@RFC and (idContratistas!=@idContratistas)) >0 )
if exists (SELECT RFC FROM Contratistas WHERE (RFC=@RFC and idContratistas!=@idContratistas))
	BEGIN
	    ROLLBACK TRAN;
		RAISERROR('Ya existe el registro del RFC del Contratista', 16, 1)
		return 
    END
else
begin
----------Estatus del Contratista
if (@idEstatus=2)
begin
		update gafete set fechainicio=null,fechavigencia=null, idestatus=2 where idgafete=@idGafeteAnte
		update gafete set fechainicio=null, idestatus=2 where idgafete=@idGafeteAnteP
		UPDATE Contratistas
		set idEstatus=@idEstatus,
			FechaIndSeguridad=@FechaIndSeguridad,
			Foto=isnull(@Foto,Foto),
			RFC=@RFC,
			idClasificacion=@idClasificacion,
			idCcostos=isnull(@idCcostos,idCcostos),
			ResponsableInvista=isnull(@ResponsableInvista,ResponsableInvista),
			idGafete=@idGafete,
			--NumeroProvisionalGafete=isnull(@idGafeteP,NumeroProvisionalGafete),
			NumeroProvisionalGafete=isnull(@idGafeteP,NumeroProvisionalGafete),
			idComedor=isnull(@idComedor,idComedor),
			Notas=isnull(@Notas,Notas),
			FechaBaja=(@FechaBaja), 
			ArchivoAdjunto=isnull(@ArchivoAdjunto,ArchivoAdjunto),
            VigenciaIMSS=(@VigenciaIMSS),
            VigenciaSeguridad=(@VigenciaSeguridad),
			idPlanta=@idPlanta
			WHERE idContratistas = @idContratistas
end
else if (@idEstatus=1)
begin

----------Inicio Gafete
begin
if (@Gafete is not null)
begin
--set @idGafete=(select idgafete from gafete where gafete=@Gafete and idtipogafete=1 and idTipoPersonal=2)
	if (@idGafete is null)
		begin
		ROLLBACK TRAN;
		RAISERROR('No existe Gafete', 16, 1)
		return 
	end
	ELSE
	begin 
		if ((select count(idgafete) from gafete where idgafete=@idgafete and idestatus=1)>0 and
		(select count(idContratistas) from Contratistas where idContratistas = @idContratistas and idgafete=@idGafete)=0)
			begin
			ROLLBACK TRAN;
			RAISERROR('El Gafete esta asignado a otro Contratista', 16, 1)
			return 
			end
	end
end

--SET @idGafeteAnte=(select idGafete from Contratistas  where idContratistas = @idContratistas)
 if (@idGafeteAnte is not null and @idGafeteAnte<>@idGafete)
	begin
		update gafete set idestatus=2 where idgafete=@idGafeteAnte
		update gafete set idestatus=1, fechainicio=getdate() where idgafete=@idGafete
	end
else
	begin
		update gafete set idestatus=2 where idgafete=@idGafeteAnte
	end
end
----------Fin Gafete

------------Inicio Gafete Provisional
begin
if (@NumeroProvisionalGafete is not null)
begin
--set @idGafeteP=(select idgafete from gafete where gafete=@NumeroProvisionalGafete and idtipogafete=2 and idTipoPersonal=2)
	if (@idGafeteP is null)
		begin
		ROLLBACK TRAN;
		RAISERROR('No existe Gafete Provisional', 16, 1)
		return 
	end
	ELSE
	begin 
		if ((select count(idgafete) from gafete where idgafete=@idGafeteP and idestatus=1)>0 
        and
        (select count(idContratistas) from Contratistas where idContratistas = @idContratistas and NumeroProvisionalGafete=@idGafeteP)=0)
     		
			begin
			ROLLBACK TRAN;
			RAISERROR('El Gafete Provisional esta asignado a otro Contratista', 16, 1)
			return 
			end
	end
end

--SET @idGafeteAnteP=(select idGafete from Contratistas  where idContratistas = @idContratistas)
 if (@idGafeteAnteP is not null and @idGafeteAnteP<>@idGafeteP)
	begin
		update gafete set idestatus=2 where idgafete=@idGafeteAnteP
		update gafete set idestatus=1, fechainicio=getdate(),fechavigencia=getdate()  where idgafete=@idGafeteP
	end
else
	begin
		update gafete set idestatus=2 where idgafete=@idGafeteAnteP
	end
end
----------Fin Gafete Provisional


     UPDATE Contratistas
     set idEstatus=@idEstatus,
         FechaIndSeguridad=@FechaIndSeguridad,
         Foto=isnull(@Foto,Foto),
	     RFC=@RFC,
         idClasificacion=@idClasificacion,
         idCcostos=isnull(@idCcostos,idCcostos),
         ResponsableInvista=isnull(@ResponsableInvista,ResponsableInvista),
         idGafete=@idGafete,
	     --NumeroProvisionalGafete=isnull(@idGafeteP,NumeroProvisionalGafete),
         NumeroProvisionalGafete=isnull(@idGafeteP,NumeroProvisionalGafete),
	     idComedor=isnull(@idComedor,idComedor),
         Notas=isnull(@Notas,Notas),
         FechaBaja=(@FechaBaja), 
         ArchivoAdjunto=isnull(@ArchivoAdjunto,ArchivoAdjunto),
		 VigenciaIMSS=(@VigenciaIMSS),
         VigenciaSeguridad=(@VigenciaSeguridad),
		 idPlanta=@idPlanta
     WHERE idContratistas = @idContratistas
end
end

	COMMIT TRAN;
END TRY
BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN;
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrSev = ERROR_SEVERITY()
		SET @ErrSt = ERROR_STATE()
		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
END CATCH
END
GO
