USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_ContResidGafeteProvisional]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Héctor Romero Badillo>
-- Create date: <29/10/2010,>
-- Description:	<Credenciales Provisionales de Empleados>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_ContResidGafeteProvisional] 
	-- Add the parameters for the stored procedure here
@pidContratistas int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	/*select gafete from gafete where idgafete=(select NumeroProvisionalGafete 
	from contratistas where idContratistas=@pidContratistas)*/

	select
    ga.gafete,
	pl.descripcion  as planta
	from contratistas as c, gafete as ga ,planta as pl
	where ga.idgafete=c.NumeroProvisionalGafete and pl.idPlanta=c.idPlanta
		and c.idContratistas=@pidContratistas


END

GO
