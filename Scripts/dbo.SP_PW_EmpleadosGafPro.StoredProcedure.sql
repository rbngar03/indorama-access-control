USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_EmpleadosGafPro]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Hector Romero Badillo>
-- ALTER date: <01/11/2010>
-- Description:	<Gafete empleados Provisional>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_EmpleadosGafPro] 
	-- Add the parameters for the stored procedure here
@pNumEmpleado nvarchar(7)
AS
DECLARE @idEmpleado int
BEGIN
BEGIN TRY
 
set @idEmpleado=(select idempleado from empleado where numempleado=@pNUmEmpleado)

if (@idEmpleado is null)
	begin
		RAISERROR('No existe Empleado', 16, 1)
		return 
	end
else
begin
  if((select idestatus from empleado where idempleado=@idEmpleado)=2)
	begin
		RAISERROR('El Empleado esta Inactivo', 16, 1)
		return 
	end
   else
	begin 
		select em.idempleado,
			(select gafete from gafete where idgafete=em.NumeroProvisionalGafete and idTipoGafete=2 and idtipopersonal=1) as gafete,
			(select idestatus from gafete where idgafete=em.NumeroProvisionalGafete and idTipoGafete=2 and idtipopersonal=1) as estatusgafete ,
			(select convert(VARCHAR(10),FechaVigencia,105) from gafete where idgafete=em.NumeroProvisionalGafete and idTipoGafete=2 and idtipopersonal=1) as fechaVigencia,
		rtrim(em.Nombre + ' ' + em.apePaterno + ' ' + isnull(em.apematerno,' ')) as Nombre
		from empleado as em where em.idempleado=@idEmpleado
	end
end
END TRY
BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrSev = ERROR_SEVERITY()
		SET @ErrSt = ERROR_STATE()
		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
		return
END CATCH
END

GO
