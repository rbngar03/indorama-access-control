USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_InterfaceVisitanteGafeteProv]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Héctor Romero Badillo
-- ALTER date: 16/11/2010 01:03:40 pm
-- Description:	Actualización de gafetes provisionales de contratistas
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_InterfaceVisitanteGafeteProv] 
	
AS
	declare @RFC nvarchar(15)
	declare @fecha_actualizacion datetime
	declare @idGafete nvarchar(5)
	declare @idGafeteP nvarchar(5)

BEGIN
BEGIN TRY
BEGIN TRAN;
   
    declare  conteo Cursor For
    SELECT idgafete 
    FROM  gafete
    WHERE idestatus=1 and idtipogafete=2 and idtipopersonal=3 and
    fechaVigencia<getdate()
    ORDER BY idgafete
   
	Open conteo
	fetch next from conteo
	into @idGafeteP
		
	while @@FETCH_STATUS=0
    BEGIN
		
		set @RFC=(select RFC from visitantes where rtrim(idGafete)=rtrim(@idGafeteP))
		--Actualiza la fecha de vigencia la fecha inicio a null y el estatus a inactivo del gafete
		UPDATE Gafete set FechaVigencia=null,FechaInicio=null,idEstatus=2 WHERE idGafete=@idGafeteP
		--Actualiza el campo Numero Provisional de Gafete a null para el empleado
		UPDATE visitantes set idGafete=null WHERE RFC=@RFC

      fetch next from conteo
	  into @idGafeteP
    END
	Close conteo
	deallocate conteo

COMMIT TRAN;
END TRY
BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN;
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrSev = ERROR_SEVERITY()
		SET @ErrSt = ERROR_STATE()
		insert into LogError(ErrMsg,ErrSev,ErrSt) values (@ErrMsg,@ErrSev,@ErrSt) 
		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
END CATCH
END
GO
