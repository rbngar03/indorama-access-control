  UPDATE Usuarios SET id_perfil = NULL
  DELETE FROM [PerfilesModulos]
  DELETE FROM [CamposPerfil]
  DELETE FROM [Acciones]
  DELETE FROM [Modulos]
  DELETE FROM [Perfiles]
  GO

  DBCC CHECKIDENT ('Acciones', RESEED, 0)
  DBCC CHECKIDENT ('Modulos', RESEED, 0)
  DBCC CHECKIDENT ('CamposPerfil', RESEED, 0)
  DBCC CHECKIDENT ('Perfiles', RESEED, 0)
  GO
  
  INSERT INTO [Perfiles] VALUES
  ('ADMIN','ADMINISTRADOR'),
  ('RH','RECURSOS HUMANOS'),
  ('VIGILANCIA','VIGILANCIA');

  INSERT INTO [Modulos] VALUES
  ('Catálogo de Contratistas',1,NULL,1,1),
  ('Catálogo de Empleados',1,NULL,1,1),
  ('Catálogo de perfiles',1,NULL,1,1),
  ('Catálogo de Terminales',1,NULL,1,1),
  ('Catálogo de Usuarios',1,NULL,1,1),
  ('Catálogo de Visitantes',1,NULL,1,1),
  ('Empleados Permanentes',2,NULL,2,1),
  ('Empleados Provisional',2,NULL,2,1),
  ('Contratistas Provisional',2,NULL,2,1),
  ('Contratistas Permanentes',2,NULL,2,1),
  ('Contratistas Corto Plazo Mayor',2,NULL,2,1),
  ('Contratistas Corto Plazo Menor',2,NULL,2,1),
  ('Visitantes',2,NULL,2,1),
  ('Parametrizaciones',4,NULL,2,1),
  ('Reporte de Contratistas',3,NULL,3,1),
  ('Reporte de empleados',3,NULL,3,1),
  ('Reporte No. comidas por turnos',3,NULL,3,1),
  ('Reporte General Comedor',3,NULL,3,1)
  GO

  INSERT INTO [Acciones] VALUES
  ('VER'),
  ('AGREGAR'),
  ('EDITAR'),
  ('ELIMINAR'),
  ('EXPORTAR'),
  ('FILTRAR')
  GO


  DECLARE @ModulesTable TABLE (Id INT, Orden INT)
  DECLARE @ProfilesTable TABLE (Id INT, Nombre VARCHAR(40))
  DECLARE @ActionsTable TABLE (Id INT, Nombre VARCHAR(20))
  DECLARE @Module INT
  DECLARE @Order INT
  DECLARE @IdProfile  INT
  DECLARE @IdAction  INT


  INSERT INTO @ProfilesTable SELECT id_perfil, descripcion FROM Perfiles
  WHILE EXISTS (SELECT * FROM @ProfilesTable)
  BEGIN
	SET @IdProfile = (SELECT TOP 1 Id FROM @ProfilesTable ORDER BY Id ASC)
	INSERT INTO @ModulesTable SELECT id_modulo, orden FROM Modulos
		WHILE EXISTS (SELECT * FROM @ModulesTable)
		BEGIN
			SET @Module = (SELECT TOP 1 Id FROM @ModulesTable ORDER BY Id ASC)
			SET @Order = (SELECT TOP 1 Orden FROM @ModulesTable WHERE Id = @Module)
			IF @Order = 1
			BEGIN 
				INSERT INTO @ActionsTable SELECT id_accion, nombre FROM Acciones
				WHILE EXISTS (SELECT * FROM @ActionsTable)
				BEGIN
					SET @IdAction = (SELECT TOP 1 Id FROM @ActionsTable ORDER BY Id DESC)
					INSERT INTO PerfilesModulos VALUES
					(@IdProfile, @Module, (@IdAction))
					DELETE FROM @ActionsTable WHERE Id = @IdAction
				END
			END
			IF @Order = 2
			BEGIN 
				INSERT INTO PerfilesModulos VALUES
				(@IdProfile,@Module, (SELECT id_accion FROM Acciones WHERE nombre = 'VER')),
				(@IdProfile,@Module, (SELECT id_accion FROM Acciones WHERE nombre = 'EDITAR'))
			END
			IF @Order = 3
			BEGIN 
				INSERT INTO PerfilesModulos VALUES
				(@IdProfile,@Module, (SELECT id_accion FROM Acciones WHERE nombre = 'VER')),
				(@IdProfile,@Module, (SELECT id_accion FROM Acciones WHERE nombre = 'EXPORTAR')),
				(@IdProfile,@Module, (SELECT id_accion FROM Acciones WHERE nombre = 'FILTRAR'))
			END
			DELETE FROM @ModulesTable WHERE Id = @Module
		END
		DELETE FROM @ProfilesTable WHERE Id = @IdProfile
	END
	GO

	DECLARE @IdRH  INT
	DECLARE @IdVig  INT

	SET @IdRH = (SELECT id_perfil FROM Perfiles WHERE clave = 'RH')
	SET @IdVig = (SELECT id_perfil FROM Perfiles WHERE clave = 'VIGILANCIA')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdRH AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Catálogo de Usuarios')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdRH AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Catálogo de Visitantes')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdRH AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Catálogo de Terminales')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdRH AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Catálogo de perfiles')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdRH AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Empleados Provisional')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdRH AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Contratistas Provisional')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdRH AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Contratistas Permanentes')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdRH AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Contratistas Corto Plazo Mayor')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdRH AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Contratistas Corto Plazo Menor')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdRH AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Visitantes')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdRH AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Parametrizaciones')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdVig AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Catálogo de Usuarios')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdVig AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Catálogo de Empleados')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdVig AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Catálogo de Terminales')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdVig AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Catálogo de perfiles')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdVig AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Reporte de Contratistas')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdVig AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Reporte de empleados')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdVig AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Reporte No. comidas por turnos')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdVig AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Reporte General Comedor')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdVig AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Empleados Permanentes')
	DELETE FROM PerfilesModulos WHERE id_perfil = @IdVig AND id_modulo = (SELECT id_modulo FROM Modulos WHERE nombre = 'Parametrizaciones')
	GO

	DECLARE @IdModule INT
	DECLARE @IdVig  INT
	DECLARE @IdRH  INT
	DECLARE @IdAdmin  INT
	SET @IdRH = (SELECT id_perfil FROM Perfiles WHERE clave = 'RH');
	SET @IdVig = (SELECT id_perfil FROM Perfiles WHERE clave = 'VIGILANCIA');
	SET @IdAdmin = (SELECT id_perfil FROM Perfiles WHERE clave = 'ADMIN');
	SET @IdModule = (SELECT id_modulo FROM Modulos WHERE nombre = 'Catálogo de Empleados');
	INSERT INTO CamposPerfil VALUES
	(@IdModule, @IdRH, 'No. Empleado', 0),
	(@IdModule, @IdRH, 'Estatus', 1),
	(@IdModule, @IdRH, 'Apellido Paterno', 0),
	(@IdModule, @IdRH, 'Apellido Materno', 0),
	(@IdModule, @IdRH, 'Nombres(s)', 0),
	(@IdModule, @IdRH, 'Archivo Foto', 1),
	(@IdModule, @IdRH, 'Activar Cámara', 1),
	(@IdModule, @IdRH, 'No. Gafete Empleado', 1),
	(@IdModule, @IdRH, 'Centro de Costos', 0),
	(@IdModule, @IdRH, 'Planta', 0),
	(@IdModule, @IdRH, 'No. Gafete provisional', 0),
	(@IdModule, @IdAdmin, 'No. Empleado', 1),
	(@IdModule, @IdAdmin, 'Estatus', 1),
	(@IdModule, @IdAdmin, 'Apellido Paterno', 1),
	(@IdModule, @IdAdmin, 'Apellido Materno', 1),
	(@IdModule, @IdAdmin, 'Nombres(s)', 1),
	(@IdModule, @IdAdmin, 'Archivo Foto', 1),
	(@IdModule, @IdAdmin, 'Activar Cámara', 1),
	(@IdModule, @IdAdmin, 'No. Gafete Empleado', 1),
	(@IdModule, @IdAdmin, 'Centro de Costos', 1),
	(@IdModule, @IdAdmin, 'Planta', 1),
	(@IdModule, @IdAdmin, 'No. Gafete provisional', 1);

	SET @IdModule = (SELECT id_modulo FROM Modulos WHERE nombre= 'Catálogo de Contratistas');
	INSERT INTO CamposPerfil VALUES
	(@IdModule, @IdRH, 'Tipo Contratista',0),
	(@IdModule, @IdRH, 'Estatus',1),
	(@IdModule, @IdRH, 'Nombre Contratista',0),
	(@IdModule, @IdRH, 'Apellido Paterno',0),
	(@IdModule, @IdRH, 'Apellido Materno',0),
	(@IdModule, @IdRH, 'Fecha Inducción',1),
	(@IdModule, @IdRH, 'Fecha Inicio Contrato',0),
	(@IdModule, @IdRH, 'Archivo Foto -Selección',1),
	(@IdModule, @IdRH, 'Activar Cámara',1),
	(@IdModule, @IdRH, 'Número IMSS',0),
	(@IdModule, @IdRH, 'Responsable Indorama',1),
	(@IdModule, @IdRH, 'Número Contrato',0),
	(@IdModule, @IdRH, 'RFC',0),
	(@IdModule, @IdRH, 'Clasificación',0),
	(@IdModule, @IdRH, 'Número Gafete',0),
	(@IdModule, @IdRH, 'Vigencia IMSS',1),
	(@IdModule, @IdRH, 'Nombre Clasificación',0),
	(@IdModule, @IdRH, 'Comedor',1),
	(@IdModule, @IdRH, 'Terminación Contrato',0),
	(@IdModule, @IdRH, 'Empresa Contratista',1),
	(@IdModule, @IdRH, 'Centro de Costos',1),
	(@IdModule, @IdVig, 'Tipo Contratista',0),
	(@IdModule, @IdVig, 'Estatus',1),
	(@IdModule, @IdVig, 'Nombre Contratista',0),
	(@IdModule, @IdVig, 'Apellido Paterno',0),
	(@IdModule, @IdVig, 'Apellido Materno',0),
	(@IdModule, @IdVig, 'Fecha Inducción',1),
	(@IdModule, @IdVig, 'Fecha Inicio Contrato',0),
	(@IdModule, @IdVig, 'Archivo Foto -Selección',1),
	(@IdModule, @IdVig, 'Activar Cámara',1),
	(@IdModule, @IdVig, 'Número IMSS',0),
	(@IdModule, @IdVig, 'Responsable Indorama',1),
	(@IdModule, @IdVig, 'Número Contrato',0),
	(@IdModule, @IdVig, 'RFC',0),
	(@IdModule, @IdVig, 'Clasificación',0),
	(@IdModule, @IdVig, 'Número Gafete',0),
	(@IdModule, @IdVig, 'Vigencia IMSS',1),
	(@IdModule, @IdVig, 'Nombre Clasificación',0),
	(@IdModule, @IdVig, 'Comedor',1),
	(@IdModule, @IdVig, 'Terminación Contrato',0),
	(@IdModule, @IdVig, 'Empresa Contratista',1),
	(@IdModule, @IdVig, 'Centro de Costos',1),
	(@IdModule, @IdAdmin, 'Tipo Contratista',1),
	(@IdModule, @IdAdmin, 'Estatus',1),
	(@IdModule, @IdAdmin, 'Nombre Contratista',1),
	(@IdModule, @IdAdmin, 'Apellido Paterno',1),
	(@IdModule, @IdAdmin, 'Apellido Materno',1),
	(@IdModule, @IdAdmin, 'Fecha Inducción',1),
	(@IdModule, @IdAdmin, 'Fecha Inicio Contrato',1),
	(@IdModule, @IdAdmin, 'Archivo Foto -Selección',1),
	(@IdModule, @IdAdmin, 'Activar Cámara',1),
	(@IdModule, @IdAdmin, 'Número IMSS',1),
	(@IdModule, @IdAdmin, 'Responsable Indorama',1),
	(@IdModule, @IdAdmin, 'Número Contrato',1),
	(@IdModule, @IdAdmin, 'RFC',1),
	(@IdModule, @IdAdmin, 'Clasificación',1),
	(@IdModule, @IdAdmin, 'Número Gafete',1),
	(@IdModule, @IdAdmin, 'Vigencia IMSS',1),
	(@IdModule, @IdAdmin, 'Nombre Clasificación',1),
	(@IdModule, @IdAdmin, 'Comedor',1),
	(@IdModule, @IdAdmin, 'Terminación Contrato',1),
	(@IdModule, @IdAdmin, 'Empresa Contratista',1),
	(@IdModule, @IdAdmin, 'Centro de Costos',1);

	SET @IdModule = (SELECT id_modulo FROM Modulos WHERE nombre= 'Catálogo de Visitantes');
	INSERT INTO CamposPerfil VALUES
	(@IdModule, @IdVig, 'Nombre del Visitante',1),
	(@IdModule, @IdVig, 'RFC Visitante',1),
	(@IdModule, @IdVig, 'Empresa del Visitante',1),
	(@IdModule, @IdVig, 'Empleado Responsable',1),
	(@IdModule, @IdVig, 'Número de Gafete Provisional',1),
	(@IdModule, @IdVig, 'Derecho a Comedor',1),
	(@IdModule, @IdVig, 'Fecha de Ingreso',0),
	(@IdModule, @IdVig, 'Horas dentro de la empresa',1),
	(@IdModule, @IdVig, 'Estatus',1),
	(@IdModule, @IdVig, 'Centro de Costos',1),	
	(@IdModule, @IdAdmin, 'Nombre del Visitante',1),
	(@IdModule, @IdAdmin, 'RFC Visitante',1),
	(@IdModule, @IdAdmin, 'Empresa del Visitante',1),
	(@IdModule, @IdAdmin, 'Empleado Responsable',1),
	(@IdModule, @IdAdmin, 'Número de Gafete Provisional',1),
	(@IdModule, @IdAdmin, 'Derecho a Comedor',1),
	(@IdModule, @IdAdmin, 'Fecha de Ingreso',1),
	(@IdModule, @IdAdmin, 'Horas dentro de la empresa',1),
	(@IdModule, @IdAdmin, 'Estatus',1),
	(@IdModule, @IdAdmin, 'Centro de Costos',1);

	SET @IdModule = (SELECT id_modulo FROM Modulos WHERE nombre= 'Empleados Permanentes');
	INSERT INTO CamposPerfil VALUES
	(@IdModule, @IdRH, 'Número de Empleado',1),
	(@IdModule, @IdRH, 'Nombre',0),
	(@IdModule, @IdRH, 'Número de Gafete',1),
	(@IdModule, @IdRH, 'Gafete Habilitado',1),
	(@IdModule, @IdAdmin, 'Número de Empleado',1),
	(@IdModule, @IdAdmin, 'Nombre',1),
	(@IdModule, @IdAdmin, 'Número de Gafete',1),
	(@IdModule, @IdAdmin, 'Gafete Habilitado',1);

	SET @IdModule = (SELECT id_modulo FROM Modulos WHERE nombre= 'Empleados Provisional');
	INSERT INTO CamposPerfil VALUES
	(@IdModule, @IdVig, 'Número de Empleado',1),
	(@IdModule, @IdVig, 'Nombre',1),
	(@IdModule, @IdVig, 'Número de Gafete Provisional',1),
	(@IdModule, @IdVig, 'Gafete Habilitado',1),
	(@IdModule, @IdVig, 'Vigencia',1),
	(@IdModule, @IdAdmin, 'Número de Empleado',1),
	(@IdModule, @IdAdmin, 'Nombre',1),
	(@IdModule, @IdAdmin, 'Número de Gafete Provisional',1),
	(@IdModule, @IdAdmin, 'Gafete Habilitado',1),
	(@IdModule, @IdAdmin, 'Vigencia',1);

	SET @IdModule = (SELECT id_modulo FROM Modulos WHERE nombre= 'Contratistas Permanentes');
	INSERT INTO CamposPerfil VALUES
	(@IdModule, @IdVig, 'RFC Contratista',1),
	(@IdModule, @IdVig, 'Nombre',0),
	(@IdModule, @IdVig, 'Número de Gafete',1),
	(@IdModule, @IdVig, 'Gafete Habilitado',1),
	(@IdModule, @IdAdmin, 'RFC Contratista',1),
	(@IdModule, @IdAdmin, 'Nombre',1),
	(@IdModule, @IdAdmin, 'Número de Gafete',1),
	(@IdModule, @IdAdmin, 'Gafete Habilitado',1);

	SET @IdModule = (SELECT id_modulo FROM Modulos WHERE nombre= 'Contratistas Provisional');
	INSERT INTO CamposPerfil VALUES
	(@IdModule, @IdVig, 'RFC Contratista',1),
	(@IdModule, @IdVig, 'Nombre',0),
	(@IdModule, @IdVig, 'Número de Gafete',1),
	(@IdModule, @IdVig, 'Gafete Habilitado',1),
	(@IdModule, @IdVig, 'Vigencia',0),
	(@IdModule, @IdAdmin, 'Número de Empleado',1),
	(@IdModule, @IdAdmin, 'Nombre',1),
	(@IdModule, @IdAdmin, 'Número de Gafete',1),
	(@IdModule, @IdAdmin, 'Gafete Habilitado',1),
	(@IdModule, @IdAdmin, 'Vigencia',1);

	SET @IdModule = (SELECT id_modulo FROM Modulos WHERE nombre= 'Contratistas Corto Plazo Mayor');
	INSERT INTO CamposPerfil VALUES
	(@IdModule, @IdVig, 'RFC Contratista',1),
	(@IdModule, @IdVig, 'Nombre',0),
	(@IdModule, @IdVig, 'Número de Gafete',1),
	(@IdModule, @IdVig, 'Gafete Habilitado',1),
	(@IdModule, @IdAdmin, 'Número de Empleado',1),
	(@IdModule, @IdAdmin, 'Nombre',1),
	(@IdModule, @IdAdmin, 'Número de Gafete',1),
	(@IdModule, @IdAdmin, 'Gafete Habilitado',1);

	SET @IdModule = (SELECT id_modulo FROM Modulos WHERE nombre= 'Contratistas Corto Plazo Menor');
	INSERT INTO CamposPerfil VALUES
	(@IdModule, @IdVig, 'RFC Contratista',1),
	(@IdModule, @IdVig, 'Nombre',0),
	(@IdModule, @IdVig, 'Número de Gafete',1),
	(@IdModule, @IdVig, 'Gafete Habilitado',1),
	(@IdModule, @IdVig, 'Vigencia',1),
	(@IdModule, @IdAdmin, 'Número de Empleado',1),
	(@IdModule, @IdAdmin, 'Nombre',1),
	(@IdModule, @IdAdmin, 'Número de Gafete',1),
	(@IdModule, @IdAdmin, 'Gafete Habilitado',1),
	(@IdModule, @IdAdmin, 'Vigencia',1);

	SET @IdModule = (SELECT id_modulo FROM Modulos WHERE nombre= 'Visitantes');
	INSERT INTO CamposPerfil VALUES
	(@IdModule, @IdVig, 'RFC Visitantes',1),
	(@IdModule, @IdVig, 'Nombre',0),
	(@IdModule, @IdVig, 'Número de Gafete Provisional',1),
	(@IdModule, @IdVig, 'Gafete Habilitado',1),
	(@IdModule, @IdVig, 'Vigencia',1),
	(@IdModule, @IdAdmin, 'RFC Visitantes',1),
	(@IdModule, @IdAdmin, 'Nombre',1),
	(@IdModule, @IdAdmin, 'Número de Gafete Provisional',1),
	(@IdModule, @IdAdmin, 'Gafete Habilitado',1),
	(@IdModule, @IdAdmin, 'Vigencia',1);
	GO

	UPDATE Usuarios SET id_perfil = 1