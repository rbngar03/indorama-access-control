USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_VisitantesAgregarGafPro]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Héctor Romero
-- alter date: 02-10-2010
-- Description: Agregar,Modificar gafete empleados.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_VisitantesAgregarGafPro]
@idVisitantes int,
@idEstatus int,
@Gafete nvarchar(5),
@fechaVigencia nvarchar(10)
AS
DECLARE @idGafete int
DECLARE @GafeteCont int
DECLARE @idGafeteAnt int
BEGIN
BEGIN TRY
 BEGIN TRAN;
------------Inicio Gafete

set @idGafete=(select idgafete from gafete where gafete=@Gafete and idtipogafete=2 and idTipoPersonal=3)
set @GafeteCont = (select count(idgafete)from gafete where gafete=@Gafete)
set @idGafeteAnt=(select idGafete from visitantes where idVisitantes=@idVisitantes )

--Asignar gafete
	if (@idGafete is null)
		begin
		if (@GafeteCont>0) 
			begin
				ROLLBACK TRAN;
    			RAISERROR('El numero del Gafete está asignado a otro tipo de Persona', 16, 1)
				return 
			end
		else
			begin
			insert into gafete (gafete,fechainicio,fechaVigencia,idestatus,idtipogafete,idtipopersonal) 
			values (@Gafete,getdate(),Convert(datetime ,@fechaVigencia,103) ,@idEstatus,2,3)
			update visitantes set idGafete=SCOPE_IDENTITY() where idVisitantes=@idVisitantes
				if (@idGafeteAnt is not null)
					begin
						update gafete set idestatus=2,fechainicio=null,fechaVigencia=null where idgafete=@idgafeteAnt
					end	
			end
		end
	else
		begin
			if (@idGafete=@idGafeteAnt)
				begin
					update gafete  set  idEstatus=@idEstatus ,
						fechaVigencia=Convert(datetime ,@fechaVigencia,103)
						where idgafete=@idgafete 
				end
			else
				begin
					if ((select idestatus from gafete where idgafete=@idGafete)=1)
						begin
							ROLLBACK TRAN;
							RAISERROR('El Gafete esta asignado a otro Visitante', 16, 1)
							return 
						end
					else
						begin
							update gafete set idestatus=@idEstatus,fechainicio=getdate(),fechaVigencia=Convert(datetime ,@fechaVigencia,103) where idgafete=@idgafete
							update visitantes set idgafete=null where idgafete=@idgafete
							update visitantes set idgafete=@idgafete where idVisitantes=@idVisitantes
							if (@idGafeteAnt is not null)
								begin
									update gafete set idestatus=2,fechainicio=null,fechaVigencia=null where idgafete=@idgafeteAnt
								end	
						end
				end
		end

	COMMIT TRAN;
END TRY
BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN;
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrSev = ERROR_SEVERITY()
		SET @ErrSt = ERROR_STATE()
		insert into LogError(ErrMsg,ErrSev,ErrSt) values (@ErrMsg,@ErrSev,@ErrSt)
		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
END CATCH
END
GO
