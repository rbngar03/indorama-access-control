USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_EmpleadosEditar]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Héctor Romero
-- ALTER date: 29-10-2010
-- Description: Modificacion de empleados.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_EmpleadosEditar]
@idEmpleado int,
@idEstatus int,
@foto image = null,
@Gafete nvarchar(5) = null,
@NumeroProvisionalGafete nvarchar(5) = null,
@Notas nvarchar(250)
AS
DECLARE @idGafete int
DECLARE @idGafeteAnte int
DECLARE @idGafeteP int
DECLARE @idGafeteAnteP int
declare @idEstatusAnt int
BEGIN
BEGIN TRY
 BEGIN TRAN;

--Gafetes
SET @idGafeteAnte=(select idGafete from empleado  where idEmpleado = @idEmpleado )
set @idGafete=(select idgafete from gafete where gafete=@Gafete and idtipogafete=1 and idTipoPersonal=1)
set @idGafeteP=(select idgafete from gafete where gafete=@NumeroProvisionalGafete and idtipogafete=2 and idTipoPersonal=1)
SET @idGafeteAnteP=(select NumeroProvisionalGafete from empleado  where idEmpleado = @idEmpleado )
--Estatus Empleado
set @idEstatusAnt=(select idEstatus from empleado where idempleado=@idEmpleado)

if (@idEstatus=2)
begin
		update gafete set fechavigencia=null, idestatus=2 where idgafete=@idGafeteAnte
			update gafete set fechainicio=null, idestatus=2 where idgafete=@idGafeteAnteP
			UPDATE empleado set idEstatus=@idEstatus, foto=isnull(@foto,foto),
				NumeroProvisionalGafete=null,Notas=@Notas
			WHERE idEmpleado = @idEmpleado
end
else if (@idEstatus=1)
begin
------------Inicio Gafete
begin
if (@Gafete is not null)
begin
	if (@idGafete is null)
		begin
		ROLLBACK TRAN;
		RAISERROR('No existe Gafete', 16, 1)
		return 
		end
	ELSE
		begin 
		if ((select count(idgafete) from gafete where idgafete=@idgafete and idestatus=1)>0 and
		(select count(idempleado) from empleado where idEmpleado = @idEmpleado and idgafete=@idGafete)=0)
			begin
			ROLLBACK TRAN;
			RAISERROR('El Gafete esta asignado a otro Empleado', 16, 1)
			return 
			end
		end
end

 if (@idGafeteAnte is not null and @idGafeteAnte<>@idGafete)
	begin
		update gafete set idestatus=2 where idgafete=@idGafeteAnte
		update gafete set idestatus=1, fechainicio=getdate() where idgafete=@idGafete
	end
end
----------Fin Gafete

------------Inicio Gafete Provisional
begin
if (@NumeroProvisionalGafete is not null)
begin
	if (@idGafeteP is null)
		begin
		ROLLBACK TRAN;
		RAISERROR('No existe Gafete Provisional', 16, 1)
		return 
	end
	ELSE
	begin 
		if ((select count(idgafete) from gafete where idgafete=@idGafeteP and idestatus=1)>0 and
		(select count(idempleado) from empleado where idEmpleado = @idEmpleado and NumeroProvisionalGafete=@idGafeteP)=0)
			begin
			ROLLBACK TRAN;
			RAISERROR('El Gafete esta asignado a otro Empleado', 16, 1)
			return 
			end
	end
end

 if (@idGafeteAnteP is not null and @idGafeteAnteP<>@idGafeteP)
	begin
		update gafete set fechainicio=null,idestatus=2 where idgafete=@idGafeteAnteP
		update gafete set idestatus=1, fechainicio=getdate(),fechavigencia=getdate()  where idgafete=@idGafeteP
	end

end
----------Fin Gafete Provisional

    UPDATE empleado 
    set idEstatus=@idEstatus,
        foto=isnull(@foto,foto) ,
		idGafete=@idGafete,
		NumeroProvisionalGafete=@idGafeteP,
		Notas=@Notas
    WHERE idEmpleado = @idEmpleado

end

	COMMIT TRAN;
END TRY
BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN;
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrSev = ERROR_SEVERITY()
		SET @ErrSt = ERROR_STATE()
		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
END CATCH
END
GO
