USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_ClasificacionAreas]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Héctor Romero Badillo
-- ALTER date: 10/11/2010 12:24
-- Description:	Selección del catalogo clasificación
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_ClasificacionAreas] 
AS
BEGIN
	SET NOCOUNT ON;
    -- Insert statements for procedure here
SELECT idclasificacion, clasificacion, descripcion
FROM clasificacion

END

GO
