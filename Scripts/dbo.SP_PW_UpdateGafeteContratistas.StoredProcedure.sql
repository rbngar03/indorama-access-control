USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_UpdateGafeteContratistas]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez
-- ALTER date: 08/11/2010 06:55 pm
-- Description:	Actualizacion de gafete para contratistas 
--              en base a su estatus y fecha de terminacion.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_UpdateGafeteContratistas] 
	
AS
	declare @NumeroContrato nvarchar(10)
    declare @idEstatus int
    declare @NumeroProvisionalGafete int
    declare @Gafete nvarchar(5)
    declare @idTipoPersonal int
    declare @idTipoGafete int
    declare @FechaTerminacion datetime
	--declare @fecha datetime
BEGIN
	
	-- SE OBTIENE EL NUMERO DE CONTRATO DEL CONTRATISTA Y EL ESTATUS DE ESTE MISMO.
	--Set @fecha=(convert(varchar(10),getdate()-1,103))
    declare  conteo Cursor For
	
    SELECT DISTINCT NumeroContrato,idEstatus
    FROM  Contratistas
	--WHERE fecha_actualizacion between @fecha + ' 00:00:00' and @fecha + ' 23:59:59'
    ORDER BY NumeroContrato

	Open conteo

	fetch next from conteo
	into @NumeroContrato,@idEstatus

	while @@FETCH_STATUS=0
    BEGIN
       --Obtener los valores para Gafete,NumeroProvisionalGafete,TipoPersonal,TipoGafete para cada contratista
	   Set @Gafete=(SELECT Gafete FROM Gafete as gaf,Contratistas as con WHERE (con.NumeroContrato=@NumeroContrato and gaf.idGafete=con.idGafete))
	   Set @NumeroProvisionalGafete=(SELECT Gafete FROM Contratistas as con,Gafete as gaf WHERE con.NumeroContrato=@NumeroContrato and con.NumeroProvisionalGafete=gaf.idGafete) 
	   Set @idTipoPersonal=(SELECT idTipoPersonal FROM Gafete as gaf,Contratistas as con WHERE con.NumeroContrato=@NumeroContrato and gaf.idGafete=con.idGafete)
       Set @idTipoGafete= (SELECT idTipoGafete FROM Gafete as gaf,Contratistas as con WHERE con.NumeroContrato=@NumeroContrato and gaf.idGafete=con.idGafete)
       if (@idEstatus=2)
       BEGIN
			if (@Gafete is not null)
				BEGIN
					 --Actualizar lo campos del Gafete contratista residente permanente y contratista corto plazo permanente
					 if ((@idTipoPersonal=2 and @idTipoGafete=1) or (@idTipoPersonal=4 and @idTipoGafete=1) )
						 BEGIN
							--Se actualiza la fecha vigencia a nulo y el estatus a inactivo para el gafete del contratista
							UPDATE Gafete set FechaVigencia=null,idEstatus=2 WHERE Gafete=@Gafete
						 END
					 --Actualizar lo campos del Gafete contratista Corto Plazo  provisional y contratista residente provisional
					 else if ((@idTipoPersonal=2 and @idTipoGafete=2) or (@idTipoPersonal=4 and @idTipoGafete=2) )
						 BEGIN
							--Se actualiza la fecha vigencia y fecha inicio a nulo y el estatus a inactivo para el gafete de contratista
							UPDATE Gafete set FechaInicio=null,FechaVigencia=null,idEstatus=2 WHERE Gafete=@Gafete
							--Se actualiza el idGafete a nulo para el contratista
							UPDATE Contratistas set idGafete=null WHERE NumeroContrato=@NumeroContrato
						 END
				END
			if (@NumeroProvisionalGafete is not null)
				BEGIN
					 --Actualizar lo campos del Numero Provisional Gafete contratista Residente provisional y contratista corto plazo provisional
					 if ((@idTipoPersonal=2 and @idTipoGafete=2) or (@idTipoPersonal=4 and @idTipoGafete=2) )
						BEGIN
							  --Se actualiza la fecha vigencia y fecha inicio a nulo y el estatus a inactivo para el NumeroProvisionalGafete del contratista
							  UPDATE Gafete set FechaInicio=null,FechaVigencia=null,idEstatus=2 WHERE Gafete=@NumeroProvisionalGafete
						      --Se actualiza el NumeroProvisionalGafete a nulo para el contratista
							  UPDATE Contratistas set NumeroProvisionalGafete=null WHERE NumeroContrato=@NumeroContrato
						END
				END	
       END
       --Si la fecha de terminacion ha sido vencida este bloque aplica
       Set @FechaTerminacion=(SELECT CONVERT(VARCHAR(10),FechaTerminacion, 103) AS [DD/MM/YYYY] FROM Contratistas WHERE NumeroContrato=@NumeroContrato)
	   if (@FechaTerminacion< getdate())
			 BEGIN
					if (@Gafete is not null)
						BEGIN
							--Se actualiza el estatus a inactivo para el gafete del contratista
							UPDATE Gafete set idEstatus=2 WHERE Gafete=@Gafete
						END
					if (@NumeroProvisionalGafete is not null)
						BEGIN
							--Se actualiza el estatus a inactivo para el NumeroProvisionalGafete del contratista
							UPDATE Gafete set idEstatus=2 WHERE Gafete=@NumeroProvisionalGafete
		                END
					--Se actualiza el estatus a inactivo para el contratista
					UPDATE Contratistas set idEstatus=2 WHERE NumeroContrato=@NumeroContrato
			END
      fetch next from conteo
	   into @NumeroContrato,@idEstatus
    END
	Close conteo
	deallocate conteo
END

GO
