USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_TerminalesEliminar]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Sánchez V.
-- ALTER date: 27/10/10 12:56 p.m.
-- Description:	Se utiliza para eliminar una terminal.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_TerminalesEliminar]
@idTerminales INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DELETE FROM Terminales WHERE idTerminales = @idTerminales

END

GO
