USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[spGetMarcajesEmpAll]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetMarcajesEmpAll] 
	-- Add the parameters for the stored procedure here
	@fechaIni datetime,
	@fechaFin datetime
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select m.Fecharegistro, e.numEmpleado, 'Torniquete' as Lector from
Marcajes m
INNER JOIN Empleado e on m.IdEmpleado = e.IdEmpleado
INNER JOIN Terminales t on m.idTerminales = t.IdTerminales
WHERE
m.AccesoOtorgado = 1 
and 
m.fecharegistro between @fechaIni and @fechaFin 
and 
m.IdTipoPersonal = 1 
and m.entradaSalida in ('E','S')
--and t.idTipoLector <> 1
UNION
SELECT     TOP (100) PERCENT DATEADD(year, 70, DATEADD(day, - 0, DATEADD(hour, + 0, DATEADD(second, nDateTime, 0)))) AS fecharegistro, nUserID AS numempleado, 
                      'Huella' AS Lector
FROM         BioStar.dbo.TB_EVENT_LOG
WHERE     (nEventIdn IN (43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 55, 58, 59, 61, 62)) AND (nReaderIdn <> 40712) AND (DATEADD(year, 70, DATEADD(day, - 0, DATEADD(hour, + 0,
                       DATEADD(second, nDateTime, 0)))) between @fechaIni and @fechaFin) 
Order by Fecharegistro 
END
GO
