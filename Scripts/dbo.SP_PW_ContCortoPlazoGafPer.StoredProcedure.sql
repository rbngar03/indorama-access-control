USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_ContCortoPlazoGafPer]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Hector Romero Badillo>
-- ALTER date: <01/11/2010>
-- Description:	<Gafete empleados permanentes>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_ContCortoPlazoGafPer] 
	-- Add the parameters for the stored procedure here
@pRFCContCortoPlazo nvarchar(15)
AS
DECLARE @idContratistas int
declare @fechacontratista datetime
BEGIN
BEGIN TRY
 

set @idContratistas=(select idContratistas from contratistas where RFC=@pRFCContCortoPlazo and idtipocontratista=1)

if (@idContratistas is null)
	begin
		RAISERROR('No existe Contratista', 16, 1)
		return 
	end
else
begin
set @fechacontratista=(select DATEADD(day,3,fechacontrato) from contratistas where idContratistas=@idContratistas)
  if ((select FechaTerminacion from contratistas where idContratistas=@idContratistas)>@fechacontratista) 
	begin
		if((select idestatus from contratistas where idContratistas=@idContratistas)=2)
			begin
				RAISERROR('El Contratista esta Inactivo', 16, 1)
				return 
			end
		else
			begin 
				select c.idContratistas,
				(select gafete from gafete where idgafete=c.idgafete and idTipoGafete=1 and idtipopersonal=4) as gafete,
				(select idestatus from gafete where idgafete=c.idgafete and idTipoGafete=1 and idtipopersonal=4) as estatusgafete,
				 rtrim(c.NombreContratista + ' ' + c.apPaterno + ' ' + isnull(c.apmaterno,' ')) as Nombre
				from Contratistas as c where c.idContratistas=@idContratistas
			end
	end
  else
	begin
		RAISERROR('Buscar Credenciales en Contratistas menor a 3 dias', 16, 1)
		return 
	end
end
END TRY
BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrSev = ERROR_SEVERITY()
		SET @ErrSt = ERROR_STATE()
		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
		return
END CATCH
END
GO
