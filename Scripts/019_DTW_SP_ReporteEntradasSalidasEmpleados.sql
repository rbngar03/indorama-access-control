IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_ReporteEntradasSalidasEmpleados')
  BEGIN
	DROP PROCEDURE DTW_SP_ReporteEntradasSalidasEmpleados
  END
GO
-- =============================================
-- Author:	Rubén García García
-- Create date: 04/10/2024
-- Description:	Obtiene los registros de entradas y salidas marcadas por empleados
-- History:
-- <27/12/2024> <k.ramirez> <Se modifica consulta para obtener max y min en fecha correcto>
-- =============================================
CREATE PROCEDURE DTW_SP_ReporteEntradasSalidasEmpleados
    @pStartDate DATE = '',
    @pEndDate DATE = '',
    @pIdPlant INT = 0,
    @pNameOrNumber VARCHAR(100) = ''
AS
BEGIN
    BEGIN TRY
            CREATE TABLE #Entradas (
            FechaRegistro DATETIME,
            idEmpleado INT,
            NombrePersonal VARCHAR(150)
        );

        CREATE TABLE #Salidas (
            FechaRegistro DATETIME,
            idEmpleado INT,
            NombrePersonal VARCHAR(150)
        );
	SET @pEndDate = DATEADD(day, 1, @pEndDate)
	WHILE @pStartDate != @pEndDate 
	BEGIN
         INSERT INTO #Entradas (FechaRegistro, idEmpleado, NombrePersonal)
		SELECT 
		MIN(T1.FechaRegistro) AS FechaRegistro,
            T1.idEmpleado,
            T1.NombrePersonal
        FROM 
            Marcajes T1
        WHERE 
            T1.EntradaSalida = 'E' AND T1.idEmpleado IS NOT NULL
			AND T1.FechaRegistro BETWEEN @pStartDate AND  DATEADD(day, 1, @pStartDate)
        GROUP BY 
            T1.idEmpleado, T1.NombrePersonal, CAST(T1.FechaRegistro AS DATE);

			 INSERT INTO #Salidas (FechaRegistro, idEmpleado, NombrePersonal)
        SELECT 
            MAX(T1.FechaRegistro) AS FechaRegistro,
            T1.idEmpleado,
            T1.NombrePersonal
        FROM 
            Marcajes T1
        WHERE 
            T1.EntradaSalida = 'S' AND T1.idEmpleado IS NOT NULL
			AND T1.FechaRegistro BETWEEN @pStartDate AND DATEADD(day, 1, @pStartDate)
        GROUP BY 
            T1.idEmpleado, T1.NombrePersonal, CAST(T1.FechaRegistro AS DATE);

		SET @pStartDate = DATEADD(day, 1, @pStartDate)
	END

	SELECT 
            E.FechaRegistro AS FechaEntrada,
            S.FechaRegistro AS FechaSalida,
            UPPER(E.NombrePersonal) AS NombreEmpleado,
            T3.NumEmpleado,
            T4.Descripcion
        FROM 
            #Entradas E
        FULL OUTER JOIN 
            #Salidas S ON E.idEmpleado = S.idEmpleado
            AND CAST(E.FechaRegistro AS DATE) = CAST(S.FechaRegistro AS DATE)
        INNER JOIN 
            Empleado T3 ON ISNULL(E.idEmpleado, S.idEmpleado) = T3.idEmpleado
        INNER JOIN 
            Planta T4 ON T3.idPlanta = T4.idPlanta
		WHERE 
			@pIdPlant = 0 OR T4.idPlanta = @pIdPlant
            AND(@pNameOrNumber = '' OR (UPPER(TRANSLATE(E.NombrePersonal, 'ÁÉÍÓÚÑ', 'AEIOUN')) LIKE CONCAT('%', @pNameOrNumber, '%'))  OR (CAST(T3.NumEmpleado AS VARCHAR) LIKE CONCAT('%', @pNameOrNumber, '%')))
        ORDER BY 
            E.FechaRegistro, ISNULL(E.FechaRegistro, S.FechaRegistro);

			DROP TABLE #Entradas;
		    DROP TABLE #Salidas;
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            ROLLBACK TRAN;
        
        DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT;
        SET @ErrMsg = ERROR_MESSAGE();
        SET @ErrSev = ERROR_SEVERITY();
        SET @ErrSt = ERROR_STATE();
        RAISERROR(@ErrMsg, @ErrSev, @ErrSt);
    END CATCH
END




