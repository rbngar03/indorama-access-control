USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_OrigenTodos]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez
-- ALTER date: 02/11/2010 10:07 p.m.
-- Description:	Selección del catalogo de Origen
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_OrigenTodos] 
	
AS
BEGIN
	SET NOCOUNT ON;
    -- Insert statements for procedure here
SELECT idOrigen,Origen,DescOrigen
FROM Origen
END

GO
