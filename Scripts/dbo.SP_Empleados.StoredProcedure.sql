USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_Empleados]    Script Date: 19/06/2024 11:52:12 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karla Ramirez
-- Create date: 10/06/24
-- Description:	Script que obtiene la consulta de Empleados
-- History:
-- =============================================
CREATE PROCEDURE [dbo].[SP_Empleados]
	--@pIdEmpleado int 
AS
BEGIN 
SELECT T0.idEmpleado, T0.NumEmpleado,T0.idEstatus, ltrim(T0.Nombre + ' ' + T0.ApePaterno + ' ' + isnull(T0.ApeMaterno,' ')) AS Nombre,
T2.ccostos AS CentroCostos, T3.TipoNomina,T0.NominaProcesada, T4.Gafete,isnull(T0.NumeroProvisionalGafete,' ') AS NumeroProvisionalGafete, T6.TipoGafete,T0.idPlanta
FROM Empleado AS T0
INNER JOIN Estatus AS T1 ON T1.idEstatus = T0.idEstatus 
INNER JOIN ccostos AS T2 ON T2.idCcostos = T0.idCcostos
INNER JOIN TipoNomina AS T3 ON T3.idTipoNomina = T0.idTipoNomina
INNER JOIN Gafete AS T4 ON T4.idGafete = T0.idGafete OR T4.idGafete =T0.NumeroProvisionalGafete
INNER JOIN TipoGafete AS T6 ON T4.idTipoGafete = T6.idTipoGafete
INNER JOIN Planta AS T5 ON T5.idPlanta = T0.idPlanta
--WHERE @pIdEmpleado IS NULL OR (T0.idEmpleado = @pIdEmpleado)  
END
GO
