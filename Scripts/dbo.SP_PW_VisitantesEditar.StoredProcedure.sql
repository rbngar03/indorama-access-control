USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_VisitantesEditar]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Debi Alexandra Sánchez V.
-- Create date: 03/11/2010 10:14 a.m.
-- Description: Modificacion de Visitantes.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_VisitantesEditar]

@idVisitantes int,
@idCcostos int,
@idComedor int,
@ArchivoAdjunto nvarchar(100)= null,
@idEstatus int,
@FechaIngreso datetime,
@FechaTerminacion datetime,
@Horas nvarchar(7)=null,
@idPlanta int


AS
DECLARE @idGafete int
BEGIN
BEGIN TRY
BEGIN TRAN;
--Estatus Visitantes
	set @idGafete=(select idGafete from Visitantes where idVisitantes=@idVisitantes)

if (@idEstatus=2)
begin
				UPDATE Gafete SET FechaInicio=null,FechaVigencia=null, idEstatus=2 where idGafete=@idGafete
			    UPDATE Visitantes
                 set idCcostos=@idCcostos,
				 idComedor=@idComedor,
			     ArchivoAdjunto=isnull(@ArchivoAdjunto,ArchivoAdjunto),
			     idEstatus=@idEstatus,
                 idGafete=null,
                 FechaIngreso=@FechaIngreso,
                 FechaTerminacion=@FechaTerminacion,
                 Horas=@Horas,
				 idPlanta=@idPlanta

				WHERE idVisitantes = @idVisitantes
end
else if (@idEstatus=1)
begin

     UPDATE Visitantes

     set idCcostos=@idCcostos,
	     idComedor=@idComedor,
		 ArchivoAdjunto=isnull(@ArchivoAdjunto,ArchivoAdjunto),
		 idEstatus=@idEstatus,
		 FechaIngreso=@FechaIngreso,
         FechaTerminacion=@FechaTerminacion,
         Horas=@Horas,
		 idPlanta=@idPlanta
     WHERE idVisitantes = @idVisitantes
end
	COMMIT TRAN;
END TRY
BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN;
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrSev = ERROR_SEVERITY()
		SET @ErrSt = ERROR_STATE()
		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
END CATCH
END
GO
