IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_CreateTableLogUsers')
  BEGIN
	DROP PROCEDURE DTW_SP_CreateTableLogUsers
  END
GO
-- =============================================
-- Author:	Rub�n Garc�a Garc�a
-- Create date: 26/06/2024
-- Description:	Crea la tabla para almacenar las operaciones de los usuarios.
-- <14/10/2024> <r.garcia> <Se agrega validaci�n de creaci�n previa de la tabla DTW_CA_LogUsers.>
-- =============================================

CREATE PROCEDURE DTW_SP_CreateTableLogUsers 	
AS
BEGIN
	
	BEGIN TRY

		IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DTW_CA_LogUsers')
		BEGIN
			CREATE TABLE DTW_CA_LogUsers (
					 IDLog INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
					 IDUser INT NOT NULL FOREIGN KEY REFERENCES Usuarios(idUsuarios),
					 DateOperation DATETIME NOT NULL,
					 MessageOperation VARCHAR(500)
			);
		END				

	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
  		SET @ErrMsg = ERROR_MESSAGE()
  		SET @ErrSev = ERROR_SEVERITY()
  		SET @ErrSt  = ERROR_STATE()
  		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)	
	END CATCH
END
GO

EXEC DTW_SP_CreateTableLogUsers