USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_InterfaceContratistasFechaTerminacion]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Héctor Romero Badillo
-- ALTER date: 23/11/2010 
-- Description:	Actualización de Empleados
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_InterfaceContratistasFechaTerminacion] 
	
AS
	declare @RFC nvarchar(15)
	declare @fechaterminacion datetime
	declare @vigenciaIMSS datetime
	declare @fechabaja datetime
	declare @idGafete nvarchar(5)
	declare @idGafeteP nvarchar(5)
    declare @fecha_actualizacion datetime
	
BEGIN
BEGIN TRY
BEGIN TRAN;
	
    declare conteo Cursor For
	--aqui se cambian los datos de los campos por los q vengan de las vistas
    SELECT rfc
    FROM  Contratistas
    WHERE fechaterminacion < getdate() and idestatus=1
    ORDER BY rfc
   
	Open conteo

	fetch next from conteo
	into @RFC

	while @@FETCH_STATUS=0
    BEGIN
		set @idGafete = (select idGafete from contratistas where rtrim(RFC)=rtrim(@RFC))
		set @idGafeteP = (select NumeroProvisionalGafete from contratistas where rtrim(RFC)=rtrim(@RFC))
		--Actualiza Campo de estatus
		UPDATE contratistas set idEstatus=2 WHERE rtrim(RFC)=rtrim(@RFC)

		--Actualizar lo campos del Gafete
		if (@idGafete is not null)
		BEGIN
			--Actualiza la fecha de vigencia a null y el estatus a inactivo del gafete
			UPDATE Gafete set FechaVigencia=null,idEstatus=2 WHERE idGafete=@idGafete
		END
		--Actualizar los campos del Numero Provisional Gafete
		if (@idGafeteP is not null)
		BEGIN
			--Actualiza la fecha de vigencia la fecha inicio a null y el estatus a inactivo del gafete
			UPDATE Gafete set FechaVigencia=null,FechaInicio=null,idEstatus=2 WHERE idGafete=@idGafeteP
			--Actualiza el campo Numero Provisional de Gafete a null para el empleado
			UPDATE contratistas set NumeroProvisionalGafete=null WHERE rtrim(RFC)=rtrim(@RFC)
		END

      fetch next from conteo
	into @RFC

    END
	Close conteo
	deallocate conteo

COMMIT TRAN;
END TRY
BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN;
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrSev = ERROR_SEVERITY()
		SET @ErrSt = ERROR_STATE()
		insert into LogError(ErrMsg,ErrSev,ErrSt) values (@ErrMsg,@ErrSev,@ErrSt) 
		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
END CATCH
END
GO
