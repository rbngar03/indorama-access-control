IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_ConsultaComidasXTurno')
  BEGIN
	DROP PROCEDURE DTW_SP_ConsultaComidasXTurno
  END
GO
/****** Object:  StoredProcedure [dbo].[DTW_SP_ConsultaComidasXTurno]    Script Date: 17/10/2024 10:25:02 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Marco J. Muñoz Matuk
-- Create date: 14/10/2024
-- Description:	Consulta que trae la cantidad de salidas a comedor que se hicieron por dia en los diferentes turnos del dia
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_ConsultaComidasXTurno]  
@pStartDate DATE,
@pEndDate   DATE,
@pSitio     NVARCHAR(30) = ''
AS
BEGIN
    -- Evitar mensajes adicionales de "N filas afectadas"
    SET NOCOUNT ON;

    BEGIN TRY
        -- Condición para verificar si el parámetro es uno de los sitios específicos o está vacío
        IF @pSitio IN ('QUERÉTARO PET', 'QUERÉTARO FT', 'SANTA FE')
        BEGIN
            -- Si se proporciona un sitio específico, ejecutar la lógica actual
            WITH Turnos AS (
                SELECT '1' AS Turno, '06:00:00' AS HoraInicio, '14:59:59' AS HoraFin
                UNION ALL
                SELECT '2' AS Turno, '15:00:00' AS HoraInicio, '21:59:59' AS HoraFin
                UNION ALL
                SELECT '3' AS Turno, '22:00:00' AS HoraInicio, '05:59:59' AS HoraFin
            ),
            Fechas AS (
                SELECT DISTINCT CAST(FechaRegistro AS DATE) AS FechaRegistro 
                FROM Marcajes
                WHERE FechaRegistro BETWEEN @pStartDate AND @pEndDate 
            )
            SELECT 
                b.FechaRegistro,
                b.Descripcion,
                b.Turno,
                COUNT(t1.idEmpleado) AS TotalRegistros
            FROM (
                SELECT 
                    F.FechaRegistro,
                    T3.Descripcion,
                    T.Turno
                FROM Fechas F
                CROSS JOIN Turnos T
                CROSS JOIN (SELECT DISTINCT Descripcion FROM Planta WHERE Descripcion = @pSitio) t3
            ) AS b
            LEFT JOIN Marcajes t1 ON 
                (
                    (b.Turno = '1' AND CONVERT(TIME, t1.FechaRegistro) >= '06:00:00' AND CONVERT(TIME, t1.FechaRegistro) < '14:59:59')
                    OR (b.Turno = '2' AND CONVERT(TIME, t1.FechaRegistro) >= '15:00:00' AND CONVERT(TIME, t1.FechaRegistro) < '21:59:59')
                    OR (b.Turno = '3' AND (
                        (CONVERT(TIME, t1.FechaRegistro) >= '22:00:00' AND CONVERT(TIME, t1.FechaRegistro) < '23:59:59')
                        OR (CONVERT(TIME, t1.FechaRegistro) < '05:59:59')
                    ))
                )
                AND CAST(t1.FechaRegistro AS DATE) = b.FechaRegistro
                AND t1.EntradaSalida = 'C'
            LEFT JOIN Empleado t2 ON t1.idEmpleado = t2.idEmpleado
            LEFT JOIN Planta t3 ON t2.idPlanta = t3.idPlanta AND t3.Descripcion = b.Descripcion
            GROUP BY 
                b.FechaRegistro,
                b.Turno,
                b.Descripcion
            ORDER BY 
                b.FechaRegistro ASC, 
                b.Turno ASC;
        END
        ELSE
        BEGIN
            -- Si el parámetro @pSitio es "TODAS", hacer un UNION ALL para todos los sitios
            IF @pSitio = 'TODAS'
            BEGIN
                WITH Turnos AS (
                    SELECT '1' AS Turno, '06:00:00' AS HoraInicio, '14:59:59' AS HoraFin
                    UNION ALL
                    SELECT '2' AS Turno, '15:00:00' AS HoraInicio, '21:59:59' AS HoraFin
                    UNION ALL
                    SELECT '3' AS Turno, '22:00:00' AS HoraInicio, '05:59:59' AS HoraFin
                ),
                Fechas AS (
                    SELECT DISTINCT CAST(FechaRegistro AS DATE) AS FechaRegistro 
                    FROM Marcajes
                    WHERE FechaRegistro BETWEEN @pStartDate AND @pEndDate 
                )
                -- Repetir la lógica para cada sitio
                SELECT 
                    b.FechaRegistro,
                    b.Descripcion,
                    b.Turno,
                    COUNT(t1.idEmpleado) AS TotalRegistros
                FROM (
                    -- Consultar QUERÉTARO PET
                    SELECT 
                        F.FechaRegistro,
                        'QUERÉTARO PET' AS Descripcion,
                        T.Turno
                    FROM Fechas F
                    CROSS JOIN Turnos T
                    UNION ALL
                    -- Consultar QUERÉTARO FT
                    SELECT 
                        F.FechaRegistro,
                        'QUERÉTARO FT' AS Descripcion,
                        T.Turno
                    FROM Fechas F
                    CROSS JOIN Turnos T
                    UNION ALL
                    -- Consultar SANTA FE
                    SELECT 
                        F.FechaRegistro,
                        'SANTA FE' AS Descripcion,
                        T.Turno
                    FROM Fechas F
                    CROSS JOIN Turnos T
                ) AS b
                LEFT JOIN Marcajes t1 ON 
                    (
                        (b.Turno = '1' AND CONVERT(TIME, t1.FechaRegistro) >= '06:00:00' AND CONVERT(TIME, t1.FechaRegistro) < '14:59:59')
                        OR (b.Turno = '2' AND CONVERT(TIME, t1.FechaRegistro) >= '15:00:00' AND CONVERT(TIME, t1.FechaRegistro) < '21:59:59')
                        OR (b.Turno = '3' AND (
                            (CONVERT(TIME, t1.FechaRegistro) >= '22:00:00' AND CONVERT(TIME, t1.FechaRegistro) < '23:59:59')
                            OR (CONVERT(TIME, t1.FechaRegistro) < '05:59:59')
                        ))
                    )
                    AND CAST(t1.FechaRegistro AS DATE) = b.FechaRegistro
                    AND t1.EntradaSalida = 'C'
                LEFT JOIN Empleado t2 ON t1.idEmpleado = t2.idEmpleado
                LEFT JOIN Planta t3 ON t2.idPlanta = t3.idPlanta AND t3.Descripcion = b.Descripcion
                GROUP BY 
                    b.FechaRegistro,
                    b.Turno,
                    b.Descripcion
                ORDER BY 
                    b.Descripcion ASC, -- Ordenar por sitio
                    b.FechaRegistro ASC, 
                    b.Turno ASC;
            END
            ELSE
            BEGIN
                -- Si el parámetro @pSitio está vacío, no se ejecuta ninguna consulta.
                -- Puedes dejar esto como un mensaje informativo o una consulta predeterminada si lo deseas.
                SELECT 'No se especificó un sitio válido.' AS Mensaje;
            END
        END
    END TRY
    BEGIN CATCH
        DECLARE @v_errMsg VARCHAR(MAX), @v_errSev INT, @v_errSt INT;
        SET @v_errMsg = ERROR_MESSAGE() + ' Linea: ' + CAST(ERROR_LINE() AS VARCHAR);
        SET @v_errSev = ERROR_SEVERITY();
        SET @v_errSt = ERROR_STATE();
        RAISERROR(@v_errMsg, @v_errSev, @v_errSt);
    END CATCH
END
