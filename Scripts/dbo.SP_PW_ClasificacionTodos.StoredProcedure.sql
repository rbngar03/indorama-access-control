USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_ClasificacionTodos]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez
-- ALTER date: 01/11/2010 12:41
-- Description:	Selección del catalogo de Clasificaciòn
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_ClasificacionTodos] 
	
AS
BEGIN
	SET NOCOUNT ON;
    -- Insert statements for procedure here
SELECT idClasificacion,Clasificacion
FROM Clasificacion

END

GO
