USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_TraerFoto]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Leopoldo Rangel>
-- ALTER date: <18/11/2010>
-- Description:	<Stored que devuelve la foto del personal>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_TraerFoto]
@idTipoPersonal INT,
@IDPERSONAL INT
AS
BEGIN
	IF @IDTIPOPERSONAL = 1
	BEGIN
		SELECT FOTO FROM EMPLEADO WHERE IDEMPLEADO = @IDPERSONAL;
	END;
	ELSE IF @IDTIPOPERSONAL = 3
	BEGIN
		SELECT FOTO FROM VISITANTES WHERE IDVISITANTES = @IDPERSONAL;
	END;
	ELSE
	BEGIN
		SELECT FOTO FROM CONTRATISTAS WHERE IDCONTRATISTAS = @IDPERSONAL;
	END;
END

GO
