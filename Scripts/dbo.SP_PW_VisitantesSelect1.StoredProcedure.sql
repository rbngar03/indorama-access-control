USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_VisitantesSelect1]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez V.
-- Create date: 02/11/2010 06:02 pm
-- Description:	Extrae la información de Visitantes de una persona especifica.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_VisitantesSelect1] 
	-- Add the parameters for the stored procedure here
@pidVisitantes int
AS
BEGIN
	
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

    SELECT convert(VARCHAR(10),vis.FechaIndSeguridad,105),
    convert(VARCHAR(10),vis.FechaIngreso,105),vis.NombreVisitante,
	convert(VARCHAR(10),vis.FechaTerminacion,105),vis.Horas,
    vis.Foto,vis.idOrigen,vis.DescOrigen,
    (SELECT gaf.gafete FROM Gafete AS gaf WHERE gaf.idGafete=vis.idGafete),
    vis.idCcostos,vis.EmpResponsable,vis.idComedor,vis.RFC,
    vis.ArchivoAdjunto,vis.idEstatus,
	vis.idPlanta
    FROM Visitantes as vis
    WHERE vis.idVisitantes=@pidVisitantes
 

END
GO
