IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_ModificaVisitante')
  BEGIN
	DROP PROCEDURE DTW_SP_ModificaVisitante
  END
GO
-- =============================================
-- Author:		Karla Ramirez
-- Create date: 27/08/24
-- Description:	Script que actualiza datos de los Visitantes
-- History:
-- =============================================
CREATE PROCEDURE [dbo].[DTW_SP_ModificaVisitante]
	@pStatus INT, -- {1-Activo / 2-Inactivo}
	@pIdVisitante INT,
	@pEmpresaVisitante VARCHAR (150),
	@pEmpleadoResponsable VARCHAR (150),
	@pDerechoComedor INT,
	@pHoras VARCHAR(7),
	@Response VARCHAR(100) OUTPUT
	

AS
BEGIN
	DECLARE @IdGafete INT
	BEGIN TRY 
		BEGIN TRAN
			BEGIN
			SET @IdGafete = (SELECT idGafete FROM VISITANTES WHERE idVisitantes = @pIdVisitante)
			IF (@pStatus = 2)
			BEGIN
				UPDATE Gafete SET FechaInicio = NULL, FechaVigencia = NULL, idEstatus=2 WHERE idGafete=@idGafete
				UPDATE [dbo].[Visitantes]
				   SET [Horas] = NULL,
					   [DescOrigen] = @pEmpresaVisitante,
					   [EmpResponsable] = NULL,
					   [idComedor] = 2,
					   [idEstatus] = @pStatus,
					   [fecha_actualizacion] = GETDATE()
				 WHERE idVisitantes = @pIdVisitante
			END 
			ELSE
				BEGIN 
					-- Actualizacion de datos --
				   UPDATE [dbo].[Visitantes]
				   SET [Horas] = @pHoras,
					   [DescOrigen] = @pEmpresaVisitante,
					   [EmpResponsable] = @pEmpleadoResponsable,
					   [idComedor] = @pDerechoComedor,
					   [idEstatus] = @pStatus,
					   [fecha_actualizacion] = GETDATE()
				 WHERE idVisitantes = @pIdVisitante
				END
			END
		 COMMIT TRAN
		 SET @Response = 'Ok'; 
	
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		ROLLBACK TRAN;
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrSev = ERROR_SEVERITY()
		SET @ErrSt = ERROR_STATE()
		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
 END
