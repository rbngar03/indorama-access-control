USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_TerminalesSelectTodos]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sanchez V.
-- ALTER date: 29/10/10
-- Description:	Selección de los registros de la tabla Terminales con table TipoLector
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_TerminalesSelectTodos] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT idTerminales,Clave,Ubicacion,IP,Nombre
FROM Terminales

END

GO
