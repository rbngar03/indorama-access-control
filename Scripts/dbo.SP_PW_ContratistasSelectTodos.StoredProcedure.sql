USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_ContratistasSelectTodos]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez V.
-- ALTER date: 29/10/2010 6:16 pm
-- Description:	Se crea el sp para consulta de los contratista
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_ContratistasSelectTodos] 
AS
BEGIN
	SET NOCOUNT ON;
    -- Insert statements for procedure here



SELECT  idContratistas,
       (SELECT TipoContratista
        FROM TipoContratista AS tp
        WHERE (idTipoContratista = con.idTipoContratista)) AS TipoContratista,
        ltrim(con.NombreContratista + ' ' + con.ApPaterno + ' ' + ISNULL(con.ApMaterno, ' ')) AS NombreContratista,
        FechaIndSeguridad, FechaContrato, FechaTerminacion, IMSS, 
        VigenciaSeguridad, VigenciaIMSS, NumeroContrato, PeriodoVigencia,
        (SELECT Compania
         FROM Compania AS com
         WHERE (idCompanias = con.idCompanias)) AS Compania, NombreCompañia, Foto, FechaBaja, RFC,
        (SELECT Clasificacion
         FROM Clasificacion AS clas
         WHERE (idClasificacion = con.idClasificacion)) AS Clasificacion, ResponsableInvista,
         (SELECT Ccostos
          FROM ccostos AS cc
          WHERE (idCcostos = con.idCcostos)) AS Ccostos,
         (SELECT Gafete
          FROM Gafete AS gaf
          WHERE(idGafete = con.idGafete)) AS Gafete, Notas,
         (SELECT Gafete
          FROM Gafete AS gaf1
          WHERE(idGafete = con.NumeroProvisionalGafete)) AS NumeroProvisionalGafete,
         (SELECT DerechoComedor
          FROM Comedor AS come
          WHERE (idComedor = con.idComedor)) AS DerechoComedor, ArchivoAdjunto,
          (SELECT Estatus
           FROM Estatus AS e
           WHERE (idEstatus = con.idEstatus)) AS Estatus
FROM Contratistas AS con

END

GO
