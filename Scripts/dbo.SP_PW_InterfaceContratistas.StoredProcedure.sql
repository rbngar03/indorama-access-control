USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_InterfaceContratistas]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Héctor Romero Badillo
-- Create date: 17/11/2010 01:03:40 pm
-- Description:	Actualización de Empleados
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_InterfaceContratistas] 
	
AS
	declare @RFC nvarchar(15)
	declare @Planta nvarchar(50)
	declare @idPlantaAn int
	declare @idPlantaAc int
	declare @TipoContratista nvarchar(50)
	declare @idTipoContratistaAn int
	declare @idTipoContratistaAc int
	declare @ApPaterno nvarchar(50)
	declare @ApMaterno nvarchar(50)
	declare @NombreContratista nvarchar(150)
	declare @fechaindseguridad datetime
	declare @fechaindseguridadAn datetime
	declare @fechacontrato datetime
	declare @fechacontratoAn datetime
	declare @fechaterminacion datetime
	declare @fechaterminacionAn datetime
	declare @IMSS nvarchar(50)
	declare @IMSSAn nvarchar(50)
	declare @vigenciaseguridad datetime
	declare @vigenciaseguridadAn datetime
	declare @vigenciaIMSS datetime
	declare @vigenciaIMSSAn datetime
	declare @numerocontrato nvarchar(10)
	declare @numerocontratoAn nvarchar(10)
	declare @periodovigencia datetime
	declare @periodovigenciaAn datetime
	declare @compania nvarchar(50)
	declare @idcompaniasAn int
	declare @idcompaniasAc int
	declare @nombrecompania nvarchar(50)
	declare @nombrecompaniaAn nvarchar(50)
	declare @fechabaja datetime
	declare @fechabajaAn datetime
	declare @clasificacion nvarchar(50)
	declare @idClasificacionAn int
	declare @idClasificacionAc int
	declare @responsableInvista nvarchar(150)
	declare @responsableInvistaAn nvarchar(150)
	declare @ccostos nvarchar(50)
	declare @idccostosAn int
	declare @idccostosAc int
	declare @Estatus nvarchar(50)
	declare @idEstatusAn int
	declare @idEstatusAc int
	declare @idGafete nvarchar(5)
	declare @idGafeteP nvarchar(5)
    declare @fecha datetime
	declare @RFCCont int
	declare @fecha_actualizacion datetime
	
BEGIN
BEGIN TRY
BEGIN TRAN;
	-- SE OBTIENE 
    Set @fecha=(convert(varchar(10),getdate()-1,103))
    declare  conteo Cursor For
	--aqui se cambian los datos de los campos por los q vengan de las vistas
    SELECT rfc,idtipocontratista,nombrecontratista,appaterno,apmaterno,fechaindseguridad,fechacontrato,fechaterminacion,IMSS,
			vigenciaseguridad,vigenciaIMSS,numerocontrato,periodovigencia,idcompanias,nombrecompañia,fechabaja,
			idclasificacion,responsableinvista,idccostos,idestatus,idPlanta,fecha_actualizacion
    FROM  v_Contratistas
    WHERE fecha_actualizacion between @fecha + ' 00:00:00' and @fecha + ' 23:59:59'
	--WHERE fecha_actualizacion between @fecha + ' 00:00:00' and getdate()
    ORDER BY rfc
   
	Open conteo

	fetch next from conteo
	into @RFC,@TipoContratista,@NombreContratista,@ApPaterno,@ApMaterno,@fechaindseguridad,@fechacontrato,@fechaterminacion,@IMSS,
		@vigenciaseguridad,@vigenciaIMSS,@numerocontrato,@periodovigencia,@compania,@nombrecompania,@fechabaja,@clasificacion,
		@responsableInvista,@ccostos,@Estatus,@Planta,@fecha_actualizacion

	while @@FETCH_STATUS=0
    BEGIN
		set @RFCCont = (select count(idContratistas) from contratistas where rtrim(RFC)=rtrim(@RFC))
		set @idTipoContratistaAn = (select idTipoContratista from contratistas where rtrim(RFC)=rtrim(@RFC))
		set @idTipoContratistaAc = (select idTipoContratista from TipoContratista where rtrim(TipoContratista)=rtrim(@TipoContratista))
		set @idcompaniasAn = (select idcompanias from contratistas where rtrim(RFC)=rtrim(@RFC))
		set @idcompaniasAc = (select idcompanias from compania where rtrim(compania)=rtrim(@compania))
		set @idclasificacionAn = (select idclasificacion from contratistas where rtrim(RFC)=rtrim(@RFC))
		set @idclasificacionAc = (select idclasificacion from clasificacion where rtrim(clasificacion)=rtrim(@clasificacion))
		set @idccostosAn = (select idccostos from contratistas where rtrim(RFC)=rtrim(@RFC))
		set @idccostosAc = (select idccostos from ccostos where rtrim(ccostos)=rtrim(@ccostos))
		set @idEstatusAn = (select idEstatus from contratistas where rtrim(RFC)=rtrim(@RFC))
		set @idEstatusAc = (select idEstatus from Estatus where rtrim(Estatus)=rtrim(@Estatus))
		set @idGafete = (select idGafete from contratistas where rtrim(RFC)=rtrim(@RFC))
		set @idGafeteP = (select NumeroProvisionalGafete from contratistas where rtrim(RFC)=rtrim(@RFC))
		set @tipoContratista= (select idTipoContratista from contratistas where rtrim(RFC)=rtrim(@RFC))
		set @idPlantaAn =  (select idplanta from contratistas where rtrim(RFC)=rtrim(@RFC))
		set @idPlantaAc = (select idplanta from planta where rtrim(clave)=rtrim(@Planta))

		if @RFCCont=0
		BEGIN
			insert into contratistas
			(idtipocontratista,nombrecontratista,appaterno,apmaterno,fechaindseguridad,fechacontrato,fechaterminacion,
			IMSS,vigenciaseguridad,vigenciaIMSS,numerocontrato,periodovigencia,idcompanias,nombrecompañia,fechabaja,rfc,
			idclasificacion,responsableinvista,idccostos,idestatus,idPlanta,fecha_actualizacion)
			values
			(@idTipoContratistaAc,@NombreContratista,@ApPaterno,@ApMaterno,@fechaindseguridad,@fechacontrato,@fechaterminacion,
			@IMSS,@vigenciaseguridad,@vigenciaIMSS,@numerocontrato,@periodovigencia,@idcompaniasAc,@nombrecompania,@fechabaja,@RFC,
			@idclasificacionAc,@responsableInvista,@idccostosAc,@idEstatusAc,@idPlantaAc,@fecha_actualizacion)
		END
		else
		BEGIN
			--Planta
			if @idPlantaAn<>@idPlantaAc and @idPlantaAc is not null
			begin
				update contratistas set idPlanta=@idPlantaAc,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			end

			--fecha de seguridad
			set @fechaindseguridadAn=(select FechaIndSeguridad from contratistas where rtrim(RFC)=rtrim(@RFC))
			if @fechaindseguridadAn<>@fechaindseguridad and @fechaindseguridad is not null
			begin
				update contratistas set fechaindseguridad=@fechaindseguridad,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			end

			--fecha de contrato
			set @fechacontratoAn=(select fechacontrato from contratistas where rtrim(RFC)=rtrim(@RFC))
			if @fechacontratoAn<>@fechacontrato and @fechacontrato is not null
			begin
				update contratistas set fechacontrato=@fechacontrato,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			end
			
			--fecha de terminacion cambia la fecha de vigencia del gafete para contratistas de corto plazo mayor a 3 dias
			set @fechaterminacionAn=(select fechacontrato from contratistas where rtrim(RFC)=rtrim(@RFC))
			if @fechaterminacionAn<>@fechaterminacion and @fechaterminacion is not null
			begin
				if @idTipoContratistaAc=1
				begin
					update contratistas set fechaterminacion=@fechaterminacion,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
						if @idGafete is not null
							begin
								UPDATE Gafete set FechaVigencia=@fechaterminacion WHERE idGafete=@idGafete
							end
				end
			end

			--imss
			set @IMSSAn=(select IMSS from contratistas where rtrim(RFC)=rtrim(@RFC))
			if @IMSSAn<>@IMSS and @IMSS is not null
			begin
				update contratistas set IMSS=@IMSS,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			end

			--vigencia de seguridad
			set @vigenciaseguridadAn=(select vigenciaseguridad from contratistas where rtrim(RFC)=rtrim(@RFC))
			if @vigenciaseguridadAn<>@vigenciaseguridad and @vigenciaseguridad is not null
			begin
				update contratistas set vigenciaseguridad=@vigenciaseguridad,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			end
				
			--vigenciaIMMS
			set @vigenciaIMSSAn=(select vigenciaIMSS from contratistas where rtrim(RFC)=rtrim(@RFC))
			if @vigenciaIMSSAn<>@vigenciaIMSS and @vigenciaIMSS is not null
			begin
				update contratistas set vigenciaIMSS=@vigenciaIMSS,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			end	

			--Numero de contrato
			set @numerocontratoAn=(select numerocontrato from contratistas where rtrim(RFC)=rtrim(@RFC))
			if @numerocontratoAn<>@numerocontrato and @numerocontrato is not null
			begin
				update contratistas set numerocontrato=@numerocontrato,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			end	
			
			--Periodo vigencia
			set @periodovigenciaAn=(select periodovigencia from contratistas where rtrim(RFC)=rtrim(@RFC))
			if @periodovigenciaAn<>@periodovigencia and @periodovigencia is not null
			begin
				update contratistas set periodovigencia=@periodovigencia,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			end	
			
			--nombre compania
			set @nombrecompaniaAn=(select NombreCompañia from contratistas where rtrim(RFC)=rtrim(@RFC))
			if @nombrecompaniaAn<>@nombrecompania and @nombrecompania is not null
			begin
				update contratistas set NombreCompañia=@nombrecompania,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			end	
			
			--fechabaja
			set @fechabajaAn=(select fechabaja from contratistas where rtrim(RFC)=rtrim(@RFC))
			if @fechabajaAn<>@fechabaja and @fechabaja is not null
			begin
				update contratistas set fechabaja=@fechabaja,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			end	

			--responsable invista
			set @responsableInvistaAn=(select responsableInvista from contratistas where rtrim(RFC)=rtrim(@RFC))
			if @responsableInvistaAn<>@responsableInvista and @responsableInvista is not null
			begin
				update contratistas set responsableInvista=@responsableInvista,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			end	
			
			--tipo contratista
			if @idTipoContratistaAn<>@idTipoContratistaAc and @idTipoContratistaAc is not null
			begin
				update contratistas set idTipoContratista=@idTipoContratistaAc,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			end

			--clasificacion
			if @idclasificacionAn<>@idclasificacionAc and @idclasificacionAc is not null
			begin
				update contratistas set idClasificacion=@idclasificacionAc,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			end
			
			--centro de costos
			if @idccostosAn<>@idccostosAc and @idccostosAc is not null
			begin
				update contratistas set idccostos=@idccostosAc,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			end

			--estatus
			if @idEstatusAn<>@idEstatusAc and @idEstatusAc is not null
			begin
				update contratistas set idEstatus=@idEstatusAc,fecha_actualizacion=@fecha_actualizacion where rtrim(RFC)=rtrim(@RFC)
			--Gafetes
				if (@idEstatusAc=2)
				BEGIN
					--Actualizar lo campos del Gafete
				if (@idGafete is not null)
						BEGIN
						--Actualiza la fecha de vigencia a null y el estatus a inactivo del gafete
						UPDATE Gafete set FechaVigencia=null,idEstatus=2 WHERE idGafete=@idGafete
						END
					--Actualizar los campos del Numero Provisional Gafete
				if (@idGafeteP is not null)
						BEGIN
							--Actualiza la fecha de vigencia la fecha inicio a null y el estatus a inactivo del gafete
							UPDATE Gafete set FechaVigencia=null,FechaInicio=null,idEstatus=2 WHERE idGafete=@idGafeteP
							--Actualiza el campo Numero Provisional de Gafete a null para el empleado
							UPDATE contratistas set NumeroProvisionalGafete=null WHERE rtrim(RFC)=rtrim(@RFC)
						END
				END
			
			END
			
			--UPDATE contratista set fecha_actualizacion=@fecha_actualizacion WHERE rtrim(RFC)=rtrim(@RFC)
		END
		

      fetch next from conteo
	into @RFC,@TipoContratista,@NombreContratista,@ApPaterno,@ApMaterno,@fechaindseguridad,@fechacontrato,@fechaterminacion,@IMSS,
		@vigenciaseguridad,@vigenciaIMSS,@numerocontrato,@periodovigencia,@compania,@nombrecompania,@fechabaja,@clasificacion,
		@responsableInvista,@ccostos,@Estatus,@Planta,@fecha_actualizacion
    END
	Close conteo
	deallocate conteo

COMMIT TRAN;
END TRY
BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN;
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrSev = ERROR_SEVERITY()
		SET @ErrSt = ERROR_STATE()
		insert into LogError(ErrMsg,ErrSev,ErrSt) values (@ErrMsg,@ErrSev,@ErrSt) 
		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
END CATCH
END
GO
