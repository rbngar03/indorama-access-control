IF EXISTS (SELECT 1
			 FROM SYSOBJECTS 
			WHERE NAME = 'DTW_SP_DepuracionCamposVisitantes')
  BEGIN
	DROP PROCEDURE DTW_SP_DepuracionCamposVisitantes
  END
GO
-- =============================================
-- Author:	Karla Ramirez
-- Create date: 08/08/2024
-- Description:	Script que realiza la depuracion de los campos de Empleado Responsable y Gafete la tabla de Visitantes .
-- =============================================

CREATE PROCEDURE [dbo].[DTW_SP_DepuracionCamposVisitantes] 	
AS

BEGIN
	
	BEGIN TRY
	UPDATE [dbo].[Visitantes]
   		SET [idGafete] = NULL,
      	[EmpResponsable] = '' 
   WHERE FechaIngreso <= DATEADD(DAY,-1,GETDATE());
	
	END TRY
	BEGIN CATCH
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
  		SET @ErrMsg = ERROR_MESSAGE()
  		SET @ErrSev = ERROR_SEVERITY()
  		SET @ErrSt  = ERROR_STATE()
  		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
	END CATCH
END