USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_Fecha]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- ALTER date: <ALTER Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_Fecha]
	-- Add the parameters for the stored procedure here
	
AS
--	declare @fecha varchar(30)
--	declare @fecha2 varchar(30)
declare @fecha datetime
--declare @fecha1 varchar(30)
--declare @fecha2 varchar(30)
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
--    set @fecha= (getdate()-1)
--    print @fecha
    --set @fecha2=(convert(varchar(30),@fecha,103)-1)
    set @fecha=(convert(varchar(10),getdate()-1,103))
  

--SELECT CONVERT(VARCHAR(10),FechaTerminacion, 103) AS [DD/MM/YYYY] 
    print @fecha
--    set @fecha2=@fecha-1
--    print @fecha2
 --  set @fecha2 =@fecha + ' 00:00:00'


--  set @fecha1 =@fecha + ' 00:00:00'
--  set @fecha2=@fecha + ' 23:59:59'
--    print @fecha1
--    print @fecha2
    select *
    from empleado
    where fecha_actualizacion between @fecha + ' 00:00:00' and @fecha + ' 23:59:59'
 
END

GO
