USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_VisitanteGafeteProvisional]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Héctor Romero Badillo>
-- Create date: <03/10/2010,>
-- Description:	<Credenciales Provisionales de Visitantes>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_VisitanteGafeteProvisional] 
	-- Add the parameters for the stored procedure here
@pidVisitante int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	/*Antes
	select gafete from gafete where idgafete=(select idgafete 
	from Visitantes where idvisitantes=@pidVisitante)*/

	
	select
    ga.gafete,
	pl.descripcion  as planta
	from visitantes as vis, gafete as ga ,planta as pl
	where ga.idgafete=vis.idgafete and pl.idPlanta=vis.idPlanta
		and vis.idvisitantes=@pidVisitante



END

GO
