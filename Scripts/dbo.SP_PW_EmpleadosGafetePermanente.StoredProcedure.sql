USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_EmpleadosGafetePermanente]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Héctor Romero Badillo>
-- Create date: <29/10/2010,>
-- Description:	<Credenciales Permanenete de Empleados>
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_EmpleadosGafetePermanente] 
	-- Add the parameters for the stored procedure here
@pidEmpleado int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select 
	em.foto,
	em.nombre,
	rtrim(em.apePaterno + ' ' + isnull(em.apematerno,' ')) as apellido,
	p.Descripcion as planta,
	p.Dato1,
	p.Dato2,
	p.Dato3,
	p.Dato4,
	p.Dato5,
	p.Dato6,
	p.Dato7,
	p.Dato8
 from empleado as em, planta as p
  WHERE em.idEmpleado=@pidEmpleado and p.idPlanta=em.idPlanta
END

GO
