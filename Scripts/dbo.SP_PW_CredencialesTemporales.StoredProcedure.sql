USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_CredencialesTemporales]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez V.
-- ALTER date: 16/11/2010 06:10 p.m.
-- Description:	Reporte de Credenciales pago a comedor (comidas) 
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_CredencialesTemporales] 
	-- Add the parameters for the stored procedure here

@fecha_inicio varchar(10),
@fecha_fin varchar(10)


AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    Set @fecha_inicio=convert(varchar(10),@fecha_inicio,103)
	Set @fecha_fin=convert(varchar(10),@fecha_fin,103)
    
    
     SELECT convert(varchar(10),mar.FechaRegistro,103) as Fecha,convert(varchar(10),mar.FechaRegistro,108) as Hora,tl.TipoLector,emp.NumEmpleado,
   ltrim(emp.nombre + ' ' + emp.apepaterno + ' ' + emp.apematerno) as 'Nombre del Empleado'
	FROM Marcajes as mar,Empleado as emp,Terminales as ter,TipoLector tl
	WHERE mar.idTerminales=ter.idTerminales and ter.idTipoLector=tl.idTipoLector and
          mar.idGafeteTmp=emp.Numeroprovisionalgafete and
          FechaRegistro between (@fecha_inicio + ' ' + '00:00:00') and (@fecha_fin + ' 23:59:59')


END
GO
