USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_ContCortoPlazoAgregarGaf]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Héctor Romero
-- alter date: 29-10-2010
-- Description: Agregar,Modificar gafete empleados.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_ContCortoPlazoAgregarGaf]
@idContratistas int,
@idEstatus int,
@Gafete nvarchar(5)
AS
DECLARE @idGafete int
DECLARE @idGafeteAnt int
DECLARE @GafeteCont int
declare @fechater datetime
BEGIN
BEGIN TRY
 BEGIN TRAN;
------------Inicio Gafete

set @idGafete=(select idgafete from gafete where gafete=@Gafete and idtipogafete=1 and idTipoPersonal=4)
set @GafeteCont = (select count(idgafete)from gafete where gafete=@Gafete)
set @idGafeteAnt=(select idgafete from Contratistas where idContratistas=@idContratistas)
set @fechater = (select FechaTerminacion from Contratistas where idContratistas=@idContratistas)
--Asignar gafete
	if (@idGafete is null)
		begin
		if (@GafeteCont>0) 
			begin
				ROLLBACK TRAN;
    			RAISERROR('El numero del Gafete está asignado a otro tipo de Persona', 16, 1)
				return 
			end
		else
			begin
			insert into gafete (gafete,fechainicio,fechavigencia,idestatus,idtipogafete,idtipopersonal)
			values (@Gafete,getdate(),@fechater,@idEstatus,1,4)
			update Contratistas set idgafete=SCOPE_IDENTITY() where idContratistas=@idContratistas
				if (@idGafeteAnt is not null)
					begin
						update gafete set fechaInicio=null,fechavigencia=null, idestatus=2 where idgafete=@idgafeteAnt
					end	
			end
		end
	else
		begin
			if (@idGafete=@idGafeteAnt)
				begin
					update gafete set idEstatus=@idEstatus where idgafete=@idgafete 
				end
			else
				begin
					if ((select idestatus from gafete where idgafete=@idGafete)=1) --or (select count(idcontratistas) from contratistas where idgafete=@idGafete)>0)
						begin
							ROLLBACK TRAN;
							RAISERROR('El Gafete esta asignado a otro Contratista', 16, 1)
							return 
						end
					else
						begin
							update gafete set idestatus=@idEstatus,fechainicio=getdate(),fechavigencia=@fechater where idgafete=@idgafete
							update contratistas set idgafete=null where idgafete=@idgafete
							update contratistas set idgafete=@idgafete where idContratistas=@idContratistas
							if (@idGafeteAnt is not null)
								begin
									update gafete set idestatus=2,fechainicio=null,fechaVigencia=null where idgafete=@idgafeteAnt
								end	
						end
				end
		end

	COMMIT TRAN;
END TRY
BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN;
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrSev = ERROR_SEVERITY()
		SET @ErrSt = ERROR_STATE()
		insert into LogError(ErrMsg,ErrSev,ErrSt) values (@ErrMsg,@ErrSev,@ErrSt)
		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
END CATCH
END
GO
