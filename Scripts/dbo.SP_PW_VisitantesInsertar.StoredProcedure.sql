USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_VisitantesInsertar]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Debi Sanchez
-- Create date: 02/11/2010
-- Description:	Inserta un registro nuevo de terminales.
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_VisitantesInsertar]

@FechaIndSeguridad datetime,
@FechaIngreso datetime,
@NombreVisitante nvarchar(150),
@FechaTerminacion datetime,
@Horas nvarchar(7)=null,
@idOrigen int,
@DescOrigen nvarchar(150),
@Gafete varchar(5)=null,
@idCcostos int,
@EmpResponsable nvarchar(150),
@idComedor int,
@RFC nvarchar(15),
@ArchivoAdjunto nvarchar(100)=null,
@idEstatus int,
@Notas nvarchar(150)=null,
@idPlanta int


AS
--DECLARE @DescOrigen nvarchar(150)
DECLARE @idGafete int

BEGIN
BEGIN TRY
 BEGIN TRAN;
    if ((SELECT count(RFC) FROM Visitantes WHERE RFC=@RFC) >0 )
	BEGIN
	    ROLLBACK TRAN;
		RAISERROR('Ya existe el registro del RFC del Visitante', 16, 1)
		return 
    END
    else
    BEGIN
		if (@Gafete is not null)
		BEGIN
			set @idGafete=(SELECT idGafete FROM Gafete WHERE Gafete=@Gafete)
			if ((select count(idGafete) from Gafete where Gafete=@Gafete and idEstatus=1 and idTipoPersonal=3) >0 )
			begin
				ROLLBACK TRAN;
				RAISERROR('El Gafete esta asignado a otro Visitante', 16, 1)
				return 
			end
		END

	    INSERT INTO Visitantes(FechaIndSeguridad,FechaIngreso,NombreVisitante,FechaTerminacion,Horas,
							idOrigen,DescOrigen,idGafete,idCcostos,EmpResponsable,idComedor,RFC,
							ArchivoAdjunto,idEstatus,Notas,idPlanta)
		VALUES (@FechaIndSeguridad,@FechaIngreso,@NombreVisitante,@FechaTerminacion,@Horas,
				@idOrigen,@DescOrigen,@idGafete,@idCcostos,@EmpResponsable,@idComedor,@RFC,
				@ArchivoAdjunto,@idEstatus,@Notas,@idPlanta)
      END
	COMMIT TRAN;
END TRY
BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN;
		DECLARE @ErrMsg VARCHAR(MAX), @ErrSev INT, @ErrSt INT
		SET @ErrMsg = ERROR_MESSAGE()
		SET @ErrSev = ERROR_SEVERITY()
		SET @ErrSt = ERROR_STATE()
		RAISERROR(@ErrMsg,@ErrSev,@ErrSt)
END CATCH
END
GO
