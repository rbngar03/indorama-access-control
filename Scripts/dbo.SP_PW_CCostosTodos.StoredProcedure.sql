USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_CCostosTodos]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Debi Alexandra Sánchez
-- ALTER date: 01/11/2010 12:52
-- Description:	Selección del catalogo de ccostos
-- =============================================
CREATE PROCEDURE [dbo].[SP_PW_CCostosTodos] 
	
AS
BEGIN
	SET NOCOUNT ON;
    -- Insert statements for procedure here
SELECT idCcostos,Ccostos
FROM ccostos

END

GO
