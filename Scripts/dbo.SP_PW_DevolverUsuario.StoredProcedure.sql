USE [Indorama_AccessControl]
GO
/****** Object:  StoredProcedure [dbo].[SP_PW_DevolverUsuario]    Script Date: 19/06/2024 11:52:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Héctor Romero Badillo
-- Create date: 17/02/2011 12:59
-- Description:	Devolver nombre de usuario
-- =============================================
create PROCEDURE [dbo].[SP_PW_DevolverUsuario] 
@p_id_usuario int,
@p_nombre varchar(30) output	
AS
BEGIN
	

set @p_nombre = (SELECT Usuario FROM Usuarios WHERE idUsuarios=@p_id_Usuario)

END

GO
